<?php

/*
 * Get options from CLI
 *
 * t: Type of Webpay
 *    o: OneClick
 *    n: Normal
 *
 * c: Commerce Code
 * o: Operation to make
 * d: JSON data
 */
$options = getopt("t:c:o:d:");

// This PHP script will always receive 3 params.

if (count($options) != 4) {
    error("Must have 4 parameters");
}

// Try to parse JSON data

$data = json_decode($options["d"], true);

// Return error if isn't a valid JSON

if ($data == null) {
    error("Argument is not a valid JSON");
}

// Constants and imports goes here to don't waste resources

define("PATH", dirname(__FILE__));
define("COMMERCE_CODE", $options["c"]);

switch (getenv("APP_ENV")) {
    case "production":

        if ($options["t"] === "oneclick") {
            // OneClick production URL
            define("WEBPAY_WSDL", "https://webpay3g.transbank.cl/webpayserver/wswebpay/OneClickPaymentService?wsdl");
            define("CERT_PATH", PATH . "/certs/production/oneclick");
        } else {
            // Normal production URL
            define("WEBPAY_WSDL", "https://webpay3g.transbank.cl/WSWebpayTransaction/cxf/WSWebpayService?wsdl");
            define("CERT_PATH", PATH . "/certs/production/webpay");
        }


        break;

    default:

        if ($options["t"] === "oneclick") {
            // OneClick development URL
            define("WEBPAY_WSDL", "https://webpay3gint.transbank.cl/webpayserver/wswebpay/OneClickPaymentService?wsdl");
            define("CERT_PATH", PATH . "/certs/development/oneclick");
        } else {
            // Normal development URL
            define("WEBPAY_WSDL", "https://webpay3gint.transbank.cl/WSWebpayTransaction/cxf/WSWebpayService?wsdl");
            define("CERT_PATH", PATH . "/certs/development/webpay");
        }

}

// Define all certs

define("TBK_CERT", CERT_PATH . "/certificate_server.crt");
define("PRIVATE_KEY", CERT_PATH . "/" . COMMERCE_CODE . ".key");
define("SELF_CERT", CERT_PATH . "/" . COMMERCE_CODE . ".crt");

// Check if all the certs exists

if (!file_exists(TBK_CERT) || !file_exists(PRIVATE_KEY) || !file_exists(SELF_CERT)) {
    error("Certificates missing");
}

if (!@require_once PATH . "/soap/WebpayService.php") {

    error("Required library missing");
}

if (!@require_once PATH . "/soap/SoapValidation.php") {

    error("Required library missing");
}

// Log components
logg(WEBPAY_WSDL);
logg(TBK_CERT);
logg(PRIVATE_KEY);
logg(SELF_CERT);
logg(COMMERCE_CODE);

// Route based in the first arg.

switch ($options["o"]) {
    case "initTransaction":
        initTransaction($data);
        break;

    case "resultTransaction":
        getTransactionResult($data);
        break;

    case "ackTransaction":
        acknowledgeTransaction($data);
        break;

    case "initInscription":
        initInscription($data);
        break;

    case "finishInscription":
        finishInscription($data);
        break;

    case "removeUser":
        removeUser($data);
        break;

    case "authorize":
        authorize($data);
        break;

    case "reverse":
        reversePayment($data);
        break;

    default:
        error("Method not implemented");
}

exit();

/*************************************************
 *              IMPLEMENTING METHODS             *
 *************************************************/

/**
 * Initiates the transaction with transbank
 *
 * @param array @data Info to initiate transactions
 */

function initTransaction($data)
{
    $required_keys = array(
        "order_id",
        "total",
        "session_id",
        "return_url",
        "final_url"
    );
    if (keys_exists($required_keys, $data) === false) {
        error("Init transaction must have every param");
    }

    logg("INIT TRANSACTION");
    /*
    * I prefer to set object variables in this way instead of using constructors
    * I think it's more readable
    */
    $transactionDetails = new wsTransactionDetail();

    // Commerce or integration code provided by Transbank

    $transactionDetails->commerceCode = COMMERCE_CODE;

    // ID of the order that we're processing

    $transactionDetails->buyOrder = $data["order_id"];

    // Order total

    $transactionDetails->amount = $data["total"];
    $initTransaction = new wsInitTransactionInput();

    // It will always be TR_NORMAL_WS

    $initTransaction->wSTransactionType = 'TR_NORMAL_WS';

    // Identify the user session or any other session

    $initTransaction->sessionId = $data["session_id"];

    // Return URL works like a proxy, you go there and then back to Transbank

    $initTransaction->returnURL = $data["return_url"];

    // URL when we will show a transaction summary

    $initTransaction->finalURL = $data["final_url"];
    $initTransaction->transactionDetails = $transactionDetails;

    // Log object

    logg($initTransaction);

    // SOAP service call

    $webService = new WebpayService(WEBPAY_WSDL);
    try {
        $result = $webService->initTransaction(array(
            "wsInitTransactionInput" => $initTransaction
        ));
    } catch(Exception $e) {
        error("We can't communicate with transbank");
    }
    
    if (validateResponse($webService) === true) {
        $responseInit = $result->return;

        // Log response

        logg("INIT TRANSACTION RESPONSE");
        logg($responseInit);
        
        $data = array(
            "url" => $responseInit->url,
            "token" => $responseInit->token
        );
        echo json_encode($data);
    }
    else {
        error("Invalid response from Transbank");
    }
}

/**
 * Get the transaction result from Transbank
 *
 * @param array $data Info to get transaction
 */

function getTransactionResult($data)
{
    $required_keys = array(
        "token"
    );
    if (keys_exists($required_keys, $data) === false) {
        error("get transaction result must have every param");
    }

    // Log action

    logg("GET TRANSACTION RESULT");

    // Transbank only needs the token to return transaction info

    $transactionResult = new getTransactionResult();
    $transactionResult->tokenInput = $data["token"];

    // Log object

    logg($transactionResult);
    $webService = new WebpayService(WEBPAY_WSDL);
    try {
        $result = $webService->getTransactionResult($transactionResult);
    } catch(Exception $e) {
        error("We can't communicate with transbank");
    }
    
    if (validateResponse($webService) === true) {
        $transactionOutput = $result->return;

        // Log response

        logg("GET TRANSACTION RESULT RESPONSE");
        logg($transactionOutput);
        $data = array(
            "token" => $data["token"],
            "session" => $transactionOutput->sessionId,
            "url" => $transactionOutput->urlRedirection,
            "card_details" => array(
                "number" => $transactionOutput->cardDetail->cardNumber,
                "expiration" => $transactionOutput->cardDetail->cardExpirationDate
            ) ,
            "details" => array(
                "authorization_code" => $transactionOutput->detailOutput->authorizationCode,
                "payment_type" => $transactionOutput->detailOutput->paymentTypeCode,
                "response_code" => $transactionOutput->detailOutput->responseCode,
                "shares_amount" => $transactionOutput->detailOutput->sharesAmount,
                "shares_number" => $transactionOutput->detailOutput->sharesNumber
            ) ,
            "accounting_date" => $transactionOutput->accountingDate,
            "transaction_date" => $transactionOutput->transactionDate,
            "vci" => $transactionOutput->VCI
        );

        // Error codes that let us know if transaction can't be realized

        $responseCode = $transactionOutput->detailOutput->responseCode;
        switch ($responseCode) {
            case '-1':
                $data["error"] = "Transaction rejected.";
                break;

            case '-2':
                $data["error"] = "Transaction must be retried.";
                break;

            case '-3':
                $data["error"] = "Transaction error.";
                break;

            case '-4':
                $data["error"] = "Transaction rejected.";
                break;

            case '-5':
                $data["error"] = "Rejected because rate error.";
                break;

            case '-6':
                $data["error"] = "Exceeds maximum monthly allowance.";
                break;

            case '-7':
                $data["error"] = "Exceed daily limit per transaction.";
                break;

            case '-8':
                $data["error"] = "Unauthorized sector.";
                break;
        }
        // ACK TRAN
    }
    else {
        error("Invalid response from Transbank");
    }

    echo json_encode($data);
}

/**
 * Acknowledge transaction to Transbank
 *
 * @param array $data Info to get transaction
 */

function acknowledgeTransaction($data) {
    $required_keys = array(
        "token"
    );
    if (keys_exists($required_keys, $data) === false) {
        error("get transaction result must have every param");
    }

    // Log action

    logg("ACKNOWLEDGE TRANSACTION");

    // Acknowledge the transaction, even if payment can't be processed

    $ackTransaction = new acknowledgeTransaction();
    $ackTransaction->tokenInput = $data["token"];

    // Log object

    logg($ackTransaction);

    $webService = new WebpayService(WEBPAY_WSDL);
    try {
        $webService->acknowledgeTransaction($ackTransaction);
    } catch(Exception $e) {
        error("We can't communicate with transbank");
    }

    // Possible MitM

    if (validateResponse($webService) === false) {
        error("Can't acknowledge transaction");
    } else {
        $data["success"] = true;
    }

    echo json_encode($data);
}


/**
 * Initialize user inscription
 *
 * @param array $data Info to initialize inscription
 */

function initInscription($data) {
    $required_keys = array(
        "username",
        "email",
        "final_url"
    );
    if (keys_exists($required_keys, $data) === false) {
        error("Initialize inscription must have every param");
    }

    // Log action

    logg("INIT INSCRIPTION");

    // Data to make the inscription
    
    $inputInscription = new oneClickInscriptionInput();
    $inputInscription->username = $data["username"];
    $inputInscription->email = $data["email"];
    $inputInscription->responseURL = $data["final_url"];

    // Log object

    logg($inputInscription);
    $webService = new WebpayService(WEBPAY_WSDL);
    try {
        $result = $webService->initInscription(array("arg0" => $inputInscription));
    } catch(Exception $e) {
        error("We can't communicate with transbank");
    }
    

    if (validateResponse($webService) === true) {
        $responseInit = $result->return;

        // Log response

        logg("INIT INSCRIPTION RESPONSE");
        logg($responseInit);

        $data = array(
            "url" => $responseInit->urlWebpay,
            "token" => $responseInit->token
        );
        echo json_encode($data);
        
    } else {
        error("Invalid response from Transbank");
    }
}

/**
 * Finish user inscription
 *
 * @param array $data Info to finish inscription
 */

function finishInscription($data) {
    $required_keys = array(
        "token"
    );
    if (keys_exists($required_keys, $data) === false) {
        error("Initialize inscription must have every param");
    }

    // Log action

    logg("FINISH INSCRIPTION");

    // Data to finish the inscription
    
    $inputFinish = new oneClickFinishInscriptionInput();
    $inputFinish->token = $data["token"];
    
    // Log object
    
    logg($inputFinish);

    $webService = new WebpayService(WEBPAY_WSDL);
    try {
        $result = $webService->finishInscription(array("arg0" => $inputFinish));
    } catch(Exception $e) {
        error("We can't communicate with transbank");
    }


    if (validateResponse($webService) === true) {
        $responseFinish = $result->return;
        
        // Log response
        logg("FINISH INSCRIPTION RESULT");
        logg($responseFinish);
        
        $data = array(
            "response_code" => $responseFinish->responseCode,
            "auth_code" => $responseFinish->authCode,
            "cc_type" => $responseFinish->creditCardType,
            "last_digits" => $responseFinish->last4CardDigits,
            "tbk_user" => $responseFinish->tbkUser
        );
        echo json_encode($data);
    } else {
        error("Invalid response from Transbank");
    }
}

/**
 * Authorize payment from transbank
 *
 * @param array $data Info to authorize payment
 */

function authorize($data) {
    $required_keys = array(
        "amount",
        "tbk_user",
        "username",
        "buy_order"
    );
    if (keys_exists($required_keys, $data) === false) {
        error("Authorize must have every param");
    }

    // Log action

    logg("AUTHORIZE");
    
    $inputPayment = new oneClickPayInput();
    $inputPayment->amount = $data["amount"];
    $inputPayment->tbkUser = $data["tbk_user"];
    $inputPayment->username = $data["username"];
    $inputPayment->buyOrder = $data["buy_order"];

    // Log object

    logg($inputPayment);

    $webService = new WebpayService(WEBPAY_WSDL);
    try {
        $result = $webService->authorize(array("arg0" => $inputPayment));
    } catch(Exception $e) {
        error("We can't communicate with transbank");
    }


    if (validateResponse($webService) === true) {
        $responseAuth = $result->return;

        // Log response
        logg("AUTHORIZE RESULT");
        logg($responseAuth);

        $data = array(
            "response_code" => $responseAuth->responseCode,
            "auth_code" => $responseAuth->authorizationCode,
            "last_digits" => $responseAuth->last4CardDigits,
            "cc_type" => $responseAuth->creditCardType,
            "transaction_id" => $responseAuth->transactionId,
        );

        if ($responseAuth->responseCode < 0) {
            $data["error"] = "Rejected";
        }

        echo json_encode($data);
    } else {
        error("Invalid response from Transbank");
    }

}

/**
 * Remove user data from OneClick
 *
 * @param $data
 */

function removeUser($data) {
    $required_keys = array(
        "tbk_user",
        "username",
    );
    if (keys_exists($required_keys, $data) === false) {
        error("Remove user must have every param");
    }

    // Log action

    logg("REMOVE USER");

    $inputRemove = new oneClickRemoveUserInput();
    $inputRemove->tbkUser = $data["tbk_user"];
    $inputRemove->username = $data["username"];

    // Log object

    logg($inputRemove);

    $webService = new WebpayService(WEBPAY_WSDL);
    try {
        $result = $webService->removeUser(array("arg0" => $inputRemove));
    } catch(Exception $e) {
        error("We can't communicate with transbank");
    }


    if (validateResponse($webService) === true) {
        $responseRemove = $result->return;

        // Log response
        logg("REMOVE USER RESULT");
        logg($responseRemove);

        $data = array(
            "response" => $responseRemove,
        );
        echo json_encode($data);
    } else {
        error("Invalid response from Transbank");
    }

}


/**
 * Reverse a Payment from Transbank
 *
 * @param $data
 */

function reversePayment($data) {
    $required_keys = array(
        "buy_order",
    );
    if (keys_exists($required_keys, $data) === false) {
        error("Reverse must have every param");
    }

    // Log action

    logg("REVERSE");

    $inputReverse = new oneClickReverseInput();
    $inputReverse->buyorder = $data["buy_order"];

    // Log object

    logg($inputReverse);

    $webService = new WebpayService(WEBPAY_WSDL);
    try {
        $result = $webService->codeReverseOneClick(array("arg0" => $inputReverse));
    } catch(Exception $e) {
        error("We can't communicate with transbank");
    }


    if (validateResponse($webService) === true) {
        $responseReverse = $result->return;

        // Log response
        logg("REVERSE RESULT");
        logg($responseReverse);

        $data = array(
            "code" => $responseReverse->reverseCode,
            "response" => $responseReverse->reversed
        );
        echo json_encode($data);
    } else {
        error("Invalid response from Transbank");
    }

}



/**
 * Validate the transaction response from Transbank
 * 
 * @param $webService
 * @return bool
 */
function validateResponse($webService) {
    // Validate the response from Transbank

    $xmlResponse = $webService->soapClient->__getLastResponse();
    $params = array(
        'xmlSoap' => $xmlResponse,
        'certServerPath' => TBK_CERT
    );
    $webServiceValidation = new SoapValidation($params);
    return $webServiceValidation->getValidationResult();
}


/**
 * Show an error message
 *
 * echo is used without array->json because is a simple string and is this way is faster.
 *
 * @param string $msg Error message to display
 */

function error($msg)
{
    fwrite(STDOUT, "{\"error\": \"$msg\"}");
    exit();
}

/**
 * Check if all keys exists in array
 *
 * @param array $keys Keys required to be in array
 * @param array $array Array to check
 * 
 * @return boolean
 */

function keys_exists($keys, $array)
{
    foreach($keys as $key => & $value) {
        if (isSet($array[$value]) === false) {
            return false;
        }
    }

    return true;
}

/**
 * Log to error log
 *
 * @param $message
 * @param string $suffix
 */

function logg($message, $suffix = '')
{
    if (is_array($message) || is_object($message)) {
        error_log(print_r($message, true));
    }
    else {
        error_log($suffix . " -> " . $message);
    }
}
