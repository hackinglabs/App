<?php

/***************************************
 * CLASSES FOR WEBPAY PLUS WEBSERVICES *
 ***************************************/

// INIT TRANSACTION

class initTransaction
{
    public $wsInitTransactionInput;
}

class wsInitTransactionInput
{
    public $wSTransactionType;
    public $commerceId;
    public $buyOrder;
    public $sessionId;
    public $returnURL;
    public $finalURL;
    public $transactionDetails;
    public $wPMDetail;
}

class initTransactionResponse
{
    public $return;
}

class wsInitTransactionOutput
{
    public $token;
    public $url;
}

// GET TRANSACTION RESULT

class getTransactionResult
{
    public $tokenInput;
}

class getTransactionResultResponse
{
    public $return;
}

class transactionResultOutput
{
    public $accountingDate;
    public $buyOrder;
    public $cardDetail;
    public $detailOutput;
    public $sessionId;
    public $transactionDate;
    public $urlRedirection;
    public $VCI;
}

class wsTransactionDetail
{
    public $sharesAmount;
    public $sharesNumber;
    public $amount;
    public $commerceCode;
    public $buyOrder;
}

class wsTransactionDetailOutput
{
    public $authorizationCode;
    public $paymentTypeCode;
    public $responseCode;
}

class cardDetail
{
    public $cardNumber;
    public $cardExpirationDate;
}

class wpmDetailInput
{
    public $serviceId;
    public $cardHolderId;
    public $cardHolderName;
    public $cardHolderLastName1;
    public $cardHolderLastName2;
    public $cardHolderMail;
    public $cellPhoneNumber;
    public $expirationDate;
    public $commerceMail;
    public $ufFlag;
}

// ACKNOWLEDGE TRANSACTION

class acknowledgeTransaction
{
    public $tokenInput;
}

class acknowledgeTransactionResponse { }

/*******************************************
 * CLASSES FOR WEBPAY ONECLICK WEBSERVICES *
 *******************************************/

// INIT INSCRIPTION

class initInscription
{
    public $arg0;
}

class oneClickInscriptionInput
{
    public $email;
    public $responseURL;
    public $username;
}

class initInscriptionResponse
{
    public $return;
}

class oneClickInscriptionOutput
{
    public $token;
    public $urlWebpay;
}

// FINISH INSCRIPTION

class finishInscription
{
    public $arg0;
}

class oneClickFinishInscriptionInput
{
    public $token;
}

class finishInscriptionResponse
{
    public $return;
}

class oneClickFinishInscriptionOutput
{
    public $authCode;
    public $creditCardType;
    public $last4CardDigits;
    public $responseCode;
    public $tbkUser;
}

// AUTHORIZE

class authorize
{
    public $arg0;
}

class authorizeResponse
{
    public $return;
}

class oneClickPayInput
{
    public $amount;
    public $buyOrder;
    public $tbkUser;
    public $username;
}


class oneClickPayOutput
{
    public $authorizationCode;
    public $creditCardType;
    public $last4CardDigits;
    public $responseCode;
    public $transactionId;
}

// REMOVE USER

class removeUser
{
    public $arg0;
}

class oneClickRemoveUserInput
{
    public $tbkUser;
    public $username;
}

class removeUserResponse
{
    public $return;
}


// CODE REVERSE ONE CLICK

class codeReverseOneClick
{
    public $arg0;
}

class oneClickReverseInput
{
    public $buyorder;
}

class codeReverseOneClickResponse
{
    public $return;
}

class oneClickReverseOutput
{
    public $reverseCode;
    public $reversed;
}

class reverse
{
    public $arg0;
}

class reverseResponse
{
    public $return;
}


class baseBean { }

require_once 'wsse-client.php';


class WebpayService
{
    var $soapClient;

    private static $classmap = array(
        'getTransactionResult' => 'getTransactionResult',
        'getTransactionResultResponse' => 'getTransactionResultResponse',
        'transactionResultOutput' => 'transactionResultOutput',
        'cardDetail' => 'cardDetail',
        'wsTransactionDetailOutput' => 'wsTransactionDetailOutput',
        'wsTransactionDetail' => 'wsTransactionDetail',
        'acknowledgeTransaction' => 'acknowledgeTransaction',
        'acknowledgeTransactionResponse' => 'acknowledgeTransactionResponse',
        'initTransaction' => 'initTransaction',
        'wsInitTransactionInput' => 'wsInitTransactionInput',
        'wpmDetailInput' => 'wpmDetailInput',
        'initTransactionResponse' => 'initTransactionResponse',
        'wsInitTransactionOutput' => 'wsInitTransactionOutput',
        'removeUser' => 'removeUser',
        'oneClickRemoveUserInput' => 'oneClickRemoveUserInput',
        'baseBean' => 'baseBean',
        'removeUserResponse' => 'removeUserResponse',
        'initInscription' => 'initInscription',
        'oneClickInscriptionInput' => 'oneClickInscriptionInput',
        'initInscriptionResponse' => 'initInscriptionResponse',
        'oneClickInscriptionOutput' => 'oneClickInscriptionOutput',
        'finishInscription' => 'finishInscription',
        'oneClickFinishInscriptionInput' => 'oneClickFinishInscriptionInput',
        'finishInscriptionResponse' => 'finishInscriptionResponse',
        'oneClickFinishInscriptionOutput' => 'oneClickFinishInscriptionOutput',
        'codeReverseOneClick' => 'codeReverseOneClick',
        'oneClickReverseInput' => 'oneClickReverseInput',
        'codeReverseOneClickResponse' => 'codeReverseOneClickResponse',
        'oneClickReverseOutput' => 'oneClickReverseOutput',
        'authorize' => 'authorize',
        'oneClickPayInput' => 'oneClickPayInput',
        'authorizeResponse' => 'authorizeResponse',
        'oneClickPayOutput' => 'oneClickPayOutput',
        'reverse' => 'reverse',
        'reverseResponse' => 'reverseResponse'
    );

    public function __construct($url)
    {
        $this->soapClient = new WsseClient($url,array("classmap"=>self::$classmap,"trace" => true,"exceptions" => true));
        /*
        $this->soapClient = new WsseClient($url, array(
            "classmap" => self::$classmap,
            "trace" => true,
            "exceptions" => true
        ));
         */
        //die(var_dump($this->soapClient));
    }

    public function initTransaction($initTransaction)
    {
        $initTransactionResponse = $this
            ->soapClient
            ->initTransaction($initTransaction);
        return $initTransactionResponse;
    }

    public function getTransactionResult($getTransactionResult)
    {
        $getTransactionResultResponse = $this
            ->soapClient
            ->getTransactionResult($getTransactionResult);
        return $getTransactionResultResponse;
    }

    public function acknowledgeTransaction($acknowledgeTransaction)
    {
        $acknowledgeTransactionResponse = $this
            ->soapClient
            ->acknowledgeTransaction($acknowledgeTransaction);
        return $acknowledgeTransactionResponse;
    }

    public function initInscription($initInscription)
    {
        $initInscriptionResponse = $this
            ->soapClient
            ->initInscription($initInscription);
        return $initInscriptionResponse;
    }

    public function finishInscription($finishInscription)
    {
        $finishInscriptionResponse = $this
            ->soapClient
            ->finishInscription($finishInscription);
        return $finishInscriptionResponse;
    }

    public function authorize($authorize)
    {
        $authorizeResponse = $this
            ->soapClient
            ->authorize($authorize);
        return $authorizeResponse;
    }

    public function removeUser($removeUser)
    {
        $removeUserResponse = $this
            ->soapClient
            ->removeUser($removeUser);
        return $removeUserResponse;
    }

    public function codeReverseOneClick($codeReverseOneClick)
    {
        $codeReverseOneClickResponse = $this
            ->soapClient
            ->codeReverseOneClick($codeReverseOneClick);
        return $codeReverseOneClickResponse;
    }
    public function reverse($reverse)
    {
        $reverseResponse = $this
            ->soapClient
            ->reverse($reverse);
        return $reverseResponse;
    }
}
