#!/bin/sh

export GOOGLE_APPLICATION_CREDENTIALS="/opt/dxpress-edcb5764bb56.json"
find . -name \*.po -execdir sh -c 'msgfmt "$0" -o `basename $0 .po`.mo' '{}' \;

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
TOOLS_DIR="$DIR/Tools"

if [ ! -f "$TOOLS_DIR/geolite2.mmdb" ]; then
    wget "https://storage.googleapis.com/dxpress/DONOTTOUCH/geolite2.mmdb" -P $TOOLS_DIR
fi

ip=$(ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}')

if [ "$ip" = "10.240.0.2" ]; then
    export APP_ENV="production"
fi

pypy manage.py
