# coding=utf-8
import json
import logging

import csv
import arrow

from wheezy.http.response import HTTPResponse
from Classes.Core.APIHandler import APIHandler
from Classes.db import ConnectCassandra
from Models import User, Area
from Models.CouponUser import CouponUser
from Models.Cassandra.Order import Order


class TestingHandler(APIHandler):
    def start(self):
        self.repository(CouponUser)
        self.repository(User)
        ConnectCassandra()
        self.logger = logging.getLogger(__name__)

    def get(self):
        route = self.route("what")
        if route is not None:
            method = getattr(self, "get_{slug}".format(slug=route), None)
            return method()
        else:
            return self.get_default()

    def get_payment(self):
        webpay = 0
        oneclick = 0
        mobile = 0
        desktop = 0

        orders = Order.filter(status="ORDER-FINISH").allow_filtering().all()

        for order in orders:
            if order.payment and order.payment.type == 'OneClick':
                oneclick = oneclick + 1
            elif order.payment and order.payment.type == 'Webpay':
                webpay = webpay + 1

            if order.is_mobile:
                mobile = mobile + 1
            else:
                desktop = desktop + 1

        return self.success({
            'webpay': webpay,
            'oneclick': oneclick,
            'mobile': mobile,
            'desktop': desktop
        })

    def get_finished(self):
        all = []
        orders = Order.filter(status="ORDER-FINISH").allow_filtering().all()

        for order in orders:
            if order.pickup is False:
                all.append([
                    order.address_order.geo.x,
                    order.address_order.geo.y,
                    1
                ])
        return self.success(all)

    def get_sales(self):
        all = []
        subsidiary_id = self.param("subsidiary_id")
        start = self.param("start")
        end = self.param("end")

        start = arrow.get(start).replace(tzinfo='America/Santiago')
        end = arrow.get(end).replace(tzinfo='America/Santiago')

        orders = Order.filter(
            order_date__gte=start.replace(hour=0, minute=0, second=0, microsecond=0).to('utc').datetime)
        orders = orders.filter(
            order_date__lte=end.replace(hour=23, minute=59, second=59, microsecond=999999).to('utc').datetime)
        orders = orders.allow_filtering().all()

        for order in orders:
            if order.user.email in ['antonio.mcuello@gmail.com',
                                    'seele_in_not@live.cl',
                                    'sebastian@hackinglabs.cl',
                                    'lmunoz.robles@gmail.com',
                                    'douglas@hackinglabs.cl',
                                    'cubomagiko@gmail.com',
                                    'luisurrutiaf@gmail.com']:
                continue
            if order.status not in ['ORDER-FINISH', 'ORDER-IN-DISPATCH', 'ORDER-TRACK', 'DXL-ORDER-FINISH',
                                    'DXL-PREPARATION', 'DXL-READY', 'DXL-SEND-DISPATCHER', 'DXL-ORDER-IN-DISPATCH',
                                    'DXL-DISPATCH']:
                continue

            user = self._con.query(User).filter(User.id == order.user.id).first()
            obj = {
                'ID de Orden': str(order.id),
                'Correlativo': order.correlative,
                'Estado': order.status,
                'Nombre': order.user.name,
                'Sexo': '',
                'Email': order.user.email,
                'Facebook': '',
                'Google': '',
                'Telefono': order.user.telephone,
                'Comuna': '',
                'Ciudad': '',
                'Alianza': order.subsidiary.commerce.alliance,
                'Tipo de pago': '',
                'Tipo de tarjeta': '',
                'Codigo de cliente': order.client_code,
                'Codigo de usuario': order.user_code,
                'Cupon': '',
                'Subtotal': order.subtotal,
                'Subtotal con descuentos': order.subtotal_with_discount,
                'Subtotal sin descuentos': order.subtotal_without_discounts,
                'Distancia': order.distance,
                'Total': order.total,
                'Nombre del Despachador': '',
                'Identificador del Despachador': None,
                'Pedido desde telefono': order.is_mobile,
                'Para llevar': order.pickup,
                'Sucursal': '',
                'Fecha de pedido': arrow.get(order.order_date).to('America/Santiago').format('DD-MM-YYYY HH:mm:ss'),
                'Fecha de confirmacion': arrow.get(order.order_confirmation).to('America/Santiago').format('DD-MM-YYYY HH:mm:ss'),
                'Fecha de preparacion': arrow.get(order.order_preparation).to('America/Santiago').format('DD-MM-YYYY HH:mm:ss'),
                'Fecha de pedido listo': arrow.get(order.order_ready).to('America/Santiago').format('DD-MM-YYYY HH:mm:ss'),
                'Fecha de Entrega': arrow.get(order.delivery_date).to('America/Santiago').format('DD-MM-YYYY HH:mm:ss'),
                'Fecha de pago': arrow.get(order.order_paid).to('America/Santiago').format('DD-MM-YYYY HH:mm:ss'),
            }

            if order.subtotal:
                obj['Subtotal'] = int(order.subtotal)
            if order.subtotal_with_discount:
                obj['Subtotal con descuentos'] = int(order.subtotal_with_discount)
            if order.subtotal_without_discounts:
                obj['Subtotal sin descuentos'] = order.subtotal_without_discounts
            if order.dispatch_amount:
                obj['Monto Despacho real'] = int(order.dispatch_amount)
            if order.total:
                obj['Total'] = int(order.total)

            # Set dispatch_amount
            if order.dispatch_amount:
                real = order.dispatch_amount
                less = real - 2000
                meters = less / 90
                dispatcher_amount = float(2000 + meters * 70)
                obj['Monto Despacho Repartidor'] = dispatcher_amount

            if user:
                obj['Nombre'] = user.fullname
                obj['Sexo'] = user.gender

                if user.facebook:
                    obj['Facebook'] = 'https://www.facebook.com/profile.php?id=' + user.facebook
                if user.google:
                    obj['Google'] = 'https://plus.google.com/' + user.google
            if order.payment:
                obj['Tipo de pago'] = order.payment.type
                payment_type = order.payment.payment_type
                if payment_type == 'VN':
                    payment_type = 'Venta normal'
                elif payment_type == 'VC':
                    payment_type = 'Venta en cuotas'
                elif payment_type == 'SI':
                    payment_type = '3 cuotas sin interes'
                elif payment_type == 'S2':
                    payment_type = '2 cuotas sin interes'
                elif payment_type == 'NC':
                    payment_type = 'N Cuotas sin interes'
                elif payment_type == 'VD':
                    payment_type = 'Venta debito'
                obj['Tipo de tarjeta'] = payment_type
            if order.coupon:
                obj['Cupon'] = order.coupon.code
            if order.dispatcher:
                obj['Nombre del Despachador'] = order.dispatcher.name
                obj['Identificador del Despachador'] = order.dispatcher.id
            if order.subsidiary:
                obj['Sucursal'] = order.subsidiary.fullname
            if order.address_order:
                area = self._con.query(Area).filter(Area.id == order.address_order.area_id).first()
                if area is None and order.address_order.area_id is None:
                    obj['Comuna'] = ''
                elif area is None:
                    obj['Comuna'] = order.address_order.area_id
                else:
                    obj['Comuna'] = area.name
                    obj['Ciudad'] = area.city.name
            all.append(obj)

        keys = all[0].keys()
        with open('ultimo.csv', 'wb') as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(all)

        csv2 = ''
        with open("ultimo.csv") as fp:
            csv2 = fp.read()

            custom_headers = [
                ('Content-disposition', 'attachment; filename=ultimo.csv'),
            ]

            response = HTTPResponse('text/csv', 'UTF-8')
            response.status_code = 200
            response.headers.extend(custom_headers)
            response.write_bytes(csv2.encode("UTF-8"))

            return response

    def get_spam(self):
        users = self.user.all()

        for user in users:
            user_data = {"nombre": user.first_name, "apellido": user.last_name, "email": user.email}
            html = self.render_template('mail/spam.html', **user_data)
            self.mailer.send_message(
                'no-reply@dxpress.cl',
                [user_data['email']],
                subject=u'Descubre la forma inteligente de pedir comida',
                html=html,
                tags=['vodka']
            )

    def get_simulated_dispatcher(self):
        order_data = {
            "type": 2,
            "order": {
                "id": "cd13d11e-de68-11e6-adbd-42010af00002",
                "correlative": 256721,
                "name": "Sandwich - Ensaladas",
                "logo": "https://storage.googleapis.com/dxpress/production%2FLogos_subsidiarie3-11-20160610172108.png",
                "order_user": 1849,
                "user": "Camila Morales",
                "order_location": {
                    "lat": -33.368446350097656,
                    "lng": -70.51110076904297,
                    "address": "San Jos\u00e9 Mar\u00eda Escriv\u00e1 de Balaguer 13105",
                    "block": " ",
                    "apartment": "813",
                    "reference": None
                }, "subsidiary_location": {
                    "lat": -33.4080595,
                    "lng": -70.56610710000001,
                    "address": "Escandinavia 18, Las Condes."
                },
                "commerce_name": "Monkey Dog",
                "products": [
                    {"id": 1975,
                     "name": "Mechada",
                     "price": 6200.0,
                     "quantity": 1,
                     "clarification": "",
                     "ingredients_extra": 3,
                     "subcategory_id": 207,
                     "description": "1 base + Mix Lechuga + 3 ingredientes + 1 salsa.",
                     "compounds": [],
                     "ingredients": [
                         {"id": 341, "name": "Queso Granulado", "price": 0.0, "ingredients_per_category_id": 72,
                          "is_base": False},
                         {"id": 342, "name": "Mayo Ciboulette", "price": 0.0, "ingredients_per_category_id": 73,
                          "is_base": True},
                         {"id": 358, "name": "Champi\u00f1\u00f3n", "price": 1000.0, "ingredients_per_category_id": 74,
                          "is_base": False}
                     ]}],
                "clarification": "",
                "overprice_percentage": 0.0}
        }

        subsidiary = self.subsidiary.find(id=1)

        lat = subsidiary.to_geo().lat
        lng = subsidiary.to_geo().lon

        dispatcher = self.dispatcher.find(lat, lng)

        if dispatcher is None:
            return self.success('Search again')
        else:
            self.redis().zrem("geo:locations:geo_dispatchers", str(dispatcher))

            data = {
                "type": 1,
                "order": {
                    'id': str(order_data['order']['id']),
                    "correlative": order_data['order']['correlative'],
                    'name': subsidiary.alias,
                    'logo': subsidiary.commerce.logo,
                    'client_code': "123123",
                    'order_user': "1",
                    'user': "Douglas Mendez",
                    'franchise': "False",
                    'order_location': {
                        'lat': -33.4080595,
                        'lng': -70.56610710000001,
                        'address': "Las verbenas 8961",
                        'block': "2",
                        'apartment': "1404",
                        'reference': ""
                    },
                    'subsidiary_location': {
                        'lat': lat,
                        'lng': lng,
                        'address': subsidiary.address
                    },
                    'commerce_name': subsidiary.commerce.name
                }
            }

            if not self.rabbit.publish(queue='dispatcher_' + str(dispatcher) + '_orders',
                                       msg=json.dumps(data)):
                return self.error("We couldn't send the order to the dispatcher.")

            return self.success("Dispatcher Found")
