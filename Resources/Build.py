# coding=utf-8
import logging

import arrow

from Classes.Core.APIHandler import APIHandler
from Models.DeviceMobile import DeviceMobile
from Models.Subsidiary import Subsidiary
from Models import Redis


class BuildHandler(APIHandler):
    def start(self):
        self.repository([Subsidiary,
                        DeviceMobile])
        self.logger = logging.getLogger(__name__)

    def get(self):
        route = self.route("what")

        if route is not None:
            method = getattr(self, "get_{slug}".format(slug=route), None)
            return method()
        else:
            return self.get_default()

    def get_subsidiaries(self):
        self.subsidiary.save_geo()
        return self.success()

    def get_fix_users(self):
        all_users = Redis.User.query.all()
        for user in all_users:
            token_devices = user.token_devices

            if token_devices is None:
                continue

            cleaned_tokens = []
            for token in token_devices:
                if isinstance(token, dict):
                    exists = [t for t in cleaned_tokens if t['token'] == token['token']]
                    if not exists:
                        cleaned_tokens.append(token)

                if isinstance(token, basestring):
                    exists = [t for t in cleaned_tokens if t['token'] == token]
                    if exists:
                        continue

                    try:
                        dm = self.devicemobile.find(token=token)
                    except:
                        dm = None

                    if dm is None:
                        cleaned_tokens.append({
                            'token': token,
                            'os': 'android',
                            'last_use': arrow.now('utc').timestamp
                        })
                    else:
                        os = 'android'
                        try:
                            os_int = int(dm.os)
                        except ValueError:
                            os_int = 1

                        if os_int == 2:
                            os = 'ios'

                        cleaned_tokens.append({
                            'token': token,
                            'os': os,
                            'last_use': arrow.now('utc').timestamp
                        })
            user.token_devices = cleaned_tokens

            try:
                user.save(force=True)
            except Exception as e:
                self.logger.error(e.message)

        return self.success()


    def get_users(self):
        dm = self.devicemobile.get_all_with_user()

        for d in dm:
            if d.user is None:
                self.logger.warning("This token hasn't user")

            ru = Redis.User.query.filter(id=d.user.id).first()

            if ru is None:
                ru = Redis.User(id=d.user.id,
                                first_name=d.user.first_name,
                                last_name=d.user.last_name,
                                nin=d.user.nin,
                                birth_date=d.user.birth_date,
                                email=d.user.email,
                                user_token=d.user.xcsrf,
                                enabled=d.user.enabled,
                                avatar=d.user.avatar)

            os = "android"
            try:
                os_int = int(d.os)
                if os_int == 2:
                    os = "ios"
            except:
                pass

            current_token = {
                'token': d.token,
                'os': os,
                'last_use': arrow.now('utc').timestamp
            }

            if ru.token_devices and isinstance(ru.token_devices, list):
                ru.token_devices.append(current_token)
            else:
                ru.token_devices = [current_token]

            try:
                ru.save()
            except Exception as e:
                self.logger.error("Can't write to redis user %s", d.user.id)
                self.logger.error(e.message)

        return self.success()
