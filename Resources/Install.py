# -*- coding: utf-8 -*-
from Classes import ConnectCassandra
from Classes.Core.APIHandler import APIHandler
from Configs import Config
import os
from os.path import isfile, join
from sqlalchemy.engine import reflection
from sqlalchemy.schema import (
    MetaData,
    Table,
    DropTable,
    ForeignKeyConstraint,
    DropConstraint,
    )



class InstallHandler(APIHandler):

    def start(self):
        self.config = Config()
        self.cassandra = ConnectCassandra()

    def get(self):
        type = self.route("type")
        secret = self.param("secret")

        our_secret = self.config.about()['secret']

        if secret != our_secret:
            return self.error('WHAT THE HECK!? WHAT ARE YOU TRYING TO DO?!')


        type_get = type
        if type_get == "allsql":
            type_get = "sql"

        files = self.__get_files(type_get)

        if type in ["sql", "all", "allsql"]:
            self.__drop_db()

        if type == "all":
            files.extend(self.__get_files("allpatch"))
        if type == "allsql":
            files.extend(self.__get_files("patch"))

        for csql in files:
            if csql.endswith(".sql"):
                self._con.begin()
                try:
                    self._con.execute(open(csql, "r").read().decode("utf-8"))
                    self._con.commit()
                except:
                    self._con.rollback()
            else:
                self.cassandra.execute(open(csql, "r").read().decode("utf-8"))

    def __get_files(self, type="sql"):
        path = os.path.dirname(os.path.abspath(__file__))
        path = os.path.join(path, "../", "Classes", "Migrations")
        extension = None

        # Just sql patch
        if type == "patch":
            path = os.path.join(path, "Patchs")
            extension = (".sql",)
        # Just CQL patch
        if type == "cqlpatch":
            path = os.path.join(path, "Patchs")
            extension = (".cql",)
        # Just CQL
        elif type == "cql":
            extension = (".cql",)
        # Just SQL
        elif type == "sql":
            extension = (".sql",)
        # Everything
        elif type == "all":
            extension = (".cql", ".sql",)
        # Just ALL patch
        elif type == "allpatch":
            path = os.path.join(path, "Patchs")
            extension = (".cql", ".sql",)
        # Just ALL sql and cql
        elif type == "csql":
            extension = (".cql", ".sql",)

        onlyfiles = [join(path, f) for f in os.listdir(path) if isfile(join(path, f)) and f.endswith(extension)]
        onlyfiles.sort()

        return onlyfiles


    def __drop_db(self):
        # the transaction only applies if the DB supports
        # transactional DDL, i.e. Postgresql, MS SQL Server
        trans = self._con.begin()

        inspector = reflection.Inspector.from_engine(self._con.get_bind())

        # gather all data first before dropping anything.
        # some DBs lock after things have been dropped in
        # a transaction.

        metadata = MetaData()

        tbs = []
        all_fks = []

        for table_name in inspector.get_table_names():
            fks = []
            for fk in inspector.get_foreign_keys(table_name):
                if not fk['name']:
                    continue
                fks.append(
                    ForeignKeyConstraint((),(),name=fk['name'])
                    )
            t = Table(table_name,metadata,*fks)
            tbs.append(t)
            all_fks.extend(fks)

        for fkc in all_fks:
            self._con.execute(DropConstraint(fkc))

        for table in tbs:
            if str(table) != "spatial_ref_sys":
                self._con.execute(DropTable(table))
        trans.commit()