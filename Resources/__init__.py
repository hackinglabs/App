# -*- coding: utf-8 -*-

from Resources.About import AboutHandler
from Resources.Install import InstallHandler
from Resources.Build import BuildHandler
from Resources.Testing import TestingHandler
