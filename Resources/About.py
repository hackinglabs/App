from Classes.Core.APIHandler import APIHandler
from Configs import Config


class AboutHandler(APIHandler):
    def get(self):
        type = self.route("type")

        if type == "api":
            conf = Config().about()
            return self.success(conf)

        elif type == "authors":
            authors = {
                'message': 'Patacooooooooooon',
                'backend': [
                    {
                        "name": "Douglas Makey Mendez Molero",
                        "alias": ""

                    },
                    {
                        "name": "Luis Sebastian Urrutia Fuentes",
                        "alias": ""

                    }
                ],
                'frontend': [
                    {
                        'name': 'Juan Ignacio Cuzmar Sotomayor',
                        'alias': '?'
                     },
                    {
                        'name': 'Simon Iribarren Godoy',
                        'alias': '?'
                    }
                ],
                'android': [
                    {
                        'name': 'Roberto Carlos Osorio Sanhueza',
                        'alias': 'El oso'
                    },
                    {
                        'name': 'Jorge Esteban Araneda Abarca',
                        'alias': '?'
                    }
                ],
                'tech': [
                    'Python',
                    'Redis',
                    'Cassandra',
                    'PostgreSQL',
                    'NodeJS'
                ]
            }
            return self.success(authors)