# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models.State import State


class StateHandler(APIHandler):
    def start(self):
        self.repository(State)
        self.logger = logging.getLogger(__name__)

    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of states or details of a specific state
        :return: JSON response
        """

        self.logger.info("Request to State API")

        # Get area ID from Route
        state_id = self.route("id")

        try:
            # If there isn't area id we are going to return all the states
            if state_id is None:
                response = self.state.all()
            else:
                # Request a specific state
                response = self.state.find(id=state_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.state.to_dict(response))

    @logged('system')
    def post(self):
        self.logger.info("Request POST create state")

        state = self.model_process(State())

        if state is None:
            return self.error()

        # return response JSON
        return self.result(self.state.save(state))

    @logged('system')
    def put(self):
        # Get state ID from Route
        state_id = self.route('id')

        try:
            # Select specific state
            state = self.state.find(id=state_id)
        except NoneResult, e:
            return self.error(e.value)

        # validate and load data in model
        state = self.model_process(state, True)

        if state is None:
            return self.error()

        # return response JSON
        return self.result(self.state.save(state))
