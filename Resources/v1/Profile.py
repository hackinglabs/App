# -*- coding: utf-8 -*-
import json
import logging
import arrow
from argon2 import PasswordHasher
from wheezy.core.descriptors import attribute

from Classes.Core import JsonCustomEncoder
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators.Logged import logged
from Classes.Webpay import Webpay
from Classes.db import ConnectCassandra
from Configs import Config
from Models import DispatchAddress, Favorite, Subsidiary
from Models.Cassandra import OrdersUser
from Models.User import User
from Models.RedZone import RedZone
from Models.UserCard import UserCard
from Models.Cassandra.DispatcherSummary import DispatcherSummary
from Models.Cassandra.Point import Point
from Models.Cassandra.DispatcherAlert import DispatcherAlert

from Models import Redis


class ProfileHandler(APIHandler):
    def start(self):
        self.repository([
            User,
            DispatchAddress,
            Favorite,
            Subsidiary,
            RedZone,
            UserCard
        ])
        self.logger = logging.getLogger(__name__)
        self.cassandra = ConnectCassandra()

    @attribute
    def translation(self):
        return self.translations['profile']

    @logged()
    def get(self):
        user_id = self.logged["user"]
        extra = self.route("extra")

        if user_id is not None and extra == "cards":
            try:
                result = self.usercard.all(user_id=user_id)
            except:
                return self.success([])

            response = []
            for r in result:
                response.append(r.to_dict())
            return self.success(response)

        # Returns all addresses
        elif user_id is not None and extra == 'addresses':
            response = self.user.get_addresses(user_id)
            rel = {
                'dispatchaddress': ['area'],
                'area': ['city'],
                'city': ['state'],
                'state': ['country']
            }
            return self.success(self.user.to_dict(response, rel=rel, depth=4))
        # Returns all favorites
        elif user_id is not None and extra == 'favorites':
            response = self.user.get_favorites(user_id)
            rel = {
                'favorite': ['subsidiary'],
                'subsidiary': ['commerce']
            }
            return self.success(self.user.to_dict(response, depth=3, rel=rel))

        # Returns all orders
        elif user_id is not None and extra == 'orders':
            # TODO: check permission user for example "is_user" or "is_dispatcher" idea ?
            orders = OrdersUser.objects(user_id=user_id).all()
            list_orders = []
            for order in orders:
                if order.pickup:
                    client_code = order.client_code
                    user_code = order.client_code
                else:
                    client_code = order.user_code
                    user_code = order.user_code

                order_new = {
                    "id": order.id,
                    "order_date": order.order_date,
                    "products": order.products,
                    "subsidiary": order.subsidiary,
                    "total": order.total,
                    "address_order": order.address_order,
                    "dispatch_amount": order.dispatch_amount,
                    "dispatcher": order.dispatcher,
                    "pickup": order.pickup,
                    "status": order.status,
                    "correlative": order.correlative,
                    "client_code": client_code,
                    "user_code": user_code,
                    "subtotal": order.subtotal,
                    "subtotal_with_discount": order.subtotal_with_discount,
                    "coupon": order.coupon

                }
                list_orders.append(order_new)

            return self.success(list_orders, custom=True)
        elif user_id is not None and extra == 'orders_dispatcher':
            if self.permissions().has_perm("login_dispatcher"):
                orders = DispatcherSummary.objects(dispatcher_id=user_id).all()
                response = []
                for order in orders:
                    order_dict = {
                        "dispatcher_id": order.dispatcher_id,
                        "vehicle_id": order.vehicle_id,
                        "order_id": order.order_id,
                        "dispatch_amount": order.dispatch_amount,
                        "dispatch_date": order.dispatch_date,
                        "delivery_date": order.delivery_date,
                        "date": order.date,
                        "subsidiary_name": order.subsidiary_name,
                        "subsidiary_address": order.subsidiary_address
                    }

                    response.append(order_dict)

                return self.success(response, custom=True)
            else:
                return self.error(self._("account_not_dispatcher"))

        try:
            user = self.user.get_by_id(id=user_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(user.to_dict())

    @logged()
    def post(self):
        extra = self.route("extra")
        user_id = self.logged["user"]

        if user_id is not None and extra == "addresses":

            area_id = self.param("area_id")

            coords = self.param("coordinates")

            if coords is not None:
                coords = coords.split(" ")
                if self.redzone.contains(coords[1], coords[0]):
                    return self.error(self._("not_in_redzone"))

            try:
                self.request.form["user_id"] = user_id
            except KeyError:
                pass
            dispatch_address = self.model_process(DispatchAddress())

            if dispatch_address is None:
                return self.error()

            if self.dispatchaddress.save(dispatch_address):

                return self.success([dispatch_address.to_dict()])
            else:
                return self.error()

        elif user_id is not None and extra == 'favorites':
            try:
                self.request.form["user_id"] = user_id
            except KeyError:
                return self.error("KeyError in favorites")

            #validate favorite is no exits
            exists = self.favorite.exists(user_id=user_id, subsidiary_id=self.param("subsidiary_id"))

            if not exists:
                favorite = self.model_process(Favorite())

                if favorite is None:
                    return self.error()

                return self.success(self.favorite.save(favorite))
            else:
                return self.success("Subsidiary is favorite")

        elif user_id is not None and extra == "push":
            # send user for Socket and GCM
            data_aps = {
                "title": "Dxpress",
                "body": "Esto es una notificacion de prueba",
                "sound": "default",
                "icon": "default",
            }

            data = {
                "title": "Dxpress",
                "message": "Esto es una notificacion de prueba",
                "status": 0,
                "location": ""
            }

            data = json.dumps(data, cls=JsonCustomEncoder)
            self.user.save_notification(user_id=self.logged['user'], data=data, order_id="")
            self.user.send_gcm_repo(user_id=self.logged['user'], type=1, msg=data, notification=data_aps)

        elif user_id is not None and extra == 'alert':

            if not self.permissions().has_perm("login_dispatcher"):
                return self.error(self._("account_not_dispatcher"))

            if 'lat' not in self.request.form or 'lng' not in self.request.form and 'order_id' not in self.request.form:
                self.logger.error("Missing coordinates")
                return self.error(self._("missing_coordinates"))

            dispatch_alert = DispatcherAlert(dispatcher_id=user_id,
                                             order_id=self.param('order_id'),
                                             geo=Point(x=self.param('lat'), y=self.param('lng')),
                                             date=arrow.now('America/Santiago').to('utc').datetime)

            try:
                dispatch_alert.save()
            except Exception as e:
                self.logger.error("Error in POST profile/alert: " + e.message)
                return self.error(self._("cant_create_alert"))

            data = {
                "type": "alert_dispatcher",
                "message": {
                    "order_id": dispatch_alert.order_id,
                    "msg": self._("dispatcher_alert").format(id=dispatch_alert.dispatcher_id),
                    "data": dispatch_alert.date,
                    "geo": {
                        "x": self.param('lat'),
                        "y": self.param('lng')
                    }
                }
            }
            self.redis().publish("channel-monitors", json.dumps(data, cls=JsonCustomEncoder))
            return self.success()

    @logged()
    def put(self):
        # Get user ID from Route
        user_id = self.logged["user"]
        extra = self.route("extra")
        id = self.route("id")

        if extra is None:

            try:
                # Select specific user
                user = self.user.find(id=user_id)
                password = user.password
            except NoneResult, e:
                return self.error(e.value)

            birth_date = self.param('birth_date')
            if birth_date is not None:
                try:
                    birth_date = arrow.get(birth_date).date()
                except:
                    return self.error("Invalid date.")

                self.request.form['birth_date'] = birth_date

            # The user cant pass an avatar url
            if 'avatar' in self.request.form:
                del self.request.form["avatar"]

            if self.param("password") is not None:
                if self.param("password") == self.param("repeat_password"):
                    old_password = self.param("old_password")
                else:
                    return self.error(self._("invalid_repeat_password"))
                user = self.model_process(user, True, required=["old_password"])
            else:
                user = self.model_process(user, True)

            if user is None:
                return self.error()

            if self.param("password") is not None:
                ph = PasswordHasher(time_cost=15, hash_len=40, salt_len=36)
                try:
                    verify = ph.verify(password, old_password)
                except:
                    verify = False

                if not verify:
                    return self.error({"old_password": self._("password_incorrect")})

            # return response JSON
            return self.result(self.user.save(user))
        elif extra == "addresses" and id is not None:
            dispatch_id = id
            coords = self.param("coordinates")
            if coords is not None:
                coords = coords.split(" ")
                if self.redzone.contains(coords[0], coords[1]):
                    return self.error(self._("not_in_redzone"))
            try:
                dispatch_address = self.dispatchaddress.find(id=dispatch_id, user_id=user_id)
            except NoneResult, e:
                return self.error(e.value)

            model = self.model_process(dispatch_address, True)

            if model is None:
                return self.error()

            return self.result(self.dispatchaddress.save(model))

    @logged()
    def delete(self):
        extra = self.route("extra")
        user_id = self.logged["user"]
        id = self.route('id')

        if id is not None and extra == 'cards':
            try:
                card = self.usercard.find(user_id=user_id, id=id)
            except NoneResult:
                return self.error(self._("wrong_card"))

            c = Config().webpay()
            w = Webpay(path=c['path'],
                       commerce_code=c['oneclick_code'])

            try:
                result = w.remove_user(tbk_user=card.tbk_user, username=card.user_id)
            except:
                return self.error(self._("cant_remove_from_tbk"))

            if result and result['response'] is True:
                return self.result(self.usercard.delete(user_id=user_id, card_id=id))
            else:
                return self.error()

        elif id is not None and extra == 'favorites':
            try:
                favorite_delete = self._con.query(Favorite).filter(Favorite.id == id,
                                                                   Favorite.user_id == user_id).delete()
            except NoneResult, e:
                return self.error()

            if favorite_delete:
                return self.success()
            else:
                return self.error(self._("cant_delete_favorites"))
        elif id is not None and extra == 'addresses':
            response = False

            try:
                self._con.query(DispatchAddress)\
                    .filter(DispatchAddress.id == id,DispatchAddress.user_id == user_id)\
                    .delete()

                response = True

            except NoneResult, e:
                self.logger.error(str(e))
                return self.error()

            if response:
                return self.success()
            else:
                return self.error(self._("cant_delete_address"))