# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models.System import System


class SystemHandler(APIHandler):
    def start(self):
        self.repository(System)
        self.logger = logging.getLogger(__name__)


    @ajax
    @logged('system')
    def get(self):
        """
        GET HTTP Method

        Return a list of systems or details of a specific system
        :return: JSON response
        """

        self.logger.info("Request to System API")

        # Get system ID from Route
        system_id = self.route("id")

        try:
            # If there isn't system id we are going to return all the systems
            if system_id is None:
                response = self.system.all()
            else:
                # Request a specific system
                response = self.system.find(id=system_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.system.to_dict(response))

    @logged('system')
    def post(self):

        system = self.model_process(System())

        if system is None:
            return self.error()

        return self.result(self.system.save(system))

    @logged('system')
    def put(self):

        system_id = self.route("id")

        try:
            system = self.system.find(id=system_id)
        except NoneResult, e:
            return self.error(e.value)

        system = self.model_process(system, True)

        if system is None:
            return self.error()

        return self.result(self.system.save(system))
