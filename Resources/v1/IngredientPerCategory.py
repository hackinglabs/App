import logging

from wheezy.core.descriptors import attribute

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models.IngredientPerCategory import IngredientPerCategory


class IngredientPerCategoryHandler(APIHandler):
    def start(self):
        self.repository(IngredientPerCategory)
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['ingredient_category']

    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of ingredients_per_category or a specific ingredient_per_category
        :return: JSON response
        """

        # Get ID from Route
        id = self.route('id')
        type = self.param('type')
        extra = self.route('extra')
        subsidiary_id = self.param('subsidiary_id')

        try:
            if id is None and subsidiary_id is not None and type is not None and type == 'premium':
                response = self.ingredientpercategory.get_all_premium(subsidiary_id)
            elif id is None and subsidiary_id is not None and type is not None and type == 'base':
                response = self.ingredientpercategory.get_all_base(subsidiary_id)
            elif id is None and subsidiary_id is not None and type is not None and type == 'normal':
                response = self.ingredientpercategory.get_all_normal(subsidiary_id)
            elif id is None:
                response = self.ingredientpercategory.all()
            elif id is not None and extra == 'ingredients':
                response = self.ingredientpercategory.get_products_category(id)
            else:
                response = self.ingredientpercategory.find(id=id)

        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.ingredientpercategory.to_dict(response))

    @logged('system')
    def post(self):
        ingredient_per_category = self.model_process(IngredientPerCategory())

        if ingredient_per_category is None:
            return self.error()

        response = self.ingredientpercategory.save(ingredient_per_category)

        if response:
            return self.success(dict(id=ingredient_per_category.id))
        return self.error(self._('ingredient_category_not_saved'))

    @logged('system')
    def put(self):
        id = self.route("id")

        try:
            ingredient_per_category = self.ingredientpercategory.find(id=id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(ingredient_per_category)

        if model is None:
            return self.error()

        return self.result(self.ingredientpercategory.save(model))
