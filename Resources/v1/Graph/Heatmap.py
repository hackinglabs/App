# coding=utf-8
import logging


from Classes.Core.APIHandler import APIHandler
from Classes.db import ConnectCassandra
from Models.Cassandra.Order import Order


class HeatmapHandler(APIHandler):
    def start(self):
        ConnectCassandra()
        self.logger = logging.getLogger(__name__)

    def get(self):
        all = []
        orders = Order.all()

        for order in orders:
            if order.status not in ['DXL-ORDER-FINISH', 'ORDER-FINISH']:
                continue

            if order.pickup is False:
                all.append([
                    order.address_order.geo.x,
                    order.address_order.geo.y
                ])
        return self.success(all)
