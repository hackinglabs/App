# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators.Ajax import ajax
from Models.Favorite import Favorite


class FavoriteHandler(APIHandler):
    def start(self):
        self.repository(Favorite)
        self.logger = logging.getLogger(__name__)


    @ajax
    def get(self):
        """
        GET HTTP Method

        return list subisdiary favorite for user
        :return: JSON response
        """


        # Si la ruta "user" es distinto de None, esta pidiendo las direcciones del usuario (la ruta "id" es el id del user)
        # Si la ruta "user" es igual a None, esta pidiendo la direccion de id "id"
        self.logger.info("Request to Favorites")

        # Get user||favorite ID from Route
        favorite_id = self.route("id")

        # get parameter user for return all favorite of user
        user = self.route('user')

        try:
            if user is not None:
                result = self.favorite.get_all_favorite_for_user(favorite_id)
            else:
                result = self.favorite.find(id=favorite_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.favorite.to_dict(result))

    def post(self):
        favorite = self.model_process(Favorite())

        if favorite is None:
            return self.error()

        return self.result(self.favorite.save(favorite))

    def put(self):
        favorite_id = self.route("id")

        try:
            favorite = self.favorite.find(id=favorite_id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(favorite, True)

        if model is None:
            return self.error()

        return self.result(self.favorite.save(model))
