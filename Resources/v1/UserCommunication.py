# coding=utf-8
import arrow
import json
import logging

from Classes.Core.JsonCustomEncoder import JsonCustomEncoder
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.db import ConnectCassandra
from Models import DispatcherOrder
from Models import DispatcherRejected
from Models import Subsidiary
from Models import User
from Models.Cassandra import Order, OrdersUser
from Models.Cassandra.ClientOrders import ClientOrders
from Models.Cassandra.DispatcherDisconnect import DispatcherDisconnect
from Models.Cassandra.OrderNotAlliance import OrderNotAlliance
from Models.Cassandra.User import User as CUser
from Models.DispatchAddress import DispatchAddress
from Models import Redis


class UserCommunicationHandler(APIHandler):
    def start(self):
        self.repository(User)
        self.repository(Subsidiary)
        self.repository(DispatchAddress)
        self.repository(DispatcherRejected)
        self.repository(DispatcherOrder)
        self.repository("Dispatcher", redis=self.redis())
        ConnectCassandra()
        self.logger = logging.getLogger(__name__)

    @logged()
    def get(self):
        extra = self.route('extra')
        if extra == 'dispatcher_order':
            return self.__dispatcher_order(dispatcher_id=self.logged['user'])

    def post(self):
        extra = self.route('extra')
        if extra == 'connect':
            return self.__connect_dispatcher_redis()
        elif extra == 'disconnect':
            return self.__disconnect_dispatcher_redis()
        elif extra == 'accept_or_rejected':
            return self.__accept_or_rejected()
        elif extra == 'order_received_franchise':
            return self.__order_received_franchise()

    # Dispatcher connect app
    def __connect_dispatcher_redis(self):
        # change status in sql
        id = self.param('id')

        # Get Dispatcher
        PDispatcher = self.user.find(id=id)

        # Check dispatcher exists
        if PDispatcher is None:
            self.logger.error("The dispatcher with id %s doesnt exists", id)
            return self.error('user No Exists')

        # Check dispatcher have vehicle
        if not PDispatcher.driver_vehicles:
            self.logger.error("The dispatcher with id %s doesnt have vehicles", id)
            return self.error("The user doesn't have vehicles")

        # Get dispatcher in Redis
        dispatcher = Redis.Dispatcher.query.filter(id=PDispatcher.id).first()

        # Validate dispatcher in redis
        if dispatcher is None:
            # create in redis
            dispatcher = Redis.Dispatcher(id=PDispatcher.id,
                                          first_name=PDispatcher.first_name,
                                          last_name=PDispatcher.last_name,
                                          nin=PDispatcher.nin,
                                          is_active=True,
                                          vehicle_id=PDispatcher.driver_vehicles[-1].id)

            try:
                dispatcher.save()
                response = True
            except Exception as e:
                self.logger.error("Error saving dispatcher")
                self.logger.error(e.message)
                response = False
        else:
            response = False

        return self.success(response)

    # Dispatcher disconnect app
    def __disconnect_dispatcher_redis(self):
        id = self.param('id')

        # Get Dispatcher
        PDispatcher = self.user.find(id=id)

        # Check dispatcher Eexists
        if PDispatcher is None:
            self.logger.error("The dispatcher with id %s doesnt exists", id)
            return self.error('user No Exists')

        # Check dispatcher have vehicle
        if not PDispatcher.driver_vehicles:
            self.logger.error("The dispatcher with id %s doesnt have vehicles", id)
            return self.error("The user doesn't have vehicles")

        # Get dispatcher in Redis
        dispatcher = Redis.Dispatcher.query.filter(id=PDispatcher.id).first()

        # Validate dispatcher in redis
        if dispatcher is not None:
            if dispatcher.with_order:
                alert_disconnect = DispatcherDisconnect(dispatcher_id=dispatcher.id,
                                                        order_id=dispatcher.order_id,
                                                        date=arrow.now('America/Santiago').to('utc').datetime,
                                                        vehicle_id=dispatcher.vehicle_id)
                try:
                    alert_disconnect.save()
                except Exception as e:
                    return self.error('no create alert_disconnect, Error: {0}'.format(e.message))

                # Notify GCM dispatcher and socket monitors
                data = {
                    "type": "dispatcher-disconnect",
                    "message": {
                        "dispatcher_id": alert_disconnect.dispatcher_id,
                        "order_id": alert_disconnect.order_id,
                        "vehicle_id": alert_disconnect.vehicle_id,
                        "date": alert_disconnect.date,
                        "geo": "",
                        "type": 1
                    }
                }

                self.redis().publish("channel-monitors", json.dumps(data, cls=JsonCustomEncoder))

                # TODO: Notify to channel owner of the vehicles
                dispatcher.delete()

                return self.success('Disconnect with order')
            else:
                dispatcher.delete()

            return self.success('Dispatcher Disconnect')

        else:

            return self.error('Dispatcher no connect')

    def __accept_or_rejected(self):
        data_confirm = self.param('data')
        order_id = self.param('id')
        dispatch_id = self.param('user_id')

        self.logger.error(data_confirm)

        # We have to get all the data
        if data_confirm is not None and order_id is not None and dispatch_id is not None:
            # Find order from cassandra
            order = Order.objects(id=order_id).first()
            if order is None:
                self.logger.error("Order is None -- __accept_or_rejected ")

                return self.error("Order is None")

            if order.status == 'DXL-SEND-DISPATCHER':

                return self.__accept_or_rejected_dxl(order, order_id, dispatch_id, data_confirm)

            order_user = OrdersUser.objects(id=order_id, user_id=order.user.id).first()

            if order_user is None:
                self.logger.error("OrdersUser is None -- __accept_or_rejected ")

                return self.error("OrderUser is None !")

            # Filter dispatcher redis
            rdispatcher = Redis.Dispatcher.query.filter(id=dispatch_id).first()

            # verify dispatcher connect in redis
            if rdispatcher is None:
                rdispatcher = self.user.create_dispatcher_in_redis(dispatch_id)

            # Find user from postgres
            dispatch = self.user.find(id=dispatch_id)

            if data_confirm.lower() == "accept":
                # The order must be ready
                if order.status == "SEND-DISPATCHER":

                    dispatcher_order = self.dispatcherorder.find(dispatcher_id=dispatch.id, current=True)
                    if dispatcher_order is None:
                        # create dispatcher_order
                        dispatcher_order = DispatcherOrder(
                            dispatcher_id=dispatch.id,
                            order_id=str(order.id),
                            current=True
                        )
                        try:
                            self.dispatcherorder.save(dispatcher_order)
                        except Exception as e:
                            self.logger.error(e.message)
                    else:
                        self.logger.error("Dispatcher now have order current state")

                    status = 'ORDER-IN-DISPATCH'

                    order.status = status
                    order_user.status = status

                    dispatcher = CUser(
                        id=dispatch.id,
                        name=dispatch.first_name,
                        avatar=dispatch.avatar,
                        patent=dispatch.driver_vehicles[-1].vehicle.patent,
                        vehicle_id=dispatch.driver_vehicles[-1].vehicle_id
                    )

                    if order.subsidiary.commerce.alliance:
                        alliance_or_not = ClientOrders.objects(id=order_id, subsidiary_id=order.subsidiary.id).first()
                        alliance_or_not.dispatcher = dispatcher
                        alliance_or_not.status = status

                        if alliance_or_not is None:
                            self.logger.error("client_order is None -- __accept_or_rejected ")

                            return self.error("client_order is None !")

                    else:
                        alliance_or_not = OrderNotAlliance(id=order.id,
                                                           subsidiary_id=order.subsidiary.id,
                                                           dispatcher_id=dispatcher.id,
                                                           order_date=order.order_date,
                                                           correlative=order.correlative,
                                                           order_ready=order.order_ready,
                                                           order_paid=arrow.now('America/Santiago').to(
                                                               'utc').datetime,
                                                           discounts=order.discounts,
                                                           order_confirmation=order.order_confirmation,
                                                           order_preparation=order.order_preparation,
                                                           dispatcher=dispatcher,
                                                           subsidiary=order.subsidiary,
                                                           products=order.products,
                                                           payment=order.payment,
                                                           status=status,
                                                           subtotal=order.subtotal,
                                                           subtotal_with_discount=order.subtotal_with_discount,
                                                           subtotal_without_discounts=order.subtotal_without_discounts)

                    # add dispatcher on order in cassandra
                    order.dispatcher = dispatcher
                    order_user.dispatcher = dispatcher

                    # add order in dispatcher in redis
                    rdispatcher.with_order = True
                    rdispatcher.order_id = order_id

                    # update order and dispatcher
                    try:
                        order.update()
                        order_user.update()
                        alliance_or_not.save()
                        rdispatcher.save()
                    except Exception as e:
                        self.logger.error('dont update order or dispatcher_redis: ' + e.message)

                        return self.error('dont update order or dispatcher_redis')

                    data = {
                        "type": "found-dispatcher",
                        "message": {
                            "order_id": str(order.id),
                            "name": dispatch.first_name,
                            "nin": dispatch.nin,
                            "avatar": order.dispatcher.avatar,
                            "patent": order.dispatcher.patent
                        },
                        "subsidiary_id": order.subsidiary.id
                    }
                    self.redis().publish("channel-clients", json.dumps(data))

                    return self.success('Ok')

                else:
                    return self.error('Order: status invalid')
            elif data_confirm.lower() == 'reject':
                # validate count_rejected dispatcher

                rejected = rdispatcher.count_rejected
                if rejected is None or rejected == 0:
                    rejected = 1
                else:
                    rejected += 1

                if rejected >= 3:
                    # TODO: Block dispatcher
                    # Force to disconnect
                    data = {
                        "id": rdispatcher.id,
                        "message": "Haz rechazado 3 ordenes seguidas, tu usuario ha sido bloqueado por favor comunicate con Soporte"
                    }
                    self.redis().publish("disconnect_force", json.dumps(data))

                rdispatcher.count_rejected = rejected

                # Save in model dispatcher_rejected
                dispatcher_rejected = DispatcherRejected()
                dispatcher_rejected.order_id = order_id
                dispatcher_rejected.user_id = dispatch.id

                try:
                    self.dispatcherrejected.save(dispatcher_rejected)
                    rdispatcher.save()
                except Exception as e:
                    self.logger.error('not update dispatcher in rejected order error: ' + e.message)

                    return self.error('not update dispatcher in rejected order')

                try:
                    subsidiary = self.subsidiary.find(id=order.subsidiary.id)
                except NoneResult:

                    return self.error('Subsidiary No Found')

                if subsidiary is not None:
                    geo = subsidiary.to_geo()
                    # Subsidiary wasn't found. How to fuck!!
                    key = "dispatcher-not-found"
                    data = {
                        'type': key,
                        'message': {
                            'order_id': str(order.id),
                            'subsidiary': {
                                'id': subsidiary.id,
                                'lat': geo.lat,
                                'lng': geo.lon
                            }
                        }
                    }
                    self.redis().publish("channel-dispatch-found", json.dumps(data, cls=JsonCustomEncoder))

                    return self.success('Search Again')

                else:

                    return self.error('Subsidiary None')

        else:

            return self.error('no receive parameters correct')

    def __accept_or_rejected_dxl(self, order, order_id, dispatch_id, data_confirm):

        # Filter dispatcher redis
        rdispatcher = Redis.Dispatcher.query.filter(id=dispatch_id).first()

        # verify dispatcher connect in redis
        if rdispatcher is None:
            rdispatcher = self.user.create_dispatcher_in_redis(dispatch_id)

        # Find user from postgres
        dispatch = self.user.find(id=dispatch_id)

        if data_confirm.lower() == "accept":
            # The order must be ready
            if order.status == "DXL-SEND-DISPATCHER":

                # Create dispatcherOrder
                dispatcher_order = self.dispatcherorder.find(dispatch.id, current=True)
                if dispatcher_order is None:

                    # create dispatcher_order
                    dispatcher_order = DispatcherOrder(dispatcher_id=dispatch.id,
                                                       order_id=str(order.id),
                                                       current=True)
                    try:
                        self.dispatcherorder.save(dispatcher_order)
                    except Exception as e:
                        self.logger.error(e.message)
                else:
                    self.logger.error("Dispatcher now have order current state")

                status = 'DXL-ORDER-IN-DISPATCH'

                order.status = status

                dispatcher = CUser(
                    id=dispatch.id,
                    name=dispatch.first_name,
                    avatar=dispatch.avatar,
                    patent=dispatch.driver_vehicles[-1].vehicle.patent,
                    vehicle_id=dispatch.driver_vehicles[-1].vehicle_id
                )

                alliance_or_not = ClientOrders.objects(id=order.id, subsidiary_id=order.subsidiary.id).first()
                alliance_or_not.dispatcher = dispatcher

                # add dispatcher on order in cassandra
                order.dispatcher = dispatcher

                # add order in dispatcher in redis
                rdispatcher.with_order = True
                rdispatcher.order_id = order_id

                # update order and dispatcher
                try:
                    order.update()
                    alliance_or_not.save()
                    rdispatcher.save()
                except Exception as e:
                    self.logger.error('dont update order or dispatcher_redis: ' + e.message)

                    return self.error('dont update order or dispatcher_redis')

                # Notify subsidiary
                data = {
                    "type": "found-dispatcher",
                    "message": {
                        "order_id": str(order.id),
                        "name": dispatch.first_name,
                        "nin": dispatch.nin,
                        "avatar": order.dispatcher.avatar,
                        "patent": order.dispatcher.patent
                    },
                    "subsidiary_id": order.subsidiary.id
                }

                self.redis().publish("channel-clients", json.dumps(data))

                return self.success('Ok')

            else:

                return self.error('Order: status invalid')

        elif data_confirm.lower() == 'reject':
            # validate count_rejected dispatcher

            rejected = rdispatcher.count_rejected
            if rejected is None or rejected == 0:
                rejected = 1
            else:
                rejected += 1

            if rejected >= 3:
                # TODO: Bloquear al despachador
                # Force to disconnect
                data = {
                    "id": rdispatcher.id,
                    "message": "Haz rechazado 3 ordenes seguidas, tu usuario ha sido bloqueado por favor comunicate con Soporte"
                }
                self.redis().publish("disconnect_force", json.dumps(data))

            rdispatcher.count_rejected = rejected

            try:
                rdispatcher.save()
            except Exception as e:
                return self.error('not update dispatcher in rejected order error:' + e.message)

            try:
                subsidiary = self.subsidiary.find(id=order.subsidiary.id)
            except NoneResult:

                return self.error('Subsidiary No Found')

            if subsidiary is not None:
                geo = subsidiary.to_geo()
                # Subsidiary wasn't found. How to fuck!!
                key = "dispatcher-not-found"
                data = {
                    'type': key,
                    'message': {
                        'order_id': str(order.id),
                        'subsidiary': {
                            'id': subsidiary.id,
                            'lat': geo.lat,
                            'lng': geo.lon
                        }
                    }
                }

                self.redis().publish("channel-dispatch-found", json.dumps(data, cls=JsonCustomEncoder))

                return self.success('Search Again')

            else:

                return self.error('Subsidiary None')

    def __order_received_franchise(self):
        id = self.param('order_id')
        self.logger.error('OrderReceived {0}'.format(id))

        if id is not None:
            order = Order.objects(id=id).first()
            self.logger.error(order)

            if order is None:
                self.logger.error("Order doesn't exist - '__client_code' ")

                return self.error("Order don't exists")

            if order.status == 'DXL-ORDER-IN-DISPATCH':

                return self.__order_received_franchise_dxl(order)

            order_by_user = OrdersUser.objects(id=id, user_id=order.user.id).first()
            client_order = ClientOrders.objects(id=id, subsidiary_id=order.subsidiary.id).first()

            status = order.status.lower()
            if status != 'order-in-dispatch' and status != 'order-ready':
                self.logger.error('Order: status invalid')
                return self.error('Order: status invalid')

            order.status = 'ORDER-TRACK'
            order.dispatch_date = arrow.now('America/Santiago').to('utc').datetime.replace(tzinfo=None)

            order_by_user.status = 'ORDER-TRACK'
            order_by_user.dispatch_date = arrow.now('America/Santiago').to('utc').datetime.replace(tzinfo=None)

            client_order.status = 'ORDER-FINISH'

            try:
                order.update()
                order_by_user.update()
                client_order.update()
            except Exception as e:
                self.logger.error("Error in update order on client_code: " + e.message)

                return self.error('Order no update')

            # Emmit client Ok
            data = {
                "type": "order-validate",
                "message": {
                    'message': 'Codigo valido',
                    'order_id': order.id
                },
                "subsidiary_id": order.subsidiary.id
            }
            self.redis().publish("channel-clients", json.dumps(data, cls=JsonCustomEncoder))

            # Emmit dispatcher Ok
            data_dispatcher = {
                "id": order.id
            }

            queue_name = 'dispatcher_' + str(order.dispatcher.id) + '_confirmation'

            if not self.rabbit.publish(queue_name, json.dumps(data_dispatcher, cls=JsonCustomEncoder)):

                return self.error(self._("cant_send_order"))

            # Emmit user for socket and gcm
            # dispatch-found
            try:
                subsidiary = self.subsidiary.find(id=order.subsidiary.id)
                geo = subsidiary.to_geo()
                subsidiary_coordinates = {'x': geo.lat, 'y': geo.lon}
            except NoneResult:
                subsidiary_coordinates = None

            data = {
                "type": "dispatch-found",
                "message": {
                    "order_id": str(order.id),
                    "dispatch": {
                        "id": order.dispatcher,
                        "name": order.dispatcher.name,
                        "avatar": order.dispatcher.avatar,
                        "patent": order.dispatcher.patent,
                        "destination": order.address_order.geo,
                        "subsidiary": subsidiary_coordinates
                    },
                    "user_code": order.user_code
                },
                "user_id": order.user.id
            }
            self.redis().publish("channel-users", json.dumps(data, cls=JsonCustomEncoder))

            data_aps = {
                "title": "Dxpress",
                "body": 'Despachador encontrado',
                "sound": "default",
                "icon": "default",
            }

            data = {
                "title": "Dxpress",
                "message": 'Despachador encontrado',
                "status": 5,
                "location": {
                    "id": order.dispatcher.id,
                    "name": order.dispatcher.name,
                    "avatar": order.dispatcher.avatar,
                    "patent": order.dispatcher.patent,
                    "user_code": order.user_code
                }
            }

            data = json.dumps(data, cls=JsonCustomEncoder)

            # Test Send gcm from UserRepository, give token database
            self.user.send_gcm_repo(user_id=order.user.id, type=1, msg=data, notification=data_aps)

            return self.success()

        else:
            self.logger.error('Bad parameters')

            return self.error("bad parameters")

    def __order_received_franchise_dxl(self, order):

        order = Order.objects(id=order.id).first()
        if order is None:
            self.logger.error("Order doesn't exist - '__client_code_dxl' ")

            return self.error("Order doesn't  exists")

        client_order = ClientOrders.objects(id=order.id, subsidiary_id=order.subsidiary.id).first()

        if client_order is None:
            self.logger.error("ClientOrders doesn't exist - '__client_code_dxl' ")
            return self.error('ClientOrders dont exists')

        now = arrow.now('America/Santiago').to('utc').datetime
        order.dispatch_date = now

        order.status = "DXL-DISPATCH"
        client_order.status = 'DXL-FINISH'

        try:
            order.save()
            client_order.save()
        except Exception as e:
            self.logger.error("Error update order in client_code: " + e.message)
            return self.error(self._("order_not_updated"))

        # Emmit CLIENT Ok
        data = {
            "type": "order-validate",
            "message": {
                'message': self._("valid_code"),
                'order_id': order.id
            },
            "subsidiary_id": order.subsidiary.id
        }

        self.redis().publish("channel-clients", json.dumps(data, cls=JsonCustomEncoder))

        # Emmit User Track
        # Notify user MAP
        msg = '{0} Ya salió su pedido de {1}, puede ver el repartidor haciendo click en el link'\
            .format(order.reference, order.subsidiary.commerce.name)

        self.send_message(order.user.telephone, msg)

        # Emmit dispatcher OK
        data_dispatcher = {
            "id": order.id
        }

        queue_name = 'dispatcher_' + str(order.dispatcher.id) + '_confirmation'

        if not self.rabbit.publish(queue_name, json.dumps(data_dispatcher, cls=JsonCustomEncoder)):
            return self.error('Cant send order')

        return self.success()

    def __dispatcher_order(self, dispatcher_id):
        # Init order
        response = None

        # Search dispatcherOrder
        dispatcher_order = self.dispatcherorder.find(dispatcher_id=dispatcher_id, current=True)

        if dispatcher_order is None:
            return self.error(response)
        else:
            # Get Order
            order = Order.objects(id=dispatcher_order.order_id).first()

            # Get Subsidiary
            subsidiary = self.subsidiary.find(id=order.subsidiary.id)

            if subsidiary is not None:
                lat = subsidiary.to_geo().lat
                lng = subsidiary.to_geo().lon

            if order.subsidiary.commerce.alliance:
                order_type = 1
            else:
                order_type = 2

            response = {
                "type": order_type,
                "order": {
                    'id': str(order.id),
                    'status': order.status,
                    'correlative': order.correlative,
                    'name': order.subsidiary.alias,
                    'logo': order.subsidiary.commerce.logo,
                    'client_code': order.client_code,
                    'order_user': order.user.id,
                    'user': order.user.name,
                    'franchise': order.subsidiary.commerce.franchise,
                    'dxl': order.dxl,
                    'order_location': {
                        'lat': order.address_order.geo.y,
                        'lng': order.address_order.geo.x,
                        'address': order.address_order.address,
                        'block': order.address_order.block,
                        'apartment': order.address_order.apartment,
                        'reference': order.address_order.reference
                    },
                    'subsidiary_location': {
                        'lat': lat,
                        'lng': lng,
                        'address': subsidiary.address
                    },
                    'commerce_name': order.subsidiary.commerce.name,
                    'products': order.products,
                    'clarification': order.reference,
                    'overprice_percentage': order.subsidiary.overprice_percentage
                }
            }

            return self.success(response, custom=True)
