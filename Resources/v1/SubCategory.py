import logging
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Core.Exceptions.UploadError import UploadError
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models.SubCategory import SubCategory


class SubCategoryHandler(APIHandler):
    def start(self):
        self.repository(SubCategory)
        self.logger = logging.getLogger(__name__)



    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of devices or SubCategories of a specific SubCategory
        :return: JSON response
        """

        # Get sub_category ID from Route
        sub_category_id = self.route('id')

        # Get param extra in route
        extra = self.route('extra')

        try:
            if sub_category_id is None:
                response = self.subcategory.all()
            elif sub_category_id is not None and extra is not None:
                if self.permissions().has_perm("system"):
                    products = self.subcategory.get_products(sub_category_id)
                else:
                    products = self.subcategory.get_products_isEnabled(sub_category_id)
                return self.success(self.to_dict(products))
            else:
                # Request a specific sub_category
                response = self.subcategory.find(id=sub_category_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.subcategory.to_dict(response))

    @logged('system')
    def post(self):
        sub_category = self.model_process(SubCategory())

        upload_file = self.file("upload")
        if upload_file is not None:
            try:
                url = self.upload(upload_file)
                self.request.form["image"] = url
            except UploadError, e:
                return self.error(e.value)
            except:
                return self.error("There is an error with file uploading.")

        if sub_category is None:
            return self.error()

        # return response JSON
        if self.subcategory.save(sub_category):
            return self.success(dict(id=sub_category.id))
        return self.error()

    @logged('system')
    def put(self):
        # Get state ID from Route
        sub_category_id = self.route('id')

        try:
            # Select specific sub_category
            sub_category = self.subcategory.find(id=sub_category_id)
        except NoneResult, e:
            return self.error(e.value)

        upload_file = self.file("upload")
        if upload_file is not None:
            try:
                url = self.upload(upload_file)
                self.request.form["image"] = url
            except UploadError, e:
                return self.error(e.value)
            except:
                return self.error("There is an error with file uploading.")

        # validate and load data in model
        sub_category = self.model_process(sub_category, True)

        if sub_category is None:
            return self.error()

        # return response JSON
        return self.success(self.subcategory.save(sub_category))

    @logged('system')
    def delete(self):
        subcategory_id = self.route("id")
        return self.success(self.subcategory.delete(subcategory_id))
