import logging

from wheezy.core.descriptors import attribute

from Classes.Core.APIHandler import APIHandler
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models.OwnerDispatcher import OwnerDispatcher


class OwnerDispatcherHandler(APIHandler):
    def start(self):
        self.logger = logging.getLogger(__name__)
        self.repository(OwnerDispatcher)

    @attribute
    def translation(self):
        return self.translations['owner']

    @logged('system')
    @ajax
    def get(self):
        pass

    @logged('system')
    @ajax
    def post(self):
        temp_user_id = self.post('temp_user_id')

        if temp_user_id is None:
            return self.error(self._("missing_id"))

        response = self.ownerdispatcher.create(temp_user_id)

        if response:
            return self.success()
        else:
            return self.error()


    @logged('system')
    @ajax
    def put(self):
        pass

    @logged('system')
    @ajax
    def delete(self):
        pass
