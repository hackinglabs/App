# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Core.Exceptions.UploadError import UploadError
from Classes.Decorators.Ajax import ajax
from Classes.Decorators.Logged import logged
from Models.Category import Category


class CategoryHandler(APIHandler):
    def start(self):
        self.repository(Category)
        self.logger = logging.getLogger(__name__)

    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of categories or details of a specific category
        :return: JSON response
        """

        # Get area ID from Route
        category_id = self.route('id')

        # Products per category
        extra = self.route('extra')

        try:
            if category_id is None:
                response = self.category.all()

            elif category_id is not None and extra == "sub_categories":
                sub_categories = self.category.get_all_sub_categories(category_id)
                return self.success(self.category.to_dict(sub_categories))

            elif category_id is not None and extra == "compounds":
                compounds = self.category.get_compounds(category_id)
                if compounds is None or len(compounds) == 0:
                    return self.success([])
                return self.success(self.to_dict(compounds))
            else:
                response = self.category.find(id=category_id)

        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.category.to_dict(response))

    @ajax
    @logged('system')
    def post(self):
        """
        POST HTTP Method

        Create a new Category
        :return: JSON response
        """

        category = self.model_process(Category())

        if category is None:
            return self.error()

        if self.category.save(category):
            return self.success(dict(id=category.id))
        return self.error()

    @ajax
    @logged('system')
    def put(self):
        """
        PUT HTTP Method

        Modify a category by ID
        :return:
        """

        category_id = self.route("id")

        try:
            category = self.category.find(id=category_id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(category, True)

        if model is None:
            return self.error()

        return self.success(self.category.save(model))

    @logged('system')
    def delete(self):
        category_id = self.route("id")
        category = self.category.find(id=category_id)
        delete = self.category.delete(category_id)
        if delete:
            for subcategory in category.sub_categories:
                subcategory.enabled = False
                self.subsidiary.save(subcategory)
        return self.success()
