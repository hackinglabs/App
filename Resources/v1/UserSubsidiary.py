import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models.UserSubsidiary import UserSubsidiary


class UserSubsidiaryHandler(APIHandler):
    def start(self):
        self.repository(UserSubsidiary)
        self.logger = logging.getLogger(__name__)


    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of subsidiaries or details of a specific subsidiary
        :return: JSON response
        """

        self.logger.info("Request to user_subsidiary")

        # Get role ID from Route
        user_subsidiary_id = self.route("id")

        try:
            # If there isn't role id we are going to return all the roles
            if user_subsidiary_id is None:
                response = self.usersubsidiary.all()
            else:
                response = self.usersubsidairy.find(id=user_subsidiary_id)
        except NoneResult, e:
            return self.error(e.value)

        # Return states of specific role
        return self.success(
            self.usersubsidiary.to_dict(response)
        )

    @logged('system')
    def post(self):
        self.logger.info("Request POST create UserSubsidiary")

        user_subsidiary = self.model_process(UserSubsidiary())
        if user_subsidiary is None:
            return self.error()

        # return response JSON
        return self.result(
            self.usersubsidiary.save(user_subsidiary)
        )
    @logged('system')
    def put(self):
        # Get role ID from Route
        user_subsidiary_id = self.route('id')

        try:
            # Select specific role
            user_subsidiary = self.usersubsidiary.find(id=user_subsidiary_id)
        except NoneResult, e:
            return self.error(e.value)

        # validate and load data in model
        user_subsidiary = self.model_process(user_subsidiary, True)

        if user_subsidiary is None:
            return self.error()

        # return response JSON
        return self.result(self.usersubsidiary.save(user_subsidiary))
