# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models.Coupon import Coupon


class CouponHandler(APIHandler):
    def start(self):
        self.repository(Coupon)
        self.logger = logging.getLogger(__name__)

    @ajax
    @logged('system')
    def get(self):
        """
        GET HTTP Method

        Return a list of Coupons or details of a specific Coupon
        :return: JSON response
        """

        # Get Coupon ID from Route
        coupon_id = self.route("id")

        try:
            # If there isn't coupon id we are going to return all the coupons
            if coupon_id is None:
                response = self.coupon.all()
            else:
                # Request a specific coupon_id
                response = self.coupon.find(id=coupon_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.coupon.to_dict(response), custom=True)

    @logged('system')
    def post(self):
        coupon = Coupon()
        if 'type_id' in self.request.form:
            coupon.type_id = self.request.form['type_id']
            del self.request.form['type_id']

        coupon = self.model_process(coupon)

        if coupon is None:
            return self.error()

        if self.coupon.exists(code=coupon.code):
            return self.error("A coupon with that code already exists.")

        coupon.code = coupon.code.upper()
        return self.result(self.coupon.save(coupon))

    @logged('system')
    def put(self):
        coupon_id = self.route("id")

        try:
            coupon = self.coupon.find(id=coupon_id)
        except NoneResult, e:
            return self.error(e.value)

        if 'type_id' in self.request.form:
            coupon.type_id = self.request.form['type_id']
            del self.request.form['type_id']

        if 'code' in self.request.form:
            del self.request.form['code']

        model = self.model_process(coupon, True)

        if model is None:
            return self.error()

        coupon.code = coupon.code.upper()
        return self.result(self.coupon.save(model))

    @logged('system')
    def delete(self):
        coupon_id = self.route("id")

        return self.success(self.coupon.delete(coupon_id))
