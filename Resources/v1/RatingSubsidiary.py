# -*- coding: utf-8 -*-
import logging

from sqlalchemy.sql import func

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators.Ajax import ajax
from Models import Subsidiary
from Models.RatingSubsidiary import RatingSubsidiary


class RatingSubsidiaryHandler(APIHandler):
    def start(self):
        self.repository([
            RatingSubsidiary,
            Subsidiary
        ])
        self.logger = logging.getLogger(__name__)

    @ajax
    def get(self):
        """
        GET HTTP Method

        :return: JSON response
        """

        self.logger.info("Request to RatingSubsidiary")

        # Get user||tag ID from Route
        id = self.route("id")

        # get parameter subsidiary for return all ratings of subsidiary
        slug = self.route("slug")

        self.logger.error(id)
        self.logger.error(slug)

        try:
            if slug == "subsidiary" and id is not None:
                ratings_for_subsidiary = self.ratingsubsidiary.ratings(id)
                return self.success(self.to_dict(ratings_for_subsidiary))
            elif slug is None and id is not None:
                rating = self.ratingsubsidiary.find_with_user(id)
                return self.success(rating.to_dict())
        except NoneResult, e:
            return self.error(e.value)

    @ajax
    def post(self):
        self.logger.info("Request POST create RatingSubsidiary")

        if self.request.form['qualification'] > 5:
            return self.error(msg="Dont  '5' max qualification")
        rating_subsidiary = self.model_process(RatingSubsidiary())

        if rating_subsidiary is None:
            return self.error()

        create = self.ratingsubsidiary.save(rating_subsidiary)

        if create:
            av = self._con.query(func.avg(RatingSubsidiary.qualification)).filter(
                RatingSubsidiary.subsidiary_id == rating_subsidiary.subsidiary_id).all()
            av = round(float(av[0][0]), 1)
            subsidiary = self.subsidiary.find(id=rating_subsidiary.subsidiary_id)
            subsidiary.rating_average = av
            self.subsidiary.save(subsidiary)

        return self.result(create)

    @ajax
    def put(self):
        rating_id = self.route("id")

        try:
            rating_subsidiary = self.ratingsubsidiary.find(rating_id)
        except:
            return self.error()

        model = self.model_process(rating_subsidiary)

        if model is None:
            return self.error()

        return self.result(
            self.ratingsubsidiary.save(model)
        )
