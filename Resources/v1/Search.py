# -*- coding: utf-8 -*-
import json
import logging
import arrow
from Classes import ConnectCassandra
from Classes.Core import JsonCustomEncoder
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Models import Subsidiary
from Models.Cassandra import Point, Order
from Models.Cassandra.CommerceNoFound import CommerceNoFound
from Models.Cassandra.TagNoFound import TagNoFound
from Models.Dictionary import Dictionary
from Models.DispatchAddress import DispatchAddress


class SearchHandler(APIHandler):
    def start(self):
        self.repository(Dictionary)
        self.repository([
            Subsidiary,
            "Dispatcher",
            DispatchAddress
        ], redis=self.redis())
        self.logger = logging.getLogger(__name__)
        ConnectCassandra()

    def get(self):
        route = self.route("extra")
        str = self.param("q")

        if route == "commerce":
            to_search = self.__validate_general(xy=False, id=True)
        elif route == "radius":
            to_search = self.__validate_general(xy=True, id=False)
        else:
            to_search = self.__validate_general(xy=True, id=True)

        # If it isn't a dict, are errors
        if not isinstance(to_search, dict):
            return to_search

        # We have to save the str from search
        if 'id' in to_search and to_search['id'] == -1 and str is not None:
            lat = self.param("lat")
            lon = self.param("lon")
            if route == "commerce":
                commerce_no_found = CommerceNoFound(
                    commerce=str.lower(),
                    date=arrow.now('America/Santiago').to('utc').datetime)
                try:
                    commerce_no_found.save()
                except:
                    return self.error('no save commerce_no_found')

            elif route == "tag":
                tag_no_found = TagNoFound(
                    tag=str.lower(),
                    geo=Point(x=lat, y=lon),
                    date=arrow.now('America/Santiago').to('utc').datetime)
                try:
                    tag_no_found.save()
                except:
                    return self.error('no save tag_no_found')

        # Define the type of search
        to_search['type'] = route


        # Set default kms to search
        to_search['kms'] = 5

        response = self.subsidiary.search(**to_search)

        if to_search['type'] == "radius":
            if 'total' in response and response['total'] <= 0 or 'response' in response and len(
                    response['response']) == 0:
                self.logger.warning("Buscando en 40 KMS")
                to_search['kms'] = 8
                to_search['type'] = "radius"
                response = self.subsidiary.search(**to_search)

        return self.success(obj=response)

    def post(self):
        route = self.route("extra")
        if route == "dispatcher":
            return self.__find_dispatcher()

    def __find_dispatcher(self):
        lat = self.param("lat")
        lng = self.param("lng")
        order_id = self.param("order_id")

        if order_id is None:
            self.logger.error("Error in find_dispatcher missing order_id")
            return None

        order = Order.objects(id=order_id).first()
        if order is None:
            self.logger.error("Error in find_dispatcher order is None")
            return self.error("DONT")

        self.logger.error(order.status)
        if order.status not in ['ORDER-PAID', 'SEND-DISPATCHER', 'ORDER-READY', 'DXL-READY', 'DXL-SEND-DISPATCHER']:
            self.logger.error('Orden in __find_dispatcher status invalid')
            return self.error('Order status invalid')

        subsidiary = self.subsidiary.find(id=order.subsidiary.id)

        if subsidiary is not None:
            lat = subsidiary.to_geo().lat
            lng = subsidiary.to_geo().lon

        dispatcher = self.dispatcher.find(lat, lng)

        if dispatcher is None:
            data = {
                'type': "dispatcher-not-found",
                'message': {
                    'order_id': str(order.id),
                    'subsidiary': {
                        'id': order.subsidiary.id,
                        'lat': lat,
                        'lng': lng
                    }
                }
            }
            self.redis().publish("channel-dispatch-found", json.dumps(data, cls=JsonCustomEncoder))
            return self.success('Search again')
        else:

            self.redis().zrem("geo:locations:geo_dispatchers", str(dispatcher))

            if order.status in ['DXL-READY', 'DXL-SEND-DISPATCHER']:
                order.status = 'DXL-SEND-DISPATCHER'
            else:
                order.status = "SEND-DISPATCHER"

            try:
                order.update()
            except Exception as e:
                self.logger.error("Error in update order, find dispatcher: " + e.message)
                return self.error('Dont update order')

            self.logger.error('Franchise')
            self.logger.error(order.subsidiary.commerce.franchise)
            data = {
                "type": 1,
                "order": {
                    'id': str(order.id),
                    "correlative": order.correlative,
                    'name': order.subsidiary.alias,
                    'logo': order.subsidiary.commerce.logo,
                    'client_code': order.client_code,
                    'order_user': order.user.id,
                    'user': order.user.name,
                    'franchise': order.subsidiary.commerce.franchise,
                    'order_location': {
                        'lat': order.address_order.geo.y,
                        'lng': order.address_order.geo.x,
                        'address': order.address_order.address,
                        'block': order.address_order.block,
                        'apartment': order.address_order.apartment,
                        'reference': order.address_order.reference
                    },
                    'subsidiary_location': {
                        'lat': lat,
                        'lng': lng,
                        'address': subsidiary.address
                    },
                    'commerce_name': order.subsidiary.commerce.name
                }
            }

            if order.subsidiary.commerce.alliance or order.status in ['DXL-READY', 'DXL-SEND-DISPATCHER']:
                if not self.rabbit.publish(queue='dispatcher_' + str(dispatcher) + '_orders',
                                           msg=json.dumps(data)):
                    return self.error("We couldn't send the order to the dispatcher.")
                # delete key count in redis
                key = "orders_count_" + str(order.id)
                self.redis().delete(key)
            else:
                try:
                    del data['order']['client_code']
                except KeyError:
                    pass

                data['type'] = 2
                data['order']['products'] = order.products
                data['order']['clarification'] = order.reference
                data['order']['overprice_percentage'] = order.subsidiary.overprice_percentage

                if not self.rabbit.publish(queue='dispatcher_' + str(dispatcher) + '_orders',
                                           msg=json.dumps(data, cls=JsonCustomEncoder)):

                    return self.error("We couldn't send the order to the dispatcher.")

            self.logger.error('Dispatcher {0} orderID {1}'.format(dispatcher, order_id))

            return self.success("Dispatcher Found")

    def __validate_general(self, xy=True, id=False):
        validate_id = id

        offset = self.param("offset")
        limit = self.param("limit")
        order = self.param("filter")

        error_messages = {}

        if offset is None or offset < 0:
            offset = 0
        if limit is None or limit < 1:
            limit = 20

        if validate_id is True:
            id = self.param("id")
            if id is None:
                error_messages['id'] = ['Required field cannot be left blank.']

            try:
                id = int(id)
            except Exception as e:
                self.logger.error('Error line 224: ' + e.message)
                # TODO: Block User
                error_messages['id'] = ['Must be a valid number.']

            if id <= 0 and id != -1:
                # TODO: Block User
                error_messages['id'] = ['Must be a number greater than 0.']

        if xy is True:
            lat = self.param("lat")
            lon = self.param("lon")
            kms = self.param("kms")

            if lat is None:
                error_messages['lat'] = ["Required field cannot be left blank."]
            if lon is None:
                error_messages['lon'] = ["Required field cannot be left blank."]
            if kms is None:
                kms = 2.5

            try:
                lat = float(lat)
            except:
                # TODO: BLOQUEAR USUARIO
                error_messages['lat'] = ["Field must be a float"]

            try:
                lon = float(lon)
            except:
                # TODO: BLOQUEAR USUARIO
                error_messages['lon'] = ["Field must be a float"]

            if lat <= -90 or lat > 90:
                # TODO: BLOQUEAR USUARIO
                error_messages['lat'] = ["Must be a valid longitude"]

            if lon <= -180 or lon > 180:
                # TODO: BLOQUEAR USUARIO
                error_messages['lon'] = ["Must be a valid latitude"]

            try:
                kms = float(kms)
            except:
                # TODO: BLOQUEAR USUARIO
                error_messages['kms'] = ["Field must be a number"]

        try:
            offset = int(offset)
        except:
            # TODO: BLOQUEAR USUARIO
            error_messages['offset'] = ["Field must be a number"]

        try:
            limit = int(limit)
        except:
            # TODO: BLOQUEAR USUARIO
            error_messages['limit'] = ["Field must be a number"]

        parsed = self.__get_order(order)

        order = parsed['order']
        where = parsed['where']

        if order is None or len(order) <= 0:
            # TODO: BLOQUEAR USUARIO
            error_messages['order'] = ["mal-formatted"]

        if len(error_messages) > 0:
            return self.error(error_messages)

        result = {
            'offset': offset,
            'limit': limit,
            'order': order,
            'where': where
        }

        if xy is True:
            result.update({
                'lat': lat,
                'lon': lon,
                'kms': kms
            })
        if validate_id is True:
            result.update({
                'id': id
            })
        return result

    def __get_order(self, order):
        order_filter = []
        where_filter = []
        if order is None:
            order_filter.append({
                'type': "rating",
                'by': "desc"
            })
        elif "|" in order:
            orders = order.lower().split("|")
            for order in orders:
                parsed = self.__parse_single_order(order)
                if parsed is not None:
                    if parsed['where']:
                        where_filter.append(parsed)
                    else:
                        order_filter.append(parsed)
        else:
            parsed = self.__parse_single_order(order)
            if parsed is not None:
                if parsed['where']:
                    where_filter.append(parsed)
                else:
                    order_filter.append(parsed)

        return {
            'order': order_filter,
            'where': where_filter
        }

    def __parse_single_order(self, order):
        where = False
        allowed_order = frozenset(["name", "distance", "time", "price", "rating"])
        if ":" in order:
            order = order.lower().split(":")

            if order[1] != "desc" and order[1] != "asc" and not order[1].isdigit():
                # TODO: BLOQUEAR USUARIO
                return None
            if order[0] not in allowed_order:
                # TODO: BLOQUEAR USUARIO
                return None

            if order[1].isdigit():
                where = True

            ordertype = order[1]
            orderby = order[0]
        else:
            if order not in allowed_order:
                # TODO: BLOQUEAR USUARIO
                return None

            orderby = order
            if order in ("name", "rating", "price"):
                ordertype = "desc"
            else:
                ordertype = "asc"
        return {
            'type': ordertype,
            'by': orderby,
            'where': where
        }
