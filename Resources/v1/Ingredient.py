# -*- coding: utf-8 -*-
import logging

from wheezy.core.descriptors import attribute

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models.Ingredient import Ingredient


class IngredientHandler(APIHandler):
    def start(self):
        self.repository(Ingredient)
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['ingredient']

    @ajax
    def get(self):

        """
        GET HTTP Method

        Return a list of ingredients or a specific ingredient
        :return: JSON response
        """

        self.logger.info("Request to  Ingredient")

        # Get presentation ID from Route
        ingredient_id = self.route('id')

        try:
            if ingredient_id is None:
                response = self.ingredient.all()
            else:
                response = self.ingredient.find(id=ingredient_id)

        except NoneResult, e:
            return self.error(e.value)

        return self.success(
            self.ingredient.to_dict(response, rel={'ingredient': ['category']}, depth=1))

    @logged('system')
    def post(self):

        ingredient = self.model_process(Ingredient())

        if ingredient is None:
            return self.error()

        return self.result(self.ingredient.save(ingredient))

    @logged('system')
    def put(self):
        ingredient_id = self.route("id")

        try:
            ingredient = self.ingredient.find(id=ingredient_id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(ingredient)

        if model is None:
            return self.error()

        return self.result(self.ingredient.save(model))

    @logged('system')
    def delete(self):
        ingredient_id = self.route("id")

        response = self.ingredient.delete(ingredient_id)

        if response is None:
            return self.error(self._("ingredient_not_exists"))
        else:
            return self.success(response)

