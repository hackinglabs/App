import logging

from wheezy.core.descriptors import attribute

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models import Compound


class CompoundHandler(APIHandler):
    def start(self):
        self.repository(Compound)
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['compound']

    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of compunds or details of a specific compound
        :return: JSON response
        """

        # Get compound ID from Route
        compound_id = self.route("id")

        try:
            # If there isn't compound id we are going to return all the compounds
            if compound_id is None:
                response = self.compound.all()
            else:
                # Request a specific country
                response = self.compound.find(id=compound_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.compound.to_dict(response))

    def post(self):
        compound = self.model_process(Compound())
        if compound is None:
            return self.error()

        if self.compound.save(compound):
            return self.success(dict(id=compound.id))
        else:
            return self.error(self._('compound_not_saved'))

    @logged('system')
    def put(self):
        compound_id = self.route("id")

        try:
            compound = self.compound.find(id=compound_id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(compound, True)

        if model is None:
            return self.error()

        return self.result(self.compound.save(model))

    @logged('system')
    def delete(self):
        compound_id = self.route("id")
        delete = self.compound.delete(compound_id)
        if delete:
            return self.success(self._('compound_deleted'))
        else:
            return self.error(self._("compound_not_deleted"))
