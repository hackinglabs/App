# -*- coding: utf-8 -*
import logging

from wheezy.core.descriptors import attribute

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Core.Exceptions.UploadError import UploadError
from Classes.Decorators import cache, logged
from Classes.Decorators.Ajax import ajax
from Models.Product import Product


class ProductHandler(APIHandler):
    def start(self):
        self.repository(Product)
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['product']

    @ajax
    def get(self):

        # Get id from route
        product_id = self.route("id")

        # compound product
        compound = self.route('compound')

        try:
            if product_id is None:
                response = self.product.all()
            elif product_id is not None and compound == 'compound':
                response = self.product.product_compound(product_id)
            elif product_id is not None and compound == "options":
                response = self.product.get_options(product_id)
            elif product_id is not None and compound == "details":
                response = self.product.get_details(product_id)

                return self.success(response, custom=True)
            else:
                response = self.product.find(id=product_id)
                rel = {
                    'product': ['sub_category'],
                    'subcategory': ['category']

                }
                return self.success(self.product.to_dict(response, depth=3, rel=rel))
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.product.to_dict(response))

    @logged('system')
    def post(self):
        self.logger.info("Request POST create Product")

        product = self.model_process(Product())

        if product is None:
            return self.error()

        if self.product.save(product):
            response = True
            self.product.create_or_use_tag(product)
            if not product.is_compounds and not product.options_ingredient:
                compound = self.product.create_compound(product)
                if compound:
                    self.logger.info("Compound created")
                else:
                    self.logger.info('Dont')
            return self.success(dict(id=product.id))
        return self.error()

    @logged('system')
    def put(self):
        product_id = self.route("id")

        try:
            product = self.product.find(id=product_id)
        except NoneResult, e:
            return self.error(e.value)

        model_product = self.model_process(product, True)

        if model_product is None:
            return self.error()

        if self.product.save(model_product):
            update = self.product.update_compound(product)
            if update:
                return self.success()
            elif update is None:
                return self.error(self._("compound_not_associated"))
            else:
                self.logger.error("Error in update Product and Compound")
                return self.error(self._("cant_update"))
        else:
            return self.error()

    @logged('system')
    def delete(self):
        product_id = self.route("id")
        if product_id is not None:
            if self.product.delete(id):
                return self.success('Ok')
            else:
                return self.error('Dont')
