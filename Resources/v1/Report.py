# coding=utf-8
import logging

import arrow

from Classes.Core.APIHandler import APIHandler
from Classes.Decorators import logged
from Classes.db import ConnectCassandra
from Models.Cassandra.ClientOrders import ClientOrders


class ReportHandler(APIHandler):
    def start(self):
        ConnectCassandra()
        self.logger = logging.getLogger(__name__)

    @logged('system')
    def get(self):
        route = self.route("extra")
        if route is not None:
            method = getattr(self, "get_{slug}".format(slug=route), None)
            return method()
        else:
            return self.get_default()

    def get_sales(self):
        all = []
        subsidiary_id = self.param("subsidiary_id")
        start = self.param("start")
        end = self.param("end")

        #Filter for finish
        orders = ClientOrders.filter(status="ORDER-FINISH")

        #TODO: Mejorar start y end
        if start is not None and end is not None:
            start = arrow.get(start).replace(tzinfo='America/Santiago')
            end = arrow.get(end).replace(tzinfo='America/Santiago')

            start = start.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
            end = end.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")

            orders = orders.filter(order_date__gte=start.datetime)
            orders = orders.filter(order_date__lte=end.datetime)

        if subsidiary_id is not None:
            orders = orders.filter(subsidiary_id=subsidiary_id)

        #get with filter
        orders = orders.allow_filtering().all()

        for order in orders:
            all.append(order)


        return self.success(all, custom=True)

    def get_user_purchases(self):
        pass
