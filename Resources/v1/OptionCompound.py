import logging

from wheezy.core.descriptors import attribute

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Models import OptionCompound


class OptionCompoundHandler(APIHandler):
    def start(self):
        self.repository(OptionCompound)
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['option_compound']

    @logged('system')
    def post(self):
        option_compound = self.model_process(OptionCompound())
        if option_compound is None:
            return self.error()

        if self.optioncompound.save(option_compound):
            return self.success(dict(id=option_compound.id))
        return self.error(self._("not_created"))

    @logged('system')
    def put(self):
        option_compound_id = self.route("id")

        try:
            option_compound = self.optioncompound.find(id=option_compound_id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(option_compound, True)

        if model is None:
            return self.error()

        return self.result(self.optioncompound.save(model))

    @logged('system')
    def delete(self):
        id = self.route('id')
        if id is not None:
            if self.optioncompound.delete(id):
                return self.success(self._('deleted'))
            else:
                return self.error(self._('not_deleted'))
