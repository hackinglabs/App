import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models import CommerceUser


class CommerceUserHandler(APIHandler):
    def start(self):
        self.repository(CommerceUser)
        self.logger = logging.getLogger(__name__)

    @ajax
    def get(self):

        # Get role ID from Route
        commerce_user_id = self.route("id")

        response = None

        try:
            if commerce_user_id is not None:
                response = self.commerceuser.find(id=commerce_user_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.commerceuser.to_dict(response))

    @logged()
    def post(self):

        commerce_user = self.model_process(CommerceUser())
        if commerce_user is None:
            return self.error()

        # return response JSON
        return self.result(
            self.commerceuser.save(commerce_user)
        )

    @logged()
    def put(self):
        # Get role ID from Route
        commerce_user_id = self.route('id')

        try:
            # Select specific role
            commerce_user = self.commerceuser.find(id=commerce_user_id)
        except NoneResult, e:
            return self.error(e.value)

        # validate and load data in model
        commerce_user = self.model_process(commerce_user, True)

        if commerce_user is None:
            return self.error()

        # return response JSON
        return self.result(self.commerceuser.save(commerce_user))
