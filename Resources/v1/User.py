# -*- coding: utf-8 -*-
import logging
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators.Logged import logged
from Helpers.Security import Security
from Models.User import User


class UserHandler(APIHandler):
    def start(self):
        self.repository(User)
        self.logger = logging.getLogger(__name__)

    @logged('system')
    def get(self):
        """
        GET HTTP Method

        Return a list of users or details of a specific user
        :return: JSON response
        """

        # Get user ID from Route
        user_id = self.route("id")
        extra = self.route("extra")

        try:
            # If there isn't tag id we are going to return all the users
            if user_id is None:
                users = self.user.all_with_group()
                user_dict = self.to_dict(users, replace={"user_groups": "groups"})

                for user in user_dict:
                    ids = [value['group_id'] for value in user['groups']]
                    user['groups'] = ids

                return self.success(user_dict)
            if extra == "addresses":
                response = self.user.get_addresses(user_id)
                rel = {
                    'dispatchaddress': ['area'],
                    'area': ['city'],
                    'city': ['state'],
                    'state': ['country']
                }
                return self.success(self.user.to_dict(response, rel=rel, depth=4))
            else:
                # Request a specific user
                response = self.user.find_with_group(user_id)
                user_dict = response.to_dict(replace={"user_groups": "groups"})
                user_dict['groups'] = [value['group_id'] for value in user_dict['groups']]

                return self.success(user_dict)
        except NoneResult, e:
            return self.error(e.value)

    @logged('system')
    def post(self):
        try:
            groups_id = self.request.form["groups"]
        except KeyError:
            groups_id = None
        if not isinstance(groups_id, list):
            self.logger.warning("Groups id isn't list.")
            groups_id = None

        # TODO: Create password
        self.request.form['salt'] = Security.generate_random(32)

        user = self.model_process(User())

        if user is None:
            return self.error()

        # return response JSON
        return self.result(self.user.create_user(user, groups_id=groups_id))

    @logged('system')
    def put(self):
        # Get user ID from Route
        user_id = self.route("id")

        try:
            groups_id = self.request.form["groups"]
        except KeyError:
            groups_id = None
        if not isinstance(groups_id, list):
            self.logger.warning("Groups id isn't list.")
            groups_id = None

        try:
            # Select specific user
            user = self.user.find(id=user_id)
        except NoneResult, e:
            return self.error(e.value)

        user = self.model_process(user, True)

        if user is None:
            return self.error()

        # return response JSON
        return self.result(self.user.edit_user(user, groups_id=groups_id))
