# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators.Ajax import ajax
from Classes.Decorators.Logged import logged
from Models.City import City


class CityHandler(APIHandler):
    def start(self):
        self.repository(City)
        self.logger = logging.getLogger(__name__)

    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of cities or details of a specific city
        :return: JSON response
        """

        # Get city ID from Route
        city_id = self.route("id")

        try:
            if city_id is None:
                response = self.city.all()
            else:
                response = self.city.find(id=city_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.city.to_dict(response))

    @ajax
    @logged('system')
    def post(self):

        city = self.model_process(City())

        if city is None:
            return self.error()

        return self.result(self.city.save(city))

    @ajax
    @logged('system')
    def put(self):
        city_id = self.route("id")

        try:
            city = self.city.find(id=city_id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(city, True)

        if model is None:
            return self.error()

        return self.result(self.city.save(model))
