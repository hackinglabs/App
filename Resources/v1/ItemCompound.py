import logging

from wheezy.core.descriptors import attribute

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Models import ItemCompound
from Models.OptionCompound import OptionCompound


class ItemCompoundHandler(APIHandler):
    def start(self):
        self.repository(ItemCompound)
        self.repository(OptionCompound)
        self.logger = logging.getLogger(__name__)

    def get(self):
        pass

    @attribute
    def translation(self):
        return self.translations['item_compound']

    @logged('system')
    def post(self):
        if 'option_compound_id' in self.request.form:
            option_id = self.request.form['option_compound_id']
        else:
            return self.error("You must send the field option_compound_id")

        if 'compounds_id' in self.request.form:
            compounds_id = self.request.form['compounds_id']
        else:
            return self.error("You must send the field compounds_id")

        option = self.optioncompound.find(id=option_id)

        if option.items_compounds is not None:
            delete_all = self.itemcompound.delete_all(option_id)
            if not delete_all:
                return self.error(self._('item_compound_not_deleted'))

        if isinstance(compounds_id, int):
            model = ItemCompound(option_compound_id=option_id, compound_id=compounds_id)
            return self.result(self.itemcompound.save(model))
        elif isinstance(compounds_id, list):
            try:
                for compound in compounds_id:
                    model = ItemCompound(option_compound_id=option_id, compound_id=compound)
                    self.itemcompound.save(model)

                return self.success(self._("item_compound_saved"))
            except:
                return self.error(self._("item_compound_not_saved"))

    @logged('system')
    def put(self):
        item_compound_id = self.route("id")

        try:
            item_compound = self.itemcompound.find(id=item_compound_id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(item_compound, True)

        if model is None:
            return self.error()

        return self.result(self.itemcompound.save(model))
