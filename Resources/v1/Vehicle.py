# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models.Vehicle import Vehicle


class VehicleHandler(APIHandler):
    def start(self):
        self.repository(Vehicle)
        self.logger = logging.getLogger(__name__)

    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of devices or vehicles of a specific vehicle
        :return: JSON response
        """


        # Get Vehicle ID from Route
        vehicle_id = self.route("id")

        try:
            # If there isn't presentation id we are going to return all the presentations
            if vehicle_id is None:
                response = self.vehicle.all()
                return self.success(self.to_dict(response))
            else:

                # Request a specific vehicle
                response = self.vehicle.find(id=vehicle_id)
                return self.success(response.to_dict())

        except NoneResult, e:
            return self.error(e.value)

    @logged('system')
    def post(self):

        vehicle = self.model_process(Vehicle())

        if vehicle is None:
            return self.error()


        if self.vehicle.save(vehicle):
            return self.success(dict(id=vehicle.id))
        return self.error()

    @logged('system')
    def put(self):

        # Get vehicle id from Route
        vehicle_id = self.route("id")


        try:
            # Select specific vehicle
            vehicle = self.vehicle.find(id=vehicle_id)
        except NoneResult, e:
            return self.error(e.value)

        # validate and load data in model
        vehicle = self.model_process(vehicle, True)

        if vehicle is None:
            return self.error()

        return self.result(self.vehicle.save(vehicle))
