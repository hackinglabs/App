# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models import CommerceTag
from Models.Tag import Tag


class TagHandler(APIHandler):
    def start(self):
        self.repository(Tag)
        self.repository(CommerceTag)
        self.logger = logging.getLogger(__name__)


    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of tags or details of a specific tag
        :return: JSON response
        """

        self.logger.info("Request to Tag API")

        # Get tag ID from Route
        tag_id = self.route("id")

        try:
            # If there isn't tag id we are going to return all the tags
            if tag_id is None:
                self.logger.info("Request all tags")
                tags = self.tag.all_names()
                response = self.to_dict(tags)
            elif tag_id == "names":
                tags = self.tag.all_names()
                response = self.to_dict(tags)
            else:
                self.logger.info("Request specific tag %s", tag_id)
                # Request a specific tag
                tag = self.tag.find(id=tag_id)
                return self.success(tag.to_dict())
        except NoneResult, e:
            return self.error(e.value)

        return self.success(response)

    @logged('system')
    def post(self):

        tag = self.model_process(Tag())

        if tag is None:
            return self.error()

        return self.result(self.tag.save(tag))

    @logged('system')
    def put(self):

        tag_id = self.route("id")

        try:
            tag = self.tag.find(id=tag_id)
        except NoneResult, e:
            return self.error(e.value)

        tag = self.model_process(tag)

        if tag is None:
            return self.error()

        return self.result(self.tag.save(tag))
