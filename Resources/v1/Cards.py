# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Decorators.Ajax import ajax
from Classes.Decorators.Logged import logged
from Models.UserCard import UserCard


class CardHandler(APIHandler):
    def start(self):
        self.repository(UserCard)
        self.logger = logging.getLogger(__name__)

