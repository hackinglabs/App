import logging
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Models.User import User
from Models.Notification import Notification
from wheezy.core.descriptors import attribute
from Classes.Decorators import logged


class NotificationHandler(APIHandler):
    def start(self):
        self.logger = logging.getLogger(__name__)
        self.repository(User)
        self.repository(Notification)

    @attribute
    def translation(self):
        return self.translations['notification']

    @logged()
    def get(self):
        user_id = self.logged['user']
        notifications = self.notification.get_notifications_for_user(user_id)
        return self.success(self.to_dict(notifications))

    def put(self):
        notification_id = self.route("id")

        try:
            notification = self.notification.find(id=notification_id)
        except NoneResult, e:
            return self.error(e.value)

        notification.is_check = True

        return self.result(self.notification.save(notification))
