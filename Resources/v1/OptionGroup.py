import logging

from wheezy.core.descriptors import attribute

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models.IngredientPerCategory import IngredientPerCategory
from Models.OptionGroup import OptionGroup


class OptionGroupHandler(APIHandler):
    def start(self):
        self.repository(OptionGroup)
        self.repository(IngredientPerCategory)
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['option_group']

    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of options_group or a specific option_group
        :return: JSON response
        """

        self.logger.info("Request to  OptionGroup")

        # Get OptionGroup ID from Route
        id = self.route('id')

        try:
            if id is None:
                response = self.optiongroup.all()
            else:
                response = self.optiongroup.find(id=id)

        except NoneResult, e:
            return self.error(e.value)

        return self.success(
            self.optiongroup.to_dict(response)
        )

    @logged('system')
    def post(self):
        self.logger.info('Post OptionGroup')

        ingredient_per_category_id = self.param("ingredient_per_category_id")
        quantity = self.param("quantity")

        try:
            ingredient_per_category_id = int(ingredient_per_category_id)
        except ValueError:
            self.logger.error("ingredient_per_category_id not cast to int")
            return self.error("ingredient_per_category_id not cast to int")

        if ingredient_per_category_id is not None:
            ingredient_per_category = self.ingredientpercategory.find(id=ingredient_per_category_id)

            if ingredient_per_category.is_base == True or ingredient_per_category.is_premium == True:
                self.request.form["quantity"] = 0

        option_group = self.model_process(OptionGroup())

        if option_group is None:
            return self.error()

        return self.result(self.optiongroup.save(option_group))

    @logged('system')
    def put(self):
        id = self.route("id")

        try:
            option_group = self.optiongroup.find(id=id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(option_group)

        if model is None:
            return self.error()

        return self.result(self.optiongroup.save(model))

    @logged('system')
    def delete(self):
        id = self.route('id')
        if id is not None:
            if self.optiongroup.delete(id):
                return self.success(self._("deleted"))
            else:
                return self.error(self._("not_deleted"))

