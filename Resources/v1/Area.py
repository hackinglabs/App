# -*- coding: utf-8 -*-
import logging

from wheezy.core.descriptors import attribute

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators.Ajax import ajax
from Classes.Decorators.Logged import logged
from Models.Area import Area


class AreaHandler(APIHandler):
    def start(self):
        self.repository(Area)
        self.logger = logging.getLogger(__name__)

    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of commerces or details of a specific commerce
        :return: JSON response
        """
        self.logger.info("Request to Area API")

        # Get area ID from Route
        area_id = self.route("id")
        extra = self.route("extra")

        if extra == "valid":
            if self.is_production:
                areas = self.area.get_area_valid()
            else:
                areas = self.area.all()

            return self.success(self.to_dict(areas))
        try:
            # If there isn't area id we are going to return all the areas
            if area_id is None:
                response = self.area.all()
            else:
                # Request a specific commerce
                response = self.area.find(id=area_id)
        # If find or all return None
        except NoneResult, e:
            return self.error(e.value)

        # Relation
        rel = {
            'area': ['city'],
            'city': ['state'],
            'state': ['country']
        }
        # Return states of specific commerce
        return self.success(self.area.to_dict(response, depth=3, rel=rel))

    @ajax
    @logged('system')
    def post(self):
        
        area = self.model_process(Area())

        if area is None:
            return self.error()

        return self.result(self.area.save(area))

    @ajax
    @logged('system')
    def put(self):
        area_id = self.route("id")

        try:
            area = self.area.find(id=area_id)
        except NoneResult, e:
            return self.error(msg=e.value)

        model = self.model_process(area, force=True)

        if model is None:
            return self.error()

        return self.result(self.area.save(model))
