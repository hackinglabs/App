# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models.RatingDispatcher import RatingDispatcher
from Models.User import User
from Models.RatingDispatcher import RatingDispatcher
from wheezy.core.descriptors import attribute


class RatingDispatcherHandler(APIHandler):
    def start(self):
        self.repository([
            User,
            RatingDispatcher

        ])
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['rating_dispatcher']

    @ajax
    @logged()
    def get(self):
        pass

    @ajax
    @logged()
    def post(self):
        self.request.form['user_id'] = self.logged['user']
        q = self.param('quantity')

        if q is None:
            return self.success()

        try:
            q = int(q)
        except ValueError:
            q = 0

        self.request.form['quantity'] = q

        keys = frozenset([u'user_id', u'quantity', u'dispatcher_id', u'comment'])
        form_keys = frozenset(self.request.form.keys())

        if not keys.issubset(form_keys):
            return self.error("Invalid keys")

        if self.user.exists(id=self.param("dispatcher_id")):
            rating_dispatcher = self.ratingdispatcher.create(self.request.form)
        else:
            return self.error(self._("dispatcher_not_exists"))

        if rating_dispatcher is not None:
            if self.ratingdispatcher.save(rating_dispatcher):
                return self.success()
            else:
                return self.error("Dont save")
        else:
            return self.error("Dont create")
