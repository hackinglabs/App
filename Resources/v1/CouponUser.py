# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators.Ajax import ajax
from Models import CouponUser


class CouponUserHandler(APIHandler):
    def start(self):
        self.repository(CouponUser)
        self.logger = logging.getLogger(__name__)



    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of CouponsUsers or details of a specific CouponUser
        :return: JSON response
        """
        self.logger.info("Request to CouponUser API")

        # Get CouponUser ID from Route
        coupon_user_id = self.route("id")

        try:
            # If there isn't CouponUser id we are going to return all the CouponsUsers
            if coupon_user_id is None:
                response = self.couponuser.all()
            else:
                # Request a specific coupon_user_id
                response = self.couponuser.find(id=coupon_user_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.couponuser.to_dict(response))

    # TODO: establecer sistema de roles
    def post(self):
        coupon_user = self.model_process(CouponUser())

        if coupon_user is None:
            return self.error()

        return self.result(self.couponuser.save(coupon_user))

    # TODO: establecer sistema de roles
    def put(self):
        coupon_user_id = self.route("id")

        try:
            coupon_user = self.couponuser.find(id=coupon_user_id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(coupon_user, True)

        if model is None:
            return self.error()

        return self.result(self.couponuser.save(model))
