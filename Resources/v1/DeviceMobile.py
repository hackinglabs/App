# coding=utf-8
import json
import logging

import arrow

from Classes.Core import JsonCustomEncoder
from Classes.Core.APIHandler import APIHandler
from Classes.Decorators import logged
from Models.DeviceMobile import DeviceMobile
from Models import Redis
from Models.User import User


class DeviceMobileHandler(APIHandler):
    def start(self):
        self.repository(DeviceMobile)
        self.repository(User)
        self.logger = logging.getLogger(__name__)

    def get(self):
        pass

    @logged()
    def post(self):
        token = self.param("token")
        os = self.param('os')


        # Add token used by user to postgres
        if not self.devicemobile.exists(token=token):
            self.logger.error("Token don't exists")

            try:
                self.request.form['user_id'] = self.logged['user']
                self.request.form['os'] = str(self.param('os'))
            except Exception as e:
                self.logger.warning('No set variable in request: ' + e.message)
                return self.error("No set variable in request")

            device_mobile = self.model_process(DeviceMobile())

            if device_mobile is None:
                self.logger.error("device_mobile is None")
                return self.error()

            save = self.devicemobile.save(device_mobile)
            self.logger.debug("Saved DeviceMobile: %s", save)

            if save:
                return self.success()
        else:
            self.devicemobile.update_user(token, self.logged['user'])

        return self.success()

    def put(self):
        pass