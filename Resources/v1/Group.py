# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Models import Group


class GroupHandler(APIHandler):
    def start(self):
        self.repository(Group)
        self.logger = logging.getLogger(__name__)

    @logged('system')
    def get(self):
        """
        GET HTTP Method

        Return a list of devices or Groups of a specific Group
        :return: JSON response
        """

        self.logger.info("Request to  Group")

        # Get Group ID from Route
        group_id = self.route("id")

        try:
            # If there isn't discount_subsidiary id we are going to return all the discounts_subsidiaries
            if group_id is None:
                response = self.group.all()
            elif group_id == 'commerce':
                users = self.group.get_commerce_users()
                return self.success(self.to_dict(users))
            else:
                self.logger.info("Request specific Group %s", group_id)

                # Request a specific Group
                response = self.group.find(id=group_id)
                return self.success(response.to_dict())

        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.to_dict(response))

    @logged('system')
    def post(self):
        self.logger.info("Request POST create Group")

        group = self.model_process(Group())

        if group is None:
            return self.error()

        return self.result(self.group.save(group))

    @logged('system')
    def put(self):

        # Get Group ID from Route
        group_id = self.route("id")

        self.logger.info("Request specific Group %s", group_id)

        try:
            # Select specific device
            group = self.group.find(id=group_id)
        except NoneResult, e:
            return self.error(e.value)

        # validate and load data in model
        model = self.model_process(group, True)

        if model is None:
            return self.error()

        return self.result(self.group.to_dict(model))

