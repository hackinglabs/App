# -*- coding: utf-8 -*-
import logging

from wheezy.core.descriptors import attribute

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models import User
from Models.DriverVehicle import DriverVehicle
from Models.Vehicle import Vehicle


class DriverVehicleHandler(APIHandler):
    def start(self):
        self.repository(DriverVehicle)
        self.repository(User)
        self.repository(Vehicle)
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['driver']

    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of devices or driversvehicle of a specific drivervehicle
        :return: JSON response
        """

        # Get driverVehicle ID from Route
        driver_vehicle_id = self.route("id")
        user_id = self.param("user_id")
        vehicle_id = self.param("vehicle_id")

        try:
            # If there isn't drivervehicle id we are going to return all the driver_vehicles

            if driver_vehicle_id is None and vehicle_id is None and user_id is not None:
                self.logger.warning("Return last_vehicle form user")
                vehicle = self.user.last_vehicle(user_id)
                if vehicle is None:
                    return self.error(self._("vehicle_not_assigned_to_user"))
                else:

                    return self.success(vehicle.to_dict())
            elif driver_vehicle_id is None and user_id is None and vehicle_id is not None:
                driver_vehicle = self.vehicle.last_driver(vehicle_id)
                if driver_vehicle is None:
                    return self.error(self._("vehicle_not_assigned_to_user"))
                else:
                    response = {
                        "vehicle": driver_vehicle.to_dict(),
                        "driver": driver_vehicle.last_driver().user.to_dict()
                    }
                    return self.success(response)

            elif driver_vehicle_id is None:
                response = self.drivervehicle.all()
                return self.success(self.to_dict(response))
            else:
                driver_vehicle = self.drivervehicle.find(id=driver_vehicle_id)
                return self.success(driver_vehicle.to_dict())
        except NoneResult, e:
            return self.error(e.value)

    @logged('system')
    def post(self):

        driver_vehicle = self.model_process(DriverVehicle())

        if driver_vehicle is None:
            return self.error()

        return self.result(self.drivervehicle.save(driver_vehicle))

    @logged('system')
    def put(self):
        # Get presentation id from Route
        driver_vehicle_id = self.route("id")

        try:
            # Select specific presentation
            driver_vehicle = self.drivervehicle.find(id=driver_vehicle_id)
        except NoneResult, e:
            return self.error(e.value)

        # validate and load data in model
        driver_vehicle = self.model_process(driver_vehicle, True)

        if driver_vehicle is False:
            return self.error()

        return self.result(self.drivervehicle.save(driver_vehicle))
