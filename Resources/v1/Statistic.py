# -*- coding: utf-8 -*-

import math
import logging
from operator import itemgetter
import arrow

from Classes import ConnectCassandra
from Classes.Core.APIHandler import APIHandler
from Classes.Decorators import logged
from Models.Cassandra.CommerceSummary import CommerceSummary
from Models.Subsidiary import Subsidiary
from Models.Vehicle import Vehicle
from Models.User import User
from Models.Cassandra import DispatcherSummary
from Models.Cassandra.ClientOrders import ClientOrders


class StatisticHandler(APIHandler):
    def start(self):
        self.repository([Subsidiary,
                         Vehicle,
                         User])
        self.cassandra = ConnectCassandra()
        self.logger = logging.getLogger(__name__)

    @logged()
    def get(self):
        # Type of statistics: date or graph
        stat_type = self.param("type")
        slug = self.route("slug")

        stat_type = "data"

        method = getattr(self, "get_{slug}_{type}".format(slug=slug, type=stat_type), None)

        if stat_type == "graph":
            # Graph only work with ranges
            try:
                start = self.param("start")
                end = self.param("end")
                start = arrow.get(start)
                end = arrow.get(end)

                # Call to method graph
                return method(start, end)
            except:
                return self.error("date mal-formatted")
        elif stat_type == "data":
            # Data work with a single date
            date = self.param("date")
            if date is not None:
                try:
                    date = arrow.get(date).replace(tzinfo='America/Santiago')
                except:
                    return self.error("date mal-formatted")
            else:
                date = arrow.now('America/Santiago')

            # Call to method data
            return method(date)

        else:
            return self.error("Unknown type")

    @logged()
    def get_dispatcher_data(self, date):
        dispatcher_id = self.logged['user']
        #vehicle = self.user.last_vehicle(self.logged['user'])

        dispatch = self.user.find(id=dispatcher_id)
        vehicle = dispatch.driver_vehicles[-1]

        self.logger.info("GET LAST VEHICLE")
        self.logger.info(vehicle.vehicle_id)
        self.logger.info(date)
        self.logger.info(dispatcher_id)

        if vehicle is None:
            self.logger.error("You dont have any vehicle. - get_dispatcher_data")
            return self.error("You dont have any vehicle.")

        vehicle_id = vehicle.vehicle_id

        return self.__get_data_vehicle_dispatcher(date, vehicle_id, dispatcher_id)

    @logged()
    def get_vehicle_data(self, date):
        user_perms = self.permissions()

        if not user_perms.has_perm("system") or \
                not user_perms.has_perm("login_dispatcher"):
            return self.error("You don't have enough permissions to see vehicle statistics")

        vehicle_id = self.route("id")
        dispatcher_id = self.param("dispatcher")

        if vehicle_id is None:
            return self.error()

        vehicles_id = self.vehicle.all_vehicles_id(self.logged['user'])
        dispatchers_id = self.user.get_all_dispatchers_id(self.logged['user'])

        # Check if vehicle belongs to him
        if vehicle_id and vehicle_id not in vehicles_id:
            return self.error("That vehicle doesn't belong to you.")

        # Check if he can see all vehicle statistics
        if not user_perms.has_perm("all_dispatchers_statistics") and \
                        dispatcher_id is None:
            return self.error("You don't have enough permissions to see vehicle statistics.")

        # Check if he can see statistics for an specific user
        if not user_perms.has_perm("dispatchers_statistics") and \
                        dispatcher_id not in dispatchers_id:
            return self.error("That dispatcher isn't working for you.")

        return self.__get_data_vehicle_dispatcher(date, vehicle_id, dispatcher_id)

    def __get_data_vehicle_dispatcher(self, date, vehicle_id, dispatcher_id):
        ranges = self.__format_ranges(date)

        info = {
            'dispatch': {},
            'revenue': {}
        }

        # Loop into days, weeks, months, years
        for key in ranges.keys():

            quantity = {}
            total = {}
            # Loop into previous and working
            for when in ranges[key].keys():
                summaries = self.__get_vehicle_summary_range(vehicle_id, dispatcher_id, ranges[key][when])
                temp_info = self.__parse_vehicle_statistic(summaries)
                if temp_info is None:
                    return self.error("")

                quantity[when] = temp_info["quantity"]
                total[when] = temp_info["total"]

            growing_dispatch = 1.0
            growing_total = 1.0

            if quantity['working'] == quantity['previous']:
                growing_dispatch = 0.0
            elif quantity['previous'] > 0:
                growing_dispatch = (float(quantity['working']) - float(quantity['previous'])) / float(
                    quantity['previous'])

            if total['working'] == total['previous']:
                growing_total = 0.0
            elif total['previous'] > 0:
                growing_total = (float(total['working']) - float(total['previous'])) / float(total['previous'])

            info["dispatch"][key] = {}
            info["revenue"][key] = {}

            info["dispatch"][key]["quantity"] = quantity['working']
            info["dispatch"][key]["growing"] = growing_dispatch

            info['revenue'][key]['amount'] = total['working']
            info['revenue'][key]['growing'] = growing_total
        return self.success(info)

    def get_subsidiary_data(self, date):
        subsidiary_id = self.route("id")

        if subsidiary_id is None:
            return self.error()

        # DEFINE DATES AND RANGES
        ranges = self.__format_ranges(date)

        # Final info dict
        info = {
            'orders': {},
            'sales': {},
            'sales_wo': {},
            'products': {},
            'paid_from': {}
        }

        commerce_id = self.subsidiary.get_commerce(subsidiary_id)
        if commerce_id is not None:
            self.logger.info("COMMERCE ID: " + str(commerce_id))
        else:
            self.logger.error("commerce id is none")

        # Loop into days, weeks, months, years
        for key in ranges.keys():
            # Loop into previous and working
            quantity = {}
            total = {}
            total_wo = {}
            products = None
            payments = None

            for when in ranges[key].keys():
                orders = self.__get_client_orders_range(subsidiary_id, commerce_id, ranges[key][when])
                temp_info = self.__parse_client_orders_statistic(orders)
                if temp_info is None:
                    return self.error("")

                if when == "working":
                    products = temp_info['products']
                    payments = temp_info['paid_from']

                quantity[when] = temp_info["quantity"]
                total[when] = temp_info["total"]
                total_wo[when] = temp_info["total_without_tax"]

            growing_orders = 1.0
            growing_total = 1.0
            growing_total_wo = 1.0

            if quantity['working'] == quantity['previous']:
                growing_orders = 0.0
            elif quantity['previous'] > 0:
                growing_orders = (float(quantity['working']) - float(quantity['previous'])) / float(
                    quantity['previous'])

            if total['working'] == total['previous']:
                growing_total = 0.0
            elif total['previous'] > 0:
                growing_total = (float(total['working']) - float(total['previous'])) / float(total['previous'])

            if total_wo['working'] == total_wo['previous']:
                growing_total_wo = 0.0
            elif total_wo['previous'] > 0:
                growing_total_wo = (float(total_wo['working']) - float(total_wo['previous'])) / float(
                    total_wo['previous'])

            info["orders"][key] = {}
            info["sales"][key] = {}
            info["sales_wo"][key] = {}

            info["orders"][key]["quantity"] = quantity['working']
            info["orders"][key]["growing"] = growing_orders

            info['sales'][key]['amount'] = int(math.floor(total['working']))
            info['sales'][key]['growing'] = growing_total

            info['sales_wo'][key]['amount'] = int(math.floor(total_wo['working']))
            info['sales_wo'][key]['growing'] = growing_total_wo

            info['products'][key] = products
            info['paid_from'][key] = payments

        return self.success(info)

    def __get_client_orders_range(self, id, commerce_id, args):
        if not isinstance(args, dict):
            return None

        start = args['start'].replace(hour=0, minute=0, second=0, microsecond=0).to('utc')
        end = args['end'].replace(hour=23, minute=59, second=59, microsecond=999999).to('utc')

        orders = CommerceSummary.objects.filter(subsidiary_id=id)
        orders = orders.filter(commerce_id=commerce_id)
        orders = orders.filter(date__gte=start.datetime)
        orders = orders.filter(date__lte=end.datetime)
        return orders.allow_filtering().all()

    def __get_vehicle_summary_range(self, id, dispatcher_id, args):
        if not isinstance(args, dict):
            return None

        start = args['start'].replace(hour=0, minute=0, second=0, microsecond=0).to('utc')
        end = args['end'].replace(hour=23, minute=59, second=59, microsecond=999999).to('utc')

        summary = DispatcherSummary.objects.filter(vehicle_id=id)
        if dispatcher_id is not None:
            summary = summary.filter(dispatcher_id=dispatcher_id)
        summary = summary.filter(date__gte=start.datetime)
        summary = summary.filter(date__lte=end.datetime)
        return summary.allow_filtering().all()

    def __parse_vehicle_statistic(self, summary):
        how_many = 0
        if summary is not None:
            how_many = summary.count()

        total = 0.0
        for s in summary:
            total += float(s.dispatch_amount)

        average_receive = 0.0
        if how_many > 0:
            average_receive = float(total) / float(how_many)

        final_data = {
            'total': total,
            'quantity': how_many,
            'average': average_receive
        }
        return final_data

    def __parse_client_orders_statistic(self, orders):
        if orders is None:
            orders = []
            how_many = 0
        else:
            how_many = orders.count()

        how_many_products = 0
        total = 0.0
        paid_from = {}
        products = {}

        for o in orders:

            total += float(o.subtotal)

            payment_type = o.payment.type
            if payment_type not in paid_from:
                paid_from[payment_type] = 0
            paid_from[payment_type] += 1

            for product in o.products:
                how_many_products += product.quantity

                if product.name not in products:
                    products[product.name] = product.quantity
                products[product.name] += product.quantity

        all_payment_type = []

        average_paid = 0.0
        if how_many > 0:
            average_paid = float(total) / float(how_many)

        for paid in paid_from:
            total_orders = paid_from[paid]
            append_data = {
                'name': paid,
                'quantity': total_orders,
                'percentage': float(total_orders) / float(how_many)
            }
            all_payment_type.append(append_data)
        all_payment_type = sorted(all_payment_type, key=itemgetter('quantity'), reverse=True)

        all_product_data = []

        for product in products:
            total_product = products[product]
            append_data = {
                'name': product,
                'quantity': total_product,
                'percentage': float(total_product) / float(how_many_products)
            }
            all_product_data.append(append_data)
        all_product_data = sorted(all_product_data, key=itemgetter('quantity'), reverse=True)

        # TODO: GET TAX AMOUNT FROM CONFIG
        tax = float(10) / 100.0
        without_tax = 1.0 - tax

        final_data = {
            'quantity': how_many,
            'total': float(total),
            'total_without_tax': float(total) * without_tax,
            'average_payment': average_paid,
            'paid_from': all_payment_type,
            'products': all_product_data
        }

        return final_data

    def __format_ranges(self, date):
        ranges = {
            'day': {
                'working': {
                    'start': date,
                    'end': date
                },
                'previous': {
                    'start': date.replace(days=-1),
                    'end': date.replace(days=-1)
                },
            },
            'week': {
                'working': {
                    'start': date.replace(days=-date.weekday()),
                    'end': date.replace(days=+(6 - date.weekday()))
                },
                'previous': {
                    'start': date.replace(days=-(date.weekday() + 7)),
                    'end': date.replace(days=date.weekday() + 1),
                }
            },
            'month': {
                'working': {
                    'start': date.replace(day=1),
                    'end': date.replace(day=1, months=+1, days=-1)
                },
                'previous': {
                    'start': date.replace(day=1, months=-1),
                    'end': date.replace(day=1, days=-1)
                },
            },
            'year': {
                'working': {
                    'start': date.replace(day=1, month=1),
                    'end': date.replace(day=1, month=1, years=+1, days=-1)
                },
                'previous': {
                    'start': date.replace(day=1, month=1, years=-1),
                    'end': date.replace(day=1, month=1, days=-1)
                }
            }
        }
        return ranges
