import logging

from wheezy.core.descriptors import attribute

from Classes.Core.APIHandler import APIHandler
from Classes.Decorators import logged
from Models.Commerce import Commerce
from Models.Subsidiary import Subsidiary
from Models.User import User


class DxlServices(APIHandler):
    def start(self):
        self.repository([
            User,
            Commerce,
            Subsidiary,

        ])
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['dxd_services']

    def get(self):
        pass

    @logged()
    def post(self):
        extra = self.route('extra')
        if extra == 'create_commerce':
            return self.__create_commerce()


    # Methods POST
    def __create_commerce(self):
        self.logger.error("__create_commerce")

        # Get user
        user = self.user.find(id=self.logged['user'])

        # Validate user if not have commerce
        user_with_commerce = self.commerce.get_commerce_for_user(user.id)
        if user_with_commerce is not None:
            self.logger.error('User already have commerce')
            return self.error('User already have commerce')

        # Get vars
        name_for_all = user.fullname

        nin = self.param('nin')
        if nin is None:
            if user.nin is not None:
                nin = user.nin
            else:
                return self.error('Nin es none')

        country_id = self.param('country_id')
        if country_id is None:
            return self.error('Country is none')

        area_id = self.param('area_id')
        if area_id is None:
            return self.error('area_id is none')

        address = self.param('address')
        if address is None:
            return self.error('address is none')

        coordinates = self.param('coordinates')
        if coordinates is None:
            return self.error('coordinates is none')

        telephone = self.param('telephone')
        if telephone is None:
            if user.telephone is not None:
                telephone = user.telephone
            else:
                return self.error('telephone is none')

        trade = self.param('trade')
        if trade is None:
            trade = 'Personal'

        # Init model commerce and set values
        commerce = Commerce()
        commerce.system_id = 1
        commerce.user_id = user.id
        commerce.nin = nin
        commerce.trade = trade
        commerce.country_id = country_id
        commerce.name = name_for_all
        commerce.business_name = 'Personal {0}'.format(name_for_all)
        commerce.address = address
        commerce.telephone = telephone
        commerce.is_person = True
        # Save commerce
        try:
            self.commerce.save(commerce)
        except Exception as e:
            self.logger.error(e.message)
            return self.error(e.message)

        # Init model subsidiary and set values
        subsidiary = Subsidiary()
        subsidiary.commerce_id = commerce.id
        subsidiary.alias = name_for_all
        subsidiary.address = address
        subsidiary.telephone = telephone
        subsidiary.area_id = area_id
        subsidiary.pap = 0
        subsidiary.dxl_overprice = 0
        subsidiary.dxl = True
        subsidiary.coordinates = coordinates

        # Save subsidiary
        try:
            self.subsidiary.save(subsidiary)
        except Exception as e:
            self.logger.error(e.message)
            return self.error(e.message)

        return self.success(dict(commerce_id=commerce.id, subsidiary_id=subsidiary.id))