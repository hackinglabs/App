import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models import Contact


class ContactHandler(APIHandler):
    def start(self):
        self.repository(Contact)
        self.logger = logging.getLogger(__name__)

    @ajax
    @logged('system')
    def get(self):
        """
        GET HTTP Method

        Return a list of countries or details of a specific country
        :return: JSON response
        """

        contact_type = self.param("type")
        self.logger.warning("Contact type: %s", contact_type)

        try:
            if contact_type == "client":
                response = self.contact.get_clients()
            else:
                response = self.contact.get_dispatchers()
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.to_dict(response))

    def post(self):
        contact = self.model_process(Contact())
        if contact is None:
            return self.error()

        return self.result(self.contact.save(contact))

