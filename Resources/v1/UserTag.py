# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators.Ajax import ajax
from Models.UserTag import UserTag


class UserTagHandler(APIHandler):
    def start(self):
        self.repository(UserTag)
        self.logger = logging.getLogger(__name__)



    @ajax
    def get(self):
        """
        GET HTTP Method

        :return: JSON response
        """

        self.logger.info("Request to UserTag")

        # Get user||tag ID from Route
        utag_id = self.route("id")

        # get parameter user for return all usertags of user
        user = self.route('user')

        try:
            if user is not None:
                response = self.usertag.get_all_user_tag(utag_id)
            else:
                response = self.usertag.find(id=utag_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.usertag.to_dict(response))