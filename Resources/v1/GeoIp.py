from Classes.Core.APIHandler import APIHandler
from Classes.Timezones import Timezone


class GeoIpHandler(APIHandler):

    def get(self):
        # Init ip
        ip = None

        try:
            if 'HTTP_CF_CONNECTING_IP' in self.request.environ:
                ip = self.request.environ["HTTP_CF_CONNECTING_IP"]
            elif 'HTTP_X_FORWARDED_FOR' in self.request.environ:
                ip = self.request.environ['HTTP_X_FORWARDED_FOR']
        except Exception as e:
            self.logger.error(e.message)
            return self.success("Ok")

        tz = Timezone(ip)

        return self.success({
            'id': tz.get_id(),
            'gmt': tz.get_gmt()
        })
