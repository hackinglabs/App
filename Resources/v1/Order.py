# -*- coding: utf-8 -*-
import json
import logging
import uuid
import math
import time
import googlemaps
import math
import requests
from sqlalchemy import func
from wheezy.core.descriptors import attribute
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Core.JsonCustomEncoder import JsonCustomEncoder
from Classes.Decorators import logged
from Classes.db import ConnectCassandra
from Configs import Config
from Helpers import Security
from Models import OptionGroup, DispatchAddress, Ingredient, Product, Subsidiary, User, \
    DriverVehicle, OptionCompound, Compound, DispatcherOrder
from Models.Coupon import Coupon
from Models.Cassandra import Address
from Models.Cassandra import Compound as CCompound
from Models.Cassandra.CouponUser import CouponUser
from Models.Cassandra.Coupon import Coupon as CCoupon
from Models.Cassandra import Ingredient as CIngredient
from Models.Cassandra import Order
from Models.Cassandra import OrdersUser
from Models.Cassandra import Point
from Models.Cassandra import Product as CProduct
from Models.Cassandra import Subsidiary as CSubsidiary
from Models.Cassandra import User as CUser
from Models.Cassandra.Discount import Discount
from Models.Cassandra.ClientOrders import ClientOrders
from Models.Cassandra.Commerce import Commerce
from Models.Cassandra.DispatcherSummary import DispatcherSummary
from Models.Cassandra.OrderNotAlliance import OrderNotAlliance
from Models.Hash import Hash
from Models.UserTelephone import UserTelephone
from Models.Discount import Discount as PDiscount
from Models import Redis
import arrow


class OrderHandler(APIHandler):
    def start(self):
        self.repository([
            DispatchAddress,
            DispatcherOrder,
            Subsidiary,
            Product,
            Ingredient,
            User,
            OptionGroup,
            DriverVehicle,
            Compound,
            OptionCompound,
            Coupon,
            PDiscount,
            Hash,
            "Order"
        ])
        self.logger = logging.getLogger(__name__)
        ConnectCassandra()

    @attribute
    def translation(self):
        return self.translations['order']

    @logged()
    def get(self):
        """
        GET HTTP Method

        Route:
            id: UUID from order
            extra:
                dispatch_amount: Calculate dispatch amount between address and subsidiary

        Params:
            subsidiary_id: ID of the subsidiary
            address_id: ID of the user address


        If ID is specified in route but not extra, it brings you the order detail from logged user

        :return:
        """
        extra = self.route('extra')
        uuid = self.route('id')

        if uuid is None and extra == 'dispatch_amount':
            subsidiary_id = self.param('subsidiary_id')
            address_id = self.param('address_id')
            lat = self.param("lat")
            lng = self.param("lng")

            # Calculate the dispatch amount
            mount, distance = self.__calculate_dispatch_amount(subsidiary_id, address_id, lat, lng)

            if mount is not None:
                return self.success({"dispatch_amount": mount})
            else:
                # TODO: Explain the error
                return self.error(self._("error_calculating_dispatch_amount"))

        elif uuid is None and extra == 'dispatch_amount_and_distance':

            subsidiary_id = self.param('subsidiary_id')
            address_id = self.param('address_id')
            lat = self.param("lat")
            lng = self.param("lng")

            # Calculate the dispatch amount
            mount, distance, distanceValue, overprice = self.__calculate_dispatch_amount_and_distance(subsidiary_id,
                                                                                                      address_id, lat,
                                                                                                      lng)

            if mount is not None and distance is not None:
                return self.success({"dispatch_amount": mount,
                                     "dispatch_distance": distance,
                                     "distance_value": distanceValue,
                                     "subsidiary_overprice": overprice})
            else:
                # TODO: Explain the error
                return self.error(self._("error_calculating_dispatch_amount"))

        elif uuid is None and extra == 'get_dxl':
            hash_order = self.hash.find(id_temp=self.logged['user'])

            order = Order.objects(id=hash_order.order_id).first()

            if order.dxl and order.user.id == self.logged['user']:
                return self.success(order, custom=True)

        elif uuid is not None and extra is None:
            if self.permissions().has_perm("system"):
                try:
                    order = Order.objects(id=uuid).first()
                    return self.success(order, custom=True)
                except Exception as e:
                    self.logger.error("Error in get Order system : " + e.message)
                    return self.error(self._("error_obtaining_order"))
            else:
                try:
                    order = self.order.user_get(uuid, self.logged['user'])
                    response = {
                        "id": order.id,
                        "correlative": order.correlative,
                        "subtotal_with_discount": order.subtotal_with_discount,
                        "subtotal_without_discounts": order.subtotal_without_discounts,
                        "discounts": order.discounts,
                        "coupon": order.coupon,
                        "user_id": order.user_id,
                        "order_date": order.order_date,
                        "products": order.products,
                        "subsidiary": order.subsidiary,
                        "total": order.total,
                        "subtotal": order.subtotal,
                        "address_order": order.address_order,
                        "status": order.status,
                        "user_code": order.user_code,
                        "dispatch_amount": order.dispatch_amount,
                        "reference": order.reference,
                        "order_paid": order.order_paid,
                        "order_ready": order.order_ready,
                        "order_confirmation": order.order_confirmation,
                        "dispatch_date": order.dispatch_date,
                        "delivery_date": order.delivery_date,
                        "is_mobile": order.is_mobile,
                        "pickup": order.pickup
                    }

                    discounts = 0
                    if order.subtotal_with_discount is not None:
                        discounts = order.subtotal - order.subtotal_with_discount
                    response['discounts'] = discounts

                    return self.success(response, custom=True)
                except NoneResult:
                    return self.error(self._("order_doesnt_belong"))
        elif uuid is not None and extra == 'get_dxl':
            order = Order.objects(id=uuid).first()

            if order is None:
                return self.error("Order is None")

            if order.dxl and order.user.id == self.logged['user']:
                return self.success(order, custom=True)

    @logged()
    def post(self):
        extra = self.route('extra')
        if extra == 'client_code':
            return self.__client_code()
        elif extra == 'user_code':
            return self.__user_code()
        elif extra == "order_canceled":
            return self.order_canceled()
        elif extra == "dxl":
            return self.process_order_dxl()
        else:

            if self.is_production:
                now = arrow.now("America/Santiago")
                min_hour = arrow.now("America/Santiago").replace(hour=12, minute=0, second=0)
                max_hour = arrow.now("America/Santiago").replace(hour=23, minute=0, second=0)

                if now < min_hour or now > max_hour:
                    return self.error(
                        "Lo sentimos, solo puedes realizar pedidos desde las 12:00 hrs. hasta las 23:00 hrs.")

            return self.__process_order()

    def __calculate_dispatch_amount(self, subsidiary_id, address_id, lat=None, lng=None):

        if subsidiary_id is not None:

            # get subsidiary
            try:
                # Check if that address belong to that user
                subsidiary = self.subsidiary.find(id=subsidiary_id)
            except NoneResult:
                return None

            # get coordinates subsidiary
            subsidiary_geo = subsidiary.to_geo()

            if address_id is not None:
                # get address
                address = self.dispatchaddress.find(id=address_id, user_id=self.logged['user'])
                address_geo = address.to_geo()
                address_cor = {"lat": address_geo.lat, "lon": address_geo.lon}
            else:
                address_cor = {"lat": lat, "lon": lng}

            c = Config().gcm()
            secret = c['key']
            gmaps = googlemaps.Client(key=secret)

            directions_result = gmaps.directions(
                "{0} {1}".format(subsidiary_geo.lat, subsidiary_geo.lon),
                "{0} {1}".format(address_cor['lat'], address_cor['lon']),
                mode="driving",
                avoid=["tolls"],
                units="metric")

            try:
                distance = directions_result[0]['legs'][0]['distance']['value']
            except Exception as e:
                self.logger.error('Exception line 249 error: ' + e.message)
                if address_id is not None:
                    distance = self._con.scalar(func.ST_Distance_Sphere(address.coordinates, subsidiary.coordinates))
                else:
                    return None

            realDistanceValue = distance

            # Transform distance every 100 mts
            distance /= 100
            distance = round(distance, 1)

            # TODO: This must be configurable
            min_kms = 4
            min_price = 2000
            mts_price = 90
            amount = min_price

            min_hundred_meters = min_kms * 10
            if distance - min_hundred_meters > 0:
                amount += (distance - min_hundred_meters) * mts_price

            return amount, realDistanceValue
        else:
            return None

    def __calculate_dispatch_amount_and_distance(self, subsidiary_id, address_id, lat=None, lng=None):

        if subsidiary_id is not None:

            # get subsidiary
            try:
                # Check if that address belong to that user
                subsidiary = self.subsidiary.find(id=subsidiary_id)
            except NoneResult:
                return None

            # get coordinates subsidiary
            subsidiary_geo = subsidiary.to_geo()

            if address_id is not None:
                # get address
                address = self.dispatchaddress.find(id=address_id, user_id=self.logged['user'])
                address_geo = address.to_geo()
                address_cor = {"lat": address_geo.lat, "lon": address_geo.lon}
            else:
                address_cor = {"lat": lat, "lon": lng}

            c = Config().gcm()
            secret = c['key']
            gmaps = googlemaps.Client(key=secret)

            directions_result = gmaps.directions(
                "{0} {1}".format(subsidiary_geo.lat, subsidiary_geo.lon),
                "{0} {1}".format(address_cor['lat'], address_cor['lon']),
                mode="driving",
                avoid=["tolls"],
                units="metric")

            try:
                distance = directions_result[0]['legs'][0]['distance']['value']
                realDistanceText = directions_result[0]['legs'][0]['distance']['text']
            except Exception as e:
                self.logger.error("Exception line 316 error: " + e.message)
                if address_id is not None:
                    distance = self._con.scalar(func.ST_Distance_Sphere(address.coordinates, subsidiary.coordinates))
                else:
                    return None

            # Subsidiary overprice ?
            overprice = False
            if subsidiary.dxl_overprice is not None and subsidiary.dxl_overprice > 0:
                overprice = True

            # set realDistanceValue
            realDistanceValue = distance

            # Transform distance every 100 mts
            distance /= 100
            distance = round(distance, 1)

            # TODO: This must be configurable
            min_kms = 4
            min_price = 2000
            mts_price = 90
            amount = min_price

            min_hundred_meters = min_kms * 10
            if distance - min_hundred_meters > 0:
                amount += (distance - min_hundred_meters) * mts_price

            return amount, realDistanceText, realDistanceValue, overprice
        else:
            return None, None

    def __process_order(self):

        order = self.param("order")

        if isinstance(order, basestring):
            try:
                order = json.loads(order)
            except:
                # TODO: BLoquear usuario
                return self.error(self._("invalid_json_order"))

        if not isinstance(order, dict):
            # order MUST be a dict
            # TODO: Bloquear usuario
            return self.error(self._("invalid_json_order"))

        total_order = 0

        # TODO: Validar keys (frozenset difference)
        keys = frozenset([u'dispatch_amount', u'products', u'subsidiary', u'total'])
        order_keys = frozenset(order.keys())

        reference = ""
        if 'reference' in order:
            reference = order['reference']

        is_mobile = False

        if 'is_mobile' in order and order['is_mobile'] == True:
            is_mobile = True

        if len(order['products']) == 0:
            return self.error(self._("order_without_products"))

        pickup = False

        if 'pickup' in order:
            pickup = order['pickup']

        if 'telephone' in order:
            if order['telephone'] is not None:
                if isinstance(order['telephone'], str):
                    try:
                        order['telephone'] = int(order['telephone'])
                    except:
                        self.error(self._('telephone_not_numeric'))

                telephone = order['telephone']
                # TODO: save telephone in table users_telephones
                model = UserTelephone(user_id=self.logged['user'], telephone=order['telephone'])
                save = self.user.save(model)
                self.logger.warning(save)
            else:
                telephone = order['telephone']
        else:
            telephone = None

        self.logger.debug("Order keys: %s", order_keys)
        # Check if the order json has the required keys
        if not keys.issubset(order_keys):
            # TODO: Bloquear Usuario
            return self.error(self._("invalid_keys"))

        # Check if subKeys has a required id
        if pickup is False and 'address_order' not in order:
            return self.error(self._("invalid_keys"))

        if (pickup is False and "id" not in order["address_order"]) or "id" not in order["subsidiary"]:
            # TODO: Bloquear Usuario
            return self.error(self._("invalid_keys"))

        # Check if that required id are integers
        try:
            if pickup is False:
                address_id = int(order["address_order"]["id"])
            subsidiary_id = int(order["subsidiary"]["id"])
        except Exception as e:
            # TODO: Bloquear Usuario
            self.logger.error(e.message)
            return self.error(self._("invalid_keys"))

        # Check if the user has that address
        if pickup is False:
            try:
                address = self.dispatchaddress.filter_by_user(address_id, self.logged['user'])
            except NoneResult:
                # TODO: Bloquear Usuario
                return self.error(self._("address_doesnt_belong"))

            # if address.area_id not in [5, 1, 6, 4, 32, 43]:
            if self.is_production and address.area_id not in [43]:
                return self.error(
                    "Estimados clientes, estamos realizando mejoras en nuestra plataforma para ofrecerles una mejor y "
                    "mas rápida experiencia de compra, próximamente volveremos recargados y con muchas sorpresas. "
                    "Discúlpenos por las molestias. Equipo Dxpress")
        else:
            address = None

        # Check if subsidiary exists
        try:
            subsidiary = self.subsidiary.find(id=subsidiary_id)
            # subsidiary_active = self.subsidiary.is_active(subsidiary_id)
        except NoneResult:
            # No bloqueamos el usuario
            return self.error(self._("subsidiary_not_exists"))

        # Check if the user make an order in a valid area
        # if subsidiary.area_id not in [5, 1, 6, 4, 32, 43]:
        if self.is_production and subsidiary.area_id not in [43]:
            return self.error(
                "Estimados clientes, estamos realizando mejoras en nuestra plataforma para ofrecerles una mejor y "
                "mas rápida experiencia de compra, próximamente volveremos recargados y con muchas sorpresas. "
                "Discúlpenos por las molestias. Equipo Dxpress")

            # Verify
        if not subsidiary.commerce.alliance and pickup:
            return self.error("Commerce not alliance, no have pickup")

        # Check coupon
        coupon = None
        if 'coupon' in order and order['coupon'] is not None:
            code = order['coupon'].upper()

            where_to_check = address

            if pickup is True:
                where_to_check = None

            ok, error_msg, coupon = self.coupon.check_and_use(self.logged['user'], code, subsidiary,
                                                              where_to_check, pickup)

            if not ok:
                return self.error(error_msg)
            else:
                self.logger.error(coupon)

        # Subsidiary in redis

        subsidiary_redis = Redis.Subsidiary.query.filter(id=subsidiary_id).first()

        # This list will have every product to final order
        products_list = []

        products_to_iterate = []
        for p in order['products']:
            if p in products_to_iterate:
                index = products_to_iterate.index(p)
                products_to_iterate[index]['quantity'] += 1
            else:
                products_to_iterate.append(p)

        for product_json in products_to_iterate:
            # Check if product exists
            try:
                product = self.product.find(id=product_json['id'])
            except NoneResult:
                return self.error(self._("product_doesnt_exists"))

            # Allowed compounds and product price
            product_price = product.price

            compound_list = []
            # Check if we have compound products
            if 'compounds' in product_json and len(product_json['compounds']) > 0:
                # This list will have every compound product to final order
                for compound_product in product_json['compounds']:
                    # Check if compound product has the required keys
                    if "option_compound_id" not in compound_product and "compound_id" not in compound_product:
                        # TODO: Bloquear Usuario
                        return self.error(self._("compound_without_default"))

                    # search option_compound
                    try:
                        option = self.optioncompound.find(id=compound_product['option_compound_id'])
                    except NoneResult:
                        # TODO: Bloquar usuario
                        return self.error(self._("option_undefined"))

                    # Check option_compound it belongs product
                    if option.product_id != product.id:
                        # TODO: Bloquear Usuario
                        return self.error(self._("option_not_from_product"))

                    # ids compounds for option_compound
                    ids_compounds = self.optioncompound.get_compounds(option.id)

                    # Check if the value compound in ids_compounds or compound_id default
                    if 'compound_id' not in compound_product:
                        return self.error(self._("compounds_mal-formatted."))

                    if compound_product['compound_id'] == option.compound_id:
                        compound = self.compound.find(id=compound_product['compound_id'])
                    elif compound_product['compound_id'] in ids_compounds:
                        try:
                            compound = self.compound.find(id=compound_product['compound_id'])
                        except NoneResult:
                            # TODO: bloquear usuario
                            return self.error(self._("compound_not_well_defined"))

                        if compound.price > option.compound.price:
                            # Modify price product
                            dif = compound.price - option.compound.price
                            product_price += dif
                        else:
                            # el valor debe quedar igual
                            pass
                    else:
                        return self.error(self._("compound_not_from_option"))

                    compound = {
                        'id': compound.id,
                        'name': compound.name,
                        'price': compound.price,
                        'description': '',
                        'clarification': '',
                        'option_compound_id': option.id
                    }
                    compound = CCompound(**compound)
                    compound_list.append(compound)

            ingredient_list = []
            if 'ingredients' in product_json and len(product_json['ingredients']) > 0:

                ingredients_id = [p['id'] for p in product_json['ingredients']]

                # Get ingredient objects and options groups
                try:
                    ingredients = self.ingredient.get_in(ingredients_id)
                    result = self.optiongroup.ids_and_bases(product_json['id'])
                except NoneResult:
                    # TODO: Bloquear usuario
                    return self.error(self._("ingredient_options_not_found"))

                base_validation = {
                    'base': 0,
                    'options': [],
                    'groups': {}
                }

                for i in result:
                    base_validation['options'].append(i[0])
                    base_validation['groups'][i[3]] = {"quantity": i[4], "is_base": i[1], "is_premium": i[2]}
                    if i[1] is True:
                        base_validation['base'] += 1
                        base_validation[i[0]] = 1

                ingredients_selected = 0
                for ingredient in ingredients:
                    subcategory_of = ingredient.ingredient_per_category_id

                    if ingredient.ingredient_per_category.subsidiary_id != subsidiary.id:
                        # TODO: Bloquear Usuario
                        return self.error(self._("ingredient_not_from_subsidiary"))

                    if subcategory_of not in base_validation['options']:
                        # TODO: Bloquear Usuario
                        return self.error(self._("ingredient_not_from_option_group"))

                    options = self.ingredient.get_options(ingredient.id, product.id)

                    for option in options:
                        if option in base_validation['groups']:
                            if base_validation['groups'][option]["is_base"] == False and \
                                            base_validation['groups'][option]['is_premium'] == False:
                                self.logger.error("Is Normal")
                                base_validation['groups'][option]["quantity"] -= 1

                                if base_validation['groups'][option]["quantity"] < 0:
                                    return self.error(self._("ingredient_exceeded"))

                    quantity = ingredients_id.count(ingredient.id)
                    if ingredient.ingredient_per_category.is_base:

                        base_validation['base'] -= 1
                        if base_validation['base'] < 0:
                            # TODO: Banear usuario
                            return self.error(self._("base_ingredient_exceeded"))

                        base_validation[subcategory_of] -= 1
                        if base_validation[subcategory_of] < 0:
                            # TODO: Bloquear Usuario
                            return self.error(self._("already_selected_base_ingredient"))
                    elif ingredient.ingredient_per_category.is_premium:
                        ingredients_selected += quantity
                        product_price += ingredient.price * quantity
                    else:
                        ingredients_selected += quantity
                        # base_validation['non_base'] -= quantity
                        # if base_validation['non_base'] < 0:
                        #     TODO: Bloquear Usuario
                        #    return self.error(msg="Ya elegiste la cantidad maxima de ingredientes")

                    for i in range(quantity):
                        temp_ingredient = {
                            'id': ingredient.id,
                            'name': ingredient.name,
                            'price': ingredient.price,
                            'ingredients_per_category_id': ingredient.ingredient_per_category_id,
                            'is_base': ingredient.ingredient_per_category.is_base
                        }
                        ingredient_cas = CIngredient(**temp_ingredient)
                        ingredient_list.append(ingredient_cas)

                if base_validation['base'] != 0:
                    # TODO: Bloquear usuario
                    return self.error(self._("base_ingredient_doesnt_match"))

                self.logger.error("Ingredientes seleccionados: %s", ingredients_selected)

                # if product.ingredients_extra <= 0 and ingredients_selected != 0:
                #    return self.error("El producto no tiene ingredientes extras que seleccionar.")
                # elif product.ingredients_extra > 0 and ingredients_selected <= 0:
                #    return self.error(msg="Debes seleccionar al menos un ingrediente")

            quantity = 1
            if "quantity" in product_json:
                try:
                    quantity = int(product_json['quantity'])
                except:
                    # TODO: Bloquear Usuario
                    return self.error(self._("quantity_must_be_number"))
            else:
                self.logger.error("Not Have Quantity in OrderProcess")

            # Create product object
            try:
                product_dict = {
                    'id': product.id,
                    'name': product.name,
                    'price': product_price,
                    'quantity': quantity,
                    'clarification': '',
                    'ingredients_extra': product.ingredients_extra,
                    'subcategory_id': product.sub_category_id,
                    'compounds': compound_list,
                    'description': product.description,
                    'ingredients': ingredient_list
                }
                new_product = CProduct(**product_dict)
            except Exception as e:
                self.logger.error("Error in create ObjectProduct: " + e.message)
                return self.error("Product Error")

            if new_product is not None:
                products_list.append(new_product)
                total_order += new_product.price * new_product.quantity
            else:
                self.logger.error("[%s] %s", self.logged['user'], str(product))
                return self.error("Product Error")

        correlative = self.subsidiary.increment_correlative(subsidiary.id)

        subsidiary_dict = {
            'id': subsidiary.id,
            'overprice_percentage': subsidiary.overprice_percentage,
            'fullname': '{0} {1}'.format(subsidiary.commerce.name.encode('utf-8'), subsidiary.alias.encode('utf-8')),
            'commerce_id': subsidiary.commerce_id,
            'alias': subsidiary.alias,
            'telephone': subsidiary.telephone,
            'commerce': Commerce(id=subsidiary.commerce.id,
                                 logo=subsidiary.commerce.logo,
                                 name=subsidiary.commerce.name,
                                 alliance=subsidiary.commerce.alliance,
                                 franchise=subsidiary.commerce.franchise)
        }
        cassandra_subsidiary = CSubsidiary(**subsidiary_dict)

        if pickup is False:
            # Get address coordinates
            coordinates = address.to_xy()
            cassandra_address = Address(
                name=address.name,
                address="{0}, {1}".format(address.address, address.area.name),
                reference=address.reference,
                apartment=address.apartament,
                block=address.block,
                area_id=address.area_id,
                geo=Point(x=coordinates.x, y=coordinates.y),
                id=address.id,
                telephone=address.telephone
            )
        else:
            cassandra_address = None

        try:
            user = self.user.find(id=self.logged['user'])
        except NoneResult:
            return self.error(self._("User doesn't found"))

        if telephone is None:
            telephone = address.telephone
        user_dict = {
            'id': user.id,
            'name': u"{0} {1}".format(user.first_name, user.last_name),
            'email': user.email,
            'telephone': telephone
        }
        cassandra_user = CUser(**user_dict)

        distance = 0
        if pickup is False:
            dispatch_amount, distance = self.__calculate_dispatch_amount(subsidiary.id, address.id)

            if dispatch_amount is None:
                return self.error(self._("cant_calculate_dispatch_amount"))
        else:
            dispatch_amount = 0

        correlative = str(subsidiary.commerce_id) + str(subsidiary.id) + str(correlative)

        # coupon
        if coupon is not None:
            if coupon.amount > total_order:
                return self.error(self._("cant_be_>_buy"))

            coupon_dict = {
                "id": coupon.id,
                "code": coupon.code,
                "with_percentage": coupon.with_percentage,
                "coupon_type": coupon.coupon_type,
                "type_id": coupon.type_id,
                "amount": coupon.amount
            }
            cassandra_coupon = CCoupon(**coupon_dict)
        else:
            cassandra_coupon = None

        cassandra_discounts = []
        amount_with_their_discount = total_order
        amount_with_discount = total_order

        d_products = []

        d_products_list = []
        for p in products_list:
            d_products_list.append(p['id'])

        if d_products_list:
            d_products = self.product.get_products_by_list(d_products_list)

        # Apply the coupon with or without an existing discount
        if coupon is not None:

            if coupon.with_percentage:
                amount_with_discount = total_order * (1 - coupon.amount)

            else:
                amount_with_discount = total_order - coupon.amount

        else:
            our_discount, subsidiary_discount = self.discount.check_subsidiary_discounts(subsidiary,
                                                                                         address,
                                                                                         d_products,
                                                                                         total_order)

            if subsidiary_discount and total_order >= subsidiary_discount.minimum_buy:
                amount_with_their_discount = total_order * (1 - subsidiary_discount.amount)

            total_discount = None
            if our_discount and subsidiary_discount \
                    and total_order >= our_discount.minimum_buy \
                    and total_order >= subsidiary_discount.minimum_buy:
                total_discount = our_discount.amount + subsidiary_discount.amount
                cassandra_discounts = [
                    Discount(id=our_discount.id,
                             our_discount=True,
                             discount_type=our_discount.discount_type,
                             type_id=our_discount.type_id,
                             amount=our_discount.amount),
                    Discount(id=subsidiary_discount.id,
                             our_discount=False,
                             discount_type=subsidiary_discount.discount_type,
                             type_id=subsidiary_discount.type_id,
                             amount=subsidiary_discount.amount),
                ]
            elif our_discount and total_order >= our_discount.minimum_buy:
                total_discount = our_discount.amount
                cassandra_discounts = [
                    Discount(id=our_discount.id,
                             our_discount=True,
                             discount_type=our_discount.discount_type,
                             type_id=our_discount.type_id,
                             amount=our_discount.amount)
                ]
            elif subsidiary_discount and total_order >= subsidiary_discount.minimum_buy:
                total_discount = subsidiary_discount.amount
                cassandra_discounts = [
                    Discount(id=subsidiary_discount.id,
                             our_discount=False,
                             discount_type=subsidiary_discount.discount_type,
                             type_id=subsidiary_discount.type_id,
                             amount=subsidiary_discount.amount),
                ]

            if total_discount is not None:
                amount_with_discount = total_order * (1 - total_discount)

        order_dict = {
            'id': str(uuid.uuid1()),
            'correlative': correlative,
            'order_date': arrow.now('America/Santiago').to('utc').datetime,
            'products': products_list,
            'subsidiary': cassandra_subsidiary,
            'total': math.ceil(round(amount_with_discount + dispatch_amount, 2)),
            'subtotal': math.ceil(round(amount_with_their_discount, 2)),
            'subtotal_with_discount': math.ceil(round(amount_with_discount, 2)),
            'subtotal_without_discounts': math.ceil(round(total_order, 2)),
            'dispatch_amount': dispatch_amount,
            'user': cassandra_user,
            'coupon': cassandra_coupon,
            'discounts': cassandra_discounts,
            'address_order': cassandra_address,
            'status': 'SEND-CLIENT',
            'client_code': Security.generate_random(5, type='digit'),
            'user_code': Security.generate_random(5, type='digit'),
            'reference': reference,
            'pickup': pickup,
            'is_mobile': is_mobile,
            'dxl': False,
            'distance': distance

        }
        order = Order(**order_dict)

        order_dict['user_id'] = self.logged['user']
        order_user = OrdersUser(**order_dict)

        if coupon is not None:
            coupon_data = {
                'order_id': order.id,
                'code': coupon.code,
                'user_id': order.user.id,
                'coupon_id': coupon.id,
                'coupon': coupon
            }
            coupon_user = CouponUser(**coupon_data)
            try:
                coupon_user.save()
            except Exception as e:
                self.logger.error("Not save coupon_user error: " + e.message)

        try:
            order.save()
            order_user.save()
        except Exception as e:
            self.logger.error(e.message)
            return self.error(self._("error_order_dont_saved"))

        total = order.subtotal

        if order.subsidiary.commerce.alliance:

            data = {
                # Debe ir los datos de la orden
                "id": str(order.id),
                "correlative": order.correlative,
                "products": products_list,
                "subtotal": total,
                "order_date": order.order_date,
                "reference": order.reference,
                "pickup": order.pickup
            }

            data = json.dumps(data, cls=JsonCustomEncoder)

            queue_name = 'subsidiary_' + str(subsidiary.id) + '_orders'

            if not self.rabbit.publish(queue_name, data):
                return self.error(self._("cant_send_order"))

            redis_key = "subsidiary_" + str(subsidiary.id) + "_orders_count"

            # Start to count orders quantity
            ok = self.redis().exists(redis_key)

            if ok:
                self.redis().incr(redis_key, amount=1)
            else:
                self.redis().set(redis_key, 1)

            if subsidiary_redis is None:
                self.logger.error("Subsidiary NONE")
            else:

                data_aps = {
                    "title": "Dxpress",
                    "message": "Order Received",
                    "status": 1,
                    "location": data
                }

                notification = {
                    "title": "Dxpress",
                    "body": "Order Received",
                    "sound": "default",
                    "icon": "myicon",
                    "content_available": True
                }

                data_aps = json.dumps(data_aps, cls=JsonCustomEncoder)

                res = subsidiary_redis.send_gcm(type=3, msg=data_aps, notification=notification)
                self.logger.error(res)
        else:
            self.__auto_accept_order(order)

        return self.success(str(order.id))

    def __client_code(self):
        id = self.param('id')
        client_code = self.param('code')

        if id is not None and client_code is not None:
            try:
                client_code = int(client_code)
            except ValueError:
                self.logger.error("Error in convert client_code to int: " + str(client_code))
                return self.error(self._("code_must_be_numeric"))

            order = Order.objects(id=id).first()

            if order is None:
                self.logger.error("Order doesn't exist - '__client_code' ")
                return self.error(self._("order_dont_exist"))

            if order.status == 'DXL-ORDER-IN-DISPATCH':
                return self.__client_code_dxl(order, client_code)

            order_by_user = OrdersUser.objects(id=id, user_id=order.user.id).first()
            client_order = ClientOrders.objects(id=id, subsidiary_id=order.subsidiary.id).first()

            order_client_code = order.client_code

            try:
                order_client_code = int(order.client_code)
            except ValueError:
                self.logger.error("The client code from order is invalid.")
                return self.error(self._("invalid_generated_code"))

            # TODO: Remove this byPass
            if client_code == order_client_code or 1 == 1:
                status = order.status.lower()
                if status != "order-in-dispatch" and status != "order-ready":
                    return self.error('Order: status invalid')

                # verify if order is pickup
                if order.pickup:
                    order.status = "ORDER-FINISH"
                    order.dispatch_date = arrow.now('America/Santiago').to('utc').datetime

                    order_by_user.status = "ORDER-FINISH"
                    order_by_user.dispatch_date = arrow.now('America/Santiago').to('utc').datetime

                    client_order.status = "ORDER-FINISH"

                    subsidiary = self.subsidiary.find(id=order.subsidiary.id)
                    self.send_finish_mail(order.user.email, subsidiary.commerce.slug, subsidiary.id)

                    try:
                        order.update()
                        order_by_user.update()
                        client_order.update()
                    except Exception as e:
                        self.logger.error("Error update order in client_code: " + e.message)
                        return self.error(self._("order_not_updated"))

                    # Emmit client Ok
                    data = {
                        "type": "order-validate",
                        "message": {
                            'message': self._("valid_code"),
                            'order_id': order.id
                        },
                        "subsidiary_id": order.subsidiary.id
                    }
                    self.redis().publish("channel-clients", json.dumps(data, cls=JsonCustomEncoder))

                    # send user for Socket and GCM
                    data_aps = {
                        "title": "Dxpress",
                        "body": self._("thanks_for_using"),
                        "sound": "default",
                        "icon": "default",
                    }

                    data = {
                        "title": "Dxpress",
                        "message": self._("thanks_for_using"),
                        "status": 11,
                        "location": str(order.id)
                    }

                    ##Test Send gcm from UserRepository, give token database
                    notification_id = self.user.save_notification(user_id=order.user.id, data=data, order_id=order.id)
                    data["notification_id"] = notification_id
                    data = json.dumps(data, cls=JsonCustomEncoder)
                    self.user.send_gcm_repo(user_id=order.user.id, type=1, msg=data, notification=data_aps)

                    # Emmit users finish
                    data = {
                        "type": "on-order-finish",
                        "message": {
                            "order_id": str(order.id),
                            "msg": self._("order_finished"),
                            "success": True
                        },
                        "user_id": order.user.id
                    }
                    self.redis().publish("channel-users", json.dumps(data, cls=JsonCustomEncoder))
                    return self.success(self._("order_finished"))
                # end verify order is pickup

                order.status = 'ORDER-TRACK'
                order.dispatch_date = arrow.now('America/Santiago').to('utc').datetime.replace(tzinfo=None)

                order_by_user.status = 'ORDER-TRACK'
                order_by_user.dispatch_date = arrow.now('America/Santiago').to('utc').datetime.replace(tzinfo=None)

                client_order.status = 'ORDER-FINISH'

                try:
                    order.update()
                    order_by_user.update()
                    client_order.update()
                except Exception as e:
                    self.logger.error("Error in update order on client_code: " + e.message)
                    return self.error(self._("order_not_updated"))

                # Emmit client Ok
                data = {
                    "type": "order-validate",
                    "message": {
                        'message': self._("valid_code"),
                        'order_id': order.id
                    },
                    "subsidiary_id": order.subsidiary.id
                }
                self.redis().publish("channel-clients", json.dumps(data, cls=JsonCustomEncoder))

                # Emmit dispacher Ok
                data_dispatcher = {
                    "id": order.id
                }

                queue_name = 'dispatcher_' + str(order.dispatcher.id) + '_confirmation'

                if not self.rabbit.publish(queue_name, json.dumps(data_dispatcher, cls=JsonCustomEncoder)):
                    return self.error(self._("cant_send_order"))

                # Emmit user for socket and gcm
                # dispatch-found
                try:
                    subsidiary = self.subsidiary.find(id=order.subsidiary.id)
                    geo = subsidiary.to_geo()
                    subsidiary_coordinates = {'x': geo.lat, 'y': geo.lon}
                except NoneResult:
                    subsidiary_coordinates = None

                data = {
                    "type": "dispatch-found",
                    "message": {
                        "order_id": str(order.id),
                        "dispatch": {
                            "id": order.dispatcher,
                            "name": order.dispatcher.name,
                            "avatar": order.dispatcher.avatar,
                            "patent": order.dispatcher.patent,
                            "destination": order.address_order.geo,
                            "subsidiary": subsidiary_coordinates
                        },
                        "user_code": order.user_code
                    },
                    "user_id": order.user.id
                }
                self.redis().publish("channel-users", json.dumps(data, cls=JsonCustomEncoder))

                data_aps = {
                    "title": "Dxpress",
                    "body": self._("dispatcher_found"),
                    "sound": "default",
                    "icon": "default",
                }

                data = {
                    "title": "Dxpress",
                    "message": self._("dispatcher_found"),
                    "status": 5,
                    "location": {
                        "id": order.dispatcher.id,
                        "name": order.dispatcher.name,
                        "avatar": order.dispatcher.avatar,
                        "patent": order.dispatcher.patent,
                        "user_code": order.user_code
                    }
                }

                data = json.dumps(data, cls=JsonCustomEncoder)

                ##Test Send gcm from UserRepository, give token database
                self.user.send_gcm_repo(user_id=order.user.id, type=1, msg=data, notification=data_aps)

                return self.success(self._("valid_code"))
            else:
                return self.error(self._("invalid_code"))
        else:
            return self.error(self._("bad_parameters"))

    def __user_code(self):
        order_id = self.param('order_id')
        user_code = self.param('user_code')

        if order_id is not None and user_code is not None:
            # try convert to int
            try:
                user_code = int(user_code)
            except ValueError:
                self.logger.error("Error in convert user_code to int: " + str(user_code))
                return self.error(self._("code_must_be_numeric"))

            order = Order.objects(id=order_id).first()
            if order is None:
                self.logger.error("Order doesn't exist - '__user_code' ")
                return self.error(self._("order_dont_exist"))

            self.logger.error(order.status)
            if order.status == 'DXL-DISPATCH':
                return self.__user_code_dxl(order, user_code)

            order_user = OrdersUser.objects(id=order_id, user_id=order.user.id).first()
            if order_user is None:
                self.logger.error(order.status)
                self.logger.error("OrdersUser doesn't exist - '__user_code' ")
                return self.error(self._("order_dont_exist"))

            try:
                order_user_code = int(order.user_code)
            except ValueError:
                self.logger.error("The user_code from order is invalid.")
                return self.error(self._("invalid_generated_code"))

            # TODO: Remove this byPass
            if user_code == order_user_code or 1 == 1:
                if order.status.upper() == "ORDER-TRACK":
                    status = 'ORDER-FINISH'
                    now = arrow.now('America/Santiago').to('utc').datetime

                    # update order
                    order.status = status
                    order.delivery_date = now

                    order_user.status = status
                    order_user.delivery_date = now

                    # Search dispatcherOrder
                    dispatcher_order = self.dispatcherorder.find(dispatcher_id=order.dispatcher.id, current=True)
                    if dispatcher_order is not None:
                        if dispatcher_order.order_id != str(order.id):
                            self.logger.error("dispatcherOrder not equals order")
                        else:
                            self.dispatcherorder.remove(dispatcher_order)
                    else:
                        self.logger.error("dispatcherOrder is None ???")

                    try:

                        if not order.subsidiary.commerce.alliance:
                            order_not_alliance = OrderNotAlliance.objects(id=order.id,
                                                                          dispatcher_id=order.dispatcher.id).first()
                            order_not_alliance.status = status
                            order_not_alliance.update()

                        order.update()
                        order_user.update()
                    except Exception as e:
                        self.logger.error("Error update order in  user_code: " + e.message)
                        return self.error(self._("order_not_updated"))

                    # search subsidiary
                    subsidiary = self.subsidiary.find(id=order.subsidiary.id)

                    self.send_finish_mail(order.user.email, subsidiary.commerce.slug, subsidiary.id)

                    # Set dispatch_amount
                    real = order.dispatch_amount
                    less = real - 2000
                    meters = less / 90
                    dispatcher_amount = float(2000 + meters * 70)

                    # create dispatcher_summary
                    dispatch_summary = DispatcherSummary(
                        dispatcher_id=order.dispatcher.id,
                        order_id=order.id,
                        dispatch_amount=dispatcher_amount,
                        date=arrow.now('America/Santiago').to('utc').datetime,
                        subsidiary_name=order.subsidiary.fullname,
                        subsidiary_address=subsidiary.address,
                        vehicle_id=order.dispatcher.vehicle_id,
                        alliance=order.subsidiary.commerce.alliance,
                        dispatch_date=order.dispatch_date,
                        delivery_date=order.delivery_date
                    )

                    try:
                        dispatch_summary.save()
                    except Exception as e:
                        self.logger.error("create dispatch_summary error: " + e.message)
                        return self.error('dispatch_summary_not_saved')

                    # restablecer el despachador
                    dispatcher = Redis.User.query.filter(id=order.dispatcher.id).first()

                    if dispatcher is None:
                        dispatcher = self.user.create_dispatcher_in_redis(order.dispatcher.id)

                    if dispatcher is not None:
                        dispatcher.with_order = False
                        dispatcher.order_id = None

                        try:
                            dispatcher.save()
                        except Exception as e:
                            self.logger.error("Error: " + e.message)
                            self.logger.error(self._("cant_reestablish_dispatcher"))

                    # Emmit user Ok
                    # Socket
                    data = {
                        "type": "on-finish",
                        "message": {
                            "msg": self._("thanks_for_using"),
                        },
                        "user_id": order.user.id
                    }
                    self.redis().publish("channel-users", json.dumps(data, cls=JsonCustomEncoder))

                    # GCM
                    data_aps = {
                        "title": "Dxpress",
                        "body": self._("thanks_for_using"),
                        "sound": "default",
                        "icon": "myicon",
                        "content_available": True
                    }

                    data = {
                        "title": "Dxpress",
                        "message": self._("thanks_for_using"),
                        "status": 11,
                        "location": str(order.id)
                    }

                    ##Test Send gcm from UserRepository, give token database
                    notification_id = self.user.save_notification(user_id=order.user.id, data=data, order_id=order.id)
                    data["notification_id"] = notification_id
                    data = json.dumps(data)
                    self.user.send_gcm_repo(user_id=order.user.id, type=1, msg=data, notification=data_aps)

                    return self.success(self._("valid_code"))
                elif order.status.upper() == "ORDER_FINISH":
                    return self.success(order.status.upper())
                else:
                    # TODO: Translate
                    return self.error("invalid order status")
            else:
                return self.error(self._("invalid_code"))
        else:
            return self.error(self._("bad_parameters"))

    def __auto_accept_order(self, order):

        # self.logger.warning(order)
        order_user = OrdersUser.objects(id=order.id, user_id=order.user.id).first()

        if order is None or order_user is None:
            self.logger.error("Order doesn't exist - 'accept_or_rejected' ")
            return self.error("Order: doesnt exists")

        if order.status.upper() != "SEND-CLIENT":
            # TODO: Banear Usuario
            return self.error("Order: invalid status")

        # set datetime order
        order.order_confirmation = arrow.now('America/Santiago').to('utc').datetime
        order_user.order_confirmation = arrow.now('America/Santiago').to('utc').datetime

        order.status = "ORDER-ACCEPTED"
        order_user.status = "ORDER-ACCEPTED"
        reason = ""
        type_status = 2
        status = True
        msg = "Su orden ha sido aceptada, por favor proceda a realizar el pago!"

        # Update Order
        try:
            order.update()
            order_user.update()
        except:
            return self.error('not update order')

        data_aps = {
            "title": "Dxpress",
            "body": msg,
            "icon": "default",
            "sound": "default",
            "priority": "high",
            "content_available": True
        }

        data = {
            "title": "Dxpress",
            "message": msg,
            "status": type_status,
            "location": {"order_id": str(order.id), "reason": reason}
        }

        ##Test Send gcm from UserRepository, give token database
        notification_id = self.user.save_notification(user_id=order.user.id, data=data, order_id=order.id)
        data["notification_id"] = notification_id
        data = json.dumps(data)
        self.user.send_gcm_repo(user_id=order.user.id, type=1, msg=data, notification=data_aps)

        data = {
            "type": "ACCEPT_OR_REJECTED",
            "message": {
                "status": status,
                "order_id": order.id,
                "msg": msg
            },
            "user_id": order.user.id
        }
        self.redis().publish("channel-users", json.dumps(data, cls=JsonCustomEncoder))

    def order_canceled(self):
        order_id = self.param('order_id')
        products = self.param('products')
        closed = self.param("closed")

        if order_id is None:
            return self.error("OrderId Missing")

        if products is None and closed is None:
            self.logger.error("Missing reason for canceled")
            return self.error("Missing reason for canceled")

        order = Order.objects(id=order_id).first()
        order_user = OrdersUser.objects(id=order.id, user_id=order.user.id).first()

        if order is None:
            return self.error("Orders None")

        if products is not None:
            # Change products
            if isinstance(products, dict):
                products = [products]

            for product in products:

                # getValue
                price = product['new_price']
                stock = product['stock']

                to_update = {
                    'stock': stock
                }

                if price is not None and price > 0:
                    over_price = int(math.ceil(round(price * (1 + order.subsidiary.overprice_percentage), 2)))
                    to_update['price'] = over_price
                self._con.begin()
                try:
                    self._con.query(Product) \
                        .filter(Product.id == product['id']) \
                        .update(to_update)

                    self._con.commit()

                except Exception as e:
                    self._con.rollback()
                    self.logger.error("Error in update product: " + e.message)

        if closed is False:
            reason = "Su pago ha sido reversado, algun producto quedo sin stock o su precio cambio. "
        else:
            reason = "Su pago ha sido reversado, debido a que el restaurante esta cerrado. "

        # Change order
        msg = "Su orden ha sido rechazada y su pago reversado"
        order.status = "ORDER-REJECTED"
        order_user.status = "ORDER-REJECTED"
        type_status = 3

        # Update Order
        try:
            order.update()
            order_user.update()
        except Exception as e:
            return self.error('not update order in rejected: ' + e.message)

        data_aps = {
            "title": "Dxpress",
            "body": msg,
            "icon": "default",
            "sound": "default",
        }

        data = {
            "title": "Dxpress",
            "message": msg,
            "status": type_status,
            "location": {"order_id": str(order.id), "reason": reason}
        }

        ##Test Send gcm from UserRepository, give token database
        notification_id = self.user.save_notification(user_id=order.user.id, data=data, order_id=order.id)
        data["notification_id"] = notification_id
        data = json.dumps(data)
        self.user.send_gcm_repo(user_id=order.user.id, type=1, msg=data, notification=data_aps)

        data = {
            "type": "ACCEPT_OR_REJECTED",
            "message": {
                "status": False,
                "order_id": order.id,
                "reason": reason,
                "msg": msg
            },
            "user_id": order.user.id
        }
        self.redis().publish("channel-users", json.dumps(data, cls=JsonCustomEncoder))

        return self.success()

    def send_finish_mail(self, email, slug, subsidiary):
        data = {
            "url": self.base_url,
            "slug": slug,
            "subsidiary_id": subsidiary
        }
        html = self.render_template('mail/finish_process.html', **data)

        self.mailer.send_message(
            'dxpress@{domain}'.format(domain=self.base_url.replace("https://", "")),
            [email],
            subject=u'Gracias por utilizar Dxpress',
            html=html,
            tags=['finished_process']
        )

    def send_subsidiary_offline_email(self, email):
        html = self.render_template('mail/view_offline.html')

        self.mailer.send_message(
            'dxpress@{domain}'.format(domain=self.base_url.replace("https://", "")),
            [email],
            subject=u'Notificación Dxpress',
            html=html,
            tags=['subsidiary_offline'])

    # Method DXL
    def process_order_dxl(self):

        if self.is_production:
            now = arrow.now("America/Santiago")
            min_hour = arrow.now("America/Santiago").replace(hour=12, minute=0, second=0)
            max_hour = arrow.now("America/Santiago").replace(hour=23, minute=59, second=0)

            if now < min_hour or now > max_hour:
                return self.error(
                    "Lo sentimos, solo puedes realizar pedidos desde las 12:00 hrs. hasta las 23:00 hrs.")

        self.logger.error("Execute process order DXL")

        # Get param
        subsidiary_id = self.param('subsidiary_id')
        address = self.param('address')
        user = self.param('user')
        correlative = self.param("correlative")
        subtotal = self.param('subtotal')

        if isinstance(address, basestring):
            try:
                address = json.loads(address)
            except:
                self.logger.error("Address not basestring")

        # Create address
        address_order = Address(
            name="Reparto DXL",
            address=address['fullAddress'],
            reference="DXL",
            apartment=address['apartment'],
            block=address['block'],
            geo=Point(x=address['coord'][1], y=address['coord'][0]),
            telephone=user['telephone']
        )

        # Create Subsidiary

        self.logger.info("Check if subsidiary exists: %s", subsidiary_id)

        try:
            subsidiary = self.subsidiary.find(id=subsidiary_id)
            # subsidiary_active = self.subsidiary.is_active(subsidiary_id)
        except NoneResult:
            # No bloqueamos el usuario
            return self.error(self._("subsidiary_not_exists"))

        if not subsidiary.dxl:
            return self.error("Sucursal no posee Servicio DXL")

        # Set default status
        status = 'DXL-PREPARATION'

        # Set and check is_person
        is_person = False
        if subsidiary.commerce.is_person:
            self.logger.error("Commerce is Person")
            is_person = True
            status = 'DXL-ORDER'

        subsidiary_dict = {
            'id': subsidiary.id,
            'overprice_percentage': subsidiary.overprice_percentage,
            'fullname': '{0} {1}'.format(subsidiary.commerce.name.encode('utf-8'), subsidiary.alias.encode('utf-8')),
            'commerce_id': subsidiary.commerce_id,
            'alias': subsidiary.alias,
            'telephone': subsidiary.telephone,
            'commerce': Commerce(id=subsidiary.commerce.id,
                                 logo=subsidiary.commerce.logo,
                                 name=subsidiary.commerce.name,
                                 alliance=subsidiary.commerce.alliance,
                                 franchise=subsidiary.commerce.franchise,
                                 is_person=is_person)
        }

        cassandra_subsidiary = CSubsidiary(**subsidiary_dict)

        # Generate UserID
        user_id = int(subsidiary.id) + int(time.time())

        # Create user
        user_dict = {
            'id': user_id,
            'name': user['name'],
            'email': "DXL",
            'telephone': user['telephone']
        }
        cassandra_user = CUser(**user_dict)

        # Create product
        # Create product object
        products = []

        try:
            product_dict = {
                'id': '-1',
                'name': 'Despachado DXL',
                'price': '0',
                'quantity': '0',
                'clarification': '',
                'ingredients_extra': None,
                'subcategory_id': None,
                'compounds': None,
                'description': None,
                'ingredients': None
            }

            # CProduct is new_product release Dispatcher
            new_product = CProduct(**product_dict)

            # Append list products with new product
            products.append(new_product)

        except Exception as e:
            self.logger.error("Error in create ObjectProduct: " + e.message)
            return self.error("Product Error")

        # Calculate dispatch_amount
        dispatch_amount, distanceText, distanceValue, overprice = self.__calculate_dispatch_amount_and_distance(
            subsidiary_id=subsidiary_id, address_id=None, lat=address['coord'][0], lng=address['coord'][1])

        # Save distanceValue
        address_order.reference = "Cordinates {0} {1}, Value Distace {2}".format(str(address['coord'][0]),
                                                                                 str(address['coord'][1]),
                                                                                 str(distanceValue))

        # Set overprice
        subsidiary_overprice = 0
        if subsidiary.dxl_overprice is not None and subsidiary.dxl_overprice > 0:
            subsidiary_overprice = subsidiary.dxl_overprice
        elif subsidiary.dxl_overprice is not None and subsidiary.dxl_overprice == 0 and distanceValue > 7500:
            self.logger.error("No tiene procentaje definido")
            subsidiary_overprice = 0.1

        self.logger.error(subsidiary_overprice)
        # Validate distance
        # if overprice == False and distanceValue > 7500:
        #    return self.error("Destino excede la distancia maxima permitida.")

        now = arrow.now('America/Santiago').to('utc').datetime

        order_dict = {
            'id': str(uuid.uuid1()),
            'correlative': correlative,
            'subsidiary': cassandra_subsidiary,
            'pickup': False,
            'is_mobile': False,
            'reference': "DXL",
            'client_code': Security.generate_random(5, type='digit'),
            'user_code': Security.generate_random(5, type='digit'),
            'order_date': now,
            'status': status,
            'address_order': address_order,
            'dispatch_amount': dispatch_amount,
            'user': cassandra_user,
            'products': products,
            'subtotal': subtotal,
            'order_preparation': now,
            'order_paid': now,
            'subtotal_with_discount': None,
            'subtotal_without_discounts': None,
            'coupon': None,
            'discounts': None,
            'total': int(subtotal) + int(dispatch_amount),
            'dxl': True,
            'dxl_overprice': subsidiary_overprice,
            'reference': 'https://map.dxpress.cl/' + str(cassandra_user.id),
            'distance': distanceValue,
        }

        # Create order object, inject dict of order
        order = Order(**order_dict)

        client_order = ClientOrders(payment=order.payment,
                                    id=order.id,
                                    correlative=order.correlative,
                                    order_date=order.order_date,
                                    discounts=order.discounts,
                                    order_paid=arrow.now('America/Santiago').to('utc').datetime,
                                    order_confirmation=order.order_confirmation,
                                    order_preparation=order.order_preparation,
                                    subsidiary_id=order.subsidiary.id,
                                    products=order.products,
                                    pickup=order.pickup,
                                    status=order.status,
                                    subtotal=order.subtotal,
                                    subtotal_with_discount=order.subtotal_with_discount,
                                    subtotal_without_discounts=order.subtotal_without_discounts,
                                    dxl_overprice=order.dxl_overprice,
                                    dispatch_amount=order.dispatch_amount,
                                    distance=order.distance,
                                    reference=order.reference,
                                    user=order.user,
                                    address_order=order.address_order,
                                    franchise=order.subsidiary.commerce.franchise,
                                    is_person=is_person)

        try:
            client_order.save()
            order.save()
        except Exception as e:
            self.logger.error(e.message)
            return self.error(self._("error_order_dont_saved"))

        # Save Order Has
        order_hash = Hash(id_temp=order.user.id, order_id=order.id)

        try:
            self.user.save(order_hash)
        except Exception as e:
            self.logger.error('Save orderHash is error: ' + e.message)

        order_json = json.dumps(order, cls=JsonCustomEncoder)

        if not self.rabbit.publish(queue='subsidiary_' + str(subsidiary_id) + '_paid',
                                   msg=order_json):
            self.logger.error("We couldn't send the payment to the restaurant")
            raise Exception("We couldn't send the payment to the restaurant")

        return self.success("OK")

    def __client_code_dxl(self, order, client_code):
        self.logger.error("Ingresando a __client_code_dxl ")
        self.logger.error(order)
        self.logger.error(client_code)

        order = Order.objects(id=order.id).first()

        client_order = ClientOrders.objects(id=order.id, subsidiary_id=order.subsidiary.id).first()

        if client_order is None:
            self.logger.error("ClientOrders doesn't exist - '__client_code_dxl' ")

            return self.error(self._("order_dont_exist"))

        try:
            client_code = int(client_code)
            order_client_code = int(order.client_code)
        except ValueError:
            self.logger.error("The client code from order is invalid.")

            return self.error(self._("invalid_generated_code"))

        # Set status

        # TODO: Remove this byPass
        if order_client_code == client_code or 1 == 1:
            now = arrow.now('America/Santiago').to('utc').datetime
            order.dispatch_date = now

            order.status = "DXL-DISPATCH"
            client_order.status = 'DXL-FINISH'

            try:
                order.save()
                client_order.save()
            except Exception as e:
                self.logger.error("Error update order in client_code: " + e.message)
                return self.error(self._("order_not_updated"))

            self.logger.error("Despues de guardar")
            self.logger.error(order.status)
            self.logger.error(client_order.status)

            # Emmit CLIENT Ok
            data = {
                "type": "order-validate",
                "message": {
                    'message': self._("valid_code"),
                    'order_id': order.id
                },
                "subsidiary_id": order.subsidiary.id
            }

            self.redis().publish("channel-clients", json.dumps(data, cls=JsonCustomEncoder))

            # Emmit User Track

            # Notify user first message
            msg = '{0} Ya salió su pedido de {1}, puede ver el repartidor haciendo click en el link'.format(
                order.reference, order.subsidiary.commerce.name)
            self.send_message(order.user.telephone, msg)

            # Emmit dispatcher Ok
            data_dispatcher = {
                "id": order.id
            }

            queue_name = 'dispatcher_' + str(order.dispatcher.id) + '_confirmation'

            if not self.rabbit.publish(queue_name, json.dumps(data_dispatcher, cls=JsonCustomEncoder)):
                return self.error(self._("cant_send_order"))

            return self.success(self._("valid_code"))
        else:
            return self.error(self._("invalid_code"))

    def __user_code_dxl(self, order, user_code):

        try:
            order_user_code = int(order.user_code)
        except ValueError:
            self.logger.error("The user_code from order is invalid.")
            return self.error(self._("invalid_generated_code"))

        # TODO: Remove this byPass
        if user_code == order_user_code or 1 == 1:
            if order.status.upper() == "DXL-DISPATCH":
                status = 'DXL-ORDER-FINISH'

                now = arrow.now('America/Santiago').to('utc').datetime

                # Search dispatcherOrder
                dispatcher_order = self.dispatcherorder.find(dispatcher_id=order.dispatcher.id, current=True)
                if dispatcher_order is not None:
                    if dispatcher_order.order_id != str(order.id):
                        self.logger.error("dispatcherOrder not equals order")
                    else:
                        self.dispatcherorder.remove(dispatcher_order)
                else:
                    self.logger.error("dispatcherOrder is None")

                # update order
                order.status = status
                order.delivery_date = now

                try:
                    order.update()
                except Exception as e:
                    self.logger.error("Error update order in  user_code: " + e.message)
                    return self.error(self._("order_not_updated"))

                # search subsidiary
                subsidiary = self.subsidiary.find(id=order.subsidiary.id)

                self.send_finish_mail(order.user.email, subsidiary.commerce.slug, subsidiary.id)

                # Set dispatch_amount
                real = order.dispatch_amount
                less = real - 2000
                meters = less / 90
                dispatcher_amount = float(2000 + meters * 70)

                # create dispatcher_summary
                dispatch_summary = DispatcherSummary(
                    dispatcher_id=order.dispatcher.id,
                    order_id=order.id,
                    dispatch_amount=dispatcher_amount,
                    date=arrow.now('America/Santiago').to('utc').datetime,
                    subsidiary_name=order.subsidiary.fullname,
                    subsidiary_address=subsidiary.address,
                    vehicle_id=order.dispatcher.vehicle_id,
                    alliance=order.subsidiary.commerce.alliance,
                    dispatch_date=order.dispatch_date,
                    delivery_date=order.delivery_date
                )

                try:
                    dispatch_summary.save()
                except Exception as e:
                    self.logger.error("create dispatch_summary error: " + e.message)
                    return self.error('dispatch_summary_not_saved')

                # Notify user Dxpress Coupon
                msg = 'Tu pedido llego gracias a Dxpress Delivery, Obtén un 10% de descuento en "{0}" con el CUPON "Dxcuento". http://dxpress.cl/'.format(
                    order.subsidiary.commerce.name)
                self.send_message(order.user.telephone, msg)

                # restablecer el despachador
                dispatcher = Redis.User.query.filter(id=order.dispatcher.id).first()

                if dispatcher is None:
                    dispatcher = self.user.create_dispatcher_in_redis(order.dispatcher.id)

                if dispatcher is not None:
                    dispatcher.with_order = False
                    dispatcher.order_id = None

                try:
                    dispatcher.save()
                except Exception as e:
                    self.logger.error("Error: " + e.message)
                    self.logger.error(self._("cant_reestablish_dispatcher"))

                return self.success(self._("valid_code"))
            elif order.status.upper() == "ORDER_FINISH":
                return self.success(order.status.upper())
            else:
                # TODO: Translate
                return self.error("invalid order status")
        else:
            return self.error(self._("invalid_code"))
