# -*- coding: utf-8 -*-
import logging

from wheezy.core.descriptors import attribute

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Core.Exceptions.UploadError import UploadError
from Classes.Decorators import cache
from Classes.Decorators.Logged import logged
from Models import Subsidiary
from Models.Commerce import Commerce
from Models.User import User
from Models.UserSubsidiary import UserSubsidiary
from Classes.Decorators.Ajax import ajax


class CommerceHandler(APIHandler):
    def start(self):
        self.repository([
            Commerce,
            Subsidiary,
            User,
            UserSubsidiary
        ])
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['commerce']

    # @cache(type="commerce")
    def get(self):

        """
        GET HTTP Method

        Return a list of commerces or details of a specific commerce
        :return: JSON response
        """

        # Get commerce ID from Route
        commerce_id = self.route('id')
        extra = self.route('extra')
        slug = self.route('slug')

        # If there isn't commerce id we are going to return all the commerces
        try:
            if slug is not None:
                commerce = self.commerce.get_commerce_by_slug(slug)

                return self.success(commerce.to_dict())
            elif commerce_id is None:
                if self.permissions().has_perm("system"):
                    response = self.commerce.all()
                else:
                    return self.error()

            elif commerce_id == "names":
                commerces = self.commerce.get_all_names()
                return self.success(self.to_dict(commerces))

            elif commerce_id is not None and extra == 'tags':
                self.logger.error(commerce_id)
                response = self.commerce.get_commerce_tags(commerce_id)
                #return self.success(self.commerce.to_dict(response))
                return self.success(response)
            elif commerce_id is not None and extra == 'subsidiaries':
                response = self.commerce.get_subsidiaries(commerce_id)
                return self.success(self.commerce.to_dict(response))
            elif commerce_id is not None and extra == 'admin':
                response = self.commerce.get_admin(commerce_id)
                if response is None:
                    return self.error('Dont')
            elif commerce_id is not None and extra == 'user':
                response = self.commerce.get_users(commerce_id)

                rel = {
                    "commerceuser": ['commerce', 'user'],
                    "user": ['users_subsidiaries']
                }

                return self.success(self.commerce.to_dict(response, depth=4, rel=rel))
            elif commerce_id is not None and extra == 'user_subsidiary':
                response = self.commerce.get_users_subsidiaries(commerce_id)

                rel = {
                    "subsidiary": ["users_subsidiaries"],
                    "usersubsidiary": ["user"]
                }
                return self.success(self.commerce.to_dict(response, depth=2, rel=rel))

            else:
                response = self.commerce.find(id=commerce_id)
                if self.permissions().has_perm("system"):
                    return self.success(self.commerce.to_dict(response, force=True))
                else:
                    return self.success(self.commerce.to_dict(response))
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.commerce.to_dict(response))

    @logged('system')
    def post(self):
        extra = self.route('extra')
        commerce_id = self.route('id')

        if "telephone" in self.request.form and isinstance(self.request.form["telephone"], basestring):
            self.request.form["telephone"] = self.request.form["telephone"]. \
                replace(" ", ""). \
                replace("(", ""). \
                replace(")", ""). \
                replace("+", "")

        # Create User for commerce
        if extra == 'user':
            user = self.model_process(User())

            if user is None:
                return self.error()
            save = self.user.save(user)
            user_id = user.id
            if save:
                commerce_user = self.commerce.create_commerce_user(user_id, commerce_id)
                if commerce_user:
                    return self.success(self._('commerce_user_saved'))
                else:
                    return self.error(self._("commerce_user_not_saved"))
            else:
                return self.error(self._("commerce_user_not_saved"))

        elif extra == 'user_subsidiary':
            user_subsidiary = self.model_process(UserSubsidiary())

            if user_subsidiary is None:
                return self.error()

            return self.success(self.usersubsidiary.save(user_subsidiary))

        commerce = self.model_process(Commerce())
        # if they send me a shit request i will response with shit :3
        if commerce is None:
            return self.error()

        if self.commerce.save(commerce):
            # asignar permiso al usuarioque acabo de crear
            return self.success(dict(id=commerce.id, user_id=commerce.user.id))
        return self.error()

    @logged('system')
    def put(self):
        commerce_id = self.route("id")

        try:
            commerce = self.commerce.find(id=commerce_id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(commerce)

        if model is None:
            return self.error()

        save = self.commerce.save(model)

        if save:
            enabled = model.enabled

            if enabled is not None and enabled == False:
                self.commerce.disable_all_subsidiaries(model.id)
        return self.result(save)

    def delete(self):
        commerce_id = self.route("id")
        commerce = self.commerce.find(id=commerce_id)
        delete = self.commerce.delete(commerce_id)
        if delete:
            for subsidiary in commerce.subsidiaries:
                subsidiary.enabled = False
                self.subsidiary.save(subsidiary)
        return self.success()
