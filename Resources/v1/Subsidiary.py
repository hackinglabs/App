# -*- coding: utf-8 -*-
import json
import logging
from sqlalchemy import func
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models import RatingSubsidiary
from Models.Subsidiary import Subsidiary
from wheezy.core.descriptors import attribute
import requests


class SubsidiaryHandler(APIHandler):
    def start(self):
        self.repository(Subsidiary, redis=self.redis())
        self.repository(RatingSubsidiary)
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['subsidiary']

    # @cache()
    @ajax
    def get(self):

        """
        GET HTTP Method

        Return a list of subsidiaries or details of a specific subsidiary
        :return: JSON response
        """

        premium = self.param('premium')

        self.logger.info("Request to  Subsidiary")

        # Get subsidiary ID from Route
        subsidiary_id = self.route("id")
        extra = self.route('extra')

        try:
            # If there isn't subsidiary id we are going to return all the subsidiaries
            if subsidiary_id is None and premium is None:
                response = self.subsidiary.all()
                return self.success(self.to_dict(response))

            elif subsidiary_id is None and premium is not None:
                subsidiaries = self.subsidiary.get_premium()
                return self.success(self.to_dict(subsidiaries))

            elif subsidiary_id is not None and extra == 'details':
                if self.permissions().has_perm("system"):
                    subsidiary = self.subsidiary.get_details_for_system(subsidiary_id)
                    response = subsidiary.to_dict()
                else:
                    subsidiary = self.subsidiary.get_details(subsidiary_id)
                    response = subsidiary.to_dict()

                    # Get Discount
                    response['discount'] = self.subsidiary.get_discount(subsidiary_id)

                response["favorites"] = self.subsidiary.favorites_count(subsidiary_id)
                response["rating"] = self.subsidiary.ratings_count(subsidiary_id)

                return self.success(response)

            elif subsidiary_id is not None and extra == 'ratings':
                ratings = self.subsidiary.get_ratings(subsidiary_id)

                if ratings is not None and len(ratings) > 0:
                    return self.success(self.to_dict(ratings))
                else:
                    return self.success(ratings)

            elif subsidiary_id is not None and extra == 'categories':
                categories = self.subsidiary.categories_for_subsidiary(subsidiary_id)
                return self.success(self.subsidiary.to_dict(categories))

            elif subsidiary_id is not None and extra == 'compounds':
                compounds = self.subsidiary.get_compounds(subsidiary_id)

                return self.success(self.to_dict(compounds))

            else:
                self.logger.info("Request specific subsidiary %s", subsidiary_id)

                # Request a specific subsidiary
                response = self.subsidiary.find(id=subsidiary_id)

        except NoneResult, e:
            return self.error(e.value)

        response = self.subsidiary.to_dict(response, depth=1, rel={'subsidiary': ['commerce']})
        return self.success(response)

    @logged()
    def post(self):

        self.logger.info("Request POST create Subsidiary")

        # receive params
        id = self.route('id')
        extra = self.route('extra')
        open_hours = self.post_param("open_hours")
        tags_base = self.post_param("tag_base")

        if id is not None and extra == 'ratings':
            user = self.is_logged()
            try:
                self.request.form['subsidiary_id'] = id
                self.request.form['user_id'] = user['user']
            except Exception as e:
                self.logger.error(e.message)
                pass

            rating_for_user_exists = self.ratingsubsidiary.exists(user_id=user['user'], subsidiary_id=id)

            self.logger.error(rating_for_user_exists)

            if rating_for_user_exists:
                return self.success(self._("already_commented"))

            ratings = self.model_process(RatingSubsidiary())

            if ratings is None:
                return self.error()

            response = self.ratingsubsidiary.save(ratings)

            if response:
                subsidiary_update = self.ratingsubsidiary.calculate_rating_average(id)

                if not subsidiary_update:
                    return self.logger.error(self._('rating_subsidiary_not_saved'))

                return self.success(None)
            else:
                return self.error()

        else:
            if self.permissions().has_perm("system"):
                if isinstance(open_hours, basestring):
                    try:
                        self.request.form["open_hours"] = json.loads(open_hours)
                    except:
                        self.errors["open_hours"] = "Invalid format."
                elif not isinstance(open_hours, dict) and open_hours is not None:
                    self.errors["open_hours"] = "Invalid format."

                if isinstance(tags_base, basestring):
                    try:
                        tags_base = json.loads(tags_base)
                    except Exception as e:
                        self.logger.error("Error: " + e.message)
                        self.errors["tag_base"] = "Invalid format."
                elif not isinstance(tags_base, dict) and tags_base is not None:
                    self.errors["tag_base"] = "Invalid format."

                subsidiary = self.model_process(Subsidiary())

                if subsidiary is None:
                    return self.error()

                if self.subsidiary.save(subsidiary):
                    if tags_base is not None:
                        self.subsidiary.save_tag_base(tags_base, subsidiary.commerce_id)
                    return self.success(dict(id=subsidiary.id))
                return self.error()

    @logged('system')
    def put(self):
        # Get subsidiary id from Route
        subsidiary_id = self.route("id")
        tags_base = self.post_param("tag_base")
        open_hours = self.post_param("open_hours")

        if isinstance(open_hours, basestring):
            try:
                self.request.form["open_hours"] = json.loads(open_hours)
            except:
                self.errors["open_hours"] = "Invalid format."
        elif not isinstance(open_hours, dict) and open_hours is not None:
            self.errors["open_hours"] = "Invalid format."

        # Validate tags_base
        if isinstance(tags_base, basestring):
            try:
                tags_base = json.loads(tags_base)
            except Exception as e:
                self.logger.error("Error: " + e.message)
                self.errors["tag_base"] = "Invalid format."
        elif not isinstance(tags_base, dict) and tags_base is not None:
            self.errors["tag_base"] = "Invalid format."

        self.logger.info("Request specific subsidiary %s", subsidiary_id)

        try:
            subsidiary = self.subsidiary.find(id=subsidiary_id)
        except NoneResult, e:
            return self.error(e.value)
        # validate and load data in model
        subsidiary = self.model_process(subsidiary)

        if subsidiary is None:
            return self.error()

        save_result = self.subsidiary.save(subsidiary)
        if save_result:
            if tags_base is not None:
                self.subsidiary.save_tag_base(tags_base, subsidiary.commerce_id)

        return self.success(save_result)

    @logged('system')
    def delete(self):

        subsidiary_id = self.route('id')
        extra = self.route('extra')
        user_id = self.logged["user"]
        id = self.param('id')

        if subsidiary_id is not None and extra == 'ratings' and id is not None:
            try:
                rating_delete = self._con.query(RatingSubsidiary).filter(RatingSubsidiary.id == id,
                                                                         RatingSubsidiary.user_id == user_id).delete()
            except NoneResult, e:
                return self.error()

            if rating_delete:
                return self.success()
            else:
                return self.error(msg='Dont')