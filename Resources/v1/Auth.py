# -*- coding: utf-8 -*-

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators.Ajax import ajax
from Classes.Factories.OAuth2Factory import OAuth2Factory
from Classes.Permission import Permission
from Helpers.Security import Security
from Models import Subsidiary
from Models.User import User
from Models.Commerce import Commerce
from Models.UserSubsidiary import UserSubsidiary
from jose import jwt
from argon2 import PasswordHasher
from wheezy.core.descriptors import attribute
import logging
import arrow
from Models import Redis


class AuthHandler(APIHandler):
    logger = None

    def start(self):
        self.repository([
            User,
            Commerce,
            UserSubsidiary,
        ])
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['auth']


    def get(self):
        action = self.route("action")
        if action == "generate_jwt":
            user_id = self.param("user_id")
            user_id = int(user_id)
            jwt = self.__generate_temp_jwt(user_id)
            return self.success(jwt)

    @ajax
    def post(self):

        action = self.route("action")
        if action == "login":
            return self.normal_login()
        elif action in ["facebook", "google"]:
            return self.social_login(action)
        elif action == "register":
            return self.register()
        elif action == "logout":
            return self.logout()

    def logout(self):
        """ Logout just remove a token device from redis

        This isn't a normal logout because it only removes a token device from
        a redis object
        """
        user_id = self.param("id")
        input_token = self.param("token")

        """
        user = Redis.User.query.filter(id=user_id).first()
        if user is None:
            self.logger.warning("Logout: Doesn't exist redis user.")
            return self.success()

        self.logger.error(user.token_devices)
        if user.token_devices is not None:
            self.logger.error(user.token_devices)
            cleaned_tokens = []
            if isinstance(user.token_devices, list):
                for token in user.token_devices:
                    if token['token'] == input_token:
                        continue

                    if arrow.get(token['last_use']).replace(days=+5) > arrow.now('utc'):
                        cleaned_tokens.append(token)

                user.token_devices = cleaned_tokens
            elif isinstance(user.token_devices, dict):
                if user.token_devices['token'] == input_token:
                    user.token_devices = []

            try:
                user.save()
            except Exception, e:
                self.logger.error(str(e))
                return self.error()

        self.logger.error(user.token_devices)
        """
        return self.success()

    def register(self):
        """
        Register a new user
        :return:
        """

        user = self.model_process(User())

        if user is None:
            return self.error()

        user.recover_token = Security.get_csrf()
        user.xcsrf = Security.get_csrf()

        ok, new = self.user.create_user(user)
        if ok:
            token = self.__generate_jwt(user)
            self.__send_email_new(user.email, "DXCUPON")
        else:
            return self.error("Can't register the account.")

        obj = {
            'user': {
                'id': user.id,
                'email': user.email,
                'full_name': user.fullname,
                'last_name': user.last_name,
                'first_name': user.first_name,
                'telephone': user.telephone,
                'avatar': user.avatar,
                'active': user.active
            },
            'XCSRF': user.xcsrf,
            'token': token
        }
        return self.success(obj)

    def normal_login(self):
        """
        User tries to login of normal way (user, password)
        :return:
        """

        email = self.param("email")

        # email to lowercase
        if email and isinstance(email, basestring):
            self.request.form["email"] = email.lower()

        # Check all params
        user = self.model_process(User(), bypass=True)
        if user is None:
            return self.error()

        # Find the user
        try:
            db_user = self.user.find(email=user.email)
        except NoneResult:
            return self.error(self._("account_doesnt_exists"))

        if db_user.enabled is False:
            return self.error(self._("account_not_enabled"))

        # Verify password
        ph = PasswordHasher(time_cost=15, hash_len=40, salt_len=36)
        try:
            verify = ph.verify(db_user.password, user.password)
        except:
            verify = False

        if verify:
            login_type = self.route("type")

            # Get the user permissions
            user_permissions = self.user.get_all_perms(user_id=db_user.id)
            user_permissions = Permission(user_permissions)

            extra = None
            if login_type == "client":
                if not user_permissions.has_perm("login_subsidiary"):
                    return self.error(self._("account_is_not_client"))

                extra = self.__get_subsidiaries(db_user)

                if extra is None or 'subsidiaries' not in extra:
                    return self.error(
                        self._("account_without_subsidiaries"))

            if login_type == "dispatcher":
                if not user_permissions.has_perm("login_dispatcher"):
                    return self.error(self._("account_is_not_dispatcher"))

                if not db_user.driver_vehicles:
                    return self.error("account_without_vehicle")

            if login_type == "admin":
                if not user_permissions.has_perm("login_administrator"):
                    return self.error(self._("account_is_not_admin"))

            # Create CSRF
            db_user.xcsrf = Security.get_csrf()

            # Guardar en DB
            self.user.save(db_user)

            # Create JWT
            token = self.__generate_jwt(db_user, extra, user_permissions)

            # Get subsidiaries id my favorites
            subsidiaries_favorites = self.user.get_favorites_ids(db_user.id)

            # Get Addresses for user
            addresses = self.user.get_addresses(db_user.id)
            if len(addresses) > 0:
                addresses = self.to_dict(addresses)

            data = {
                'user': {
                    'id': db_user.id,
                    'email': db_user.email,
                    'permissions': user_permissions.to_dict(),
                    'full_name': db_user.fullname,
                    'first_name': db_user.first_name,
                    'last_name': db_user.last_name,
                    'telephone': db_user.telephone,
                    'avatar': db_user.avatar,
                    'active': db_user.active,
                    'addresses': addresses,
                    'subsidiary_favorites': subsidiaries_favorites
                },
                'XCSRF': db_user.xcsrf,
                'token': token
            }

            if extra is not None:
                data['user'].update(extra)

            return self.success(data)
        else:
            # Check if password is an empty string
            try:
                verify = ph.verify(db_user.password, '')
            except:
                verify = False

            # Show to the user which service used to login
            if verify:
                if db_user.facebook is not None:
                    return self.error(
                        self._("account_with_facebook"))

                if db_user.google is not None:
                    return self.error(
                        self._("account_with_google"))
            else:
                return self.error(self._("invalid_email_password"))

    def social_login(self, social_network):
        social_network = social_network.capitalize()

        social = OAuth2Factory()

        try:
            # Create social object and try to get user data
            social = social.create(social_network, self.request.form)
            social.get_data()
        except Exception, e:
            # Show errors trying to obtain data or create social object
            return self.error(e.value)
        except:
            # Any other error
            return self.error(self._("error_processing_login") % {'type': social_network})

        logged = self.is_logged()

        try:
            # Try to find user by id or social id
            if logged is not False:
                data = {'id': logged["user"]}
            else:
                data = {social.type: social.id}
            user = self.user.find(**data)
        except:
            user = None

        if user is None and hasattr(social, 'email'):
            try:
                user = self.user.find(email=social.email)
            except NoneResult:
                user = None

        if user is not None:
            if user.enabled is False:
                return self.error(self._("account_not_enabled"))

        try:
            # Trying to get user object
            user = social.get_user(user)
        except Exception, e:
            return self.error(e.value)

        # Register or update user
        ok, new = self.user.create_user(user, True)
        if ok:
            if new:
                pass
                self.__send_email_new(user.email, "DXCUPON")
            token = self.__generate_jwt(user)

            # Get permissions user
            user_permissions = self.user.get_all_perms(user_id=user.id)

            # Get Addresses for user
            addresses = self.user.get_addresses(user.id)
            if len(addresses) > 0:
                addresses = self.to_dict(addresses)

            data = {
                'user': {
                    'id': user.id,
                    'email': user.email,
                    'permissions': user_permissions,
                    'full_name': user.fullname,
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                    'telephone': user.telephone,
                    'addresses': addresses,
                    'avatar': user.avatar,
                    'active': user.active
                },
                'XCSRF': user.xcsrf,
                'token': token
            }

            return self.success(data)

        return self.error(self._("cant_login_with") % {'type': social_network})

    def __get_subsidiaries(self, user):
        is_manager = False
        user_subsidiaries = None
        try:
            commerce = self.commerce.find(user_id=user.id)
            is_manager = True
        except NoneResult:
            pass

        if is_manager is False:
            try:
                user_subsidiaries = self.usersubsidiary.find(user_id=user.id)
            except NoneResult:
                pass

        if is_manager is True:
            # subsidiaries = commerce.subsidiaries
            subsidiaries = [subsidiary for subsidiary in commerce.subsidiaries if subsidiary.enabled == True]
        elif user_subsidiaries is not None:
            subsidiaries = user_subsidiaries.subsidiary
        else:
            subsidiaries = []

        subsidiaries_list = []
        if isinstance(subsidiaries, list) and subsidiaries:
            commerce_id = subsidiaries[0].commerce_id
            commerce_name = commerce.name
            for subsidiary in subsidiaries:
                subsidiaries_list.append({
                    'id': subsidiary.id,
                    'name': subsidiary.alias
                })
        elif isinstance(subsidiaries, Subsidiary):
            commerce_id = subsidiaries.commerce_id
            subsidiaries_list.append({
                "id": subsidiaries.id,
                "name": subsidiaries.alias
            })
        else:
            return None

        return {
            'is_manager': is_manager,
            'commerce_name': commerce_name,
            'commerce_id': commerce_id,
            'subsidiaries': subsidiaries_list
        }

    def __generate_jwt(self, user, extra=None, permissions=None):

        redis_user = Redis.User.query.filter(id=user.id).first()
        loaded_columns, unloaded_columns, relations, columns = user.get_columns()

        if redis_user is None:
            redis_user = Redis.User.get_blank(user.id)

        for attr in redis_user.to_dict():
            if attr in loaded_columns:
                user_prop_value = getattr(user, attr, None)
                if user_prop_value is not None and attr != 'id':
                    setattr(redis_user, attr, user_prop_value)
        redis_user.user_token = user.xcsrf

        try:
            redis_user.save()
        except Exception, e:
            self.logger.error(e.message)

        if permissions is None:
            permissions = self.user.get_all_perms(user_id=user.id)
        if isinstance(permissions, Permission):
            permissions = permissions.to_dict()

        payload = {
            'user': user.id,
            'fullname': user.fullname,
            'permissions': permissions,
            'logged': str(arrow.now('America/Santiago').to('utc').datetime),
            'expiration': str(arrow.now('America/Santiago').to('utc').replace(years=+10).datetime)
        }

        if extra is not None:
            payload.update(extra)

        return jwt.encode(
            payload, self.options["token_secret"], algorithm='HS256')

    def __send_email_new(self, email, coupon):
        data = {
            "url": self.base_url,
            "description": "Disfruta de un 10 % de descuento, en tu primera compra ",
            "coupon": coupon,
        }
        html = self.render_template('mail/new_user.html', **data)

        self.mailer.send_message(
            'dxpress@{domain}'.format(domain=self.base_url.replace("https://", "")),
            [email],
            subject=u'Bienvenido a Dxpress',
            html=html,
            tags=['new_user']
        )


    def __generate_temp_jwt(self, user_id):


        payload = {
            'user': user_id,
            'fullname': 'User DXL',
            'logged': str(arrow.now('America/Santiago').to('utc').datetime),
            'expiration': str(arrow.now('America/Santiago').to('utc').replace(years=+10).datetime)
        }

        return jwt.encode(
            payload, self.options["token_secret"], algorithm='HS256')
