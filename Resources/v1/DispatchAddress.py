# -*- coding: utf-8 -*-
import logging

from wheezy.core.descriptors import attribute

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Models.RedZone import RedZone
from Models.DispatchAddress import DispatchAddress


class DispatchAddressHandler(APIHandler):
    def start(self):
        self.repository([DispatchAddress,
                         RedZone])
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['address']

    def get(self):
        """
        GET HTTP Method

        Return a list of devices or DispatchAddress of a specific DispatchAddress
        :return: JSON response
        """
        # Get dispatchAddress||user ID from Route
        dispatch_id = self.route("id")
        try:
            dispatch_address = self.dispatchaddress.find_full(dispatch_id)
        except NoneResult, e:
            return self.error(e.value)
        return self.success(dispatch_address.to_dict())

    def post(self):

        area_id = self.param("area_id")

        dispatch_address = self.model_process(DispatchAddress())

        coords = self.param("coordinates")

        if coords is not None:
            coords = coords.split(" ")
            if self.redzone.contains(coords[0], coords[1]):
                return self.error(self._("not_in_redzone"))

        if dispatch_address is None:
            return self.error()

        if self.dispatchaddress.save(dispatch_address):
            return self.success(dict(id=dispatch_address.id))
        else:
            return self.error()

    def put(self):
        dispatch_id = self.route("id")

        coords = self.param("coordinates")
        if coords is not None:
            coords = coords.split(" ")
            if self.redzone.contains(coords[0], coords[1]):
                return self.error(self._("not_in_redzone"))

        try:
            dispatch_address = self.dispatchaddress.find(id=dispatch_id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(dispatch_address, True)

        if model is None:
            return self.error()

        return self.result(self.dispatchaddress.save(model))
