# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators.Logged import logged
from Models.DevicesSupport import DevicesSupport


class DevicesSupportHandler(APIHandler):
    def start(self):
        self.repository(DevicesSupport)
        self.logger = logging.getLogger(__name__)

    def get(self):
        """
        GET HTTP Method

        Return a list of devices or DevicesSupport of a specific DeviceSupport
        :return: JSON response
        """

        # Get device_support ID from Route
        device_sopport_id = self.route("id")

        try:
            # If there isn't device_support id we are going to return all the devices_supports
            if device_sopport_id is None:
                response = self.devicessupport.all()
            else:
                # Request a specific DeviceSupport
                response = self.devicessupport.find(id=device_sopport_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.devicessupport.to_dict(response))

    @logged('system')
    def post(self):

        devicesupport = self.model_process(DevicesSupport())

        if devicesupport is None:
            return self.error()

        return self.result(self.devicessupport.save(devicesupport))

    @logged('system')
    def put(self):

        # Get presentation ID from Route
        device_sopport_id = self.route("id")

        try:
            # Select specific device
            devicesupport = self.devicessupport.find(id=device_sopport_id)
        except NoneResult, e:
            return self.error(e.value)

        # validate and load data in model
        model = self.model_process(devicesupport, True)

        if model is None:
            return self.error()

        return self.result(self.devicessupport.to_dict(model))

