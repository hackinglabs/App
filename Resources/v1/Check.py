# -*- coding: utf-8 -*-
import logging


from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Models.Discount import Discount
from Models.Coupon import Coupon
from Models.Subsidiary import Subsidiary
from Models.DispatchAddress import DispatchAddress
from wheezy.core.descriptors import attribute


class CheckHandler(APIHandler):
    def start(self):
        self.repository([
            Coupon,
            Subsidiary,
            DispatchAddress,
            Discount])
        self.logger = logging.getLogger(__name__)

    @attribute
    def translation(self):
        return self.translations['check']

    def get(self):
        """
        @api {get} /check/coupon Check if a coupon is valid.
        @apiName ValidateCoupon
        @apiGroup Utils

        @apiParam {String} code The code of the coupon.

        """

        what = self.route("what")
        self.logger.debug("ROUTE: %s", what)

        if what == "coupon":
            return self.check_coupon()
        elif what == "discount":
            return self.check_discount()

    def check_discount(self):
        subsidiary_id = self.param("subsidiary_id")
        address_id = self.param("address_id")
        product_ids = self.param("product_ids")
        total = self.param("subtotal")

        try:
            subsidiary = self.subsidiary.find(id=subsidiary_id)
        except NoneResult:
            return self.error("The subsidiary doesn't exists.")

        log = self.is_logged()
        address = None

        if address_id is not None and log:
            user_id = log["user"]
            try:
                address = self.dispatchaddress.find(id=address_id, user_id=user_id)
            except NoneResult:
                return self.error(self._("provide_a_valid_dispatch_address"))

        products = None
        if product_ids is not None and isinstance(product_ids, list):
            products_ids = []
            for p in product_ids:
                if isinstance(p, int):
                    products_ids.append(p)

            products = self.product.get_products_by_list(products_ids)


        if total is None:
            total = 0
        try:
            total = int(total)
        except ValueError:
            return self.error("Subtotal field must be an integer")

        our_discount, subsidiary_discount = self.discount.check_subsidiary_discounts(subsidiary, address, products, total)

        to_return = []
        if our_discount:
            to_return.append(our_discount.to_dict())
        if subsidiary_discount:
            to_return.append(subsidiary_discount.to_dict())

        return self.success(to_return)

    def check_coupon(self):
        code = self.param("code")

        subsidiary_id = self.param("subsidiary_id")

        address_id = self.param("address_id")

        pickup = self.param("pickup")
        if pickup is None:
            pickup = False
        else:
            pickup = True

        self.logger.debug("CODE: %s", code)
        self.logger.debug("SUBSIDIARY ID: %s", subsidiary_id)
        self.logger.debug("ADDRESS ID: %s", address_id)

        if code is None or subsidiary_id is None:
            return self.error("You must provide the code and the subsidiary identifier.")

        code = code.upper()

        try:
            coupon = self.coupon.find(code=code)
        except NoneResult:
            return self.error("The coupon doesn't exists.")

        try:
            subsidiary = self.subsidiary.find(id=subsidiary_id)
        except NoneResult:
            return self.error("You must provide a valid subsidiary.")

        log = self.is_logged()
        user_id = None

        if address_id is not None and log:
            user_id = log["user"]
            try:
                address = self.dispatchaddress.find(id=address_id, user_id=user_id)
            except NoneResult:
                return self.error(self._("provide_a_valid_dispatch_address"))
        else:
            address = None

        ok, msg = self.coupon.check(user_id, coupon, subsidiary, address, pickup)

        if ok:
            data = {
                'with_percentage': coupon.with_percentage,
                'amount': coupon.amount,
                'type': coupon.coupon_type,
                'id': coupon.type_id
            }
            return self.success(data)
        else:
            return self.error(msg)

    def check_subsidiary_discount(self):
        subsidiary_id = self.param("subsidiary_id")

        try:
            subsidiary = self.subsidiary.get_details(subsidiary_id)
        except:
            return self.success([])

        return self.success(self.discount.get_subsidiary_discounts(subsidiary))

