# -*- coding: utf-8 -*
import datetime
import json
import logging
import math

import arrow
import dateutil.parser
from cassandra.cqlengine import ValidationError
from khipu import Khipu
from wheezy.core.descriptors import attribute
from wheezy.http.response import redirect

from Classes import ConnectCassandra
from Classes.Core import JsonCustomEncoder
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import SaveError
from Classes.Core.Exceptions.NoneResult import NoneResult
from Classes.Decorators import logged
from Classes.Webpay import Webpay
from Configs.Config import Config
from Helpers.Utils import subsidiary_is_closed_hour_hand
from Helpers.Security import Security
from Models import Redis
from Models.Cassandra.ClientOrders import ClientOrders
from Models.Cassandra.CommerceSummary import CommerceSummary
from Models.Cassandra.Order import Order
from Models.Cassandra.OrdersUser import OrdersUser
from Models.Cassandra.Payment import Payment
from Models.Cassandra.WebpayOrder import WebpayOrder
from Models.Subsidiary import Subsidiary
from Models.User import User
from Models.UserCard import UserCard
from Models.BlackListPhone import BlackListPhone


class PaymentHandler(APIHandler):
    def start(self):
        self.repository([
            User,
            UserCard,
            Subsidiary,
            BlackListPhone,
            "Order"
        ])
        self.logger = logging.getLogger(__name__)
        self.cassandra = ConnectCassandra()

    @attribute
    def translation(self):
        return self.translations['payment']

    def post(self):
        provider = self.route("type")

        if self.is_production:
            now = arrow.now("America/Santiago")
            min_hour = arrow.now("America/Santiago").replace(hour=12, minute=0, second=0)
            max_hour = arrow.now("America/Santiago").replace(hour=23, minute=0, second=0)

            if now < min_hour or now > max_hour:
                return self.error(
                    "Lo sentimos, solo puedes realizar pagos desde las 12:00 hrs. hasta las 23:00 hrs.")

        if provider == "khipu":
            notification = self.param("notification_token")

            if notification is None:
                return self.generate_payment_khipu()
            else:
                return self.validate_payment_khipu()
        elif provider == "webpay":
            if self.param("token_ws") is not None:
                return self.show_proxy_page()
            elif self.param("token") is not None:
                return self.validate_payment_webpay()
            else:
                return self.generate_payment_webpay()
        elif provider == "oneclick":
            return self.oneclick_payment()
        elif provider == "inscription":
            if self.param("TBK_TOKEN"):
                return self.oneclick_finish()
            else:
                return self.oneclick_inscription()
        elif provider == "summary":
            token_ws = self.param("token_ws")
            if token_ws is None:
                token_ws = self.param("TBK_TOKEN")
            if token_ws is not None:
                return self.show_summary_webpay()
        elif provider == "cancel":
            return self.cancel_payment()
        elif provider == "force_paid":
            force_id = self.param("id")
            if force_id is None:
                return self.error('ID is None')
            if self.permissions().has_perm("admin"):
                self.__payment_accepted(force_id)
            else:
                return self.error('User is not admin')
        elif provider == 'cash':
            return self.send_code_payment_cash()
        elif provider == 'verify_code':
            return self.validate_code_payment_cash()

    def cancel_payment(self):
        order_id = self.param("order_id")

        try:
            order = Order.objects(id=order_id).first()
        except ValidationError:
            order = None

        if order is None:
            return self.error("Order is None")

        if order.status not in ['ORDER-IN-DISPATCH']:
            return self.error()

        if order.payment.ntoken is None:
            return self.error()

        order_tbk_code = order.payment.ntoken

        c = Config().webpay()
        w = Webpay(path=c['path'],
                   commerce_code=c['oneclick_code'])

        response = w.reverse(order_tbk_code)

        if response['response']:
            return self.success()
        else:
            return self.error()

    def oneclick_finish(self):
        tbk_token = self.param("TBK_TOKEN")
        final_url = "/error"

        oct = Redis.OneClickToken.get_by(token=tbk_token)

        if oct is None:
            # Redirect to main page?
            return self.error("")

        user_id = oct.user_id
        redirect_url = oct.redirect_url

        # Delete old tokens
        # TODO: WHAT? This isn't going to work
        for oc in Redis.OneClickToken.get_by(user_id=oct.user_id):
            oc.delete()

        c = Config().webpay()
        w = Webpay(path=c['path'],
                   commerce_code=c['oneclick_code'])

        finish = None
        response_code = -9

        try:
            finish = w.finish_inscription(tbk_token)
            response_code = int(finish['response_code'])
        except Exception as e:
            self.logger.error("Can't finish card inscription: " + str(e))

        if response_code == 0:
            self.usercard.delete_by_details(user_id,
                                            finish['cc_type'],
                                            finish['last_digits'])

            card = UserCard()
            card.auth_code = finish['auth_code']
            card.credit_card = finish['cc_type']
            card.last_digits = finish['last_digits']
            card.tbk_user = finish['tbk_user']
            card.user_id = user_id

            if self.usercard.save(card):
                final_url = "/success"

        return redirect(redirect_url + final_url)

    @logged()
    def oneclick_inscription(self):
        user_id = self.logged['user']
        redirect_url = self.param("redirect")

        if redirect_url is None:
            return self.error("You must set a redirect URL")
        redirect_url = redirect_url.rstrip("/")

        try:
            user = self.user.find(id=user_id)
        except NoneResult:
            return self.error("We can't find this logged user.")

        has_cards = self.usercard.exists(user_id=user.id)
        if has_cards:
            return self.error("You already has a defined card. Please remove that one and try again.")

        c = Config().webpay()
        w = Webpay(path=c['path'],
                   commerce_code=c['oneclick_code'])

        data = {
            'username': user.id,
            'email': user.email,
            'final_url': "{0}/oneclick".format(self.base_url)
        }

        try:
            inscription = w.init_inscription(**data)
        except Exception as e:
            self.logger.error(e.message)
            return self.error("Can't communicate with Transbank.")

        oct = Redis.OneClickToken(token=inscription['token'],
                                  user_id=user_id,
                                  redirect_url=redirect_url)

        try:
            oct.save()
        except Exception as e:
            self.logger.error(e.message)
            self.logger.error(oct.errors)
            return self.error("Unexpected error.")

        return self.success(inscription)

    def show_summary_webpay(self):
        tokenws = self.param("token_ws")

        if tokenws is None:
            tokenws = self.param("TBK_TOKEN")

        order = WebpayOrder.objects(token_ws=tokenws).first()

        if order is None:
            return self.render_response('error.html')

        if order.response_code is None:
            order = WebpayOrder.objects(token_ws=tokenws).first()

        real_order = Order.objects(id=order.order_id).first()

        data = {
            'correlative': order.correlative,
            'products': [],
            'authorization_code': order.authorization_code,
            'total': order.total,
            'commerce_url': self.base_url,
            'subtotal': order.subtotal,
            'dispatch': order.dispatch_amount
        }

        if self.get_user_agent() and \
                        self.get_user_agent().lower() not in ['dxpress-android', 'dxpress-ios']:
            data['commerce_url'] = self.base_url + "/prepare"

        for product in order.products:
            data['products'].append({
                'name': product.name,
                'price': product.price,
                'quantity': product.quantity,
                'description': product.description,
                'subtotal': (product.price * product.quantity)
            })

        # If order doesn't have data from tbk
        if order.response_code is None or order.response_code != "0":
            self.logger.error("Response_code in payment : " + str(order.response_code))
            return self.render_response('error.html', **data)

        transaction_date = dateutil.parser.parse(order.transaction_date)

        data.update({
            'order_paid': transaction_date,
            'transaction_type': 'Venta',
            'last_digits': order.card_number
        })

        payment_type_value = u'Crédito'
        shares_type = u'Sin interés'

        payment_type = order.payment_type.strip().upper()
        if payment_type == "VD":
            payment_type_value = 'Debito'
            shares_type = 'Venta Debito'
        elif payment_type == "VN":
            shares_type = 'Sin Cuotas'
        elif payment_type == "VC":
            shares_type = 'Cuotas normales'

        data['payment_type'] = payment_type_value
        data['shares_type'] = shares_type

        if payment_type in ["VC", "SI", "S2", "NC"]:
            data['shares_number'] = order.shares_number
        else:
            data['shares_number'] = '0'

        data['url'] = self.base_url

        if real_order.pickup:
            data['code'] = real_order.client_code
        else:
            data['code'] = real_order.user_code

        self.send_paid_email(real_order.user.email, data)

        return self.render_response('final.html', **data)

    @logged()
    def oneclick_payment(self):
        order_id = self.param("order_id")
        user_id = self.logged["user"]

        if order_id is None:
            return self.error("order_id parameter missing.")

        try:
            card = self.usercard.find(user_id=user_id)
        except NoneResult:
            return self.error("You doesn't have a credit card.")

        try:
            order = Order.objects(id=order_id).first()
        except ValidationError:
            order = None

        if order is None:
            return self.error("The order doesn't exist")

        confirmation_date = arrow.get(order.order_confirmation).to('utc').replace(minutes=+45)

        if arrow.now('utc') > confirmation_date:
            self.logger.error("OrderID: {0} Expired".format(order.id))
            order.status = "EXPIRED"
            try:
                order.update()
            except:
                self.logger.error("Cant set order as expired")

            return self.error(self._("order_expired"))

        subsidiary_id = order.subsidiary.id

        try:
            subsidiary = self.subsidiary.find(id=subsidiary_id)
        except NoneResult:
            return self.error("The subsidiary doesn't exists.")

        if subsidiary.commerce.alliance and not subsidiary.is_active:
            return self.error("The subsidiary isn't active.")

        status = order.status.upper()
        if status == "ORDER-ACCEPTED":

            c = Config().webpay()
            w = Webpay(path=c['path'],
                       commerce_code=c['oneclick_code'])

            order.order_paid = arrow.now('America/Santiago').to('utc').datetime

            buy_order = arrow.now('America/Santiago').format('YYYYMMDDHHmmss') + str(order.correlative)[-3:]
            try:
                authorize = w.authorize(amount=int(math.ceil(order.total)),
                                        tbk_user=card.tbk_user,
                                        username=card.user_id,
                                        buy_order=buy_order)
            except Exception as e:
                self.logger.error("Error in authorize: " + e.message)
                return self.error()

            response_code = -9
            if 'response_code' in authorize:
                response_code = int(authorize['response_code'])

            if 'error' not in authorize and \
                            response_code == 0:

                payment_data = {
                    'type': 'OneClick',
                    'ntoken': buy_order,
                    'card_number': authorize['last_digits'],
                    'authorization_code': authorize['auth_code'],
                    'response_code': authorize['response_code'],
                    'payment_type': authorize['cc_type'],
                    'id': str(authorize['transaction_id'])
                }

                self.order.update_payment(uuid=order_id, user=user_id, data=payment_data)

                data = {
                    'url': self.base_url,
                    'correlative': order.correlative,
                    'products': [],
                    'order_paid': order.order_paid,
                    'order_date': order.order_date,
                    'subtotal': order.subtotal,
                    'discounts': order.subtotal - order.subtotal_with_discount,
                    'dispatch_amount': order.dispatch_amount,
                    'total': order.total,
                }

                for product in order.products:
                    data['products'].append({
                        'name': product.name,
                        'price': product.price,
                        'quantity': product.quantity,
                        'description': product.description,
                    })

                if order.pickup:
                    data['code'] = order.client_code
                else:
                    data['code'] = order.user_code

                self.send_paid_email(order.user.email, data)

                try:
                    self.__payment_accepted(order.id)
                except SaveError as e:
                    return self.error(e.value)
                except Exception as e:
                    self.logger.error(e.message)
                    return self.error(self._("cant_send_payment"))

                authorize['buy_order'] = buy_order

                final_data = {
                    'oneclick': authorize,
                    'order': data
                }
                return self.success(final_data, custom=True)
            else:
                return self.error(buy_order)

        elif status == "ORDER-PAID":
            return self.error("The order was already paid.")
        elif status == "ORDER-IN-DISPATCH":
            return self.error("The order was already paid and has been shipped. ")
        elif status == "ORDER-DELIVERED":
            return self.error("The order was already paid and has been delivered.")
        elif status == "ORDER-FINISH":
            return self.error("The process has been finished.")
        elif status == "SEND-CLIENT":
            return self.error("Wait the answer from the restaurant")
        else:
            return self.error("Order error, contact us.")

    def validate_payment_webpay(self):
        c = Config().webpay()
        w = Webpay(path=c['path'],
                   commerce_code=c['commerce_code'])

        transaction = w.result_transaction(self.param("token"))

        # Get authorization code
        try:
            response_code = int(transaction['details']['response_code'])
        except ValueError:
            response_code = -1
        except KeyError:
            response_code = -1

        order = Order.objects(id=transaction['session']).first()

        payment_data = {
            'id': order.payment.id,
            'type': 'Webpay',
            'creation': order.payment.creation,
            'payment_url': order.payment.payment_url,
            'ntoken': order.payment.ntoken,
            'card_number': transaction['card_details']['number'],
            'card_expiration': transaction['card_details']['expiration'],
            'authorization_code': transaction['details']['authorization_code'],
            'payment_type': transaction['details']['payment_type'],
            'response_code': str(transaction['details']['response_code']),
            'shares_amount': transaction['details']['shares_amount'],
            'shares_number': transaction['details']['shares_number'],
            'accounting_date': transaction['accounting_date'],
            'transaction_date': transaction['transaction_date']
        }
        try:
            self.order.update_payment(uuid=order.id, user=order.user.id,
                                      token=order.payment.ntoken, data=payment_data)
        except SaveError, e:
            return self.error(e.value)

        ack = w.ack_transaction(self.param("token"))

        self.logger.error(ack)
        self.logger.error(transaction)

        if 'error' not in transaction and \
                        'error' not in ack and \
                        response_code == 0:
            try:
                self.__payment_accepted(order.id)
            except SaveError, e:
                return self.error(e.value)
            except Exception as e:
                self.logger.error(e.message)
                return self.error(self._("cant_send_payment"))

        return self.success(transaction)

    def show_proxy_page(self):
        host = self.base_url.replace("https://", "api.")
        return self.render_response('proxy.html', host=host, token=self.param("token_ws"))

    def validate_payment_khipu(self):
        notification = self.param("notification_token")
        api_version = self.param("api_version")

        if notification is None or api_version is None:
            return self.error("Dont")

        config = Config().khipu()
        api = Khipu(config['receiver'], config['secret'])

        data = {'notification_token': notification}

        try:
            data = api.service('GetPaymentByToken', **data)
        except:
            return self.error("Can't get payment data")

        if 'status' in data and 'transaction_id' in data and data['status'] == "done":
            uuid = data['transaction_id'].strip()

            if uuid is None or uuid == "":
                uuid = data['custom'].strip()
            if uuid is None or uuid == "":
                return self.error("dont")

            # Set order paid
            self.__payment_accepted(uuid)

            return self.success(self._("khipu_accepted"))

    @logged()
    def generate_payment_webpay(self):
        order_id = self.param("order_id")

        order = OrdersUser.objects(id=order_id, user_id=self.logged['user']).first()

        if order is None:
            return self.error(self._("invalid_order_id"))

        subsidiary_id = order.subsidiary.id
        try:
            subsidiary = self.subsidiary.find(id=subsidiary_id)
        except NoneResult:
            return self.error(self._("subsidiary_dont_exist"))

        if not order.subsidiary.commerce.alliance:
            return self.error("Subsidiary not alliance")

        if order.subsidiary.commerce.alliance:
            if subsidiary_is_closed_hour_hand(subsidiary.open_hours):
                return self.error(self._("subsidiary_inactive"))

        confirmation_date = arrow.get(order.order_confirmation).to('utc').replace(minutes=+45)

        if arrow.now('utc') > confirmation_date:
            self.logger.error("OrderID: {0} Expired".format(order.id))
            order.status = "EXPIRED"
            try:
                order.update()
            except Exception as e:
                self.logger.error("Cant set order as expired")
                self.logger.error(e.message)

            return self.error(self._("order_expired"))

        status = order.status.upper()
        if status == "ORDER-ACCEPTED":
            c = Config().webpay()
            w = Webpay(path=c['path'],
                       commerce_code=c['commerce_code'])

            data = {
                'order_id': order.correlative,
                'total': order.total,
                'session_id': str(order.id),
                'return_url': self.base_url + '/webpay',
                'final_url': self.base_url + '/success'
            }

            try:
                transaction = w.init_transaction(**data)
            except Exception as e:
                self.logger.error(e.message)
                self.logger.error('Cant communicate tbk error: ' + str(e))
                return self.error(self._("cant_communicate_tbk"))

            if "error" in transaction:
                if isinstance(transaction, set):
                    return self.error(list(transaction)[0])
                else:
                    return self.error(transaction["error"])

            payment_data = {
                'id': transaction['token'],
                'type': 'Webpay',
                'creation': arrow.now('America/Santiago').datetime,
                'payment_url': transaction['url'],
                'ntoken': transaction['token']
            }
            webpay_data = {
                "token_ws": transaction['token'],
                "order_id": order.id,
                "correlative": order.correlative,
                "products": order.products,
                "dispatch_amount": order.dispatch_amount,
                "subtotal": order.subtotal,
                "total": order.total,
                "subtotal_with_discount": order.subtotal_with_discount,
                "subtotal_without_discounts": order.subtotal_without_discounts
            }

            try:
                self.logger.warning(webpay_data)
                webpay_order = WebpayOrder(**webpay_data)
                webpay_order.save()

                self.order.update_payment(uuid=order_id, user=self.logged['user'], data=payment_data)
            except SaveError, e:
                return self.error(e.value)

            return self.success({
                'url': transaction["url"],
                'token': transaction["token"]
            })
        elif status == "ORDER-PAID":
            return self.error(self._("already_paid"))
        elif status == "ORDER-IN-DISPATCH":
            return self.error(self._("paid_and_shipped"))
        elif status == "ORDER-DELIVERED":
            return self.error(self._("paid_and_delivered"))
        elif status == "ORDER-FINISH":
            return self.error(self._("finished"))
        elif status == "SEND-CLIENT":
            return self.error(self._("wait_restaurant"))
        else:
            return self.error(self._("order_error"))

    @logged()
    def generate_payment_khipu(self):
        config = Config().khipu()
        return_url = self.param("return_url")
        error_url = self.param("error_url")
        order_id = self.param("order_id")

        if order_id is None or error_url is None or return_url is None:
            return self.error(self._("every_param"))

        order = OrdersUser.objects(id=order_id, user_id=self.logged['user']).first()
        order2 = Order.objects(id=order_id).first()

        if order is None:
            return self.error(self._("invalid_order_id"))

        status = order.status.upper()

        if status == "ORDER-ACCEPTED":
            if order.payment is not None:
                time = order.payment.creation
                date = time + datetime.timedelta(minutes=5)

                now = arrow.now('America/Santiago').datetime

                if date > now:
                    return self.success({
                        'app_url': order.payment.app_url,
                        'payment_url': order.payment.payment_url,
                        'simplified_transfer_url': order.payment.simplified_transfer_url,
                        'transfer_url': order.payment.transfer_url
                    })

            api = Khipu(config['receiver'], config['secret'])

            date = arrow.now('America/Santiago').replace(minutes=+5).datetime

            subject = "DXPRESS {0}".format(str(order.id))
            if order.subsidiary.fullname is not None:
                subject = "DXPRESS - {0}".format(order.subsidiary.fullname)

            data = {
                'currency': 'CLP',
                'amount': str(order.total),
                'subject': subject,
                'custom': str(order.id),
                'transaction_id': str(order.id),
                'return_url': return_url,
                'cancel_url': error_url,
                'expires_date': '%sZ' % (date.isoformat()[0:-7],),
            }

            mail = order.user.email
            name = order.user.name

            if mail is not None and name is not None:
                mail = mail.strip()
                name = name.strip()

                if mail != '' and name != '':
                    data.update({
                        'send_email': 'true',
                        'payer_email': mail,
                        'payer_name': name
                    })

            try:
                data = api.service('CreatePayment', **data)
            except Exception as e:
                self.logger.error(e.message)
                return self.error(self._("cant_communicate_khipu"))

            payment_data = {
                'id': data['payment_id'],
                'type': 'Khipu',
                'creation': arrow.now('America/Santiago').datetime,
                'app_url': data['app_url'],
                'payment_url': data['payment_url'],
                'simplified_transfer_url': data['simplified_transfer_url'],
                'transfer_url': data['transfer_url'],
                'token': ''
            }
            payment = Payment(**payment_data)

            try:
                order.update(payment=payment)
                order2.update(payment=payment)
            except Exception as e:
                self.logger.error(e.message)
                return self.error(self._("cant_create_payment"))

            return self.success({
                'app_url': data['app_url'],
                'payment_url': data['payment_url'],
                'simplified_transfer_url': data['simplified_transfer_url'],
                'transfer_url': data['transfer_url']
            })
        elif status == "ORDER-PAID":
            return self.error(self._("already_paid"))
        elif status == "ORDER-IN-DISPATCH":
            return self.error(self._("paid_and_shipped"))
        elif status == "ORDER-DELIVERED":
            return self.error(self._("paid_and_delivered"))
        elif status == "ORDER-FINISH":
            return self.error(self._("finished"))
        elif status == "SEND-CLIENT":
            return self.error(self._("wait_restaurant"))
        else:
            return self.error(self._("order_error"))

    def __payment_accepted(self, uuid):

        order = Order.objects(id=uuid).first()

        if order is None:
            return self.error("Order is None")

        order_user = OrdersUser.objects(id=uuid, user_id=order.user.id).first()

        status = 'ORDER-PAID'
        now = arrow.now('America/Santiago').to('utc').datetime

        # set datetime payment
        order.order_paid = now
        order_user.order_paid = now

        # change status
        order.status = status
        order_user.status = status

        try:
            order.update()
            order_user.update()
        except Exception as e:
            self.logger.warning("Can't update order or cant create client_order: " + e.message)
            raise SaveError(self._("cant_update_order"))

        if order.subsidiary.commerce.alliance is True:

            order_json = json.dumps(order, cls=JsonCustomEncoder)
            subsidiary_redis = Redis.Subsidiary.query.filter(id=order.subsidiary.id).first()

            summary_data = {
                'commerce_id': order.subsidiary.commerce_id,
                'subsidiary_id': order.subsidiary.id,
                'date': arrow.now('America/Santiago').to('utc').datetime,
                'products': order.products,
                'payment': order.payment,
                'total': order.total,
                'subtotal': order.subtotal,
                'subtotal_with_discount': order.subtotal_with_discount,
                'discounts': order.discounts
            }

            cs = CommerceSummary(**summary_data)

            client_order = ClientOrders(payment=order.payment,
                                        id=order.id,
                                        correlative=order.correlative,
                                        order_date=order.order_date,
                                        discounts=order.discounts,
                                        order_paid=arrow.now('America/Santiago').to('utc').datetime,
                                        order_confirmation=order.order_confirmation,
                                        subsidiary_id=order.subsidiary.id,
                                        products=order.products,
                                        pickup=order.pickup,
                                        status="ORDER-PAID",
                                        subtotal=order.subtotal,
                                        subtotal_with_discount=order.subtotal_with_discount,
                                        subtotal_without_discounts=order.subtotal_without_discounts,
                                        user=order.user,
                                        address_order=order.address_order,
                                        franchise=order.subsidiary.commerce.franchise)
            try:
                client_order.save()
                cs.save()
            except Exception as e:
                self.logger.warning("Can't update order or cant create client_order: " + e.message)
                raise SaveError(self._("cant_update_order"))

            if not self.rabbit.publish(queue='subsidiary_' + str(order.subsidiary.id) + '_paid',
                                       msg=order_json):
                self.logger.error("We couldn't send the payment to the restaurant")
                raise Exception("We couldn't send the payment to the restaurant")

            data_aps = {
                "title": "Dxpress",
                "message": "Order Paid",
                "status": 2,
                "location": order_json
            }

            notification = {
                "title": "Dxpress",
                "body": "Order Received",
                "sound": "default",
                "icon": "myicon",
                "content_available": True
            }

            data_aps = json.dumps(data_aps, cls=JsonCustomEncoder)

            subsidiary_redis.send_gcm(type=3, msg=data_aps, notification=notification)

        else:

            try:
                subsidiary = self.subsidiary.find(id=order.subsidiary.id)
            except NoneResult:
                subsidiary = None

            if subsidiary is not None:
                geo = subsidiary.to_geo()
                key = "dispatcher-not-found"

                data = {
                    'type': key,
                    'message': {
                        'order_id': str(order.id),
                        'subsidiary': {
                            'id': subsidiary.id,
                            'lat': geo.lat,
                            'lng': geo.lon
                        }
                    }
                }
                self.redis().publish("channel-dispatch-found", json.dumps(data, cls=JsonCustomEncoder))

        # SEnd user notification
        data = {
            "user_id": order.user.id,
            "type": "ORDER_PAID",
            "message": {
                "order_id": str(order.id),
                "msg": self._("order_paid"),
            }
        }
        self.redis().publish('channel-users', json.dumps(data, cls=JsonCustomEncoder))

        # Send GCM notification
        user_order = Redis.User.query.filter(id=order.user.id).first()

        if user_order is None:
            user_order = self.user.create_user_in_redis(order.user.id)

        if user_order is not None and user_order.token_devices:
            data_aps = {
                "title": "Dxpress",
                "body": self._("processed_and_verified"),
                "sound": "default",
                "icon": "default",
            }

            data = {
                "title": "Dxpress",
                "message": self._("processed_and_verified"),
                "status": 9,
                "location": str(order.id)
            }

            data = json.dumps(data)

            user_order.send_gcm(type=1, msg=data, notification=data_aps)

        return True

    def send_paid_email(self, email, data):
        html = self.render_template('mail/order.html', **data)

        self.mailer.send_message(
            'dxpress@{domain}'.format(domain=self.base_url.replace("https://", "")),
            [email],
            subject=u'Revisa el detalle de tu pedido en Dxpress',
            html=html,
            tags=['order_paid']
        )

    def send_canceled_mail(self, email, data):
        html = self.render_template('mail/order.html', **data)

        self.mailer.send_message(
            'dxpress@{domain}'.format(domain=self.base_url.replace("https://", "")),
            [email],
            subject=u'Tu pedido fue rechazado y tu pago reversado',
            html=html,
            tags=['order_canceled']
        )

    @logged()
    def send_code_payment_cash(self):

        # Capture params
        phone = self.param('phone')
        order_id = self.param('order_id')

        # Valid params
        if order_id is None or phone is None:
            return self.error(self._("missing_params"))

        # Search Order
        order = OrdersUser.objects(id=order_id, user_id=self.logged['user']).first()
        if order is None:
            return self.error(self._('order_invalid'))

        # TODO: Change this to config in DB
        if order.total > 30000:
            return self.error(self._('amount_not_permit'))

        black_list_phone = self.blacklistphone.find(telephone=phone)
        self.logger.error(black_list_phone)
        if black_list_phone is None:
            block = False
        else:
            block = True

        # Check Block
        if block:
            return self.error('El numero {0} se encuentra bloqueado para el sistema de pago en efectivo,'
                              ' si quiere mas informacion contactarse con info@dxpress.cl'.format(phone))

        # Generate Code
        code = Security.generate_random(5, 'digit')

        payment_data = {
            'id': code,
            'type': 'Cash',
            'creation': arrow.now('America/Santiago').datetime,
            'payment_url': None,
            'ntoken': None
        }

        try:
            self.order.update_payment(uuid=order_id, user=self.logged['user'], data=payment_data)
        except SaveError, e:
            return self.error(e.value)

        # Send message with code
        message = 'Tu codigo para el pago en efectivo es {0}'.format(code)
        self.send_message(phone, message)
        return self.success(self._('send_code_success'))

    @logged()
    def validate_code_payment_cash(self):
        # Capture params
        code = self.param('code')
        order_id = self.param('order_id')

        # Valid params
        if order_id is None or code is None:
            return self.error(self._("missing_params"))

        # Search Order
        order = Order.objects(id=order_id).first()
        order_user = OrdersUser.objects(id=order_id, user_id=self.logged['user']).first()
        if order_user is None or order is None:
            return self.error(self._('order_invalid'))

        # Validate code
        if order.payment.id != int(code):
            return self.error(self._('payment_cash_code_invalid'))

        # Process Pay order
        status = 'ORDER-PAID'
        now = arrow.now('America/Santiago').to('utc').datetime

        # set datetime payment
        order.order_paid = now
        order_user.order_paid = now

        # change status
        order.status = status
        order_user.status = status

        try:
            order.update()
            order_user.update()
        except Exception as e:
            self.logger.warning("Can't update order or cant create client_order: " + str(e))
            raise SaveError(self._("cant_update_order"))

        order_json = json.dumps(order, cls=JsonCustomEncoder)

        summary_data = {
            'commerce_id': order.subsidiary.commerce_id,
            'subsidiary_id': order.subsidiary.id,
            'date': arrow.now('America/Santiago').to('utc').datetime,
            'products': order.products,
            'payment': order.payment,
            'total': order.total,
            'subtotal': order.subtotal,
            'subtotal_with_discount': order.subtotal_with_discount,
            'discounts': order.discounts
        }

        cs = CommerceSummary(**summary_data)

        client_order = ClientOrders(payment=order.payment,
                                    id=order.id,
                                    correlative=order.correlative,
                                    order_date=order.order_date,
                                    discounts=order.discounts,
                                    order_paid=arrow.now('America/Santiago').to('utc').datetime,
                                    order_confirmation=order.order_confirmation,
                                    subsidiary_id=order.subsidiary.id,
                                    products=order.products,
                                    pickup=order.pickup,
                                    status="ORDER-PAID",
                                    subtotal=order.subtotal,
                                    subtotal_with_discount=order.subtotal_with_discount,
                                    subtotal_without_discounts=order.subtotal_without_discounts,
                                    user=order.user,
                                    address_order=order.address_order,
                                    franchise=order.subsidiary.commerce.franchise)
        try:
            client_order.save()
            cs.save()
        except Exception as e:
            self.logger.warning("Can't update order or cant create client_order: " + str(e))
            raise SaveError(self._("cant_update_order"))

        if not self.rabbit.publish(queue='subsidiary_' + str(order.subsidiary.id) + '_paid',
                                   msg=order_json):
            self.logger.error("We couldn't send the payment to the restaurant")
            raise Exception("We couldn't send the payment to the restaurant")

        return self.success()
