# -*- coding: utf-8 -*-
import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators.Ajax import ajax
from Classes.Decorators.Logged import logged
from Models.Country import Country


class CountryHandler(APIHandler):
    def start(self):
        self.repository(Country)
        self.logger = logging.getLogger(__name__)

    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of countries or details of a specific country
        :return: JSON response
        """
        self.logger.info("Request to Country API")

        # Get country ID from Route
        country_id = self.route("id")

        try:
            # If there isn't country id we are going to return all the countries
            if country_id is None:
                response = self.country.all()
            else:
                # Request a specific country
                response = self.country.find(id=country_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.country.to_dict(response))

    @ajax
    @logged('system')
    def post(self):
        country = self.model_process(Country())

        if country is None:
            return self.error()

        return self.result(self.country.save(country))

    @logged('system')
    def put(self):
        country_id = self.route("id")

        try:
            country = self.country.find(id=country_id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(country, True)

        if model is None:
            return self.error()

        return self.result(self.country.save(model))
