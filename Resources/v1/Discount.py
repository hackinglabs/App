# -*- coding: utf-8 -*-
import logging

from wheezy.core.descriptors import attribute
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Models.Discount import Discount
from Classes.Decorators.Ajax import ajax


class DiscountHandler(APIHandler):
    def start(self):
        self.repository([
            Discount
        ])
        self.logger = logging.getLogger(__name__)

    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of discounts or a specific dsicount
        :return: JSON response
        """


        # Get Discoount ID from Route
        discount_id = self.route("id")

        try:
            # If there isn't presentation id we are going to return all the presentations
            if discount_id is None:
                discounts = self.discount.all()
                return self.success(self.to_dict(discounts))
            else:

                # Request a specific discount
                discount = self.discount.find(id=discount_id)
                return self.success(discount.to_dict())

        except NoneResult, e:
            return self.error(e.value)

    @logged('system')
    def post(self):

        discount = Discount()

        if 'type_id' in self.request.form:
            discount.type_id = self.request.form['type_id']
            del self.request.form['type_id']

        discount = self.model_process(discount)

        if discount is None:
            return self.error()


        if self.discount.save(discount):
            return self.success(dict(id=discount.id))
        return self.error()

    @ajax
    @logged('system')
    def put(self):
        discount_id = self.route('id')

        try:
            discount = self.discount.find(id=discount_id)
        except NoneResult, e:
            return self.error(e.value)

        if 'type_id' in self.request.form:
            discount.type_id = self.request.form['type_id']
            del self.request.form['type_id']

        model = self.model_process(discount, True)

        if model is None:
            return self.error()

        return self.result(self.discount.save(model))


    @ajax
    @logged('system')
    def delete(self):
        discount_id = self.route('id')

        try:
            response = self.discount.delete(id=discount_id)
            return self.success(response)
        except (NoneResult, Exception) as e:
            self.logger.error(str(e))
            return self.error(str(e))

