import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models.TempUser import TempUser


class TempUserHandler(APIHandler):
    def start(self):
        self.logger = logging.getLogger(__name__)
        self.repository(TempUser)

    #@logged("login_")
    # @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of temp_users or details of a specific temp_user
        :return: JSON response
        """

        self.logger.info("Request to TempUser API")

        # Get temp_user ID from Route
        temp_user_id = self.route("id")

        try:
            if temp_user_id is None:
                response = self.tempuser.all()
            else:
                # Request a specific temp_user
                response = self.tempuser.find(id=temp_user_id)
        except NoneResult, e:
            return self.error(e.value)

        rel = {
            "tempuser": ["user"]
        }

        return self.success(self.tempuser.to_dict(response, rel=rel, depth=1))



    # Create permision add dispatcher in tempuser
    @logged()
    #@ajax
    def post(self):

        self.request.form['user_id'] = self.logged['user']
        temp_user = self.model_process(TempUser())

        if temp_user is None:
            return self.error()

        return self.result(self.tempuser.save(temp_user))

    @logged()
    @ajax
    def put(self):
        pass

    @logged()
    @ajax
    def delete(self):
        pass
