import logging

from geoalchemy2.shape import to_shape

from Classes.Core.APIHandler import APIHandler
from geoalchemy2 import WKTElement

from Classes.Core.Exceptions import NoneResult
from Models import RedZone


class RedZoneHandler(APIHandler):
    def start(self):
        self.logger = logging.getLogger(__name__)
        self.repository(RedZone)

    def get(self):
        redzone_id = self.route("id")

        try:
            if redzone_id is None:
                response = self.redzone.all()
            else:
                response = self.redzone.find(id=redzone_id)
                return self.success(response.to_dict())
        except NoneResult, e:
            return self.error(e.value)

        response = [r.to_dict() for r in response]
        return self.success(response)

    def delete(self):
        zone_id = self.param("id")
        return self.result(self.redzone.delete(zone_id))

    def post(self):
        name = self.param("name")
        first_latitude = self.param("lat")
        second_latitude = self.param("lat2")
        first_longitude = self.param("lng")
        second_longitude = self.param("lng2")

        if first_longitude is None or \
                second_longitude is None or \
                first_latitude is None or \
                second_latitude is None:
            return self.error("Missing fields")

        first_point = "{lng} {lat}".format(lng=first_longitude, lat=first_latitude)
        second_point = "{lng} {lat}".format(lng=second_longitude, lat=first_latitude)
        third_point = "{lng} {lat}".format(lng=second_longitude, lat=second_latitude)
        fourth_point = "{lng} {lat}".format(lng=first_longitude, lat=second_latitude)

        self.logger.info("FIRST POINT : Longitude %s - Latitude %s", first_longitude, first_latitude)
        self.logger.info("SECOND POINT: Longitude %s - Latitude %s", second_longitude, first_latitude)
        self.logger.info("THIRD POINT : Longitude %s - Latitude %s", second_longitude, second_latitude)
        self.logger.info("FOURTH POINT: Longitude %s - Latitude %s", first_longitude, second_latitude)

        wkt = WKTElement("SRID=4326; POLYGON(({first},{second},{third},{fourth},{first}))"
                         .format(first=first_point,
                                 second=second_point,
                                 third=third_point,
                                 fourth=fourth_point))

        return self.result(self.redzone.create_zone(name, wkt))
