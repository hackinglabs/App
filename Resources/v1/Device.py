# -*- coding: utf-8 -*-
import logging
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators.Logged import logged
from Models.Device import Device


class DeviceHandler(APIHandler):
    def start(self):
        self.repository(Device)
        self.logger = logging.getLogger(__name__)

    def get(self):
        """
        GET HTTP Method

        Return a list of devices or details of a specific device
        :return: JSON response
        """

        self.logger.info("Request to  Device")

        # Get device ID from Route
        device_id = self.route("id")

        try:
            # If there isn't subsidiary id we are going to return all the subsidiaries
            if device_id is None:
                response = self.device.all()
            else:

                self.logger.info("Request specific device %s", device_id)
                # Request a specific subsidiary
                response = self.device.find(id=device_id)
        except NoneResult, e:
            return self.error(e.value)

        return self.success(self.device.to_dict(response))

    # @logged(role=['administrador'])
    def post(self):

        self.logger.info("Request POST create device")

        device = self.model_process(Device())

        if device is None:
            return self.error()

        # return response JSON
        return self.result(self.device.save(device))

    # @logged(role=['administrador'])
    def put(self):
        # Get device ID from Route
        device_id = self.route("id")

        self.logger.info("Request specific device %s", device_id)

        try:
            # Select specific device
            device = self.device.find(id=device_id)
        except NoneResult, e:
            return self.error(e.value)

        device = self.model_process(device, True)

        if device is None:
            return self.error()

        return self.result(self.device.save(device))
