# -*- coding: utf-8 -*-
import logging
import arrow
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Models.Cassandra.DispatcherLogger import DispatcherLogger


class LoggerHandler(APIHandler):
    def start(self):
        self.log = logging.getLogger(__name__)
        self.repository("Logger")

    @logged('system')
    def get(self):
        pass

    def post(self):
        extra = self.route('extra')

        if extra == "dispatcher":
            user_id = self.param("user_id")
            tag = self.param("tag")
            message = self.param("message")

            if user_id is None or tag is None or message is None:
                return self.error("Parameters missing")

            try:
                dp = DispatcherLogger(user_id=user_id,
                                      tag=tag,
                                      message=message,
                                      date=arrow.now('America/Santiago').to('utc').datetime)
                dp.save()
            except Exception as e:
                self.log.error("Error in save DispatcherLogger: " + e.message)
                return self.error("Dont save DispatcherLogger")

            return self.success()
