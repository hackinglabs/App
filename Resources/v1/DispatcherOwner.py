import logging

from wheezy.core.descriptors import attribute

from Classes import ConnectCassandra
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Models import Vehicle, DriverVehicle
from Models.Cassandra import DispatcherSummary
from Models.User import User
from Models import Redis


class DispatcherOwnerHandler(APIHandler):
    def start(self):
        self.repository([
            Vehicle,
            DriverVehicle,
            User
        ])
        self.logger = logging.getLogger(__name__)
        ConnectCassandra()

    @attribute
    def translation(self):
        return self.translations['owner']

    @logged()
    def get(self):
        owner_id = self.logged['user']
        try:
            vehicles = self.vehicle.all_vehicle_for_owner(owner_id)
        except NoneResult:
            return self.error(self._("owner_has_not_vehicles"))

        rel = {
            "vehicle": ["driver_vehicles"],
            "drivervehicle": ["user"]
        }

        to_json = self.vehicle.to_dict(vehicles, depth=3, rel=rel)

        for vehicle in to_json:
            # return last driver
            vehicle['driver_vehicles'] = vehicle['driver_vehicles'][-1]
            # validate vehicle is active in dispatcher
            user_driver = vehicle['driver_vehicles']['user']['id']
            dispatcher = Redis.Dispatcher.query.filter(id=user_driver).first()
            dispatcher_summary = DispatcherSummary(dispatcher_id=user_driver, vehicle_id=vehicle['driver_vehicles']['vehicle_id']).all()
            count_summary = dispatcher_summary.count()
            vehicle['count_summary'] = count_summary
            if dispatcher is not None:
                vehicle['active'] = True
            else:
                vehicle['active'] = False

        # validate dispatcher connect and statics
        return self.success(to_json)
