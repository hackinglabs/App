# -*- coding: utf-8 -*-
import logging

import arrow

from Classes.Core.APIHandler import APIHandler
from Classes.Decorators.Ajax import ajax
from Models.CouponUser import CouponUser


class CouponHandler(APIHandler):
    def start(self):
        self.repository(CouponUser)
        self.logger = logging.getLogger(__name__)

    @ajax
    def get(self):
        start = self.param("start")
        end = self.param("end")

        if end is None:
            end = start

        if start is None:
            return self.error("You have to provide a start date.")

        try:
            start = arrow.get(start).date()
            end = arrow.get(end).date()
        except:
            return self.error("The date(s) are mal-formatted.")

        all = self.couponuser.get_coupons_used(start, end)

        return self.success(all)