import logging

from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models.CommerceTag import CommerceTag


class CommerceTagHandler(APIHandler):
    def start(self):
        self.repository(CommerceTag)
        self.repository("Dispatcher", redis=self.redis())
        self.logger = logging.getLogger(__name__)

    @ajax
    def get(self):
        """
        GET HTTP Method

        Return a list of commerce_tags or details of a specific commerce_tag
        :return: JSON response
        """

        self.dispatcher.find(-33.409259, -70.571896)
        # Get country ID from Route
        id_ctag = self.route("id")

        try:
            if id_ctag is None:
                response = self.commercetag.all()
            else:
                response = self.commercetag.find(id=id_ctag)
        except NoneResult, e:
            return self.error(e.value)

        rel = {
            'commercetag': ['commerce', 'tag']
        }
        return self.success(self.commercetag.to_dict(response, depth=2, rel=rel))


    @logged('system')
    def post(self):
        commerce_tag = self.model_process(CommerceTag())

        if commerce_tag is None:
            return self.error()

        return self.result(self.commercetag.save(commerce_tag))


    @logged('system')
    def put(self):
        commerce_tag_id = self.route("id")

        try:
            commerce_tag = self.commercetag.find(id=commerce_tag_id)
        except NoneResult, e:
            return self.error(e.value)

        model = self.model_process(commerce_tag, True)

        if model is None:
            return self.error()

        return self.result(self.commercetag.save(model))

