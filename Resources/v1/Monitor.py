import json
import logging

import arrow
from wheezy.core.descriptors import attribute
from sqlalchemy import func
from Classes import ConnectCassandra
from Classes.Core import GcmNotification
from Classes.Core import JsonCustomEncoder
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Decorators import logged
from Models import DispatchAddress
from Models import User
from Models.Cassandra.ClientOrders import ClientOrders
from Models.Cassandra.DispatcherDisconnect import DispatcherDisconnect
from Models.Cassandra.DispatcherAlert import DispatcherAlert
from Models.Cassandra import Order
from Models.Cassandra.DispatcherReceive import DispatcherReceive
from Models.Cassandra.SubsidiaryConnection import SubsidiaryConnection
from Models.Subsidiary import Subsidiary
from Models.DeviceMobile import DeviceMobile
from Models import Redis


class MonitorHandler(APIHandler):
    def start(self):
        self.repository(User)
        self.repository(DispatchAddress)
        self.repository(Subsidiary)
        self.repository(DeviceMobile)
        self.logger = logging.getLogger(__name__)
        self.cassandra = ConnectCassandra()

    @attribute
    def translation(self):
        return self.translations['monitor']

    @logged('system')
    def get(self):
        id = self.param("id")
        uuid = self.param("uuid")

        extra = self.route('extra')
        extra_id = self.route("extra_id")

        if extra == 'orders':
            other_day = self.param("date")

            if other_day is None:
                date = arrow.now('America/Santiago')
                start = date.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
                end = date.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")
            else:
                date = arrow.get(other_day).replace(tzinfo='America/Santiago')
                start = date.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
                end = date.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")

            orders = Order.filter(order_date__gte=start.datetime)
            orders = orders.filter(order_date__lte=end.datetime)
            orders = orders.allow_filtering().all()

            response = []

            for order in orders:
                order_dict = {
                    "order_id": order.id,
                    'subsidiary': order.subsidiary,
                    "order_status": self.__status_to_code(order.status),
                    "order_paid": order.order_paid,
                    "order_date": order.order_date,
                    "order_ready": order.order_ready,
                    "order_preparation": order.order_preparation,
                    "delivery_date": order.delivery_date,
                    "subsidiary_id": order.subsidiary.id,
                    "correlative": order.correlative,
                    "pickup": order.pickup,
                    "dispatcher_id": None,
                    "dispatcher_receive": None
                }
                response.append(order_dict)

            return self.success(response, custom=True)
        elif extra == 'orders_status':

            date = arrow.now('America/Santiago')

            start = date.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
            end = date.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")
            orders = Order.filter(order_date__gte=start.datetime)
            orders = orders.filter(order_date__lte=end.datetime)
            orders = orders.allow_filtering().all()

            count_for_status = {
                "SEND-CLIENT": orders.filter(status="SEND-CLIENT").count(),
                "ORDER-ACCEPTED": orders.filter(status="ORDER-ACCEPTED").count(),
                "ORDER-REJECTED": orders.filter(status="ORDER-REJECTED").count(),
                "ORDER-PAID": orders.filter(status="ORDER-PAID").count(),
                "ORDER-READY": orders.filter(status="ORDER-READY").count(),
                "SEND-DISPATCHER": orders.filter(status="SEND-DISPATCHER").count(),
                "ORDER-IN-DISPATCH": orders.filter(status="ORDER-IN-DISPATCH").count(),
                "ORDER-TRACK": orders.filter(status="ORDER-TRACK").count()
            }

            return self.success(count_for_status, custom=True)
        elif extra == 'subsidiary_connects':
            subsidiaries = self.subsidiary.all_active()

            if subsidiaries is not None:
                return self.success(self.to_dict(subsidiaries))
            else:
                self.success("Not have subsidiaries connect")
        elif extra == 'subsidiary_connections':
            subsidiary_id = self.param('subsidiary_id')
            if subsidiary_id is None:
                return self.error(self._("missing_subsidiary_id"))

            # set date
            date = arrow.now('America/Santiago')

            start = date.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
            end = date.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")

            connections_history = SubsidiaryConnection.objects.filter(subsidiary_id=subsidiary_id)
            connections_history = connections_history.filter(date__gte=start.datetime)
            connections_history = connections_history.filter(date__lte=end.datetime)
            connections_history = connections_history.allow_filtering().all()

            response = []

            for connection in connections_history:
                connection_dict = {
                    "subsidiary_id": connection.subsidiary_id,
                    "date": connection.date,
                    "connected": connection.connected
                }
                response.append(connection_dict)

            return self.success(response, custom=True)

        elif extra == 'dispatcher_receive':
            date = arrow.now('America/Santiago')
            start = date.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
            end = date.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")

            dispatcher_receive = DispatcherReceive.filter(date__gte=start.datetime)
            dispatcher_receive = dispatcher_receive.filter(date__lte=end.datetime)
            dispatcher_receive = dispatcher_receive.allow_filtering().all()

            response = []

            for d in dispatcher_receive:
                d_dict = {
                    "id": d.id,
                    "order_id": d.order_id,
                    "date": d.date,
                    "dispatcher_id": d.dispatcher_id
                }
                response.append(d_dict)

            return self.success(response, custom=True)
        elif extra == "alerts":
            date = arrow.now('America/Santiago')
            start = date.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
            end = date.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")

            """
            #Search all dispatcher_disconnect today
            dispatcher_disconnects = DispatcherDisconnect.filter(date__gte=start.datetime)
            dispatcher_disconnects = dispatcher_disconnects.filter(date__lte=end.datetime)
            dispatcher_disconnects = dispatcher_disconnects.allow_filtering().all()

            #Search all dispatcher_alert today
            dispatcher_alerts = DispatcherAlert.filter(date__gte=start.datetime)
            dispatcher_alerts = dispatcher_alerts.filter(date__lte=end.datetime)
            dispatcher_alerts = dispatcher_alerts.allow_filtering().all()
            """

            dispatcher_alerts = DispatcherAlert.objects.all()
            dispatcher_disconnects = DispatcherDisconnect.objects.all()

            list_alerts = []

            for dd in dispatcher_disconnects:
                dd_dict = {
                    "dispatcher_id": dd.dispatcher_id,
                    "order_id": dd.order_id,
                    "date": dd.date,
                    "vehicle_id": dd.vehicle_id,
                    "geo": "",
                    "type": 1

                }
                list_alerts.append(dd_dict)

            for da in dispatcher_alerts:
                da_dict = {
                    "dispatcher_id": da.dispatcher_id,
                    "order_id": da.order_id,
                    "vehicle_id": "",
                    "date": da.date,
                    "geo": da.geo,
                    "type": 2
                }

                list_alerts.append(da_dict)
            return self.success(list_alerts, custom=True)
        elif extra == 'coordinates':

            # Search All Address register
            addresses = self.dispatchaddress.all()

            # Init var container info
            coordinates_addresses = []

            for address in addresses:
                address_geo = address.to_geo()
                coordinates_addresses.append([address_geo.lon, address_geo.lat])

            return self.success(coordinates_addresses)

        elif extra == 'indicators':
            # Get users register
            users = self._con.query(func.count(User.id)).scalar()

            # Get subsidiary connected
            subsidiaries = self._con.query(func.count(Subsidiary.id)).filter(Subsidiary.is_active == True).scalar()

            # Get dispatcher connected
            dispatchers = Redis.Dispatcher.query.all()

            dispatcher_count = [x.id for x in dispatchers]

            date = arrow.now('America/Santiago')

            # Get today
            start = date.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
            end = date.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")

            # get order today
            orders_count = self.cassandra.execute(
                "SELECT COUNT(*) "
                "FROM dxpress.orders "
                "WHERE order_date >= %s "
                "AND order_date <= %s "
                "AND status = 'ORDER-FINISH' "
                "ALLOW FILTERING",
                (start.datetime, end.datetime))[0]

            # get gain today
            today_gain = self.cassandra.execute(
                "SELECT SUM(subtotal) "
                "FROM dxpress.orders "
                "WHERE order_date >= %s "
                "AND order_date <= %s "
                "AND status = 'ORDER-FINISH' "
                "ALLOW FILTERING",
                (start.datetime, end.datetime))[0]
            today_gain = today_gain['system.sum(subtotal)']

            # get gain month
            start = date.replace(day=1, hour=0, minute=0, second=0, microsecond=0).to("utc")
            end = date.replace(day=1, months=+1, days=-1, hour=23, minute=59, second=59, microsecond=999999).to("utc")

            month_gain = self.cassandra.execute(
                "SELECT SUM(subtotal) "
                "FROM dxpress.orders "
                "WHERE order_date >= %s "
                "AND order_date <= %s "
                "AND status = 'ORDER-FINISH' "
                "ALLOW FILTERING",
                (start.datetime, end.datetime))[0]

            month_gain = month_gain['system.sum(subtotal)']

            response = {
                "orders_finish": orders_count["count"],
                "users_register": users,
                "subsidiaries_connected": subsidiaries,
                "dispatcher_connected": len(dispatcher_count),
                "amount_today": today_gain,
                "amount_month": month_gain
            }
            return self.success(response)
        elif extra == "count_orders_user":
            counts = \
            self.cassandra.execute("SELECT COUNT(*) FROM dxpress.orders_by_user WHERE user_id = %d AND status = 'ORDER-FINISH'" % int(extra_id))[0]

            return self.success({
                "orders": counts
            })

        if id is None:
            return self.error(self._("missing_id"))

        try:
            user = self.user.find(id=id)
        except NoneResult:
            return self.error(self._("user_not_found"))

        order = None
        if uuid is not None:
            order = Order.objects(id=uuid).first()

        if order is not None:
            order = {
                'id': str(order.id),
                'total': str(order.total),
                'subsidiary': order.subsidiary.fullname,
                'destination': {
                    'lat': order.address_order.geo.y,
                    'lng': order.address_order.geo.x
                }
            }

        patent = None
        vehicle = None
        if user.driver_vehicles is not None:
            vehicle = user.driver_vehicles[-1].vehicle.trademark
            patent = user.driver_vehicles[-1].vehicle.patent

        user = {
            'nin': user.nin,
            'name': user.fullname,
            'telephone': user.telephone,
            'vehicle': vehicle,
            'patent': patent
        }

        data = {
            'order': order,
            'user': user
        }

        return self.success(data)

    @logged('system')
    def post(self):
        extra = self.route('extra')
        if extra == "send_gcm":
            to_all = True
            message = self.param("message")

            if message is None or message == "":
                return self.error("The message cant be blank")

            data_aps = {
                "title": "Dxpress",
                "body": message,
                "sound": "default",
                "icon": "myicon",
                "content_available": True
            }

            data = {
                "title": "Dxpress",
                "message": message,
                "status": 0,
                "location": ""
            }

            data = json.dumps(data, cls=JsonCustomEncoder)

            if to_all:
                tokens = self.devicemobile.all()

                tokens_send = []
                for token in tokens:
                    self.user.save_notification(user_id=token.user_id, data=data, order_id="")
                    try:
                        os = int(token.os)

                        if os == 2:
                            os = "ios"
                        else:
                            os = "android"
                    except ValueError:
                        pass

                    tokens_send.append({"token": token.token, "os": os})

                gcm = GcmNotification()
                gcm.add(tokens_send)
                gcm.msg(data)
                gcm.type(1)
                gcm.notification(data_aps)
                gcm.priority('high')
                self.logger.error(gcm.send())

                return self.success()

        elif extra == 'canceled':

            # Get orderId
            order_id = self.param('order_id')

            # Validate orderID
            if order_id is None:
                return self.error("OrderID none")

            # Get order
            order = Order.filter(id=order_id).first()
            if order is None:
                self.logger.error("order is None -- __monitor_order_canceled ")
                return self.error("Order is None")

            client_order = ClientOrders.objects(id=order_id, subsidiary_id=order.subsidiary.id).first()
            if client_order is None:
                self.logger.error("client_order is None -- __monitor_order_canceled ")
                return self.error("client_order is None !")

            # Check status order
            if order.status in ['ORDER-FINISH', 'DXL-ORDER-FINISH']:
                return self.error()

            # Validate order is DXL and change status corrs.
            if order.dxl:
                order.status = 'DXL-ORDER-REJECTED'
                client_order.status = 'DXL-ORDER-REJECTED'
            else:
                order.status = 'ORDER-REJECTED'
                client_order.status = 'ORDER-REJECTED'

            try:
                order.update()
                client_order.update()
            except Exception as e:
                self.logger.error("Error update order: " + e.message)
                return self.error(self._("order_not_updated"))

            return self.success()


        return self.success()

    def __status_to_code(self, status):
        list_status = {
            "SEND-CLIENT": 0,
            "ORDER-REJECTED": 1,
            "ORDER-ACCEPTED": 2,
            "ORDER-PAID": 3,
            "ORDER-PREPARATION": 4,
            "ORDER-READY": 5,
            "SEND-DISPATCHER": 6,
            "ORDER-IN-DISPATCH": 7,
            "ORDER-TRACK": 8,
            "ORDER-FINISH": 9,
            "EXPIRED": 10,
            "DXL-PREPARATION": 11,
            "DXL-READY": 12,
            'DXL-SEND-DISPATCHER': 13,
            'DXL-ORDER-IN-DISPATCH': 14,
            'DXL-DISPATCH': 15,
            'DXL-ORDER-FINISH': 16

        }

        return list_status.get(status, "nothing")
