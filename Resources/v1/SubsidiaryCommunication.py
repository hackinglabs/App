import json
import logging
import arrow
import requests
import math
from Classes.Core.APIHandler import APIHandler
from Classes.Core.Exceptions import NoneResult
from Classes.Core.JsonCustomEncoder import JsonCustomEncoder
from Models.Commerce import Commerce
from Models import Subsidiary
from Models.Cassandra.ClientOrders import ClientOrders
from Models.Cassandra.OrderNotAlliance import OrderNotAlliance
from Models.Cassandra import Order, OrdersUser
from Classes.db import ConnectCassandra
from Models.User import User
from Models.Cassandra.SubsidiaryConnection import SubsidiaryConnection
from Models.DispatchAddress import DispatchAddress
from Models.CouponUser import CouponUser
from Models import Cassandra
from Models import Redis


class SubsidiaryCommunicationHandler(APIHandler):
    def start(self):
        self.repository("Dispatcher", redis=self.redis())
        self.repository([Subsidiary, Commerce, DispatchAddress, User, CouponUser])
        self.cassandra = ConnectCassandra()
        self.logger = logging.getLogger(__name__)

    def post(self):
        # Parameter in route
        extra = self.route("extra")
        if extra == 'connect':
            return self.connect_redis()
        elif extra == 'disconnect':
            return self.disconnect_redis()
        elif extra == 'accept_or_rejected':
            return self.accept_or_rejected()
        elif extra == 'order_preparation':
            return self.order_preparation()
        elif extra == 'order_ready':
            return self.order_ready()
        elif extra == 'order_ready_na':
            return self.order_ready_not_alliance()
        elif extra == 'order_finish':
            return self.order_finish()

    def connect_redis(self):
        # change status in sql
        id = self.param('subsidiary_id')

        if id is None:
            return self.error('Missing id')

        subsidiary = self.subsidiary.find(id=id)
        commerce = self.commerce.find(id=subsidiary.commerce_id)

        # Set is_active True
        subsidiary.is_active = True

        commerce.alliance = True

        self.subsidiary.save(subsidiary)
        self.commerce.save(commerce)

        self.__create_in_redis(subsidiary)

        return self.success("Subsidiary Connect")

    def disconnect_redis(self):
        id = self.param('subsidiary_id')

        if id is None:
            return self.error('Missing id')

        subsidiary = self.subsidiary.find(id=id)

        # Set is_active False
        subsidiary.is_active = False
        subsidiary.commerce.alliance = False
        self.subsidiary.save(subsidiary)

        # save cassandra connection
        subsidiary_connect = SubsidiaryConnection(subsidiary_id=id,
                                                  date=arrow.now('America/Santiago').to('utc').datetime,
                                                  connected=False)
        try:
            subsidiary_connect.save()
        except Exception as e:
            self.logger.error("Error in save SubsidiaryConnection: " + e.message)

        return self.success("Subsidiary Disconnect")

    def accept_or_rejected(self):
        self.logger.info('Aceptar o rechazar')
        id = self.param("id")
        data = self.param("data")

        # capture message rejected
        reason = self.param("reason")

        if reason is None:
            reason = ""

        response = False

        # find order in cassandra
        order = Order.objects(id=id).first()

        # self.logger.warning(order)
        order_user = OrdersUser.objects(id=id, user_id=order.user.id).first()

        if order is None or order_user is None:
            self.logger.error("Order doesn't exist - 'accept_or_rejected' ")
            return self.error("Order: doesnt exists")

        if order.status.upper() in ['ORDER-ACCEPTED', 'ORDER-REJECTED']:
            return self.error('Orden ya confirmada !')

        if order.status.upper() != "SEND-CLIENT":
            # TODO: Banear Usuario
            return self.error("Order: invalid status")

        # set datetime order
        order.order_confirmation = arrow.now('America/Santiago').to('utc').datetime
        order_user.order_confirmation = arrow.now('America/Santiago').to('utc').datetime

        type_status = None
        if data.lower() == "accept":
            order.status = "ORDER-ACCEPTED"
            order_user.status = "ORDER-ACCEPTED"
            type_status = 2
            status = True
            msg = "Su orden ha sido aceptada, por favor proceda a realizar el pago!"
        elif data.lower() == "rejected":
            order.status = "ORDER-REJECTED"
            order_user.status = "ORDER-REJECTED"
            status = False
            msg = "Su orden ha sido rechazada."
            type_status = 3

            if order.coupon is not None \
                    and order.coupon.id is not None \
                    and order.user is not None \
                    and order.user.id is not None:
                self.couponuser.delete_used_coupon(order.user.id, order.coupon.id)
                coupon_user = Cassandra.CouponUser.objects(order_id=id).first()
                coupon_user.delete()

        # Update Order
        try:
            order.update()
            order_user.update()
        except Exception as e:
            return self.error('not update order in rejected: ' + e.message)

        data = {
            "type": "GENERAL",
            "message": {
                "status": status,
                "order_id": order.id,
                "msg": "Ok"
            },
            "subsidiary_id": order.subsidiary.id
        }
        self.redis().publish("channel-clients", json.dumps(data, cls=JsonCustomEncoder))

        data_aps = {
            "title": "Dxpress",
            "body": msg,
            "sound": "default",
            "icon": "myicon",
        }

        data = {
            "title": "Dxpress",
            "message": msg,
            "status": type_status,
            "location": {"order_id": str(order.id), "reason": reason}
        }

        ##Test Send gcm from UserRepository, give token database
        notification_id = self.user.save_notification(user_id=order.user.id, data=data, order_id=order.id)
        data["notification_id"] = notification_id
        data = json.dumps(data)
        res = self.user.send_gcm_repo(user_id=order.user.id, type=1, msg=data, notification=data_aps)

        if res is True:
            self.logger.error('SEND GCM')
        else:
            self.logger.error("NOT-SENDED")

        data = {
            "type": "ACCEPT_OR_REJECTED",
            "message": {
                "status": status,
                "order_id": order.id,
                "reason": reason,
                "msg": msg
            },
            "user_id": order.user.id
        }
        self.redis().publish("channel-users", json.dumps(data, cls=JsonCustomEncoder))

        return self.success("Notify for Device {0}".format(response))

    def order_preparation(self):
        id = self.param("order_id")

        if id is None:
            return self.error("OrderID is None")

        order = Order.objects(id=id).first()

        if order is None:
            self.logger.error("Order doesn't exist - 'order_preparation' ")
            return self.error("Order doesn't exist - 'order_preparation' ")

        status = order.status.lower()

        if status not in ['order-paid', 'order-in-dispatch']:
            return self.error("Order: status invalid")

        order_user = OrdersUser.objects(id=order.id, user_id=order.user.id).first()

        if order_user is None:
            self.logger.error("OrderUser doesn't exist - 'order_preparation' ")
            return self.error("OrderUser doesn't exist - 'order_preparation' ")

        if order.subsidiary.commerce.alliance:
            destination_order = ClientOrders.objects(id=order.id, subsidiary_id=order.subsidiary.id).first()
        else:
            destination_order = OrderNotAlliance.objects(id=order.id, dispatcher_id=order.dispatcher.id).first()

        if destination_order is None:
            return self.error("Order destination doesn't exist - 'order_preparation' ")


        # set change for order
        status = 'ORDER-PREPARATION'
        order_preparation_date = arrow.now('America/Santiago').to('utc').datetime

        order.status = status
        order.order_preparation = order_preparation_date

        order_user.status = status
        order_user.order_preparation = order_preparation_date

        destination_order.status = status
        destination_order.order_preparation = order_preparation_date

        try:
            order.update()
            order_user.update()
            destination_order.update()
        except Exception as e:
            self.logger.error("Dont update order in Order-Preparation: " + e.message)
            return self.error('Dont update order')

        # search user owner order in redis and subsidiary
        redis_user = Redis.User.query.filter(id=order.user.id).first()

        if redis_user is not None and redis_user.token_devices:
            # Verify if order is pickup and implement logic
            data_aps = {
                "title": "Dxpress",
                "body": "Tu pedido en preparación",
                "icon": "myicon",
                "sound": "default",
                "content_available": True
            }

            data = {
                "title": "Dxpress",
                "message": "Tu pedido esta en preparación",
                "status": 12,
                "location": {
                    "order_id": str(order.id),
                }
            }

            # res = redis_user.send_gcm(type=1, msg=data, notification=data_aps)


            ##Test Send gcm from UserRepository, give token database
            notification_id = self.user.save_notification(user_id=order.user.id, data=data, order_id=order.id)

            data["notification_id"] = notification_id
            data = json.dumps(data)
            res = self.user.send_gcm_repo(user_id=order.user.id, type=1, msg=data, notification=data_aps)

            self.logger.info(res)

        # Notify user
        data = {
            "type": "on-order-preparation",
            "user_id": int(order.user.id),
            "message": {
                "msg": "Tu pedido esta en preparación",
                "order_id": str(order.id),
            }
        }
        self.redis().publish("channel-users", json.dumps(data))

        # Notify all session order in preparation
        data = {
            "type": "on-order-preparation",
            "subsidiary_id": int(order.subsidiary.id),
            "message": {
                "order_id": str(order.id),
            }
        }
        self.redis().publish("channel-clients", json.dumps(data))

        return self.success()

    def order_finish(self):
        order_id = self.param('order_id')

        if order_id is None:
            return self.error('Not order_id')

        # find order
        order = Order.objects(id=order_id).first()

        if order is None:
            self.logger.error("Order doesn't exist - 'order_finish' ")
            return self.error('Order is none')

        # notifiy users for gcm and socketio

        data_aps = {
            "title": "Dxpress",
            "body": "Tu repartidor ha llegado",
            "icon": "myicon",
            "content_available": True,
            "sound": "default"
        }

        data = {
            "title": "Dxpress",
            "message": "Tu repartidor ha llegado",
            "status": 8,
            "location": {
                "id": order.dispatcher.id,
                "name": order.dispatcher.name,
                "avatar": order.dispatcher.avatar,
                "patent": order.dispatcher.patent,
                "user_code": order.user_code
            }
        }

        ##Test Send gcm from UserRepository, give token database
        if not order.dxl:
            notification_id = self.user.save_notification(user_id=order.user.id, data=data, order_id=order.id)

            data["notification_id"] = notification_id
            data = json.dumps(data)
            self.user.send_gcm_repo(user_id=order.user.id, type=1, msg=data, notification=data_aps)

            data = {
                "type": "on-order-finish",
                "message": {
                    "dispatch": {
                        "id": order.dispatcher,
                        "name": order.dispatcher.name,
                        "avatar": order.dispatcher.avatar,
                        "patent": order.dispatcher.patent
                    },
                    "user_code": order.user_code,
                    "msg": "Tu repartidor ha llegado"
                },
                "user_id": order.user.id
            }
            self.redis().publish("channel-users", json.dumps(data, cls=JsonCustomEncoder))

        return self.success('Ok')

    def order_ready(self):
        uuid = self.param("order_id")

        if uuid is not None and uuid != "":
            order = Order.objects(id=uuid).first()
            if order is None:
                self.logger.error("Order doesn't exist - 'orde_ready' ")
                return self.error("Order doesn't exist - 'orde_ready' ")

            # Verify status in order
            status = order.status.lower()

            self.logger.error(status)
            if status == 'dxl-preparation':
                return self.__order_ready_dxl(order)

            # if status != "order-preparation":
            if status not in ["order-preparation", "order-paid"]:
                return self.error('Order: Status invalid')

            order_user = OrdersUser.objects(id=uuid, user_id=order.user.id).first()
            if order_user is None:
                self.logger.error("OrdersUser doesn't exist - 'orde_ready' ")
                return self.error("OrdersUser doesn't exist - 'orde_ready' ")

            client_order = ClientOrders.objects(id=uuid, subsidiary_id=order.subsidiary.id).first()
            if client_order is None:
                self.logger.error("ClientOrders doesn't exist - 'orde_ready' ")
                return self.error("ClientOrders doesn't exist - 'orde_ready' ")

            # set change for order
            status = 'ORDER-READY'
            order_ready_date = arrow.now('America/Santiago').to('utc').datetime

            order.status = status
            order.order_ready = order_ready_date

            order_user.status = status
            order_user.order_ready = order_ready_date

            client_order.status = status
            client_order.order_ready = order_ready_date

            try:
                order.update()
                order_user.update()
                client_order.update()
            except Exception as e:
                self.logger.error("Dont update order in Order-Ready: " + e.message)
                return self.error('Dont update order')

            # Notify all session clients order is change
            data = {
                "type": "on-order-ready-notify-all",
                "subsidiary_id": int(order.subsidiary.id),
                "message": {
                    "order_id": str(order.id),
                }
            }
            self.redis().publish("channel-clients", json.dumps(data))


            # get subsidiary_id
            subsidiary_id = order.subsidiary.id

            # calculate average_time for subsidiary, update in redis
            try:
                average_time = self.__calculate_average_time(order.subsidiary.id)
            except Exception as e:
                self.logger.error("Function averga_time call Error: ")
                self.logger.error(e.message)

            if average_time is None:
                self.logger.error("__calculate_average_time return None with Error")
            elif average_time == 0:
                self.logger.warning("__calculate_average_time return 0")
            else:
                # find in redis
                subsidiary_redis = Redis.Subsidiary.query.filter(id=subsidiary_id).first()

                # check subsidiary is not None and update
                if subsidiary_redis is not None:
                    try:
                        subsidiary_redis.avg_time = average_time
                        subsidiary_redis.save()
                    except Exception as e:
                        self.logger.error("Error: " + e.message)
                        self.logger.error("Error in save __calculate_average_time return Value: ")
                        self.logger.error(average_time)
                else:
                    self.logger.error("Subsidiary in redis is None on Order-Ready'")

            redis_user = Redis.User.query.filter(id=order.user.id).first()

            if redis_user is None:
                redis_user = self.user.create_user_in_redis(order.user.id)

            if redis_user is not None and redis_user.token_devices:
                # Verify if order is pickup and implement logic
                if order.pickup:
                    data_aps = {
                        "title": "Dxpress",
                        "body": "Tu pedido esta Listo, puedes retirarlo",
                        "sound": "default",
                        "icon": "myicon",
                        "content_available": True
                    }

                    data = {
                        "title": "Dxpress",
                        "message": "Tu pedido esta Listo, puedes retirarlo",
                        "status": 10,
                        "location": {
                            "client_code": order.client_code,
                            "order_id": str(order.id)
                        }
                    }

                    ##Test Send gcm from UserRepository, give token database
                    notification_id = self.user.save_notification(user_id=order.user.id, data=data, order_id=order.id)
                    data["notification_id"] = notification_id
                    data = json.dumps(data)
                    res = self.user.send_gcm_repo(user_id=order.user.id, type=1, msg=data, notification=data_aps)

                    self.logger.info(res)
                else:

                    data_aps = {
                        "title": "Dxpress",
                        "body": "Estamos buscando un despachador",
                        "icon": "default",
                        "sound": "default",
                        "content_available": True
                    }

                    data = {
                        "title": "Dxpress",
                        "message": "Estamos buscando un despachador",
                        "status": 4,
                        "location": str(order.id)
                    }

                    ##Test Send gcm from UserRepository, give token database
                    notification_id = self.user.save_notification(user_id=order.user.id, data=data, order_id=order.id)
                    data["notification_id"] = notification_id
                    data = json.dumps(data)

                    res = self.user.send_gcm_repo(user_id=order.user.id, type=1, msg=data, notification=data_aps)

                    self.logger.info("ENVIANDO")
                    self.logger.info(res)

            # notify user with socket
            # Verify if order is pickup
            if order.pickup:
                data = {
                    "type": "on-order-ready-pickup",
                    "user_id": int(order.user.id),
                    "message": {
                        "msg": "Tu pedido esta listo, puedes retirarlo",
                        "order_id": str(order.id),
                        "client_code": order.client_code
                    }
                }
                self.redis().publish("channel-users", json.dumps(data))

                data = {
                    "type": "on-user-pickup",
                    "subsidiary_id": int(order.subsidiary.id),
                    "message": {
                        "order_id": str(order.id),
                        "name": order.user.name
                    }
                }
                self.redis().publish("channel-clients", json.dumps(data))
                return self.success()

            data = {
                "type": "on-order-ready",
                "user_id": int(order.user.id),
                "message": {
                    "msg": "Estamos buscando un despachador",
                    "order_id": str(order.id)
                }
            }
            self.redis().publish("channel-users", json.dumps(data))

            try:
                subsidiary = self.subsidiary.find(id=order.subsidiary.id)
            except NoneResult:
                subsidiary = None

            if subsidiary is not None:
                geo = subsidiary.to_geo()
                self.logger.info('Notify nodejs not dispatch')
                key = "dispatcher-not-found"
                data = {
                    'type': key,
                    'message': {
                        'order_id': str(order.id),
                        'subsidiary': {
                            'id': subsidiary.id,
                            'lat': geo.lat,
                            'lng': geo.lon
                        }
                    }
                }
                self.redis().publish("channel-dispatch-found", json.dumps(data, cls=JsonCustomEncoder))
                return self.success('notify nodejs')
            else:
                return self.error("Subsidiary is None")
        else:
            return self.error("UUID can't be null")

    def order_ready_not_alliance(self):
        uuid = self.param("order_id")
        amount_paid = self.param("amount_paid")

        if uuid is not None and uuid != "":
            order = Order.objects(id=uuid).first()

            if amount_paid is None:
                return self.error("Missing AmountPaid !")

            if order is None:
                return self.logger.error("Order doesn't exist - 'orde_ready not alliance' ")

            order_user = OrdersUser.objects(id=order.id, user_id=order.user.id).first()
            order_not_alliance = OrderNotAlliance.objects(id=order.id, dispatcher_id=order.dispatcher.id).first()

            if order_not_alliance is None or order_user is None:
                return self.logger.error("Order doesn't exist - 'orde_ready not alliance' ")

            # Verify status in order
            status = order.status.lower()
            # if status != "order-preparation":
            if status != 'order-preparation':
                return self.error('Order: Status invalid')

            # SET amount paid
            order_not_alliance.amount_paid = amount_paid

            # set change for order
            status = 'ORDER-TRACK'
            order_ready_date = arrow.now('America/Santiago').to('utc').datetime

            order.status = status
            order.dispatch_date = arrow.now('America/Santiago').to('utc').datetime.replace(tzinfo=None)
            order.order_ready = order_ready_date

            order_user.status = status
            order_user.order_ready = order_ready_date
            order_user.dispatch_date = arrow.now('America/Santiago').to('utc').datetime.replace(tzinfo=None)

            order_not_alliance.status = 'ORDER-TRACK'
            order_not_alliance.order_ready = order_ready_date

            try:
                order.update()
                order_user.update()
                order_not_alliance.update()
            except Exception as e:
                self.logger.error("Dont update order in Order-Ready: " + e.message)
                return self.error('Dont update order')

            # CALCULATE AND APPLY AVG_TIME

            # get subsidiary_id
            subsidiary_id = order.subsidiary.id

            # calculate average_time for subsidiary, update in redis
            average_time = self.__calculate_average_time(order.subsidiary.id)

            if average_time is None:
                self.logger.error("__calculate_average_time return None with Error")
            elif average_time == 0:
                self.logger.warning("__calculate_average_time return 0")
            else:
                # find in redis
                subsidiary_redis = Redis.Subsidiary.query.filter(id=subsidiary_id).first()

                # check subsidiary is not None and update
                if subsidiary_redis is not None:
                    subsidiary_redis.avg_time = average_time

                    try:
                        subsidiary_redis.save(force=True)
                    except Exception as e:
                        self.logger.error("Error save average_time: " + e.message)
                        self.logger.error(type(average_time))
                        self.logger.error(average_time)
                else:
                    self.logger.error("Subsidiary in redis is None on Order-Ready'")

            # NOTIFY USER
            try:
                subsidiary = self.subsidiary.find(id=order.subsidiary.id)
                geo = subsidiary.to_geo()
                subsidiary_coordinates = {'x': geo.lat, 'y': geo.lon}
            except NoneResult:
                subsidiary_coordinates = None

            data = {
                "type": "dispatch-found",
                "message": {
                    "order_id": str(order.id),
                    "dispatch": {
                        "id": order.dispatcher,
                        "name": order.dispatcher.name,
                        "avatar": order.dispatcher.avatar,
                        "patent": order.dispatcher.patent,
                        "destination": order.address_order.geo,
                        "subsidiary": subsidiary_coordinates
                    },
                    "user_code": order.user_code
                },
                "user_id": order.user.id
            }
            self.redis().publish("channel-users", json.dumps(data, cls=JsonCustomEncoder))

            data_aps = {
                "title": "Dxpress",
                "body": "Tu pedido esta listo, el repartidor va en camino",
                "sound": "default",
                "icon": "default",
                "content_available": True

            }

            data = {
                "title": "Dxpress",
                "message": "Tu pedido esta listo, el repartidor va en camino",
                "status": 5,
                "location": {
                    "id": order.dispatcher.id,
                    "name": order.dispatcher.name,
                    "avatar": order.dispatcher.avatar,
                    "patent": order.dispatcher.patent,
                    "user_code": order.user_code
                }
            }

            ##Test Send gcm from UserRepository, give token database
            notification_id = self.user.save_notification(user_id=order.user.id, data=data, order_id=order.id)
            data["notification_id"] = notification_id
            data = json.dumps(data, cls=JsonCustomEncoder)
            self.user.send_gcm_repo(user_id=order.user.id, type=1, msg=data, notification=data_aps)

            return self.success()
        else:
            return self.error()

    def __calculate_average_time(self, subsidiary_id):
        """
        # Set date today
        date = arrow.now('America/Santiago').format('YYYY-MM-DD')
        date += " 00:00:00+0000"
        date = dateutil.parser.parse(date)
        """

        date = arrow.now('America/Santiago')

        # convert  date
        start = date.replace(hour=0, minute=0, second=0, microsecond=0).to('utc').datetime
        end = date.replace(hour=23, minute=59, second=59, microsecond=999999).to('utc').datetime

        # filter orders
        orders = ClientOrders.objects.filter(subsidiary_id=subsidiary_id)
        orders = orders.filter(status='ORDER-READY')

        orders = orders.filter(order_date__gte=start)
        orders = orders.filter(order_date__lte=end)
        orders = orders.allow_filtering().all()

        orders_quantity = orders.count()

        if orders_quantity == 0:
            self.logger.error("Cantidad de Ordenes: ")
            self.logger.error(orders_quantity)
            return None

        average_time = 0

        # itere in orders
        for order in orders:
            try:
                difference_minutes = (order.order_ready - order.order_preparation).total_seconds() // 60
            except Exception as e:
                difference_minutes = 0
                self.logger.error("Error in calculate average time" + e.message)
                self.logger.error(order.order_ready)
                self.logger.error(order.order_preparation)

            average_time += difference_minutes

        if average_time == 0:
            return 1.0

        average_time = average_time / orders_quantity

        self.logger.warning("Value average_time")
        self.logger.warning(average_time)
        return math.ceil(average_time)

    def __create_in_redis(self, subsidiary):
        subsidiary_find = Redis.Subsidiary.query.filter(id=subsidiary.id).first()
        geo = subsidiary.to_geo()

        now = arrow.now('America/Santiago').to('utc').date()

        if subsidiary_find is None:
            # BACKWARD COMPATIBILITY
            rs = Redis.Subsidiary(id=subsidiary.id,
                                  full_name=subsidiary.commerce.name + " " + subsidiary.alias,
                                  alias=subsidiary.alias,
                                  avg_rating=subsidiary.rating_average,
                                  avg_time_date=now,
                                  active=subsidiary.is_active,
                                  lat=geo.lat,
                                  lon=geo.lon)
            try:
                rs.save()
            except Exception as e:
                self.logger.error("ConnectRedis subsidiary in redis errors:" + e.message)
        else:
            subsidiary_avg_date = subsidiary_find.avg_time_date
            subsidiary_avg_date = arrow.get(subsidiary_avg_date).replace(tzinfo='America/Santiago')
            if subsidiary_find.avg_time_date and subsidiary_avg_date.day != now.day:
                subsidiary_find.avg_time_date = now
                subsidiary_find.avg_time = float(0.0)
                try:
                    subsidiary_find.save(force=True)
                except Exception as e:
                    self.logger.error("Error in Update subsidiary redis: " + e.message)

    # Method DXL

    def __order_ready_dxl(self, order):

            client_order = ClientOrders.objects(id=order.id, subsidiary_id=order.subsidiary.id).first()
            if client_order is None:
                self.logger.error("ClientOrders doesn't exist - 'orde_ready' ")
                return self.error("ClientOrders doesn't exist - 'orde_ready' ")


            # set change for order
            order_ready_date = arrow.now('America/Santiago').to('utc').datetime


            status = 'DXL-READY'

            order.status = status
            order.order_ready = order_ready_date

            client_order.status = status
            client_order.order_ready = order_ready_date


            try:
                order.update()
                client_order.update()
            except Exception as e:
                self.logger.error("Dont update order in Order-Ready: " + e.message)
                return self.error('Dont update order')

            # get subsidiary_id
            subsidiary_id = order.subsidiary.id

            # calculate average_time for subsidiary, update in redis
            try:
                average_time = self.__calculate_average_time(order.subsidiary.id)
            except Exception as e:
                self.logger.error("Function averga_time call Error: ")
                self.logger.error(e.message)

            if average_time is None:
                self.logger.error("__calculate_average_time return None with Error")
            elif average_time == 0:
                self.logger.warning("__calculate_average_time return 0")
            else:
                # find in redis
                subsidiary_redis = Redis.Subsidiary.query.filter(id=subsidiary_id).first()

                # check subsidiary is not None and update
                if subsidiary_redis is not None:
                    try:
                        subsidiary_redis.avg_time = average_time
                        subsidiary_redis.save()
                    except Exception as e:
                        self.logger.error("Error: " + e.message)
                        self.logger.error("Error in save __calculate_average_time return Value: ")
                        self.logger.error(average_time)
                else:
                    self.logger.error("Subsidiary in redis is None on Order-Ready'")

            try:
                subsidiary = self.subsidiary.find(id=order.subsidiary.id)
            except NoneResult:
                subsidiary = None

            if subsidiary is not None:
                geo = subsidiary.to_geo()
                self.logger.info('Notify nodejs not dispatch')
                key = "dispatcher-not-found"
                data = {
                    'type': key,
                    'message': {
                        'order_id': str(order.id),
                        'subsidiary': {
                            'id': subsidiary.id,
                            'lat': geo.lat,
                            'lng': geo.lon
                        }
                    }
                }
                self.redis().publish("channel-dispatch-found", json.dumps(data, cls=JsonCustomEncoder))
                return self.success('Notify Services NODEJS')
            else:
                return self.error("Subsidiary is None")
