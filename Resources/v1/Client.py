import logging
import math
import json
import arrow
from wheezy.core.descriptors import attribute
from Classes.Core.JsonCustomEncoder import JsonCustomEncoder
from Classes import ConnectCassandra
from Classes.Core.APIHandler import APIHandler
from Classes.Decorators import logged
from Classes.Decorators.Ajax import ajax
from Models import Product, Hash
from Models.Cassandra import Order
from Models.Cassandra.ClientOrders import ClientOrders
from Models.User import User
from Models.DeviceMobile import DeviceMobile
from Models.Subsidiary import Subsidiary
from Models import Redis


class ClientHandler(APIHandler):
    def start(self):
        self.repository(User)
        self.repository(Product)
        self.repository(Subsidiary)
        ConnectCassandra()
        self.logger = logging.getLogger(__name__)


    @attribute
    def translation(self):
        return self.translations['client']

    @ajax
    @logged()
    def get(self):
        """
        GET HTTP Method
        :return: JSON response
        """

        # Get  ID from Route
        subsidiary_id = self.route('id')

        subsidiaries_auth = [subsidiary['id'] for subsidiary in self.logged['subsidiaries']]

        try:
            subsidiary_id = int(subsidiary_id)
        except ValueError:

            return self.error(self._("id_not_provided"))

        if subsidiary_id not in subsidiaries_auth:
            return self.error(self._("without_permission"))

        # Get extra from Route
        extra = self.route('extra')

        if subsidiary_id is not None and extra == "subsidiaries":
            response = self.user.client_subsidiaries(id)

            return self.success(response)

        elif subsidiary_id is not None and extra == "products":
            products = self.subsidiary.products_for_client(subsidiary_id)

            return self.success(self.to_dict(products))

        elif subsidiary_id is not None and extra == "orders":
            other_day = self.param("date")
            start = self.param("start")
            end = self.param("end")

            list_orders = []

            # TODO: Mejorar start y end
            if other_day is None and start is None and end is None:
                orders = ClientOrders.objects(subsidiary_id=subsidiary_id).all()
                for order in orders:
                    list_orders.append(order)
            elif start is not None and end is not None:
                start = arrow.get(start).replace(tzinfo='America/Santiago')
                end = arrow.get(end).replace(tzinfo='America/Santiago')
                start = start.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
                end = end.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")

                orders = ClientOrders.filter(subsidiary_id=subsidiary_id)
                orders = orders.filter(order_date__gte=start.datetime)
                orders = orders.filter(order_date__lte=end.datetime)
                orders = orders.allow_filtering().all()

                for order in orders:
                    list_orders.append(order)

            else:
                date = arrow.get(other_day).replace(tzinfo='America/Santiago')
                start = date.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
                end = date.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")

                orders = ClientOrders.filter(subsidiary_id=subsidiary_id)
                orders = orders.filter(order_date__gte=start.datetime)
                orders = orders.filter(order_date__lte=end.datetime)
                orders = orders.allow_filtering().all()

                for order in orders:
                    list_orders.append(order)

            return self.success(list_orders, custom=True)

        elif subsidiary_id is not None and extra == "orders_day":
            date = arrow.now('America/Santiago')

            start = date.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
            end = date.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")

            orders = ClientOrders.filter(subsidiary_id=subsidiary_id)
            orders = orders.filter(order_date__gte=start.datetime)
            orders = orders.filter(order_date__lte=end.datetime)
            orders = orders.allow_filtering().all()

            orders_dict = []
            for order in orders:
                if order.status in ["ORDER-PAID", "DXL-ORDER", "DXL-PREPARATION", "ORDER-PREPARATION", "DXL-READY",
                                    "ORDER-READY", "ORDER-IN-DISPATCH"]:
                    orders_dict.append(order)

            return self.success(orders_dict, custom=True)

        elif subsidiary_id is not None and extra == 'with_dxl':
            subsidiary = self.subsidiary.find(id=subsidiary_id)
            if subsidiary.dxl:

                return self.success(True)

            else:

                return self.error(False)

        # Return OrderDXL
        elif subsidiary_id is not None and extra == 'order_dxl':
            other_day = self.param("date")
            start = self.param("start")
            end = self.param("end")

            list_orders = []

            if other_day is None and start is None and end is None:
                # Set time
                date = arrow.now('America/Santiago')
                start = date.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
                end = date.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")

                orders = ClientOrders.filter(subsidiary_id=subsidiary_id)
                orders = orders.filter(status='DXL-FINISH')
                orders = orders.filter(order_date__gte=start.datetime)
                orders = orders.filter(order_date__lte=end.datetime)
                orders = orders.allow_filtering().all()

                for order in orders:
                    list_orders.append({
                        "id": order.id,
                        "correlative": order.correlative,
                        "dxl_overprice": order.dxl_overprice,
                        "order_date": order.order_date,
                        "status": order.status,
                        "dispatch_amount": order.dispatch_amount,
                        "distance": order.distance,
                        "subtotal": order.subtotal
                    })

            elif start is not None and end is not None:
                start = arrow.get(start).replace(tzinfo='America/Santiago')
                end = arrow.get(end).replace(tzinfo='America/Santiago')
                start = start.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
                end = end.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")

                orders = ClientOrders.filter(subsidiary_id=subsidiary_id)
                orders = orders.filter(status='DXL-FINISH')
                orders = orders.filter(order_date__gte=start.datetime)
                orders = orders.filter(order_date__lte=end.datetime)
                orders = orders.allow_filtering().all()

                for order in orders:
                    list_orders.append({
                        "id": order.id,
                        "correlative": order.correlative,
                        "dxl_overprice": order.dxl_overprice,
                        "order_date": order.order_date,
                        "status": order.status,
                        "subtotal": order.subtotal
                    })

            else:
                date = arrow.get(other_day).replace(tzinfo='America/Santiago')
                start = date.replace(hour=0, minute=0, second=0, microsecond=0).to("utc")
                end = date.replace(hour=23, minute=59, second=59, microsecond=999999).to("utc")

                # Filters
                orders = ClientOrders.filter(subsidiary_id=subsidiary_id)
                orders = orders.filter(status='DXL-FINISH')
                orders = orders.filter(order_date__gte=start.datetime)
                orders = orders.filter(order_date__lte=end.datetime)
                orders = orders.allow_filtering().all()

                for order in orders:
                    list_orders.append({
                        "id": order.id,
                        "correlative": order.correlative,
                        "dxl_overprice": order.dxl_overprice,
                        "order_date": order.order_date,
                        "status": order.status,
                        "subtotal": order.subtotal
                    })

            return self.success(list_orders, custom=True)

    @ajax
    @logged()
    def put(self):
        subsidiary_id = self.route('id')
        subsidiaries_auth = [subsidiary['id'] for subsidiary in self.logged['subsidiaries']]

        try:
            subsidiary_id = int(subsidiary_id)
        except ValueError:
            return self.error(self._("id_not_provided"))

        if subsidiary_id not in subsidiaries_auth:
            return self.error(self._("without_permission"))

        # Get extra
        extra = self.route('extra')

        # get extra_id
        extra_id = self.route('extra_id')

        if extra == "product" and extra_id is not None:
            self.logger.warning('PUT PRODUCT')
            product = self.product.validate_product_belong_subsidiary(subsidiary_id, extra_id)

            self.logger.warning("Product: " + str(product))
            if product is not None:
                model_product = self.model_process(product, True)

                if model_product is None:
                    return self.error()

                if self.product.save(model_product):
                    return self.success()
                else:
                    return self.error()
            else:
                return self.error(self._("product_is_not_from_subsidiary"))

    @ajax
    @logged()
    def post(self):
        # Get params
        subsidiary_id = self.route('id')
        extra = self.route("extra")

        try:
            subsidiary_id = int(subsidiary_id)
        except ValueError:
            return self.error(self._("id_not_provided"))

        # Get id of subsidiaries authorization
        subsidiaries_auth = [subsidiary['id'] for subsidiary in self.logged['subsidiaries']]

        # Validate subsidiary authorization
        if subsidiary_id not in subsidiaries_auth:
            return self.error()

        if subsidiary_id is not None and extra is not None and extra == "device_mobile":
            token = self.param("token")

            if token is None:
                return self.error(self._('must_provide_token'))

            # find subsidiary in redis and update
            subsidiary_redis = Redis.Subsidiary.query.filter(id=subsidiary_id).first()
            if subsidiary_redis is not None:
                subsidiary_redis.token_device = str(token)
                # validate subsidiary is valid and save
                try:
                    subsidiary_redis.save()
                    return self.success('token_saved')
                except Exception, e:
                    self.logger.error(e.message)
                    return self.error(self._("token_not_saved"))
            else:
                self.logger.error("Subsidiary NONE REDIS")

        if subsidiary_id is not None and extra is not None and extra == 'payment':
            order_id = self.param('order_id')
            if order_id is None:
                self.logger.error('ClientPayment OrderID is None')

                return self.error('Missing param OrderID')

            else:

                return self.__client_person_payment_order(order_id)

    def __client_person_payment_order(self, order_id=None):
        # Find order
        order = Order.objects(id=order_id).first()
        if order is None:
            self.logger.error('__client_person_payment_order: Order is None [304]')

            return self.error('Order is None')

        # Find ClientOrder
        client_order = ClientOrders.objects(order_id=order.id, subsidiary_id=order.subsidiary.id).all()
        if client_order is None:
            self.logger.error('__client_person_payment_order: ClientOrder is None [311]')

            return self.error('ClientOrder is None')

        # Implement Payment
        # For now, Bypass payment

        # Set status
        status = 'ORDER-DXL-PAID'

        # Change status objects
        order.status = status
        client_order.status = status

        # Send for RabbitMQ
        order_json = json.dumps(order, cls=JsonCustomEncoder)
        if not self.rabbit.publish(queue='subsidiary_' + str(order.subsidiary.id) + '_paid',
                                   msg=order_json):
            self.logger.error("We couldn't send the payment to the restaurant")
            raise Exception("We couldn't send the payment to the restaurant")

        return self.success("OK")


