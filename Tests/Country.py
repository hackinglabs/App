import unittest
from wheezy.http.functional import WSGIClient

from Tests import CommonMixin
from manage import main
from Helpers.Strings import Strings


class Country(unittest.TestCase, CommonMixin):
    def setUp(self):
        self.client = WSGIClient(main)
        self.minimal = frozenset(['response', 'success'])
        self.errors = frozenset(['errors', 'success'])
        self.minkeys = frozenset(['id', 'name'])

    def tearDown(self):
        del self.client
        self.client = None

    def test_all(self):
        # Check if response code is 200
        self.assertEqual(self.client.get, 200)
        json = Strings.is_json(self.client.content)
        self.common_success(json)
        # Check if response key is a list
        self.assertTrue(isinstance(json['response'], list))

        obj = json['response'][0]
        # Check if first response is a dict
        self.assertTrue(isinstance(obj, dict))
        # Check if has the minimal keys for an object
        self.assertTrue(self.minkeys <= obj.keys())

    def test_one(self):
        # Check if response code is 200
        self.assertEqual(self.client.get, 200)
        json = Strings.is_json(self.client.content)
        self.common_success(json)
        # Check if response key is a dict
        self.assertTrue(isinstance(json['response'], dict))
        # Check if has the minimal keys for an object
        self.assertTrue(self.minkeys <= json['response'].keys())

    def test_one_fail(self):
        # Check if response code is 400
        self.assertEqual(self.client.get, 400)
        json = Strings.is_json(self.client.content)
        self.common_errors(json)

