
class CommonMixin(object):

    def common_success(self, json):
        # Check if response is JSON
        self.assertNotEquals(json, False)
        # Check if has the minimal keys for a response
        self.assertTrue(self.minimal <= json.keys())
        # Check if success is true
        self.assertTrue(json['success'])

    def common_errors(self, json):
        # Check if response is JSON
        self.assertNotEquals(json, False)
        # Check if has the minimal keys for a response
        self.assertTrue(self.errors <= json.keys())
        # Check if success is False
        self.assertFalse(json['success'])