# -*- coding: utf-8 -*-
import json
import os
from ConfigParser import ConfigParser

import multiprocessing
from wheezy.core.i18n import TranslationsManager

from Helpers import Strings


class Config:

    production = False

    def __init__(self):
        self._base_path = os.path.dirname(os.path.abspath(__file__))
        env = os.environ.get('APP_ENV')

        if env is None or env == "development":
            self.config_path = os.path.join(self._base_path, 'development.cfg')
        elif env == "production":
            self.production = True
            self.config_path = os.path.join(self._base_path, 'production.cfg')
        else:
            exit()

        self.translation_path = os.path.join(self._base_path, '../', 'i18n')
        self.tools_path = os.path.join(self._base_path, '../', 'Tools')

        self.config = ConfigParser({
            'threads': 2 - (multiprocessing.cpu_count() * 4),
            'worker_class': 'meinheld.gmeinheld.MeinheldWorker',
            'host': '127.0.0.1'
        })
        self.config.read(self.config_path)

    def server_data(self):
        tmp_cfg = {
            'host': self.config.get('server', 'host'),
            'port': self.config.get('server', 'port'),
            'worker_class': self.config.get('server', 'worker_class'),
            'reload': True,
            'preload': False,
            'loglevel': 'debug'
        }

        threads = (multiprocessing.cpu_count() * 4) - 2
        workers = (multiprocessing.cpu_count() * 2) + 1
        try:
            threads = self.config.getint('server', 'threads')
        except:
            pass
        try:
            workers = self.config.getint('server', 'workers')
        except:
            pass

        if self.production is True:
            tmp_cfg['reload'] = False
            tmp_cfg['preload'] = True
            tmp_cfg['loglevel'] = 'error'

        tmp_cfg['threads'] = threads
        tmp_cfg['workers'] = workers

        return tmp_cfg

    def pgsql_data(self):
        obj = {
            'host': self.config.get('postgres', 'host'),
            'port': self.config.get('postgres', 'port'),
            'database':   self.config.get('postgres', 'database'),
            'username': self.config.get('postgres', 'username'),
            'password': self.config.get('postgres', 'password')
        }
        obj['postgres'] = 'postgresql+psycopg2://{username}:{password}@{host}:{port}/{database}'.format(**obj)

        return obj

    def redis_data(self):
        rdata = {
            'host': self.config.get('redis', 'host'),
            'port': self.config.get('redis', 'port'),
            'db': self.config.getint('redis', 'database')
        }
        if self.config.has_option('redis', 'password'):
            rdata['password'] = self.config.get('redis', 'password')
        return rdata

    def tile_data(self):
        return {
            'host': self.config.get('tile', 'host'),
            'port': self.config.get('tile', 'port')
        }

    def cassandra_data(self):
        cdata = {
            'host': self.config.get('cassandra', 'host'),
            'port': self.config.get('cassandra', 'port'),
            'database': self.config.get('cassandra', 'database'),
            'protocol': self.config.getint('cassandra', 'protocol')
        }

        if self.config.has_option('cassandra', 'password'):
            cdata['password'] = self.config.get('cassandra', 'password')
            cdata['username'] = self.config.get('cassandra', 'username')
        return cdata

    def security(self):
        return {
            'token_secret': self.config.get('crypto', 'encryption-key'),
            'token_maxage': self.config.getint('crypto', 'ticket-max-age')
        }

    def translation(self):
        translations = TranslationsManager(
                directories=[self.translation_path],
                default_lang='en')

        return {
            'translations_manager': translations
        }

    def social(self):
        return {
            'facebook-secret': self.config.get('oauth2', 'facebook'),
            'google-secret':  self.config.get('oauth2', 'google')
        }


    def rabbit(self):
        return {
            'host': self.config.get('rabbit', 'host'),
            'port': self.config.get('rabbit', 'port'),
            'user': self.config.get('rabbit', 'user'),
            'password': self.config.get('rabbit', 'password')
        }

    def cloudflare(self):
        return {
            'mail': self.config.get('cloudflare', 'email'),
            'key': self.config.get('cloudflare', 'key'),
            'zone': self.config.get('cloudflare', 'zone'),
            'ip': self.config.get('cloudflare', 'ip')
        }

    def various(self):
        from jinja2 import Environment
        from jinja2 import FileSystemLoader
        from wheezy.html.ext.jinja2 import InlineExtension
        from wheezy.html.ext.jinja2 import WidgetExtension
        from wheezy.html.ext.jinja2 import WhitespaceExtension
        from wheezy.html.utils import format_value
        from wheezy.web.templates import Jinja2Template

        searchpath = ['Views']
        env = Environment(
            loader=FileSystemLoader(searchpath),
            auto_reload=False,
            extensions=[
                InlineExtension(searchpath, False),
                WidgetExtension,
                WhitespaceExtension
            ])
        env.globals.update({
            'format_value': format_value,
            '__version__': '1.0'
        })
        env.filters['currency'] = Strings.format_currency
        env.filters['datetime'] = Strings.format_datetime

        render_template = Jinja2Template(env)
        return {
            'ticket': None,
            'render_template': render_template,
            'production': self.production
        }

    def captcha(self):
        return {
            'secret': self.config.get('captcha', 'secret')
        }

    def gcm(self):
        return {
            'key': self.config.get('gcm', 'key'),
            'package': self.config.get('gcm', 'package'),
            'client_package': self.config.get('gcm', 'client_package')
        }

    def khipu(self):
        return {
            'receiver': self.config.get('khipu', 'receiver'),
            'secret': self.config.get('khipu', 'secret')
        }

    def webpay(self):
        return {
            'path': self.config.get('webpay', 'cli_path'),
            'commerce_code': self.config.get('webpay', 'commerce_code'),
            'oneclick_code': self.config.get('webpay', 'oneclick_code')
        }

    def about(self):
        return {
            'version': self.config.get('api', 'version'),
            'secret': self.config.get('api', 'secret'),
            'one_click_paid': True,
            'webpay_paid': True,
        }

    def mailgun(self):
        return {
            'domain': self.config.get('mailgun', 'domain'),
            'private': self.config.get('mailgun', 'private'),
            'public': self.config.get('mailgun', 'public'),
        }

    def uploads(self):
        return {
            'allowed_extensions': self.config.get('uploads', 'allowed_extensions').split(','),
            'max_content_length': self.config.get('uploads', 'max_content_length'),
            'bucket': self.config.get('uploads', 'bucket'),
            'project': self.config.get('uploads', 'project'),
            'upload_folder': self.config.get('uploads', 'upload_folder')
        }

    def defaults(self):
        return {
            'group_id': self.config.get('defaults', 'default_group_id')
        }

    def geo_ip(self):
        file_path = os.path.join(self.tools_path, self.config.get('geoip', 'file_name'))
        return {
            'file_path': file_path
        }

    def logging(self):
        if self.production:
            logging_file = "production_logger.json"
        else:
            logging_file = "development_logger.json"

        logging_file = os.path.join(self._base_path, logging_file)
        if os.path.exists(logging_file):
            with open(logging_file, 'rt') as f:
                return json.load(f)
        return None

    def all(self):
        obj = self.server_data()
        obj.update(self.translation())
        obj.update(self.security())
        obj.update(self.social())
        obj.update(self.uploads())
        obj.update(self.various())
        return obj
