# -*- coding: utf-8 -*-
import json
import os
import re
import math


class Strings(object):

    @staticmethod
    def format_currency(value):
        int_value = int(math.ceil(value))
        formatted = "{:,}".format(int_value)
        return formatted.replace(",", ".")

    @staticmethod
    def format_datetime(value):
        return value.strftime('%d/%m/%Y %H:%M')

    @staticmethod
    def is_json(string):
        try:
            json_object = json.loads(string)
        except ValueError, e:
            return False
        return json_object

    @staticmethod
    def remove_preposition(string, lang='es'):
        if lang == 'es':
            prep = [
                'a',
                'con',
                'de',
                'en',
                'para',
                'por',
                'sin'
            ]
        elif lang == 'en':
            prep = [
                'on',
                'in',
                'at',
                'since',
                'for',
                'ago',
                'before',
                'to',
                'past',
                'till',
                'until',
                'by',
                'under',
                'over',
                'below',
                'above',
                'from',
                'off',
                'about'
            ]
        pipe = "|".join(prep)
        regex = "(\s+)(" + pipe + ")(\s+)"
        return re.sub(regex, '\1 \3', string)

    @staticmethod
    def underscore_to_camelcase(value):
        output = ""
        first_word_passed = True
        for word in value.split("_"):
            if not word:
                output += "_"
                continue
            if first_word_passed:
                output += word.capitalize()
            else:
                output += word.lower()
            first_word_passed = True
        return str(output)

    @staticmethod
    def secure_filename(filename):
        _filename_ascii_strip_re = re.compile(r'[^A-Za-z0-9_.-]')
        _windows_device_files = ('CON', 'AUX', 'COM1', 'COM2', 'COM3', 'COM4', 'LPT1',
                                 'LPT2', 'LPT3', 'PRN', 'NUL')
        if isinstance(filename, unicode):
            from unicodedata import normalize
            filename = normalize('NFKD', filename).encode('ascii', 'ignore')
        for sep in os.path.sep, os.path.altsep:
            if sep:
                filename = filename.replace(sep, ' ')
        filename = str(_filename_ascii_strip_re.sub('', '_'.join(
            filename.split()))).strip('._')

        # on nt a couple of special files are present in each folder.  We
        # have to ensure that the target file is not such a filename.  In
        # this case we prepend an underline
        if os.name == 'nt' and filename and \
                filename.split('.')[0].upper() in _windows_device_files:
            filename = '_' + filename

        return filename