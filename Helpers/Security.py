# -*- coding: utf-8 -*-

import hashlib
import random
import string


class Security(object):

    @staticmethod
    def generate_random(length=None, type="all"):
        if type == "digit":
            all = string.digits
        elif type == "string":
            all = string.ascii_uppercase
        else:
            all = string.ascii_uppercase + string.digits

        if length is None:
            length = 32
        return ''.join(random.SystemRandom().choice(all) for _ in range(length))

    @staticmethod
    def get_csrf():
        m = hashlib.sha512()
        m.update(Security.generate_random())
        return m.hexdigest()

    @staticmethod
    def random_n_digits(n):
        range_start = 10**(n-1)
        range_end = (10**n)-1
        return random.randint(range_start, range_end)