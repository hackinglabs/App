import datetime

def subsidiary_is_closed_hour_hand(openHours):
    today = datetime.datetime.today()
    day = openHours[today.weekday()]
    if 'start' in day and 'end' in day and 'day' in day and 'name' in day:
        hoursNow = today.strftime("%H:%S")
        if hoursNow >= day['end']:
            return True
        else:
            return False
    else:
        return False