# -*- coding: utf-8 -*-
import json
from Configs.Config import Config
import requests


class Captcha:
    @staticmethod
    def verify(challenge, solution):
        config = Config()
        captcha = config.captcha()

        try:
            if challenge is not None:
                params = {
                    'privatekey': captcha['secret'],
                    'challenge': challenge,
                    'response': solution,
                    'remoteip': '127.0.0.1'
                }
                response = requests.get('http://www.google.com/recaptcha/api/verify', params=params).text
                response = response.splitlines()
                if response[0] == "true":
                    return True
            else:
                params = {
                    'secret': captcha['secret'],
                    'response': solution
                }
                response = requests.get('https://www.google.com/recaptcha/api/siteverify', params=params).text
                response = json.loads(response)
                return response['success']
        except:
            pass
        return False
