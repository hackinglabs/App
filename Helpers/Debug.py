# -*- coding: utf-8 -*-

import json

from termcolor import colored


class Debug(object):
    @staticmethod
    def any(msg, sufix=None, color="cyan"):
        if sufix is not None:
            sufix = colored(sufix, color, attrs=['bold', 'reverse'])
            print sufix, colored(msg, 'cyan')
        print colored(msg, 'cyan')

    @staticmethod
    def error(msg, place=None):
        if place is None:
            error = '[ERROR]'
        else:
            error = '[ERROR - %s]' % (place,)

        error = colored(error, 'red', attrs=['bold', 'reverse'])
        print error, colored(msg, 'red')

    @staticmethod
    def warning(msg, place=None):
        if place is None:
            warn = '[WARNING]'
        else:
            warn = '[WARNING - %s]' % (place,)
        warn = colored(warn, 'yellow', attrs=['bold', 'reverse'])
        print warn, colored(msg, 'yellow')

    @staticmethod
    def info(msg, place=None):
        is_json = False
        if place is None:
            info = '[INFO]'
        else:
            info = '[INFO - %s]' % (place,)
        info = colored(info, 'green', attrs=['bold', 'reverse'])
        if isinstance(msg, dict):
            try:
                msg = json.loads(msg)
                msg = json.dumps(msg, indent=4)
                is_json = True
            except:
                msg = msg
        else:
            msg = msg

        msg = colored(msg, 'green')
        if is_json:
            print info, colored('Printing a JSON object',
                                'green',
                                attrs=['bold'])
            print msg
        else:
            print info, msg

    @staticmethod
    def json(obj, place=None):
        if obj is None:
            return None
        print colored(json.dumps(obj, indent=4), 'cyan')

    @staticmethod
    def req(req, place=None):
        print req.content_type
        method = colored(req.method, 'cyan', attrs=['bold', 'reverse'])
        path = colored(req.path, 'cyan', attrs=['concealed'])
        print method, path
        try:
            obj = req.form
        except:
            obj = {}
        print colored(json.dumps(obj, indent=4), 'cyan')
