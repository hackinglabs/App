import logging

import rabbitpy
from Configs import Config


class Rabbit(object):

    _con_string = None

    def __init__(self):
        rmq = Config().rabbit()
        self._con_string = 'amqp://{user}:{password}@{host}:{port}/%2f'.format(**rmq)
        self.logger = logging.getLogger(__name__)

    def publish(self, queue=None, msg=None, exchange=''):

        # Connect to RabbitMQ on localhost, port 5672 as guest/guest
        with rabbitpy.Connection(self._con_string) as conn:
            self.logger.debug(conn)
            # Open the channel to communicate with RabbitMQ
            with conn.channel() as channel:
                q = rabbitpy.Queue(channel, queue)
                q.durable = True
                q.declare()

                # Turn on publisher confirmations
                channel.enable_publisher_confirms()

                # Create the message to publish
                message = rabbitpy.Message(channel, msg)

                # Publish the message, looking for the return value to be a bool True/False
                if message.publish(exchange, queue, mandatory=True):
                    return True
                else:
                    return False