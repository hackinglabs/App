# -*- coding: utf-8 -*-

# General Imports and Core Handlers
from wheezy.routing import url

from Resources.About import AboutHandler
from Resources.Build import BuildHandler
from Resources.Install import InstallHandler
from Resources.Testing import TestingHandler

from Resources import v1
from Resources.v1 import Statistics
from Resources.v1 import Graph

locale_pattern = '{locale:(en|es)}/'
locale_defaults = {'locale': 'en'}

core_urls = [
    url('about/(?P<type>api|authors)', AboutHandler),
    url('install/(?P<type>all|sql|cql|patch|allsql)', InstallHandler),
    url('test(/(?P<what>set|find|found|delete|sales|finished|spam|simulated_dispatcher))?', TestingHandler),
    url('build(/(?P<what>subsidiaries|users|fix_users))?', BuildHandler)

]


v1_urls = [
    url('area(/(?P<id>\d+)|/(?P<extra>valid))?', v1.AreaHandler),
    url('auth/(?P<action>register|login|logout|facebook|google|generate_jwt)/?(?P<type>client|dispatcher|admin)?', v1.AuthHandler),
    url('category(/(?P<id>\d+)/?(?P<extra>(sub_categories|compounds)?))?', v1.CategoryHandler),
    url('city(/(?P<id>\d+))?', v1.CityHandler),
    url('check/(?P<what>coupon|discount|subsidiary_discount)', v1.CheckHandler),
    url('client(/(?P<id>\d+)/?(?P<extra>(orders|subsidiaries|product|device_mobile|orders_day|products|with_dxl|order_dxl|payment)?)/?(?P<extra_id>\d+)?)?', v1.ClientHandler),
    url('commerce(/(?P<id>(\d+|names))/?(?P<extra>(subsidiaries|admin|user|user_subsidiary|tags)?))?', v1.CommerceHandler),
    url('commerce_tag(/(?P<id>\d+))?', v1.CommerceTagHandler),
    url('commerce_user(/(?P<id>\d+))?', v1.CommerceUserHandler),
    url('compound(/(?P<id>\d+))?', v1.CompoundHandler),
    url('contacts', v1.ContactHandler),
    url('country(/(?P<id>\d+))?', v1.CountryHandler),
    url('coupon(/(?P<id>\d+))?', v1.CouponHandler),
    url('notification(/(?P<id>\d+))?', v1.NotificationHandler),
    url('coupon_user(/(?P<id>\d+))?', v1.CouponUserHandler),
    url('device(/(?P<id>\d+))?', v1.DeviceHandler),
    url('devices_mobile(/(?P<id>\d+))?', v1.DeviceMobileHandler),
    url('device_support(/(?P<id>\d+))?', v1.DevicesSupportHandler),
    url('dispatch_address(/(?P<id>\d+))?', v1.DispatchAddressHandler),
    url('driver_vehicle(/(?P<id>\d+)(?P<extra>/other)?)?', v1.DriverVehicleHandler),
    url('favorite(/(?P<user>user/)?(?P<id>\d+))', v1.FavoriteHandler),
    url('geo_ip/?', v1.GeoIpHandler),
    url('graph/heatmap', Graph.HeatmapHandler),
    url('group(/(?P<id>\d+|commerce))?', v1.GroupHandler),
    url('ingredient(/(?P<id>\d+))?', v1.IngredientHandler),
    url('ingredient_per_category(/(?P<id>\d+)/?(?P<extra>(ingredients|extra)?))?', v1.IngredientPerCategoryHandler),
    url('item_compound(/(?P<id>\d+))?', v1.ItemCompoundHandler),
    url('monitor(/(?P<extra>(orders|subsidiary_connects|dispatcher_receive|orders_status|subsidiary_connections|alerts|indicators|send_gcm|count_orders_user|coordinates|canceled))/?(?P<extra_id>\d+)?)?', v1.MonitorHandler),
    url('option_compound(/(?P<id>\d+))?', v1.OptionCompoundHandler),
    url('option_group(/(?P<id>\d+))?', v1.OptionGroupHandler),
    #url('order(/(?P<extra>other|dispatch_amount|dispatch_amount_and_distance|client_code|user_code|order_canceled|dxl/?(?P<id>[0-9a-f-]+))?|get_dxl)|/(?P<id>[0-9a-f-]+))?', v1.OrderHandler),
    url('order(/(?P<extra>other|dispatch_amount|dispatch_amount_and_distance|client_code|user_code|order_canceled|dxl|get_dxl)?/?(?P<id>[0-9a-f-]+)?)?', v1.OrderHandler),
    url('owner_dispatcher', v1.DispatcherOwnerHandler),
    url('payment(/(?P<type>khipu|webpay|summary|oneclick|inscription|cancel|force_paid|cash|verify_code))?', v1.PaymentHandler),
    url('product(/(?P<id>\d+)/?(?P<compound>(compound|options|details)?))?', v1.ProductHandler),
    url('profile(/(?P<extra>(addresses|cards|favorites|push|orders|orders_dispatcher|alert))/?(?P<id>\d+)?)?', v1.ProfileHandler),
    url('rating_subsidiary(/(?P<slug>subsidiary)?/?(?P<id>\d+)?)?', v1.RatingSubsidiaryHandler),
    url('redzone(/(?P<id>\d+))?', v1.RedZoneHandler),
    url('search/(?P<extra>radius|commerce|tag|dispatcher)', v1.SearchHandler),
    url('slug/(?P<slug>[A-Za-z0-9]*)', v1.CommerceHandler),
    url('state(/(?P<id>\d+))?', v1.StateHandler),
    url('statistic/(?P<slug>subsidiary|vehicle|dispatcher)(/(?P<id>\d+))?', v1.StatisticHandler),
    url('statistic/coupon', Statistics.CouponHandler),
    url('sub_category(/(?P<id>\d+)(?P<extra>/products)?)?', v1.SubCategoryHandler),
    url('subsidiary(/(?P<id>\d+)/?(?P<extra>(details|ratings|categories|compounds)?))?', v1.SubsidiaryHandler),
    url(
        'subsidiary_communication(/(?P<extra>connect|disconnect|order_ready|accept_or_rejected|order_finish|order_preparation|order_ready_na))?',
        v1.SubsidiaryCommunicationHandler
    ),
    url('system(/(?P<id>\d+))?', v1.SystemHandler),
    url('tag(/(?P<id>(\d+|names)))?', v1.TagHandler),
    url('temp_user(/(?P<id>\d+)/?(?P<extra>extra)?)?', v1.TempUserHandler),
    url('user(/(?P<id>\d+))?/?(?P<extra>addresses)?', v1.UserHandler),
    url('user_communication(/(?P<extra>(connect|disconnect|on_change|accept_or_rejected|order_received_franchise|dispatcher_order)))?', v1.UserCommunicationHandler),
    url('user_subsidiary(/(?P<id>\d+))?', v1.UserSubsidiaryHandler),
    url('user_tag(/(?P<user>user/)?(?P<id>\d+))?', v1.UserTagHandler),
    url('vehicle(/(?P<id>\d+))?', v1.VehicleHandler),
    url('rating_dispatcher(/(?P<id>\d+))?', v1.RatingDispatcherHandler),
    url('logger/(?P<extra>dispatcher|other)', v1.LoggerHandler),
    url('discount(/(?P<id>\d+))?', v1.DiscountHandler),
    url('report(/(?P<extra>sales))?', v1.ReportHandler),
    url('dxl_services/(?P<extra>create_commerce|none)', v1.DxlServices),
]

locale_urls = []


all_urls = [
    ('', core_urls, locale_defaults),
    ('v1/' + locale_pattern, v1_urls, locale_defaults),
]
