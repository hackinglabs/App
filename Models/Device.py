# -*- coding: utf-8 -*-
import arrow
from sqlalchemy import Boolean, Column, Date, ForeignKey, Integer, String, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import length, required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Commerce import Commerce
from State import State

Base = declarative_base()


class Device(Base, DictSerializableMixin):
    __tablename__ = 'devices'

    device_validator = Validator({
        'uuid': [required],
        'model': [required, length(max=15)],
        'trademark': [required, length(max=15)],
        'name': [required, length(max=15)],
        "serial_number": [required],
        "reception_date": [required],
        "commerce_id": [required, int_adapter(required)],
        "state_id": [required, int_adapter(required)]

    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('devices_id_seq'::regclass)"))
    model = Column(String(45), nullable=False)
    trademark = Column(String(45), nullable=False)
    name = Column(String(45), nullable=False)
    serial_number = Column(String, nullable=False)
    reception_date = Column(Date, default=arrow.now('America/Santiago').to('utc').datetime)
    contract = Column(Boolean, default=False)
    commerce_id = Column(ForeignKey(Commerce.id), nullable=False)
    commerce = relationship(Commerce, backref="devices")
    enabled = Column(Boolean, default=True, nullable=True)
    state_id = Column(ForeignKey(State.id), nullable=False)
    state = relationship(State, backref="devices")

    def __repr__(self):
        return self.name

    def validate(self):
        return self.device_validator

