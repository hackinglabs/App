import arrow
from sqlalchemy import Column, Date, ForeignKey, Integer, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Models.Coupon import Coupon
from Models.User import User

Base = declarative_base()


class CouponUser(Base, DictSerializableMixin):
    __tablename__ = 'coupons_users'

    coupon_user_validator = Validator({
        'user_id': [required, int_adapter(required)],
        'coupon_id': [required, int_adapter(required)]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('coupons_users_id_seq'::regclass)"))
    user_id = Column(ForeignKey(User.id), nullable=False)
    coupon_id = Column(ForeignKey(Coupon.id), nullable=False)
    use_date = Column(Date, nullable=False, default=arrow.now('America/Santiago').to('utc').datetime)
    user = relationship(User, backref="coupons_users")
    coupon = relationship(Coupon, backref="coupons_users")

    def validate(self):
        return self.coupon_user_validator
