# -*- coding: utf-8 -*-
import arrow
from sqlalchemy import Column, Date, ForeignKey, Integer, text, String, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter

from Classes.Core.SQLAlchemyMixins import DictSerializableMixin
from User import User

Base = declarative_base()


class DispatcherOrder(Base, DictSerializableMixin):
    __tablename__ = 'dispatcher_orders'

    dispatcher_order_validation = Validator({
        "dispatcher_id": [required, int_adapter(required)],
    })

    dispatcher_id = Column(ForeignKey(User.id), nullable=False, primary_key=True)
    created_at = Column(Date, nullable=False, default=arrow.now('America/Santiago').to('utc').datetime)
    order_id = Column(String)
    current = Column(Boolean, default=True)
    dispatcher = relationship(User, backref="dispatcher_order")

    def validate(self):
        return self.dispatcher_order_validation

