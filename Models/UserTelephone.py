from sqlalchemy import Column, ForeignKey, Integer, text, Date, BigInteger
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Models.User import User
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class UserTelephone(Base, DictSerializableMixin):
    __tablename__ = 'users_telephones'

    user_telephone_validator = Validator({
        "user_id": [required],
        "telephone": [required, int_adapter(required)]
    })
    id = Column(Integer, primary_key=True, server_default=text("nextval('users_telephones_id_seq'::regclass)"))
    user_id = Column(ForeignKey(User.id), nullable=False)
    telephone = Column(BigInteger, nullable=False)
    registration_date = Column(Date, server_default=text("('now'::text)::date"))
    user = relationship(User, backref="users_telephones")

    def validate(self):
        return self.user_telephone_validator
