# -*- coding: utf-8 -*-
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy import Column, ForeignKey, Integer, text, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from Models.IngredientPerCategory import IngredientPerCategory
from Models.Product import Product
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class OptionGroup(Base, DictSerializableMixin):
    __tablename__ = 'options_group'

    option_group_validator = Validator({
        "product_id": [required, int_adapter(required)],
        "ingredient_per_category_id": [required, int_adapter(required)],
        "edit": [required]

    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('options_group_id_seq'::regclass)"))
    product_id = Column(ForeignKey(Product.id), nullable=False)
    ingredient_per_category_id = Column(ForeignKey(IngredientPerCategory.id))
    product = relationship(Product, backref=backref("options_group", uselist=False))
    quantity = Column(Integer, default=0, nullable=False)
    ingredient_per_category = relationship(IngredientPerCategory, backref="options_group")
    defaults = Column(ARRAY(Integer), nullable=True, server_default=text('NULL'))
    edit = Column(Boolean, default=False)
    enabled = Column(Boolean, default=True)

    def validate(self):
        return self.option_group_validator
