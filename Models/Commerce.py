# -*- coding: utf-8 -*-
import arrow
from sqlalchemy import Column, Date, ForeignKey, Integer, String, Text, text, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from Classes.Actions.CommerceActions import CommerceActions
from User import User
from Country import Country
from System import System
from wheezy.validation import Validator
from wheezy.validation.rules import length
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class Commerce(CommerceActions, Base, DictSerializableMixin):
    __tablename__ = 'commerces'

    commerce_validator = Validator({
        'name': [required, length(min=3)],
        'nin': [required, length(min=3)],
        'trade': [required, length(min=3)],
        'country_id': [required, int_adapter(required)],
        'address': [required],
        'business_name': [required],
        'telephone': [required, int_adapter(required)],
        'user_id': [required, int_adapter(required)],
        'system_id': [required, int_adapter(required)],
    })

    default_fields = [
        'name',
        'slug',
        'trade',
        'id',
        'logo',
        'enabled',
        'address',
        'trade',
        'business_name',
        'user_id',
        'nin',
        'telephone',
        'alliance',
        'franchise',
        'colors',
        'is_person',
        'system_id'
    ]

    hidden_fields = [
        'nin',
        'telephone',
    ]

    image_fields = {
        'logo': False
    }

    id = Column(Integer(), primary_key=True,
                server_default=text("nextval('commerces_id_seq'::regclass)"))
    name = Column(String, nullable=False)
    nin = Column(String(45), nullable=False)
    trade = Column(String, nullable=False)
    slug = Column(String, nullable=False)
    subdomain_id = Column(String)
    address = Column(String(), nullable=False)
    telephone = Column(Integer())
    business_name = Column(String, nullable=False)
    logo = Column(String, nullable=False)
    banner = Column(String(), nullable=True)
    description = Column(Text())
    colors = Column(String, nullable=True)
    registration_date = Column(Date, nullable=False, default=arrow.now('America/Santiago').to('utc').datetime)
    country_id = Column(ForeignKey(Country.id), nullable=False)
    user_id = Column(ForeignKey(User.id), nullable=False)
    system_id = Column(ForeignKey(System.id), nullable=False)
    profit_percentage = Column(Integer, nullable=True)
    user = relationship(User, backref='commerces')
    system = relationship(System, backref='commerces')
    country = relationship(Country, backref='commerces')
    enabled = Column(Boolean, default=True)
    alliance = Column(Boolean, default=True)
    franchise = Column(Boolean, default=False)
    is_person = Column(Boolean, default=False)

    def __repr__(self):
        return self.name

    def validate(self):
        return self.commerce_validator

    def unique(self):
        return [
            'nin'
        ]


Commerce.register()
