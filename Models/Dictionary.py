# -*- coding: utf-8 -*-

from sqlalchemy import Column, String
from sqlalchemy.ext.declarative import declarative_base
from wheezy.validation import Validator
from wheezy.validation.rules import required
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class Dictionary(Base, DictSerializableMixin):
    __tablename__ = 'dictionaries'

    dictionary_rules = Validator({
        'name': [required],
        'value': [required]
    })

    name = Column(String(120), primary_key=True)
    value = Column(String(120), nullable=False)

    def validate(self):
        return self.dictionary_rules