# -*- coding: utf-8 -*-

import rom


class Area(rom.Model):
    _id = rom.PrimaryKey()
    id = rom.Integer(index=True)
    name = rom.Text(required=True)
    country = rom.ManyToOne('Country', on_delete='cascade')
