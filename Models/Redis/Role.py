# -*- coding: utf-8 -*-
import rom


class Role(rom.Model):
    _id = rom.PrimaryKey()
    id = rom.Integer(index=True)
    name = rom.Text(required=True)
