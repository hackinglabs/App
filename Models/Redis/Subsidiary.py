# -*- coding: utf-8 -*-
import rom

from Classes.Core.GcmNotification import GcmNotification


class Subsidiary(rom.Model):
    _id = rom.PrimaryKey()
    id = rom.Integer(index=True)
    full_name = rom.Text()
    alias = rom.Text(required=True)
    avg_rating = rom.Float(index=True)
    active = rom.Boolean(default=True)
    token_device = rom.Text()
    avg_time = rom.Float(required=False)
    avg_time_date = rom.Date()
    lat = rom.Float()
    lon = rom.Float()

    def send_gcm(self, type, msg, notification, pack=None):
        if self.token_device:
            gcm = GcmNotification(for_clients=True)
            gcm.add(self.token_device)
            gcm.msg(msg)
            gcm.type(type)
            gcm.notification(notification)
            gcm.priority('high')

            return gcm.send()
        return False
