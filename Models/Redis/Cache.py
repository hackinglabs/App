import rom


class Cache(rom.Model):
    id = rom.PrimaryKey()
    url = rom.String(required=True, unique=True)
    headers = rom.Text(required=True)
    content_type = rom.String(required=True)
    status_code = rom.Integer(required=True)
    buffer = rom.Text(required=True)
    type_cache = rom.String(index=True, required=True, keygen=rom.FULL_TEXT)
    type = rom.Integer()
