# -*- coding: utf-8 -*-
import rom


class Commerce(rom.Model):
    _id = rom.PrimaryKey()
    id = rom.Integer(index=True)
    name = rom.Text(required=True)
    nin = rom.Text(required=True, unique=True)
    address = rom.Text()
    business_name = rom.Text()
    logo = rom.Text()
    description = rom.Text()
    registration_date = rom.Date()
    state = rom.ManyToOne('State', on_delete='cascade')
    user = rom.ManyToOne('User', on_delete='cascade')
