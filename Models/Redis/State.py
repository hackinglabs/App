# -*- coding: utf-8 -*-
import rom


class State(rom.Model):
    _id = rom.PrimaryKey()
    id = rom.Integer(index=True)
    name = rom.Text(required=True)
    city = rom.ManyToOne('City', on_delete='cascade')
