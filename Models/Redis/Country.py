# -*- coding: utf-8 -*-
import rom


class Country(rom.Model):
    _id = rom.PrimaryKey()
    id = rom.Integer(index=True)
    name = rom.Text(required=True)
    phone_code = rom.Integer()
    iva = rom.Float(required=True)