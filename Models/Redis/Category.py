# -*- coding: utf-8 -*-

import rom


class Category(rom.Model):
    _id = rom.PrimaryKey()
    id = rom.Integer(index=True)
    name = rom.Text(required=True)
    subsidiary = rom.ManyToOne('Subsidiary', on_delete='cascade')
