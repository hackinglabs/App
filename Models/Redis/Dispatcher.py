# -*- coding: utf-8 -*-
import rom


class Dispatcher(rom.Model):
    _id = rom.PrimaryKey()
    id = rom.Integer(index=True)
    first_name = rom.Text()
    last_name = rom.Text()
    nin = rom.Text()
    with_order = rom.Boolean()
    order_id = rom.Text()
    count_rejected = rom.Integer()
    is_active = rom.Boolean()
    vehicle_id = rom.Integer()

