# -*- coding: utf-8 -*-
import rom


class City(rom.Model):
    _id = rom.PrimaryKey()
    id = rom.Integer(index=True)
    name = rom.Text(required=True)
    area = rom.ManyToOne('Area', on_delete='cascade')
