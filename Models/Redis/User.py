# -*- coding: utf-8 -*-
import rom

from Classes.Core.GcmNotification import GcmNotification
from Classes.Core.Exceptions.SendError import SendError


class User(rom.Model):
    _id = rom.PrimaryKey()
    id = rom.Integer(index=True)
    first_name = rom.Text()
    last_name = rom.Text()
    nin = rom.Text()
    birth_date = rom.Date()
    email = rom.Text(required=True)
    user_token = rom.Text(required=True)
    enabled = rom.Boolean(required=True, default=True)
    avatar = rom.Text()
    token_devices = rom.Json()

    def send_gcm(self, type, msg, notification):
        if self.token_devices:
            gcm = GcmNotification()
            gcm.add(self.token_devices)
            gcm.msg(msg)
            gcm.type(type)
            gcm.notification(notification)
            gcm.priority('high')

            return gcm.send()

        return False

    @staticmethod
    def get_blank(id=None):
        if id is None:
            return User(email='', user_token='', enabled=True)
        else:
            return User(id=id, email='', user_token='', enabled=True)


