# -*- coding: utf-8 -*-
import rom


class OneClickToken(rom.Model):
    _id = rom.PrimaryKey()
    id = rom.Integer()
    token = rom.Text(required=True, unique=True)
    user_id = rom.Integer(required=True, index=True)
    redirect_url = rom.Text(required=True)