import arrow
from sqlalchemy import Column, ForeignKey, Integer, Boolean, text, String, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin


from Models import Area

Base = declarative_base()


class Contact(Base, DictSerializableMixin):
    __tablename__ = 'contacts'

    contact_validator = Validator({
        "first_name": [required],
        "email": [required],
        "nin": [required],
        "phone": [required],
        "area_id": [required, int_adapter(required)]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('contacts_id_seq'::regclass)"))
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=True)
    contact = Column(String, nullable=True)
    nin = Column(String, nullable=False)
    phone = Column(String, nullable=False)
    email = Column(String, nullable=True)
    address = Column(String, nullable=True)
    client = Column(Boolean, default=False)
    area_id = Column(ForeignKey(Area.id), nullable=False)
    registration_date = Column(Date, default=arrow.now('America/Santiago').to('utc').datetime)
    area = relationship(Area, backref="contacts")

    def validate(self):
        return self.contact_validator
