import arrow
from sqlalchemy import Column, ForeignKey, Integer, text, String, Date, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.postgresql.json import JSON
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required
from Models.User import User
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class Notification(Base, DictSerializableMixin):
    __tablename__ = 'notifications'

    notification_validator = Validator({
        "user_id": [required],

    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('notifications_id_seq'::regclass)"))
    user_id = Column(ForeignKey(User.id), nullable=False)
    order_id = Column(String)
    data = Column(JSON)
    created_at = Column(Date, nullable=False, default=arrow.now('America/Santiago').to('utc').datetime)
    is_check = Column(Boolean, default=False)
    user = relationship(User, backref="notifications")

    def validate(self):
        return self.notification_validator
