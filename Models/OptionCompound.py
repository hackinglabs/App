from sqlalchemy import Column, ForeignKey, Integer, text, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required

from Models.Compound import Compound
from Models.Product import Product
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class OptionCompound(Base, DictSerializableMixin):
    __tablename__ = 'options_compounds'

    option_compound_validator = Validator({
        "product_id": [required],
        "compound_id": [required],

    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('options_compounds_id_seq'::regclass)"))
    product_id = Column(ForeignKey(Product.id), nullable=False)
    compound_id = Column(ForeignKey(Compound.id), nullable=False)
    change_for_all = Column(Boolean, default=False)
    quantity = Column(Integer, nullable=True, default=1)
    compound = relationship(Compound, backref="options_compounds")
    product = relationship(Product, backref="options_compounds")

    def validate(self):
        return self.option_compound_validator
