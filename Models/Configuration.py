from sqlalchemy import Column, ForeignKey, Integer, text, Float, String, SMALLINT
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from Models.Country import Country
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin


Base = declarative_base()


class Configuration(Base, DictSerializableMixin):
    __tablename__ = 'configurations'

    configuration_validator = Validator({
        "country_id": [required, int_adapter(required)],
        "mim_meters": [required, int_adapter(required)],
        "dispatch_amount_base": [required, int_adapter(required)],
        "dispatch_amount_extra": [required, int_adapter(required)],
        "exchange_rate": [required, int_adapter(required)],
        "divisa_local": [required],
        "divisa_international": [required],
        "code_currency": [required],
        "language": [required]
    })

    id = Column(Integer, primary_key=True,
                server_default=text("nextval('configurations_id_seq'::regclass)"))
    country_id = Column(ForeignKey(Country.id), nullable=False)
    country = relationship(Country, backref="configurations")
    min_meters = Column(Integer, nullable=False)
    dispatch_amount_base = Column(Float, nullable=False)
    dispatch_amount_extra = Column(Float, nullable=False)
    exchange_rate = Column(Float, nullable=False)
    code_currency = Column(String(5), nullable=False)
    language = Column(String(5), nullable=False)
    profit_percentage = Column(Integer, nullable=False)
    iva = Column(Float, nullable=False)
    currency_symbol = Column(String(2), nullable=False)
    thousands_separators = Column(String(1), nullable=False)
    decimals_separator = Column(String(1), nullable=False)
    decimals_quantity = Column(SMALLINT, nullable=False)

    def validate(self):
        return self.configuration_validator
