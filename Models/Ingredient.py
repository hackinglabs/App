# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey, Integer, text, String, Boolean, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Models.IngredientPerCategory import IngredientPerCategory

Base = declarative_base()


class Ingredient(Base, DictSerializableMixin):
    __tablename__ = 'ingredients'

    ingredient_validator = Validator({
        'name': [required],
        'ingredient_per_category_id': [required, int_adapter(required)],
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('ingredients_id_seq'::regclass)"))
    name = Column(String, nullable=True)
    ingredient_per_category_id = Column(ForeignKey(IngredientPerCategory.id), nullable=False)
    price = Column(Float, nullable=False, default=0)
    ingredient_per_category = relationship(IngredientPerCategory, backref="ingredients")
    enabled = Column(Boolean, default=True)

    def validate(self):
        return self.ingredient_validator
