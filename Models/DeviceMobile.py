# -*- coding: utf-8 -*-
import arrow
from sqlalchemy import Column, Date, ForeignKey, Integer, text, Enum
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from Models.User import User
from wheezy.validation import Validator
from wheezy.validation.rules import required
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


# 1 = Android
# 2 = IOS

class DeviceMobile(Base, DictSerializableMixin):
    __tablename__ = 'devices_mobile'

    device_mobile_validator = Validator({
        "token": [required],
        "os": [required],
        "user_id": [required]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('devices_mobile_id_seq'::regclass)"))
    token = Column(Integer, nullable=False)
    registration_date = Column(Date, default=arrow.now('America/Santiago').to('utc').datetime)
    user_id = Column(ForeignKey(User.id), nullable=False)
    user = relationship(User, backref='devices_mobile')
    os = Column(Enum("1", "2", "3", "4", name="os_devices"))

    def validate(self):
        return self.device_mobile_validator
