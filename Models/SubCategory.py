# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey, Integer, String, text, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Models.Category import Category

Base = declarative_base()


class SubCategory(Base, DictSerializableMixin):
    __tablename__ = 'sub_categories'

    sub_categories_validator = Validator({
        'name': [required],
        'category_id': [required, int_adapter(required)],

    })

    image_fields = {
        'image': False
    }

    id = Column(Integer, primary_key=True, server_default=text("nextval('sub_categories_id_seq'::regclass)"))
    name = Column(String(45), nullable=False)
    category_id = Column(ForeignKey(Category.id), nullable=False)
    category = relationship(Category, backref='sub_categories')
    enabled = Column(Boolean, default=True)
    is_visible = Column(Boolean, default=True)
    description = Column(String)
    image = Column(String)
    navigable = Column(Boolean, default=True)

    def validate(self):
        return self.sub_categories_validator
