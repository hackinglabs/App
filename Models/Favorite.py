# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey, Integer, text, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Models.Subsidiary import Subsidiary
from Models.User import User

Base = declarative_base()


class Favorite(Base, DictSerializableMixin):
    __tablename__ = 'favorites'

    favorite_validator = Validator({
        'user_id': [required, int_adapter(required)],
        'subsidiary_id': [required, int_adapter(required)]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('favorites_id_seq'::regclass)"))
    user_id = Column(ForeignKey(User.id), nullable=False)
    subsidiary_id = Column(ForeignKey(Subsidiary.id), nullable=False)
    description = Column(String(200), nullable=True)
    user = relationship(User, backref="favorites")
    subsidiary = relationship(Subsidiary, backref=backref("favorites", uselist=False))

    def validate(self):
        return self.favorite_validator
