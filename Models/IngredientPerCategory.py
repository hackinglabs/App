# -*- coding: utf-8 -*-
from sqlalchemy import Column, Integer, text, String, Boolean, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from Models.Subsidiary import Subsidiary
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


# Category ingredient
class IngredientPerCategory(Base, DictSerializableMixin):
    __tablename__ = 'ingredients_per_category'

    ingredient_per_category_validator = Validator({
        "name": [required],
        "subsidiary_id": [required, int_adapter(required)],
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('ingredients_per_category_id_seq'::regclass)"))
    name = Column(String(60))
    is_base = Column(Boolean, default=False)
    subsidiary_id = Column(ForeignKey(Subsidiary.id), nullable=False)
    subsidiary = relationship(Subsidiary, backref='ingredients_per_category')
    enabled = Column(Boolean, default=True)
    is_premium = Column(Boolean, default=False)

    def validate(self):
        return self.ingredient_per_category_validator
