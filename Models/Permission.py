# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, text, String, Boolean
from sqlalchemy.ext.declarative import declarative_base
from wheezy.validation import Validator
from wheezy.validation.rules import required
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class Permission(Base, DictSerializableMixin):
    __tablename__ = 'permissions'

    permission_validator = Validator({
        'name': [required]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('permissions_id_seq'::regclass)"))
    name = Column(String(129), nullable=False)
    create = Column(Boolean, nullable=False, default=False)
    read = Column(Boolean, nullable=False, default=False)
    update = Column(Boolean, nullable=False, default=False)
    delete = Column(Boolean, nullable=False, default=False)
    level = Column(Integer, nullable=False, default=0)

    def validate(self):
        return self.permission_validator
