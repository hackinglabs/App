# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey, Integer, String, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import length, required, int_adapter

from Classes.Core.SQLAlchemyMixins import DictSerializableMixin
from Models.City import City

Base = declarative_base()


class Area(Base, DictSerializableMixin):
    __tablename__ = 'areas'

    area_validator = Validator({
        "name": [required, length(max=20)],
        "city_id": [required, int_adapter(required)]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('areas_id_seq'::regclass)"))
    name = Column(String(45), nullable=False)
    city_id = Column(Integer, ForeignKey(City.id), nullable=False)
    city = relationship(City, backref="areas")

    def validate(self):
        return self.area_validator
