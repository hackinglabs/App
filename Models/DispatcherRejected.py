# -*- coding: utf-8 -*-
import arrow
from sqlalchemy import Column, Date, ForeignKey, Integer, text, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter

from Classes.Core.SQLAlchemyMixins import DictSerializableMixin
from User import User

Base = declarative_base()


class DispatcherRejected(Base, DictSerializableMixin):
    __tablename__ = 'dispatcher_rejected'

    dispatcher_rejected_validation = Validator({
        "user_id": [required, int_adapter(required)],
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('driver_vehicles_id_seq'::regclass)"))
    user_id = Column(ForeignKey(User.id), nullable=False)
    created_at = Column(Date, nullable=False, default=arrow.now('America/Santiago').to('utc').datetime)
    order_id = Column(String)
    user = relationship(User, backref="dispatcher_rejected")

    def validate(self):
        return self.dispatcher_rejected_validation

