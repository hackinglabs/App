# -*- coding: utf-8 -*-
from geoalchemy2 import Geography, WKTElement
from geoalchemy2.shape import to_shape
from sqlalchemy import Column, ForeignKey, Integer, String, text, Boolean, Text
from sqlalchemy import event
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Models.Area import Area
from User import User

Base = declarative_base()


class DispatchAddress(Base, DictSerializableMixin):
    __tablename__ = 'dispatch_addresses'

    dispatch_address_validator = Validator({
        "name": [required],
        "address": [required],
        "telephone": [required, int_adapter(required)],
        'user_id': [required, int_adapter(required)],
        'area_id': [required, int_adapter(required)]
    })

    not_updatable = {
        'user_id': {"system": 100}
    }

    id = Column(Integer, primary_key=True, server_default=text("nextval('dispatch_addresses_id_seq'::regclass)"))
    name = Column(String(45), nullable=False)
    address = Column(Text, nullable=False)
    reference = Column(Text, default=" ")
    telephone = Column(Integer, nullable=False)
    is_default = Column(Boolean, default=False)
    user_id = Column(ForeignKey(User.id), nullable=False)
    area_id = Column(ForeignKey(Area.id), nullable=False)
    block = Column(String, nullable=True, default=" ")
    apartament = Column(String, nullable=True, default=" ")
    coordinates = Column(Geography(geometry_type='POINT', srid=4326), nullable=False)

    area = relationship(Area, backref="dispatch_addresses")
    user = relationship(User, backref="dispatch_addresses")

    def to_xy(self):
        return to_shape(self.coordinates)

    def to_geo(self):
        shape = to_shape(self.coordinates)
        # Postgis save like lon, lat... and we need it like lat, lon
        from Classes.Geo import Geo
        return Geo().add(shape.y, shape.x, str(self.id))

    def validate(self):
        return self.dispatch_address_validator


@event.listens_for(DispatchAddress, 'before_insert')
@event.listens_for(DispatchAddress, 'before_update')
def receive_before_insert(mapper, connection, target):
    if isinstance(target.coordinates, basestring):
        # They send us lat, lon.. and postgis save it like lon, lat.
        # FUCKING POSTGIS
        xy = target.coordinates.split(" ")
        target.coordinates = WKTElement('Point(%s %s)' % (xy[1], xy[0],), srid=4326)
