from sqlalchemy import Column, ForeignKey, Integer, Boolean, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

from Models.Product import Product
from Models.Tag import Tag
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class ProductTag(Base, DictSerializableMixin):
    __tablename__ = 'product_tags'

    product_tags_validator = Validator({
        'tag_id': [required, int_adapter(required)],
        'product_id': [required, int_adapter(required)],
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('product_tags_id_seq'::regclass)"))
    tag_id = Column(Integer, ForeignKey(Tag.id), primary_key=True)
    product_id = Column(Integer, ForeignKey(Product.id), primary_key=True)
    enabled = Column(Boolean, default=True)
    tag = relationship(Tag, backref="product_tags", uselist=False)
    product = relationship(Product, backref='product_tags', uselist=False, cascade="delete")

    def validate(self):
        return self.product_tags_validator
