# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey, Integer, String, text, Text, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from Subsidiary import Subsidiary
from wheezy.validation import Validator
from wheezy.validation.rules import length, required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class Category(Base, DictSerializableMixin):
    __tablename__ = 'categories'

    category_validator = Validator({
        "name": [required, length(max=45)],
        "subsidiary_id": [required, int_adapter(required)],
        "position" : [required, int_adapter(required)]
    })

    image_fields = {
        'image': False
    }

    id = Column(Integer, primary_key=True,
                server_default=text("nextval('categories_id_seq'::regclass)"))
    name = Column(String(45), nullable=False)
    position = Column(Integer)
    description = Column(Text)
    image = Column(String)
    subsidiary_id = Column(ForeignKey(Subsidiary.id), nullable=False)
    subsidiary = relationship(Subsidiary,
                              backref=backref("categories", order_by=position))
    enabled = Column(Boolean, default=True)

    def __repr__(self):
        return self.name

    def validate(self):
        return self.category_validator