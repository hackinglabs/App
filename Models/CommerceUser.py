from sqlalchemy import Column, ForeignKey, Integer, text, Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter

from Models.User import User
from Models.Commerce import Commerce
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin


Base = declarative_base()


class CommerceUser(Base, DictSerializableMixin):
    __tablename__ = 'commerce_users'

    commerce_user_validator = Validator({
        "user_id": [required, int_adapter(required)],
        "commerce_id": [required, int_adapter(required)]
    })

    id = Column(Integer, primary_key=True,
                server_default=text("nextval('commerce_users_id_seq'::regclass)"))

    user_id = Column(Integer, ForeignKey(User.id), nullable=False)
    commerce_id = Column(Integer, ForeignKey(Commerce.id), primary_key=True)

    user = relationship(User, backref='commerce_users')
    commerce = relationship(Commerce, backref='commerce_users', uselist=False)
    registration_date = Column(Date, nullable=False,
                               server_default=text("('now'::text)::date"))

    def validate(self):
        return self.commerce_user_validator
