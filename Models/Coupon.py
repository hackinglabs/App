# -*- coding: utf-8 -*-
import arrow
import enum
from sqlalchemy import ForeignKey, Column, Date, Integer, text, String, Boolean, Float, Enum
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter

from Classes.Core.SQLAlchemyMixins import DictSerializableMixin
from Models.System import System

Base = declarative_base()


class Coupon(Base, DictSerializableMixin):
    __tablename__ = 'coupons'

    coupon_validator = Validator({
        "code": [required],
        "expiration_date": [required],
        "stock": [required],
        "amount": [required],
        "type_id": [required],
        "coupon_type": [required]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('coupons_id_seq'::regclass)"))
    code = Column(String(45), nullable=False)
    description = Column(String, nullable=True)
    registration_date = Column(Date, nullable=False, default=arrow.now('America/Santiago').to('utc').datetime)
    with_expiration = Column(Boolean, default=True)
    expiration_date = Column(Date, nullable=False)
    stock = Column(Integer, nullable=False)
    with_percentage = Column(Boolean, nullable=False, default=True)
    amount = Column(Float, nullable=False)
    coupon_type = Column(Enum("COUNTRY", "STATE", "CITY", "AREA",
                              "COMMERCE", "SUBSIDIARY", name="coupon_discount_type"))
    type_id = Column(Integer, nullable=False)
    discount_allowed = Column(Boolean, default=False)
    allow_pickup = Column(Boolean, default=False)
    allow_dispatch = Column(Boolean, default=True)
    enabled = Column(Boolean, default=True)
    system_id = Column(ForeignKey(System.id), nullable=False)
    system = relationship(System, backref='coupons')

    def validate(self):
        return self.coupon_validator
