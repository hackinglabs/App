# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey, Integer, String, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from Models.State import State
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin


Base = declarative_base()


class City(Base, DictSerializableMixin):
    __tablename__ = 'cities'

    id = Column(Integer, primary_key=True,
                server_default=text("nextval('cities_id_seq'::regclass)"))
    name = Column(String(45), nullable=False)
    state_id = Column(ForeignKey(State.id), nullable=False)
    state = relationship(State, backref="cities")
