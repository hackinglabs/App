# -*- coding: utf-8 -*-
import arrow
from sqlalchemy import Column, Date, ForeignKey, Integer, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from User import User
from Vehicle import Vehicle
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class DriverVehicle(Base, DictSerializableMixin):
    __tablename__ = 'driver_vehicles'

    driver_vehicle_validator = Validator({
        "assignation_date": [required],
        "user_id": [required, int_adapter(required)],
        "vehicle_id": [required, int_adapter(required)]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('driver_vehicles_id_seq'::regclass)"))
    assignation_date = Column(Date, nullable=False, default=arrow.now('America/Santiago').to('utc').datetime)
    vehicle_id = Column(ForeignKey(Vehicle.id), nullable=False)
    user_id = Column(ForeignKey(User.id), nullable=False)
    user = relationship(User,backref=backref("driver_vehicles", uselist=True, order_by="DriverVehicle.assignation_date"))
    vehicle = relationship(Vehicle,
                           backref=backref("driver_vehicles", uselist=True, order_by="DriverVehicle.assignation_date"))

    def validate(self):
        return self.driver_vehicle_validator

