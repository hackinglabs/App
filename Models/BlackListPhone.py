# -*- coding: utf-8 -*-
import arrow
from sqlalchemy import Column, Date, ForeignKey, Integer, String, text
from sqlalchemy.ext.declarative import declarative_base
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter

from Classes.Core.SQLAlchemyMixins import DictSerializableMixin
from User import User

Base = declarative_base()


class BlackListPhone(Base, DictSerializableMixin):
    __tablename__ = 'black_list_phone'

    black_list_validator = Validator({
        'telephone': [required, int_adapter(required)],
        'order_id': required,
        'user_id': [required, int_adapter(required)],
    })

    id = Column(Integer, primary_key=True,
                server_default=text("nextval('categories_id_seq'::regclass)"))
    user_id = Column(ForeignKey(User.id), nullable=False)
    order_id = Column(String)
    telephone = Column(Integer())
    created_at = Column(Date, nullable=False, default=arrow.now('America/Santiago').to('utc').datetime)

    def validate(self):
        return self.black_list_validator
