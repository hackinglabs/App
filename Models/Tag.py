# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey, Integer, String, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter

from Classes.Core.SQLAlchemyMixins import DictSerializableMixin
from System import System

Base = declarative_base()


class Tag(Base, DictSerializableMixin):
    __tablename__ = 'tags'

    tag_validator = Validator({
        'name': [required],
        'system_id': [required, int_adapter(required)]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('tags_id_seq'::regclass)"))
    name = Column(String(45), nullable=False)
    image = Column(String, nullable=True)
    system_id = Column(ForeignKey(System.id), nullable=False)
    system = relationship(System, backref="tags")

    def validate(self):
        return self.tag_validator
