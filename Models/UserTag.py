# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey, Integer, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Models.Tag import Tag
from User import User

Base = declarative_base()


class UserTag(Base, DictSerializableMixin):
    __tablename__ = 'users_tag'
    id = Column(Integer, primary_key=True, server_default=text("nextval('users_tag_id_seq'::regclass)"))
    user_id = Column(Integer, ForeignKey(User.id), primary_key=True)
    tags_id = Column(Integer, ForeignKey(Tag.id), primary_key=True)
    tag = relationship(Tag, backref='users_tag')
    user = relationship(User, backref='users_tag')
