# -*- coding: utf-8 -*-

from geoalchemy2 import Geography
from geoalchemy2.shape import to_shape
from sqlalchemy import Column, ForeignKey, Integer, String, text, Boolean, Float, Text, BigInteger
from sqlalchemy.dialects.postgresql.json import JSON
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from Classes.Actions import SubsidiaryActions
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin
from Classes.Geo import Geo
from Commerce import Commerce
from Area import Area
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter

Base = declarative_base()


class Subsidiary(SubsidiaryActions, Base, DictSerializableMixin):
    __tablename__ = 'subsidiaries'

    subsidiary_validator = Validator({
        "alias": [required],
        "address": [required],
        "telephone": [required],
        "commerce_id": [required, int_adapter(required)],
        "area_id": [required, int_adapter(required)],
        "pap": [required, int_adapter(required)],
        "coordinates": [required],
        "average_time": [required, int_adapter(required)]
    })

    image_fields = {
        'facebook_image': False,
        'banner': False,
        'cover': False
    }

    id = Column(Integer(), primary_key=True, server_default=text("nextval('subsidiaries_id_seq'::regclass)"))
    alias = Column(String(45), nullable=False)
    address = Column(String(), nullable=False)
    telephone = Column(BigInteger(), nullable=False)
    open_hours = Column(JSON)
    average_time = Column(Integer)
    pap = Column(Float)
    is_premium = Column(Boolean, nullable=True, default=False)
    coordinates = Column(Geography(geometry_type='POINT', srid=4326), nullable=False)
    commerce_id = Column(ForeignKey(Commerce.id), nullable=False)
    commerce = relationship(Commerce, backref="subsidiaries")
    area_id = Column(ForeignKey(Area.id), nullable=False)
    area = relationship(Area, backref="subsidiaries")
    headquarter = Column(Boolean, nullable=False, default=False)
    enabled = Column(Boolean(), nullable=False, server_default=text("true"))
    short_description = Column(String(50))
    description = Column(Text)
    rating_average = Column(Integer, default=0)
    is_active = Column(Boolean, default=False)
    correlative = Column(Integer, default=0)
    facebook_title = Column(String, nullable=True)
    facebook_url = Column(String, nullable=True)
    facebook_image = Column(String, nullable=True)
    facebook_description = Column(String, nullable=True)
    facebook_sitename = Column(String, nullable=True)
    banner = Column(JSON)
    suggestion_type_vehicle = Column(String(12), default='')
    suggestion_qualification_vehicle = Column(String(6), default='')
    suggestion_require = Column(Boolean, default=False)
    payment_term = Column(Integer, nullable=True, default=30)
    overprice_percentage = Column(Float, nullable=True, default=0.0)
    dxl = Column(Boolean, default=False)
    dxl_overprice = Column(Integer, default=0)
    cover = Column(String, nullable=True)


    def validate(self):
        return self.subsidiary_validator

    def to_geo(self):
        shape = to_shape(self.coordinates)
        # Postgis save like lon, lat... and we need it like lat, lon
        return Geo().add(shape.y, shape.x, str(self.id))


Subsidiary.register()
