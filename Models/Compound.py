import arrow
from sqlalchemy import Column, ForeignKey, Integer, text, Date, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter

from Models import Category
from Models.User import User
from Models.Commerce import Commerce
from Models.Subsidiary import Subsidiary
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin


Base = declarative_base()


class Compound(Base, DictSerializableMixin):
    __tablename__ = 'compounds'

    compound_validator = Validator({
        "name": [required],
        "subsidiary_id": [required, int_adapter(required)],
        "category_id": [required]
    })

    id = Column(Integer, primary_key=True,
                server_default=text("nextval('compounds_id_seq'::regclgass)"))
    name = Column(String, nullable=False)
    price = Column(Float, nullable=False, default=0)
    category_id = Column(ForeignKey(Category.id), nullable=False)
    subsidiary_id = Column(ForeignKey(Subsidiary.id), nullable=False)
    registration_date = Column(Date, default=arrow.now('America/Santiago').to('utc').datetime)
    subsidiary = relationship(Subsidiary, backref="compounds")
    category = relationship(Category, backref="compounds")

    def validate(self):
        return self.compound_validator
