# -*- coding: utf-8 -*-
import arrow
from sqlalchemy import Column, Date, ForeignKey, Integer, String, text, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_method
from sqlalchemy.orm import relationship
from User import User
from wheezy.validation import Validator
from wheezy.validation.rules import length, required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class Vehicle(Base, DictSerializableMixin):
    __tablename__ = 'vehicles'

    vehicle_validator = Validator({
        'patent': [required, length(max=8)],
        'trademark': [required],
        'year': [required, int_adapter(required)],
        'user_id': [required, int_adapter(required)],
        'model': [required],
        'type': [required]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('vehicles_id_seq'::regclass)"))
    patent = Column(String(10), nullable=False)
    registration_date = Column(Date, nullable=False, default=arrow.now('America/Santiago').to('utc').datetime)
    trademark = Column(String(45), nullable=False)
    enabled = Column(Boolean, server_default=text("true"))
    year = Column(Integer, nullable=False)
    user_id = Column(ForeignKey(User.id), nullable=False)  # Owner
    type = Column(String(45), nullable=False)
    model = Column(String(123), nullable=False)
    qualification = Column(String(6), default='', nullable=False)
    status = Column(String(60), nullable=False, default='Ok')

    owner = relationship(User, backref="vehicles")

    def validate(self):
        return self.vehicle_validator

    @hybrid_method
    def last_driver(self):
        return self.driver_vehicles[-1]
