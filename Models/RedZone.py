# -*- coding: utf-8 -*-
from geoalchemy2 import Geography
from sqlalchemy import Column, Integer, text, String
from sqlalchemy.ext.declarative import declarative_base

from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class RedZone(Base, DictSerializableMixin):
    __tablename__ = 'red_zones'

    id = Column(Integer, primary_key=True, server_default=text("nextval('red_zones_id_seq'::regclass)"))
    name = Column(String(45), nullable=True)
    area = Column(Geography(geometry_type='POLYGON', srid=4326), nullable=False)