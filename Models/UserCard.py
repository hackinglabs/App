from sqlalchemy import Column, ForeignKey, Integer, String, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter

from Classes.Core.SQLAlchemyMixins import DictSerializableMixin
from Models.User import User

Base = declarative_base()


class UserCard(Base, DictSerializableMixin):
    __tablename__ = 'user_cards'

    user_card_validator = Validator({
        'user_id': [required, int_adapter(required)],
        'tbk_user': [required],
        'auth_code': [required],
        'credit_card': [required],
        'last_digits': [required]
    })

    hybrid_fields = [
        'card'
    ]

    hidden_fields = [
        'auth_code',
        'credit_card',
        'last_digits',
        'tbk_user',
        'user_id'
    ]

    id = Column(Integer, primary_key=True,
                server_default=text("nextval('user_cards_id_seq'::regclass)"))
    user_id = Column(ForeignKey(User.id), nullable=False, unique=True)
    tbk_user = Column(String, nullable=False, unique=True)
    auth_code = Column(Integer, nullable=False)
    credit_card = Column(String, nullable=False)
    last_digits = Column(Integer, nullable=False)

    user = relationship(User, backref='user_cards')

    @hybrid_property
    def card(self):
        if not self.last_digits and not self.credit_card:
            return ''
        if not self.last_digits and self.credit_card:
            return self.credit_card
        if self.last_digits and not self.credit_card:
            return '************' + str(self.last_digits)
        else:
            return self.credit_card + ' ************' + str(self.last_digits)

    def validate(self):
        return self.user_card_validator
