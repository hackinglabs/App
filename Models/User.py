# -*- coding: utf-8 -*-

from argon2 import PasswordHasher
from sqlalchemy import Boolean, Column, Date, Integer, String, text
from sqlalchemy import event
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from wheezy.validation import Validator
from wheezy.validation.rules import length, required, email, int_adapter, range
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Helpers import Security

Base = declarative_base()


class User(Base, DictSerializableMixin):
    __tablename__ = 'users'

    user_validator = Validator({
        'telephone': [int_adapter(range(min=10000000, max=99999999999))],
        'email': [required, email],
        'password': [required, length(min=8)],
    })

    default_fields = [
        'id',
        'first_name',
        'last_name',
        'email',
        'avatar',
        'nin',
        'gender',
        'birth_date'
    ]

    hidden_fields = [
        'password',
        'xcsrf',
        'recover_token',
        'nin',
    ]

    unique = [
        "email"
    ]

    restricted_update = {
        "enabled": {
            "edit_enabled": 50,
            "system": 100
        },
        "registration_date": {"system": 50},
    }

    restricted_insert = {
        "registration_date": {"system": 100},
        "tutorial": {"system": 100},
        "recover_token": {"system": 100},
        "xcsrf": {"system": 100},
        "enabled": {"system": 100},
        "active": {"system": 100},
    }

    image_fields = {
        'avatar': False
    }

    id = Column(Integer, primary_key=True,
                server_default=text("nextval('users_id_seq'::regclass)"))
    first_name = Column(String(45))
    last_name = Column(String(45))
    gender = Column(String(1))
    nin = Column(String(45))
    birth_date = Column(Date)
    email = Column(String(45), nullable=False)
    password = Column(String(128), nullable=False)
    facebook = Column(String(120))
    google = Column(String(120))
    xcsrf = Column(String(128))
    recover_token = Column(String(128))
    avatar = Column(String(45))
    enabled = Column(Boolean, default=True)
    active = Column(Boolean, default=False)
    tutorial = Column(Boolean, default=True)
    telephone = Column(Integer, nullable=True)
    registration_date = Column(Date, server_default=text("('now'::text)::date"))

    def unique(self):
        return ["email"]

    def __repr__(self):
        return '%s %s' % (self.first_name, self.last_name,)

    def validate(self):
        return self.user_validator

    @hybrid_property
    def fullname(self):
        if self.first_name is None and self.last_name is None:
            return ''
        elif self.first_name is None:
            return self.last_name
        elif self.last_name is None:
            return self.first_name
        else:
            return "%s %s" % (self.first_name, self.last_name)


@event.listens_for(User, 'before_insert')
@event.listens_for(User, 'before_update')
def receive_before_insert(mapper, connection, target):
    email = target.email
    if email and isinstance(email, basestring):
        target.email = email.lower()

    if not target.password.startswith("$argon2") and len(target.password) < 120:
        ph = PasswordHasher(time_cost=15, hash_len=40, salt_len=36)
        target.password = ph.hash(target.password)
    target.xcsrf = Security.get_csrf()
