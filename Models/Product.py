# -*- coding: utf-8 -*-

from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, text, Text, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Models.SubCategory import SubCategory

Base = declarative_base()


class Product(Base, DictSerializableMixin):
    __tablename__ = 'products'

    product_validator = Validator({
        "sub_category_id": [required, int_adapter(required)],
        'name': [required],
        'position': [required, int_adapter(required)],
        "price": [required, int_adapter(required)],
    })

    image_fields = {
        'image': False
    }

    id = Column(Integer, primary_key=True, server_default=text("nextval('products_id_seq'::regclass)"))
    enabled = Column(Boolean, nullable=False, default=True)
    name = Column(String(120), nullable=False)
    description = Column(Text, nullable=True)
    is_compounds = Column(Boolean, nullable=True, default=False)
    ingredients_extra = Column(Integer, nullable=False, default=0)
    image = Column(String, nullable=False, default='')
    price = Column(Float, nullable=False)
    position = Column(Integer, nullable=False)
    sub_category_id = Column(ForeignKey(SubCategory.id), nullable=False)
    sub_category = relationship(SubCategory, backref=backref("products", order_by=position))
    options_ingredient = Column(Boolean, default=False)
    stock = Column(Boolean, default=True)

    def validate(self):
        return self.product_validator

