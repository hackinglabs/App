from cassandra.cqlengine.columns import Text, Integer, Float, Text, Boolean
from cassandra.cqlengine.usertype import UserType


class Discount(UserType):
    __keyspace__ = 'dxpress'
    id = Integer()
    our_discount = Boolean()
    discount_type = Text()
    type_id = Integer()
    amount = Float()
