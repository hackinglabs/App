import arrow
from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model


class SubsidiaryViewOffline(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'subsidiary_view_offline'
    subsidiary_id = Integer(primary_key=True, partition_key=True, index=True)
    date = DateTime(default=arrow.now('America/Santiago').to('utc').datetime)