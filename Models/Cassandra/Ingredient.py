from cassandra.cqlengine.columns import Text, Integer, Float, Boolean
from cassandra.cqlengine.usertype import UserType


class Ingredient(UserType):
    __keyspace__ = 'dxpress'
    id = Integer()
    name = Text()
    price = Float()
    ingredients_per_category_id = Integer()
    is_base = Boolean()