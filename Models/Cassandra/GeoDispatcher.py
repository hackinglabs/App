# -*- coding: utf-8 -*-
import arrow
from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model
from Point import Point

class GeoDispatcher(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'geodispatcher'
    id = TimeUUID(primary_key=True)
    dispatcher_id = Integer(index=True)
    date = DateTime(default=arrow.now('America/Santiago').to('utc').datetime)
    geo = UserDefinedType(Point)
    vehicle_id = Integer()

