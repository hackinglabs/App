import arrow
from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model
from uuid import uuid1


class DispatcherDisconnect(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'dispatcher_disconnect'
    id = UUID(primary_key=True, default=lambda: uuid1())
    dispatcher_id = Integer(index=True)
    order_id = UUID(primary_key=True, index=True)
    date = DateTime(default=arrow.now('America/Santiago').to('utc').datetime)
    vehicle_id = Integer()
