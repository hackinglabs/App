from cassandra.cqlengine.columns import Text, Integer, Float, Boolean, DateTime
from cassandra.cqlengine.usertype import UserType


class Payment(UserType):
    __keyspace__ = 'dxpress'
    id = Text()
    creation = DateTime()
    type = Text()
    app_url = Text()
    payment_url = Text()
    simplified_transfer_url = Text()
    transfer_url = Text()
    ntoken = Text()
    card_number = Integer()
    card_expiration = Text()
    authorization_code = Integer()
    payment_type = Text()
    response_code = Integer()
    shares_amount = Float()
    shares_number = Integer()
    accounting_date = Text()
    transaction_date = Text()