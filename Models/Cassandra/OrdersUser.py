# -*- coding: utf-8 -*-

from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model
from Address import Address
from uuid import uuid1

from Models.Cassandra.Coupon import Coupon
from Payment import Payment
from Product import Product
from Discount import Discount
from Subsidiary import Subsidiary
from User import User
import arrow


class OrdersUser(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'orders_by_user'
    id = UUID(default=lambda: uuid1(), primary_key=True, index=True)
    correlative = Integer()
    user_id = Integer(primary_key=True, index=True)
    order_date = DateTime(default=arrow.now('America/Santiago').to('utc').datetime)
    products = Set(UserDefinedType(Product))
    subsidiary = UserDefinedType(Subsidiary)
    coupon = UserDefinedType(Coupon)
    discounts = Set(UserDefinedType(Discount))
    user = UserDefinedType(User)
    dispatcher = UserDefinedType(User)
    address_order = UserDefinedType(Address)
    status = Text()
    payment = UserDefinedType(Payment)
    client_code = Text()
    user_code = Text()
    dispatch_amount = Float()
    reference = Text()
    order_paid = DateTime()
    order_ready = DateTime()
    order_confirmation = DateTime()
    order_preparation = DateTime()
    dispatch_date = DateTime()
    delivery_date = DateTime()
    programmed = Map(Text(), DateTime())
    is_mobile = Boolean()
    pickup = Boolean()
    total = Float()
    subtotal = Float()
    subtotal_with_discount = Float()
    subtotal_without_discounts = Float()

