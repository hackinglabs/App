import arrow
from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model

from Models.Cassandra.Coupon import Coupon


class CouponUser(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'coupons_users'
    order_id = UUID(index=True)
    code = Text(index=True)
    date = DateTime(default=arrow.now('America/Santiago').to('utc').datetime, index=True)
    user_id = Integer(index=True)
    coupon_id = Integer(primary_key=True)
    coupon = UserDefinedType(Coupon)


