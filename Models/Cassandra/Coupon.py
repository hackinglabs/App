from cassandra.cqlengine.columns import Text, Integer, Float, Text, Boolean
from cassandra.cqlengine.usertype import UserType


class Coupon(UserType):
    __keyspace__ = 'dxpress'
    id = Integer()
    code = Text()
    with_percentage = Boolean()
    coupon_type = Text()
    type_id = Integer()
    amount = Float()

