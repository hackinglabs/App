# -*- coding: utf-8 -*-

from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model
import arrow
from Models.Cassandra import Product, Payment

class CommerceSummary(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'commerce_summary'
    commerce_id = Integer(primary_key=True, partition_key=True, index=True)
    subsidiary_id = Integer(primary_key=True, partition_key=True, index=True)
    date = DateTime(primary_key=True, default=arrow.now('America/Santiago').to('utc').datetime, clustering_order="DESC")
    products = Set(UserDefinedType(Product))
    payment = UserDefinedType(Payment)
    subtotal_with_discount = Float()
    total = Float()
    subtotal = Float()
