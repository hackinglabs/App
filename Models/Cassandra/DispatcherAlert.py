import arrow
from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model
from uuid import uuid1
from cassandra.cqlengine.usertype import UserType


from Models.Cassandra import Point


class DispatcherAlert(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'dispatcher_alert'
    id = UUID(primary_key=True, default=lambda: uuid1())
    dispatcher_id = Integer(index=True)
    order_id = UUID(index=True)
    date = DateTime(default=arrow.now('America/Santiago').to('utc').datetime)
    check = Boolean(default=False)
    geo = UserDefinedType(Point)
