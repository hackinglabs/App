# -*- coding: utf-8 -*-

from cassandra.cqlengine.columns import Float
from cassandra.cqlengine.usertype import UserType


class Point(UserType):
    __keyspace__ = 'dxpress'
    x = Float()
    y = Float()
