# -*- coding: utf-8 -*-

from cassandra.cqlengine import management
from cassandra.cqlengine.columns import *
from cassandra.cqlengine.usertype import UserType

from Models.Cassandra.Commerce import Commerce


class Subsidiary(UserType):
    __keyspace__ = 'dxpress'
    id = Integer()
    fullname = Text()
    commerce_id = Integer()
    commerce = UserDefinedType(Commerce)
    telephone = BigInt()
    alias = Text()
    overprice_percentage = Float()

