# -*- coding: utf-8 -*-

from cassandra.cqlengine.columns import *
from cassandra.cqlengine.usertype import UserType
from Point import Point


class User(UserType):
    __keyspace__ = 'dxpress'
    id = Integer()
    name = Text()
    email = Text()
    avatar = Text()
    patent = Text()
    vehicle_id = Integer()
    telephone = BigInt()
