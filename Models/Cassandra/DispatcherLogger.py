import arrow
from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model


class DispatcherLogger(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'dispatchers_logger'
    user_id = Integer(primary_key=True)
    date = DateTime()
    tag = Text(index=True)
    message = Text(primary_key=True)