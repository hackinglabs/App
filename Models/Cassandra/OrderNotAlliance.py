from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model
from Models.Cassandra import Point, Product, Payment, User
from Models.Cassandra.Discount import Discount
from Models.Cassandra.Subsidiary import Subsidiary


class OrderNotAlliance(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'orders_not_alliance'
    id = UUID(index=True)
    subsidiary_id = Integer(index=True)
    dispatcher_id = Integer(primary_key=True)
    order_date = DateTime(primary_key=True, clustering_order="DESC")
    correlative = Integer()
    order_ready = DateTime()
    order_paid = DateTime()
    discounts = Set(UserDefinedType(Discount))
    order_confirmation = DateTime()
    order_preparation = DateTime()
    dispatcher = UserDefinedType(User)
    subsidiary = UserDefinedType(Subsidiary)
    products = Set(UserDefinedType(Product))
    payment = UserDefinedType(Payment)
    status = Text()
    amount_paid = Float()
    subtotal = Float()
    subtotal_with_discount = Float()
    subtotal_without_discounts = Float()
