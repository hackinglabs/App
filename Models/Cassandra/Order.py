# -*- coding: utf-8 -*-

from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model
from Address import Address
from uuid import uuid1

from Discount import Discount
from Product import Product
from Subsidiary import Subsidiary
from User import User
from Payment import Payment
from Coupon import Coupon
import arrow


class Order(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'orders'
    id = UUID(primary_key=True, default=lambda: uuid1())
    correlative = Integer()
    order_date = DateTime(default=arrow.now('America/Santiago').to('utc').datetime)
    products = Set(UserDefinedType(Product))
    subsidiary = UserDefinedType(Subsidiary)
    coupon = UserDefinedType(Coupon)
    discounts = Set(UserDefinedType(Discount))
    total = Float()
    subtotal = Float()
    user = UserDefinedType(User)
    dispatcher = UserDefinedType(User)
    address_order = UserDefinedType(Address)
    status = Text()
    payment = UserDefinedType(Payment)
    client_code = Text()
    user_code = Text()
    dispatch_amount = Float()
    distance = Float()
    reference = Text()
    order_paid = DateTime()
    order_preparation = DateTime()
    order_ready = DateTime()
    order_confirmation = DateTime()
    dispatch_date = DateTime()
    delivery_date = DateTime()
    programmed = Map(Text(), DateTime())
    is_mobile = Boolean()
    pickup = Boolean()
    subtotal_with_discount = Float()
    subtotal_without_discounts = Float()
    dxl = Boolean(default=False)
    dxl_overprice = Float(default=0)




