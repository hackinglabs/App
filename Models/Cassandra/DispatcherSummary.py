import arrow
from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model
from uuid import uuid1


class DispatcherSummary(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'dispatcher_summary'
    id = UUID(primary_key=True, default=lambda: uuid1())
    dispatcher_id = Integer(index=True)
    vehicle_id = Integer(index=True)
    order_id = UUID()
    dispatch_amount = Float()
    dispatch_date = DateTime()
    delivery_date = DateTime()
    subsidiary_name = Text()
    subsidiary_address = Text()
    alliance = Boolean(index=True)
    date = DateTime(default=arrow.now('America/Santiago').to('utc').datetime)

