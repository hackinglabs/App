# -*- coding: utf-8 -*-+

from cassandra.cqlengine.columns import *
from cassandra.cqlengine.usertype import UserType
from Point import Point


class Address(UserType):
    __keyspace__ = 'dxpress'
    name = Text()
    address = Text()
    reference = Text()
    apartment = Text()
    block = Text()
    area_id = Integer()
    geo = UserDefinedType(Point)
    telephone = BigInt()
    id = Integer()
