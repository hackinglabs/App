# -*- coding: utf-8 -*-

from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model
from Product import Product


class WebpayOrder(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'webpay_orders'
    token_ws = Text(primary_key=True)
    order_id = UUID()
    correlative = Integer()
    dispatch_amount = Float()
    subtotal = Float()
    total = Float()
    products = Set(UserDefinedType(Product))
    card_number = Integer()
    card_expiration = Text()
    authorization_code = Integer()
    payment_type = Text()
    response_code = Text()
    shares_amount = Float()
    shares_number = Integer()
    accounting_date = Text()
    transaction_date = Text()
    subtotal_with_discount = Float()
    subtotal_without_discounts = Float()



