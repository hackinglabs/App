from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model
from Models.Cassandra import Point, Product, Payment, User, Address
from Models.Cassandra.Discount import Discount


class ClientOrders(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'client_orders'
    id = UUID(index=True)
    subsidiary_id = Integer(primary_key=True, partition_key=True, index=True)
    user = UserDefinedType(User)
    address_order = UserDefinedType(Address)
    order_date = DateTime(primary_key=True, clustering_order="DESC")
    correlative = Integer(index=True)
    order_ready = DateTime()
    order_paid = DateTime()
    discounts = Set(UserDefinedType(Discount))
    order_confirmation = DateTime()
    order_preparation = DateTime()
    dispatcher = UserDefinedType(User)
    products = Set(UserDefinedType(Product))
    payment = UserDefinedType(Payment)
    status = Text()
    pickup = Boolean()
    programmed = Map(Text(), DateTime())
    subtotal = Float()
    subtotal_with_discount = Float()
    alliance = Boolean(index=True)
    subtotal_without_discounts = Float()
    dxl = Boolean(default=False)
    dxl_overprice = Float(default=0)
    reference = Text()
    distance = Float()
    dispatch_amount = Float()
    franchise = Boolean(default=False)
    is_person = Boolean(default=False)
