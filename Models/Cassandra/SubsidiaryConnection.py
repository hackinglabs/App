import arrow
from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model
from uuid import uuid1


class SubsidiaryConnection(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'subsidiary_connections'

    subsidiary_id = Integer(index=True, primary_key=True)
    date = DateTime(default=arrow.now('America/Santiago').to('utc').datetime)
    connected = Boolean(index=True)