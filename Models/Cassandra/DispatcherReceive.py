import arrow
from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model
from uuid import uuid1


class DispatcherReceive(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'dispatcher_receive_order'
    order_id = UUID(index=True, primary_key=True)
    dispatcher_id = Integer(index=True)
    date = DateTime(primary_key=True, index=True, default=arrow.now('America/Santiago').to('utc').datetime)

