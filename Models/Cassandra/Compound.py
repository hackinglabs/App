from cassandra.cqlengine.columns import Text, Integer, Float
from cassandra.cqlengine.usertype import UserType


class Compound(UserType):
    __keyspace__ = 'dxpress'
    id = Integer()
    name = Text()
    price = Float()
    clarification = Text()
    description = Text()
    option_compound_id = Integer()