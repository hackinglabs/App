# -*- coding: utf-8 -*-


from cassandra.cqlengine.columns import Integer, Text, Float, UserDefinedType, Set
from cassandra.cqlengine.usertype import UserType

from Models.Cassandra.Compound import Compound
from Models.Cassandra.Ingredient import Ingredient


class Product(UserType):
    __keyspace__ = 'dxpress'
    id = Integer()
    name = Text()
    price = Float()
    quantity = Integer()
    clarification = Text()
    ingredients_extra = Integer()
    subcategory_id = Integer()
    description = Text()
    compounds = Set(UserDefinedType(Compound, required=False))
    ingredients = Set(UserDefinedType(Ingredient, required=False))