import arrow
from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model
from uuid import uuid1
from Models.Cassandra import Point


class TagNoFound(Model):
    __keyspace__ = 'dxpress'
    __table_name__ = 'tag_no_found'
    id = UUID(primary_key=True, default=lambda: uuid1())
    tag = Text(index=True)
    geo = UserDefinedType(Point)
    date = DateTime(default=arrow.now('America/Santiago').to('utc').datetime)