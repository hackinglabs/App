from cassandra.cqlengine.columns import *
from cassandra.cqlengine.models import Model

from Models.Cassandra.Address import Address
from Models.Cassandra.Product import Product
from Models.Cassandra.Subsidiary import Subsidiary
from Models.Cassandra.User import User
from Point import Point


class Test(Model):
    __keyspace__ = 'dondeviene'
    __table_name__ = 'test'
    id = Integer(primary_key=True)
    name = Text(required=False)
    geo = UserDefinedType(Point, required=False)
    address_order = UserDefinedType(Address)
    products = Set(UserDefinedType(Product))
    subsidiary = UserDefinedType(Subsidiary)
    user = UserDefinedType(User)
    dispatcher = UserDefinedType(User)
    status = Text()
    client_code = Text()
    user_code = Text()
    dispatch_amount = Integer()
    reference = Text()
    dispatch_date = DateTime()
    delivery_date = DateTime()