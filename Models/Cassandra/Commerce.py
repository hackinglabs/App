from cassandra.cqlengine.columns import *
from cassandra.cqlengine.usertype import UserType


class Commerce(UserType):
    __keyspace__ = 'dxpress'
    id = Integer()
    logo = Text()
    name = Text()
    alliance = Boolean()
    franchise = Boolean()
    is_bolean = Boolean()
    is_person = Boolean()

