# -*- coding: utf-8 -*-
import arrow
import enum
from sqlalchemy import Column, ForeignKey, Date, Integer, text, String, Boolean, Float, Enum, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter

from Classes.Core.SQLAlchemyMixins import DictSerializableMixin
from Models.System import System

Base = declarative_base()


class Discount(Base, DictSerializableMixin):
    __tablename__ = 'discounts'

    discount_validator = Validator({
        "amount": [required]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('discounts_id_seq'::regclass)"))
    description = Column(Text, nullable=True)
    creation_date = Column(Date, nullable=False, default=arrow.now('utc').datetime)
    with_expiration = Column(Boolean, default=True, nullable=False)
    expiration_date = Column(Date, nullable=False)
    amount = Column(Float, nullable=False)
    minimum_buy = Column(Float, nullable=False, default=0)
    our_discount = Column(Boolean, nullable=False, default=False)
    discount_type = Column(Enum("COUNTRY", "STATE", "CITY", "AREA",
                                "COMMERCE", "SUBSIDIARY", "CATEGORY",
                                "SUBCATEGORY", "PRODUCT", name="coupon_discount_type"))
    type_id = Column(Integer, nullable=False)
    enabled = Column(Boolean, default=True)
    system_id = Column(ForeignKey(System.id), nullable=False, default=1)
    system = relationship(System, backref='discounts')

    def validate(self):
        return self.discount_validator
