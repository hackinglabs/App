from sqlalchemy import Column, ForeignKey, Integer, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required

from Models.Compound import Compound
from Models.OptionCompound import OptionCompound
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class ItemCompound(Base, DictSerializableMixin):
    __tablename__ = 'items_compounds'

    option_compound_validator = Validator({
        "option_compound_id": [required],
        "compound_id": [required],

    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('options_compounds_id_seq'::regclass)"))
    compound_id = Column(ForeignKey(Compound.id), nullable=False)
    option_compound_id = Column(ForeignKey(OptionCompound.id), nullable=False)
    compound = relationship(Compound, backref="items_compounds")
    option_compound = relationship(OptionCompound, backref="items_compounds")

    def validate(self):
        return self.option_compound_validator
