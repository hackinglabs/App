# -*- coding: utf-8 -*-
import arrow
from sqlalchemy import Column, Date, ForeignKey, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Models.User import User

Base = declarative_base()


class OwnerDispatcher(Base, DictSerializableMixin):
    __tablename__ = 'owner_dispatchers'

    owner_dispatcher_validator = Validator({
        "user_id": [required, int_adapter(required)],
        "owner_id": [required, int_adapter(required)]
    })

    user_id = Column(ForeignKey(User.id),nullable=False,primary_key=True)
    owner_id = Column(ForeignKey(User.id),nullable=False,primary_key=True)
    enabled = Column(Boolean, default=True)
    registration_date = Column(Date, nullable=False, default=arrow.now('America/Santiago').to('utc').datetime)

    user = relationship(User, foreign_keys='OwnerDispatcher.user_id')
    owner = relationship(User, foreign_keys='OwnerDispatcher.owner_id')

    def validate(self):
        return self.owner_dispatcher_validator
