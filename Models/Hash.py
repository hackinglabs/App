# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, text, String
from sqlalchemy.ext.declarative import declarative_base
from wheezy.validation import Validator
from wheezy.validation.rules import required
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class Hash(Base, DictSerializableMixin):
    __tablename__ = 'dxl_order_hash'

    hash_validator = Validator({
        "order_id": [required]
    })

    id_temp = Column(Integer, primary_key=True)
    order_id = Column(String, nullable=False);

    def validate(self):
        return self.group_validator
