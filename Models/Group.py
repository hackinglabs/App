# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, text, String
from sqlalchemy.ext.declarative import declarative_base
from wheezy.validation import Validator
from wheezy.validation.rules import required
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class Group(Base, DictSerializableMixin):
    __tablename__ = 'groups'

    group_validator = Validator({
        "name": [required]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('groups_id_seq'::regclass)"))
    name = Column(String(45), nullable=False);

    def validate(self):
        return self.group_validator
