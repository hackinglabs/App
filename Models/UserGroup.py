from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Models import Group
from Models.User import User

Base = declarative_base()


class UserGroup(Base, DictSerializableMixin):
    __tablename__ = 'user_groups'

    user_group_validator = Validator({
        'group_id': [required, int_adapter(required)],
        'user_id': [required, int_adapter(required)],
    })

    group_id = Column(ForeignKey(Group.id), nullable=False, primary_key=True)
    user_id = Column(ForeignKey(User.id), nullable=False, primary_key=True)
    level = Column(Integer, nullable=False, default=0)
    description = Column(String(45), nullable=True)
    user = relationship(User, backref='user_groups')
    group = relationship(Group, backref='user_groups')

    def validate(self):
        return self.user_group_validator
