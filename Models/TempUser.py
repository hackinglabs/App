# -*- coding: utf-8 -*-

from sqlalchemy import Column, Date, ForeignKey, Integer, String, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base
from Models.User import User
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class TempUser(Base, DictSerializableMixin):
    __tablename__ = 'temp_users'

    temp_user_validator = Validator({
        'user_id': [required, int_adapter(required)],
        'first_name': [required],
        'last_name': [required],
        'gender': [required],
        'nin': [required],
        'email': [required],
        'password': [required]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('temp_users_id_seq'::regclass)"))
    user_id = Column(Integer, ForeignKey(User.id), primary_key=True)
    first_name = Column(String(45))
    last_name = Column(String(45))
    gender = Column(String(1))
    nin = Column(String(45))
    birth_date = Column(Date)
    email = Column(String(45), nullable=False)
    password = Column(String(128), nullable=False)
    xcsrf = Column(String(128))
    telephone = Column(Integer, nullable=True)
    registration_date = Column(Date, server_default=text("('now'::text)::date"))
    user = relationship(User, backref="my_dispatchers")

    def validate(self):
        return self.temp_user_validator
