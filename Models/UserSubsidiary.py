# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Models import Subsidiary, User

Base = declarative_base()


class UserSubsidiary(Base, DictSerializableMixin):
    __tablename__ = 'users_subsidiaries'

    user_subsidiary_validator = Validator({
        "user_id": [required],
        "subsidiary_id": [required]
    })

    user_id = Column(Integer, ForeignKey(User.id), nullable=False, primary_key=True)
    subsidiary_id = Column(Integer, ForeignKey(Subsidiary.id), nullable=False, primary_key=True)
    user = relationship(User, backref='users_subsidiaries')
    subsidiary = relationship(Subsidiary, backref='users_subsidiaries')

    def validate(self):
        return self.user_subsidiary_validator
