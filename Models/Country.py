# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, String, text
from sqlalchemy.ext.declarative import declarative_base
from wheezy.validation import Validator
from wheezy.validation.rules import required
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin


Base = declarative_base()


class Country(Base, DictSerializableMixin):
    __tablename__ = 'countries'

    country_validator = Validator({
        "name": [required],
        "phone_code": [required]

    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('countries_id_seq'::regclass)"))
    name = Column(String(45), nullable=False)
    phone_code = Column(Integer)

    default_fields = ['name', 'id']

    def __repr__(self):
        args = ['{}={}'.format(k, repr(v)) for (k, v) in vars(self).items()]
        return 'Country({})'.format(', '.join(args))

    def validate(self):
        return self.country_validator
