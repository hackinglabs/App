# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, String, text, Date, Enum, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Models.Subsidiary import Subsidiary
from Models.User import User
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


# Verificar si el tipo ENUM es soportado por el ORM si no cambiar a numero entero y validar por FyB

class RatingSubsidiary(Base, DictSerializableMixin):
    __tablename__ = 'ratings_subsidiary'

    rating_subsidiary_validator = Validator({
        'user_id': [required, int_adapter(required)],
        'subsidiary_id': [required, int_adapter(required)],
        'qualification': [required, int_adapter(required)]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('ratings_subsidiary_id_seq'::regclass)"))
    comment = Column(String, nullable=True)
    date = Column(Date, nullable=False, server_default=text("('now'::text)::date"))
    qualification = Column(Integer, nullable=False)
    user_id = Column(ForeignKey(User.id), nullable=False)
    subsidiary_id = Column(ForeignKey(Subsidiary.id), nullable=False)
    user = relationship(User, backref='ratings', uselist=False)
    subsidiary = relationship(Subsidiary, backref='ratings', uselist=False)

    def validate(self):
        return self.rating_subsidiary_validator
