# -*- coding: utf-8 -*-

from sqlalchemy import Column, Date, ForeignKey, Integer, text, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from Device import Device
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class DevicesSupport(Base, DictSerializableMixin):
    __tablename__ = 'devices_support'

    device_support_rules = Validator({
        'support_date': [required],
        'reception_date': [required],
        'device_id': [required, int_adapter(required)]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('devices_support_id_seq'::regclass)"))
    support_date = Column(Date, nullable=False, server_default=text("('now'::text)::date"))
    reception_date = Column(Date)
    device_id = Column(ForeignKey(Device.id), nullable=False)
    status = Column(String(60))
    device = relationship(Device, backref="devices_support")

    def validate(self):
        return self.device_support_rules