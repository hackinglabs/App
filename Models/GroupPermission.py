# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey, Boolean, SMALLINT
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Models.Group import Group
from Models.Permission import Permission

Base = declarative_base()


class GroupPermission(Base, DictSerializableMixin):
    __tablename__ = 'groups_permissions'

    groups_permission_validator = Validator({
        "group_id": [required, int_adapter(required)],
    })

    permission_id = Column(ForeignKey(Permission.id),
                           nullable=False,
                           primary_key=True)
    group_id = Column(ForeignKey(Group.id),
                      nullable=False,
                      primary_key=True)
    create = Column(Boolean, nullable=False, default=False)
    read = Column(Boolean, nullable=False, default=False)
    update = Column(Boolean, nullable=False, default=False)
    delete = Column(Boolean, nullable=False, default=False)
    level = Column(SMALLINT, default=0, nullable=False)

    group = relationship(Group, backref='groups_permissions')
    permission = relationship(Permission, backref='groups_permissions')

    def validate(self):
        return self.groups_permission_validator
