# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey, Integer, String, text, SMALLINT
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from Models.Country import Country
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class State(Base, DictSerializableMixin):
    __tablename__ = 'states'
    state_validator = Validator({
        'name': [required],
        'country_id': [required, int_adapter(required)],
        'gmt': [required, int_adapter(required)]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('states_id_seq'::regclass)"))
    name = Column(String(45), nullable=False)
    country_id = Column(ForeignKey(Country.id), nullable=False)
    country = relationship(Country, backref="states")
    gmt = Column(SMALLINT, nullable=False)

    def __repr__(self):
        return self.name

    def validate(self):
        return self.state_validator
