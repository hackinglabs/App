# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, String, text, Date, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

from Models.User import User
import arrow

Base = declarative_base()



class RatingDispatcher(Base, DictSerializableMixin):
    __tablename__ = 'ratings_dispatchers'

    rating_dispatchers_validator = Validator({
        'user_id': [required, int_adapter(required)],
        'dispatcher_id': [required, int_adapter(required)],
        'qualification': [required, int_adapter(required)]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('ratings_dispatchers_id_seq'::regclass)"))
    comment = Column(String, nullable=True)
    date = Column(Date, nullable=False, default=arrow.now('America/Santiago').to('utc').datetime)
    qualification = Column(Integer, nullable=False)
    user_id = Column(ForeignKey(User.id), nullable=False)
    dispatcher_id = Column(ForeignKey(User.id), nullable=False)

    user = relationship(User, foreign_keys='RatingDispatcher.user_id')
    dispatcher = relationship(User, foreign_keys='RatingDispatcher.dispatcher_id')

    def validate(self):
        return self.rating_dispatchers_validator
