# -*- coding: utf-8 -*-

from sqlalchemy import Column, Integer, String, text, Text
from sqlalchemy.ext.declarative import declarative_base
from wheezy.validation import Validator
from wheezy.validation.rules import required
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class System(Base, DictSerializableMixin):
    __tablename__ = 'systems'

    system_validator = Validator({
        'name': [required],
        'description': [required]
    })

    id = Column(Integer, primary_key=True, server_default=text("nextval('systems_id_seq'::regclass)"))
    name = Column(String(45), nullable=False)
    description = Column(Text, nullable=False)
    routing = Column(String(45), nullable=False)
    logo = Column(String)

    def validate(self):
        return self.system_validator
