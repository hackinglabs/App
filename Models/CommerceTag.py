# -*- coding: utf-8 -*-

from sqlalchemy import Column, ForeignKey, Integer, Boolean, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from Models.Tag import Tag
from Models.Commerce import Commerce
from wheezy.validation import Validator
from wheezy.validation.rules import required, int_adapter
from Classes.Core.SQLAlchemyMixins import DictSerializableMixin

Base = declarative_base()


class CommerceTag(Base, DictSerializableMixin):
    __tablename__ = 'commerce_tags'

    commerce_tags_validator = Validator({
        'tag_id': [required, int_adapter(required)],
        'commerce_id': [required, int_adapter(required)],
    })

    id = Column(Integer, primary_key=True,
                server_default=text("nextval('commerce_tags_id_seq'::regclass)"))
    tag_id = Column(Integer, ForeignKey(Tag.id), primary_key=True)
    commerce_id = Column(Integer, ForeignKey(Commerce.id), primary_key=True)
    enabled = Column(Boolean, default=True)
    tag = relationship(Tag, backref="commerce_tags", uselist=False)
    commerce = relationship(Commerce, backref='commerce_tags', uselist=False)

    def validate(self):
        return self.commerce_tags_validator
