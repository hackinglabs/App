# -*- coding: utf-8 -*-

import logging.config
from wheezy.http import WSGIApplication
from wheezy.web.middleware import bootstrap_defaults
from wheezy.web.middleware import path_routing_middleware_factory
import gunicorn.app.base
from gunicorn.six import iteritems

#from Classes.Factories.MiddlewareFactory import MiddlewareFactory
from Classes.Factories import MiddlewareFactory
from Classes.db import ConnectRedis
from Configs.Config import Config
from Helpers.Rabbit import Rabbit
from urls import all_urls
import sys

reload(sys)
sys.setdefaultencoding('utf8')

conf = Config()
options = conf.all()

logging_conf = conf.logging()
if logging_conf is not None:
    logging.config.dictConfig(logging_conf)

ConnectRedis()

options.update({
    'rabbit_class': Rabbit()
})

main = WSGIApplication(
    middleware=[
        MiddlewareFactory("Debug").create,
        bootstrap_defaults(url_mapping=all_urls),
        path_routing_middleware_factory
    ],
    options=options
)

if not options['production']:
    from linesman.middleware import make_linesman_middleware
    main = make_linesman_middleware(main)


class StandaloneApplication(gunicorn.app.base.BaseApplication):

    def __init__(self, app, options=None):
        self.options = options or {}
        self.application = app
        super(StandaloneApplication, self).__init__()

    def load_config(self):
        config = dict([(key, value) for key, value in iteritems(self.options)
                       if key in self.cfg.settings and value is not None])
        for key, value in iteritems(config):
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application


if __name__ == '__main__':

    gunicorn_options = {
        'bind': '%s:%s' % (options['host'], options['port']),
        'workers': 1,
        'threads': 5,
        'worker-class': options['worker_class'],
        'check_config': True,
        'accesslog': 'access.log',
        'access_log_format': "%(h)s %(l)s %(u)s %(t)s %(r)s %(s)s %(b)s %(f)s %(a)s %(T)s %(D)s",
    }
    StandaloneApplication(main, gunicorn_options).run()
