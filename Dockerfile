FROM pypy:2

ENV PYTHONIOENCODING "UTF-8"
ENV CQLSH_NO_BUNDLED TRUE

RUN apt-get update
RUN apt-get install wget

RUN echo 'deb http://packages.dotdeb.org jessie all' >> /etc/apt/sources.list
RUN echo 'deb-src http://packages.dotdeb.org jessie all' >> /etc/apt/sources.list

RUN wget https://www.dotdeb.org/dotdeb.gpg
RUN apt-key add dotdeb.gpg

RUN apt-get update
RUN apt-get install -y php7.0 php7.0-xml php7.0-soap php7.0-readline php7.0-opcache libpq-dev php7.0-mcrypt php7.0-json php7.0-fpm php7.0-common libgeos-dev php7.0-cli gettext

RUN mkdir -p /opt/dxpress

ADD . /opt/dxpress

ENV GOOGLE_APPLICATION_CREDENTIALS="/opt/dxpress/dxpress-edcb5764bb56.json"
ENV APP_ENV="development"

RUN rm -rf /opt/dxpress/khipu
ADD repo-key /
RUN chmod 600 /repo-key && echo "IdentityFile /repo-key" >> /etc/ssh/ssh_config && \
 echo "StrictHostKeyChecking no" >> /etc/ssh/ssh_config && git clone git@github.com:HackingLabsChile/khipu.git /opt/dxpress/khipu

WORKDIR /opt/dxpress/khipu
RUN pypy setup.py install

WORKDIR /opt/dxpress
RUN find . -name \*.po -execdir sh -c 'msgfmt "$0" -o `basename $0 .po`.mo' '{}' \;
RUN pip install -r requirements.txt
RUN echo "from psycopg2cffi import compat\ncompat.register()" > /usr/local/site-packages/psycopg2.py

EXPOSE 8080

CMD [ "pypy", "manage.py" ]
