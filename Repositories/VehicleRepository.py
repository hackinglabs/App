# -*- coding: utf-8 -*-
from sqlalchemy.orm import joinedload

from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models.DriverVehicle import DriverVehicle
from Models.Vehicle import Vehicle
from Models.User import User

class VehicleRepository(Repository):
    def __init__(self, db_context):
        super(VehicleRepository, self).__init__(db_context)

    def all(self):
        result = self.con.query(Vehicle) \
            .options(joinedload(Vehicle.owner).load_only(User.id,
                                                         User.first_name,
                                                         User.last_name,
                                                         User.email,
                                                         User.avatar,
                                                         User.gender,
                                                         User.birth_date))\
            .all()
        if not result:
            raise NoneResult("We can't found any vehicle")

        return result

    def find(self, id):
        try:
            result = self.con.query(Vehicle) \
                .options(joinedload(Vehicle.owner).load_only(User.id,
                                                             User.first_name,
                                                             User.last_name,
                                                             User.email,
                                                             User.avatar,
                                                             User.gender,
                                                             User.birth_date)) \
                .filter(Vehicle.id == id) \
                .first()
        except:
            raise NoneResult("We can't found that vehicle")
        return result


    def all_vehicle_for_owner(self, id):
        result = self.con.query(Vehicle).options(
            joinedload(Vehicle.driver_vehicles).joinedload(DriverVehicle.user)).filter(
            Vehicle.user_id == id).all()

        return result

    def all_vehicles_id(self, user_id):
        vehicles = self.con.query(Vehicle.id) \
            .filter(Vehicle.user_id == user_id) \
            .all()

        if not vehicles:
            return []

        return [v[0] for v in vehicles]

    def last_driver(self, vehicle_id):

        driver = self.con.query(Vehicle)\
            .join(Vehicle.driver_vehicles)\
            .options(joinedload(Vehicle.driver_vehicles))\
            .filter(
            Vehicle.id == vehicle_id)\
            .order_by(DriverVehicle.assignation_date).first()

        if driver is not None:
            return driver
        else:
            return None
