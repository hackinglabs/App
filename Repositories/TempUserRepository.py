# -*- coding: utf-8 -*-
from Models import TempUser
from sqlalchemy.orm import joinedload
from Classes.Core.Repository import Repository


class TempUserRepository(Repository):
    def __init__(self, db_context):
        super(TempUserRepository, self).__init__(db_context)

    def all(self):
        temp_users = self.con.query(TempUser).options(joinedload(TempUser.user)).all()

        return temp_users
