# -*- coding: utf-8 -*-

from Classes.Core.Repository import Repository
from Models.DeviceMobile import DeviceMobile


class DeviceRepository(Repository):
    def __init__(self, db_context):
        super(DeviceRepository, self).__init__(db_context)
