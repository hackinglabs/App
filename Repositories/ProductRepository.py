# -*- coding: utf-8 -*
import logging
import unicodedata
from sqlalchemy.orm import joinedload
from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models import Compound, ItemCompound, OptionCompound, SubCategory
from Models.IngredientPerCategory import IngredientPerCategory
from Models.OptionGroup import OptionGroup
from Models.Product import Product
from Models.ProductTag import ProductTag
from Models.Tag import Tag


class ProductRepository(Repository):
    def __init__(self, db_context):
        super(ProductRepository, self).__init__(db_context)
        self.logger = logging.getLogger(__name__)

    def find(self, id):
        product = self.con.query(Product).filter(Product.id == id).first()
        return product

    def update(self, params):
        self.logger.warning("Pasando por Update, Los PARAMS: ")
        self.logger.warning(params)
        if 'id' in params.keys():
            id = params['id']
            del params['id']

            dict = {}
            for param in params:
                self.logger.warning(param)
                param = unicodedata.normalize('NFKD', param).encode('ascii', 'ignore')
                dict[param] = params[param]
            self.logger.warning(dict)

            self.con.begin()
            try:
                self.con.query(Product).filter(Product.id == id).update(dict)
                self.con.commit()
            except:
                self.con.rollback()
                return False
            return True

    def product_compound(self, id):
        product = self.con.query(Product)\
            .options(joinedload(Product.options_compounds)\
                     .joinedload(OptionCompound.compound))\
            .filter(Product.id == id)\
            .first()

        option_list = []
        for option in product.options_compounds:
            compound = {}
            rel = {
                "optioncompound": ["compound"]
            }
            option_dict = self.to_dict(option, rel=rel, depth=2)
            compound['option'] = option_dict
            compound['original'] = option_dict['compound']
            if option.change_for_all:
                all_compound = self.con.query(Compound).all()
                compound['change'] = self.to_dict(all_compound)
            else:
                compound_changes = self.con.query(ItemCompound)\
                    .options(joinedload(ItemCompound.compound))\
                    .filter(ItemCompound.option_compound_id == option.id)\
                    .all()
                rel = {
                    "itemcompound": ["compound"]
                }
                compound['change'] = self.to_dict(compound_changes, rel=rel, depth=2)
                compound['list_compounds_id'] = [x.compound.id for x in compound_changes]

            option_list.append(compound)
        return option_list

    def get_options(self, id):
        list_options = []

        try:
            options = self.con.query(OptionGroup) \
                .options(joinedload(OptionGroup.ingredient_per_category) \
                         .joinedload(IngredientPerCategory.ingredients)) \
                .filter(OptionGroup.product_id == id, OptionGroup.enabled == True) \
                .all()
        except:
            options = None

        # TODO: Translate
        if options is None:
            raise NoneResult("We didn't found any data")
        else:
            for option in options:
                list_ingredients = []
                for ingredient in option.ingredient_per_category.ingredients:
                    list_ingredients.append(self.to_dict(ingredient))

                list_options.append({'id': option.id,
                                     'name': option.ingredient_per_category.name,
                                     'quantity': option.quantity,
                                     'is_base': option.ingredient_per_category.is_base,
                                     'is_premium': option.ingredient_per_category.is_premium,
                                     'ingredient_default': option.defaults,
                                     'ingredients': list_ingredients})

            return list_options

    def get_details(self, id):
        try:
            product = self.con.query(Product) \
                .filter(Product.id == id, Product.enabled == True) \
                .first()
        except:
            product = None

        # TODO: Translate
        if product is None:
            raise NoneResult("We didn't found any data")
        else:
            compound_list = self.product_compound(product.id)
            options_list = self.get_options(product.id)
            data = {
                "compound": compound_list,
                "ingredient": options_list
            }
            return data

    def create_or_use_tag(self, product):
        tag_name = product.name.capitalize()
        tag = self.con.query(Tag).filter(Tag.name == tag_name).first()
        if tag is None:
            tag_create = Tag(name=tag_name, system_id=1)
            if self.save(tag_create):
                product_tag = ProductTag(tag_id=tag_create.id, product_id=product.id)
                self.save(product_tag)
        else:
            product_tag = ProductTag(tag_id=tag.id, product_id=product.id)
            self.save(product_tag)

    def create_compound(self, product):
        compound = Compound(name=product.name, price=product.price,
                            subsidiary_id=product.sub_category.category.subsidiary_id,
                            category_id=product.sub_category.category.id)
        return self.save(compound)

    def update_compound(self, product):
        compound = self.con.query(Compound).filter(Compound.name == product.name).first()
        if compound is not None:
            compound.price = product.price
            self.logger.error(compound.price)
            return self.save(compound)
        else:
            self.logger.error("No find compound")
            return None

    def delete(self, id):
        product = self.con.query(Product).filter(Product.id == id).first()
        self.logger.warning(product)
        if product is not None:
            self.con.begin()
            try:
                # DELETE product_tag associate product
                self.con.query(ProductTag).filter(ProductTag.product_id == product.id).delete()
                self.con.query(OptionCompound).filter(OptionCompound.product_id == product.id).delete()
                self.con.commit()
            except:
                self.con.rollback()

    def validate_product_belong_subsidiary(self, subsidiary_id, product_id):

        try:
            subsidiary_id = int(subsidiary_id)
        except:
            self.logger.warning('Error convert to int')
            return None

        product = self.con.query(Product).options(
            joinedload(Product.sub_category).joinedload(SubCategory.category)).filter(
            Product.id == product_id).first()

        if product is None:
            return None

        subsidiary_belong = product.sub_category.category.subsidiary_id

        if subsidiary_belong == subsidiary_id:
            return product
        else:
            return None

    def get_products_by_list(self, ids_list):
        try:
            products = self.con.query(Product).filter(Product.id.in_(ids_list)).all()
        except Exception as e:
            self.logger.error(e.message)
            products = []

        return products


