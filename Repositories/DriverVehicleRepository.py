# -*- coding: utf-8 -*-

from Classes.Core.Repository import Repository


class DriverVehicleRepository(Repository):
    def __init__(self, db_context):
        super(DriverVehicleRepository, self).__init__(db_context)
