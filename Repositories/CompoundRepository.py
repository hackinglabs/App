# -*- coding: utf-8 -*-

from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models import Compound


class CompoundRepository(Repository):
    def __init__(self, db_context):
        super(CompoundRepository, self).__init__(db_context)

    def delete(self, id):
        try:
            compound = self.con.query(Compound).filter(Compound.id == id).first()
        except:
            raise NoneResult("We didn't found compound")

        if compound.options_compounds is not None or compound.items_compounds is not None:
            return False
        else:
            self.con.begin()
            try:
                self.con.delete(compound)
                self.con.commit()
                response = True
            except:
                self.con.rollback()
                raise NoneResult("delete compound")

            return response
