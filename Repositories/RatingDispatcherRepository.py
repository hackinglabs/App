# -*- coding: utf-8 -*-
from Classes.Core.Repository import Repository
from Models import RatingDispatcher
from Models.User import User


class RatingDispatcherRepository(Repository):
    def __init__(self, db_context):
        super(RatingDispatcherRepository, self).__init__(db_context)

    def create(self, params):
        rating_dispatcher = RatingDispatcher(user_id=params['user_id'],
                                             dispatcher_id=params['dispatcher_id'],
                                             qualification=params['quantity'],
                                             comment=params['comment'])
        return rating_dispatcher
