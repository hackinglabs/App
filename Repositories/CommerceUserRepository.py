# -*- coding: utf-8 -*-
from Classes.Core.Repository import Repository


class CommerceUserRepository(Repository):
    def __init__(self, db_context):
        super(CommerceUserRepository, self).__init__(db_context)
