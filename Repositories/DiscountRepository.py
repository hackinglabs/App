# -*- coding: utf-8 -*-
import arrow

from Classes.Core.Repository import Repository
from Models.Discount import Discount
from sqlalchemy import or_, and_


class DiscountRepository(Repository):
    def __init__(self, db_context):
        super(DiscountRepository, self).__init__(db_context)

    def delete(self, id):
        self.con.begin()
        try:
            self.con.query(Discount).filter(Discount.id == id).delete()
            self.con.commit()

        except Exception as e:
            self.con.rollback()
            self.logger.error(e.message)
            return False

    def __compare_discounts(self, discount_old, discount_new, total=0):

        if discount_old is None:
            return discount_new
        elif discount_new is None:
            return discount_old
        elif discount_old is None and discount_new is None:
            return None

        if 0 < total < discount_new.minimum_buy:
            return discount_old

        if discount_old.amount > discount_new.amount:
            return discount_old
        elif discount_old.amount == discount_new.amount:
            if discount_old.minimum_buy < discount_new.minimum_buy:
                return discount_old
        return discount_new

    def get_subsidiary_discounts(self, subsidiary):
        category_ids = []
        subcategory_ids = []
        product_ids = []

        for c in subsidiary.categories:
            category_ids.append(c.id)

            for sc in c.sub_categories:
                subcategory_ids.append(sc.id)

                for p in sc.products:
                    product_ids.append(p.id)

        return self.con.query(Discount) \
            .filter(
            Discount.enabled == True,
            Discount.system_id == subsidiary.commerce.system_id,
            or_(
                and_(
                    Discount.discount_type == "SUBSIDIARY",
                    Discount.type_id == subsidiary.id
                ),
                and_(
                    Discount.discount_type == "COMMERCE",
                    Discount.type_id == subsidiary.commerce_id,
                ),
                and_(
                    Discount.discount_type == "CATEGORY",
                    Discount.type_id.in_(category_ids)
                ),
                and_(
                    Discount.discount_type == "SUBCATEGORY",
                    Discount.type_id.in_(subcategory_ids)
                ),
                and_(
                    Discount.discount_type == "PRODUCT",
                    Discount.type_id.in_(product_ids)
                )
            )
        ).all()

    def check_subsidiary_discounts(self, subsidiary, address, products, total=0):
        products_ids = []
        subcategory_ids = []
        category_ids = []

        if products is not None:

            for p in products:
                products_ids.append(p.id)
                subcategory_ids.append(p.sub_category_id)
                category_ids.append(p.sub_category.category_id)

        # Return all discounts for subsidiary or commerce
        discounts_subsidiary = self.con.query(Discount) \
            .filter(
            Discount.enabled == True,
            Discount.system_id == subsidiary.commerce.system_id,
            or_(
                and_(
                    Discount.discount_type == "SUBSIDIARY",
                    Discount.type_id == subsidiary.id
                ),
                and_(
                    Discount.discount_type == "COMMERCE",
                    Discount.type_id == subsidiary.commerce_id,
                ),
                and_(
                    Discount.discount_type == "CATEGORY",
                    Discount.type_id.in_(category_ids)
                ),
                and_(
                    Discount.discount_type == "SUBCATEGORY",
                    Discount.type_id.in_(subcategory_ids)
                ),
                and_(
                    Discount.discount_type == "PRODUCT",
                    Discount.type_id.in_(products_ids)
                )
            )
        ).all()

        # Will contain every discount that we (dxpress) created
        our_discounts = []

        # Discounts created by the subsidiary
        subsidiary_discount = []
        commerce_discount = []
        category_discount = []
        subcategory_discount = []
        product_discount = []

        for ds in discounts_subsidiary:
            if ds.our_discount:
                our_discounts.append(ds)
            elif ds.discount_type == "SUBSIDIARY":
                if not subsidiary_discount:
                    subsidiary_discount.append(ds)
                elif subsidiary_discount:
                    better_discount = self.__compare_discounts(subsidiary_discount[0], ds, total)
                    subsidiary_discount = [better_discount]
            elif ds.discount_type == "COMMERCE":
                if not commerce_discount:
                    commerce_discount.append(ds)
                elif commerce_discount:
                    better_discount = self.__compare_discounts(commerce_discount[0], ds, total)
                    commerce_discount = [better_discount]
            elif ds.discount_type == "CATEGORY":
                if not category_discount:
                    category_discount.append(ds)
                elif category_discount:
                    better_discount = self.__compare_discounts(category_discount[0], ds, total)
                    category_discount = [better_discount]
            elif ds.discount_type == "SUBCATEGORY":
                if not subcategory_discount:
                    subcategory_discount.append(ds)
                elif subcategory_discount:
                    better_discount = self.__compare_discounts(subcategory_discount[0], ds, total)
                    subcategory_discount = [better_discount]
            elif ds.discount_type == "PRODUCT":
                if not product_discount:
                    product_discount.append(ds)
                elif product_discount:
                    better_discount = self.__compare_discounts(product_discount[0], ds, total)
                    product_discount = [better_discount]

        all_sub_discounts = []

        if subsidiary_discount:
            all_sub_discounts.append(subsidiary_discount[0])

        if commerce_discount:
            all_sub_discounts.append(commerce_discount[0])

        if category_discount:
            all_sub_discounts.append(category_discount[0])

        if subcategory_discount:
            all_sub_discounts.append(subcategory_discount[0])

        if product_discount:
            all_sub_discounts.append(product_discount[0])

        definitive_subsidiary_discount = None
        for sd in all_sub_discounts:
            if definitive_subsidiary_discount is None:
                definitive_subsidiary_discount = sd
            else:
                definitive_subsidiary_discount = self.__compare_discounts(definitive_subsidiary_discount, sd, total)

        if address:
            discount_address = self.con.query(Discount) \
                .filter(
                Discount.enabled == True,
                Discount.system_id == subsidiary.commerce.system_id,
                or_(
                    and_(
                        Discount.discount_type == "AREA",
                        Discount.type_id == subsidiary.area_id,
                    ),
                    and_(
                        Discount.discount_type == "CITY",
                        Discount.type_id == subsidiary.area.city_id
                    ),
                    and_(
                        Discount.discount_type == "STATE",
                        Discount.type_id == subsidiary.area.city.state_id
                    ),
                    and_(
                        Discount.discount_type == "COUNTRY",
                        Discount.type_id == subsidiary.area.city.state.country_id
                    )
                )
            ) \
                .all()
            for da in discount_address:
                our_discounts.append(da)

        our_better_discount = None

        for od in our_discounts:
            if our_better_discount is None:
                our_better_discount = od
            else:
                our_better_discount = self.__compare_discounts(our_better_discount, od, total)

        if definitive_subsidiary_discount is None and our_better_discount is None:
            return None, None
        elif definitive_subsidiary_discount is None:
            return our_better_discount, None
        elif our_better_discount is None:
            return None, definitive_subsidiary_discount
        else:
            return our_better_discount, definitive_subsidiary_discount
