from Classes.Core import Repository
from Classes.Core.Exceptions import NoneResult
from Models import OptionGroup, IngredientPerCategory


class OptionGroupRepository(Repository):
    def __init__(self, db_context):
        super(OptionGroupRepository, self).__init__(db_context)

    def ids_and_bases(self, id):
        try:
            result = self.con.query(OptionGroup) \
                .with_entities(OptionGroup.ingredient_per_category_id,
                               IngredientPerCategory.is_base,
                               IngredientPerCategory.is_premium,
                               OptionGroup.id,
                               OptionGroup.quantity) \
                .join(OptionGroup.ingredient_per_category) \
                .filter(OptionGroup.product_id == id) \
                .all()
        except:
            result = None

        if result is None or len(result) <= 0:
            raise NoneResult("We didn't found any optiongroup")

        return result

    def delete(self, id):
        op = self.con.query(OptionGroup).filter(OptionGroup.id == id).first()
        if op is not None:
            self.con.begin()
            try:
                self.con.delete(op)
                self.con.commit()
                response = True
            except:
                self.con.rollback()
                response = False
        else:
            response = False

        return response
