# -*- coding: utf-8 -*-
from Classes.Core.Repository import Repository
from Models.Notification import Notification


class NotificationRepository(Repository):
    def __init__(self, db_context):
        super(NotificationRepository, self).__init__(db_context)

    def get_notifications_for_user(self, user_id):
        notifications = self.con.query(Notification).filter(Notification.user_id == user_id).all()
        return notifications
