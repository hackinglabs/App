from Classes.Core import Repository
from Models import BlackListPhone
from sqlalchemy.orm import joinedload, contains_eager


class BlackListPhoneRepository(Repository):
    def __init__(self, db_context):
        super(BlackListPhoneRepository, self).__init__(db_context)