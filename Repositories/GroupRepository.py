# -*- coding: utf-8 -*-
from sqlalchemy.orm import load_only

from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models.User import User
from Models.UserGroup import UserGroup
from Models.Permission import Permission
from Models.GroupPermission import GroupPermission


class GroupRepository(Repository):
    def __init__(self, db_context):
        super(GroupRepository, self).__init__(db_context)

    def get_commerce_users(self):

        try:
            users = self.con.query(User)\
                .options(load_only(User.id,
                                   User.first_name,
                                   User.last_name,
                                   User.email,
                                   User.avatar,
                                   User.gender,
                                   User.birth_date))\
                .join(UserGroup, UserGroup.user_id == User.id)\
                .join(GroupPermission, UserGroup.group_id == GroupPermission.group_id)\
                .join(Permission, GroupPermission.permission_id == Permission.id)\
                .filter(Permission.name == 'login_subsidiary')\
                .all()
        except:
            users = None

        if not users:
            raise NoneResult("We dind't found any user with some commerce permission.")

        return users