# -*- coding: utf-8 -*-
import logging

from sqlalchemy import func

from Classes.Core.Repository import Repository
from Models import RedZone


class RedZoneRepository(Repository):
    def __init__(self, db_context):
        super(RedZoneRepository, self).__init__(db_context)
        self.logger = logging.getLogger(__name__)

    def create_zone(self, name, wkt):
        redzone = RedZone()
        redzone.name = name
        redzone.area = wkt

        self.con.begin()
        try:
            self.con.add(redzone)
            self.con.commit()
            return True
        except:
            self.con.rollback()
            return False

    def delete(self, zone_id):
        self.con.begin()
        try:
            rows = self.con.query(RedZone).filter(RedZone.id == zone_id). \
                delete(synchronize_session=False)
            self.con.commit()
            return True
        except:
            self.con.rollback()
            return False

    def contains(self, lng, lat):
        query = self.con.query(RedZone).filter(
            RedZone.area.ST_Intersects(
                             'POINT({lng} {lat})'
                             .format(lng=lng, lat=lat))
            ).limit(1)

        if query is not None:
            result = self.con.query(query.exists()).scalar()
            return result