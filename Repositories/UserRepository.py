# -*- coding: utf-8 -*-
import logging

import arrow
import json
from sqlalchemy import func
from sqlalchemy.orm import joinedload, contains_eager, load_only
from sqlalchemy.orm.strategy_options import Load

from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Configs import Config
from Models import Area, City, State, Favorite, Permission, GroupPermission, \
    Group
from Models.Commerce import Commerce
from Models.DispatchAddress import DispatchAddress
from Models.Notification import Notification
from Models.Subsidiary import Subsidiary
from Models.User import User
from Models.UserGroup import UserGroup
from Models.DriverVehicle import DriverVehicle
from Models.Vehicle import Vehicle
from Models import Redis


class UserRepository(Repository):
    def __init__(self, db_context):
        super(UserRepository, self).__init__(db_context)
        self.logger = logging.getLogger(__name__)

    def get_by_id(self, id):
        try:
            user = self.con.query(User) \
                .options(Load(User).load_only(User.id,
                                              User.first_name,
                                              User.last_name,
                                              User.nin,
                                              User.email,
                                              User.gender,
                                              User.birth_date)) \
                .filter(User.id == id) \
                .first()
        except Exception as e:
            self.logger.error(str(e))
            raise NoneResult("We can't found any user with that id.")
        return user

    def client_subsidiaries(self, user_id):
        """Returns subsidiaries and commerce for an user

        Args:
            user_id: User identifier

        Returns:
            Dict with the commerce (Single one) and all subsidiaries of that
            commerce

        """
        client_commerce = self.con.query(Commerce) \
            .filter(Commerce.user_id == user_id) \
            .first()

        subsidiaries = self.con.query(Subsidiary) \
            .filter(Subsidiary.commerce_id == client_commerce.id) \
            .all()

        subsidiaries = self.to_dict(subsidiaries)
        subsidiaries = {
            "commerce": client_commerce.name,
            "subsidiaries": subsidiaries
        }

        return subsidiaries

    def get_addresses(self, user_id):
        """Returns all addresses for an user

        Args:
            user_id: User identifier

        Returns:
            List of of DispatchAddress objects for an user. Each
            DispatchAddress object has an Area object that also have City and
            State object inside.

        """
        addresses = self.con.query(DispatchAddress) \
            .options(
            joinedload(DispatchAddress.area)
                .joinedload(Area.city)
                .joinedload(City.state)
                .joinedload(State.country)
        ) \
            .filter(DispatchAddress.user_id == user_id) \
            .all()

        return addresses

    def get_favorites(self, user_id):
        """Returns all Favorites for an user

        Args:
            user_id: User identifier

        Returns:
            List with Favorite objects of an user. Each Favorite object has
            a Subsidiary object and this one also have Commerce object.

        """
        favorites = self.con.query(Favorite) \
            .options(
            joinedload(Favorite.subsidiary)
                .joinedload(Subsidiary.commerce)
        ) \
            .filter(Favorite.user_id == user_id) \
            .all()

        return favorites

    def get_favorites_ids(self, user_id):
        favorites_ids = self.con.query(Favorite.subsidiary_id) \
            .filter(Favorite.user_id == user_id) \
            .all()

        return favorites_ids

    def get_all_dispatchers_id(self, user_id):
        # TODO: Bring all history users
        dispatchers = self.con.query(User.id) \
            .join(Vehicle, User.id == Vehicle.user_id) \
            .join(DriverVehicle, Vehicle.id == DriverVehicle.vehicle_id) \
            .filter(User.id == user_id) \
            .all()
        if not dispatchers or dispatchers is None:
            return []

        return [d[0] for d in dispatchers]

    def find_with_vehicle(self, user_id):
        user = self.con.query(User) \
            .options(joinedload(User.driver_vehicles)) \
            .filter(User.id == user_id) \
            .first()

        return user

    def edit_user(self, user_object, groups_id=None):
        self.con.begin()
        try:
            self.con.add(user_object)
            self.con.flush()

            if groups_id is not None:
                groups_raw = self.con.query(Group.id) \
                    .filter(Group.id.in_(groups_id)) \
                    .all()
                groups = [dict(group_id=g[0], user_id=user_object.id) for g in groups_raw]

                rows = self.con.query(UserGroup) \
                    .filter(UserGroup.user_id == user_object.id) \
                    .delete(synchronize_session='fetch')

                self.con.flush()

                self.con.bulk_insert_mappings(UserGroup, groups)

            self.con.commit()
            return True
        except Exception as e:
            self.logger.error("Error: " + str(e))
            self.con.rollback()
            return False

    def create_user(self, user_object, active=False, groups_id=None):
        old_id = user_object.id
        if old_id is None:
            new = True
        else:
            new = False

        self.con.begin()
        try:
            user_object.active = active
            self.con.add(user_object)
            self.con.flush()

            if old_id is None:
                if groups_id is None:
                    conf = Config().defaults()

                    user_group = UserGroup()
                    user_group.user_id = user_object.id
                    user_group.group_id = conf['group_id']

                    self.con.add(user_group)
                else:
                    groups_raw = self.con.query(Group.id) \
                        .filter(Group.id.in_(groups_id)) \
                        .all()
                    groups = [dict(group_id=g[0], user_id=user_object.id) for g in groups_raw]
                    self.con.bulk_insert_mappings(UserGroup, groups)

            self.con.commit()
            return True, new
        except Exception as e:
            self.logger.error("Error in create_user: " + str(e))
            self.con.rollback()
            return False, False

    def get_all_perms(self, user_id=None, user_email=None):
        if user_email is not None and user_id is None:
            user_id = self.con.query(User.id) \
                .filter(User.email == user_email) \
                .scalar()

        if user_id is None:
            return []

        result = self.con.query(Permission.name, Permission.level) \
            .join(GroupPermission,
                  GroupPermission.permission_id == Permission.id) \
            .join(Group, Group.id == GroupPermission.group_id) \
            .join(UserGroup, UserGroup.group_id == Group.id) \
            .filter(UserGroup.user_id == user_id) \
            .all()

        permissions = {}
        for r in result:
            if r[0] in permissions and r[1] > permissions[r[0]]:
                permissions[r[0]] = r[1]
            else:
                permissions[r[0]] = r[1]
        return permissions

    def last_vehicle(self, user_id):
        try:

            user_id = int(user_id)
        except ValueError:
            self.logger.error("Error in last_vehicle")
            user_id = None

        vehicle = self.con.query(DriverVehicle) \
            .join(DriverVehicle.vehicle) \
            .options(joinedload(DriverVehicle.vehicle)) \
            .filter(DriverVehicle.user_id == user_id) \
            .order_by(DriverVehicle.assignation_date) \
            .first()

        return vehicle

    def find_with_group(self, user_id):
        try:
            user = self.con.query(User) \
                .options(Load(User).load_only(User.id,
                                              User.first_name,
                                              User.last_name,
                                              User.email,
                                              User.avatar,
                                              User.nin,
                                              User.gender,
                                              User.birth_date,
                                              User.telephone),
                         joinedload(User.user_groups).load_only(UserGroup.group_id)) \
                .filter(User.id == user_id) \
                .first()
        except:
            raise NoneResult("We can't found the user.")
        return user

    def all_with_group(self):
        users = self.con.query(User) \
            .options(Load(User).load_only(User.id,
                                          User.first_name,
                                          User.last_name,
                                          User.email,
                                          User.avatar,
                                          User.nin,
                                          User.gender,
                                          User.birth_date),
                     joinedload(User.user_groups).load_only(UserGroup.group_id)) \
            .all()

        return users

    def create_dispatcher_in_redis(self, id):
        user = self.con.query(User).filter(User.id == id).first()

        dispatcher = Redis.Dispatcher(id=str(user.id),
                                      first_name=user.first_name,
                                      last_name=user.last_name,
                                      nin=user.nin,
                                      is_active=True,
                                      vehicle_id=str(user.driver_vehicles[-1].id))
        try:
            self.logger.error(dispatcher)
            dispatcher.save()
            return dispatcher
        except Exception, e:
            self.logger.error(str(e))
            return None

    def create_user_in_redis(self, id):
        # Get USER
        user = self.con.query(User).filter(User.id == id).first()

        loaded_columns, unloaded_columns, relations, columns = user.get_columns()
        redis_user = Redis.User.get_blank(user.id)

        for attr in redis_user.to_dict():
            if attr in loaded_columns:
                user_prop_value = getattr(user, attr, None)
                if user_prop_value is not None and attr != 'id':
                    setattr(redis_user, attr, user_prop_value)
        redis_user.user_token = user.xcsrf

        try:
            redis_user.save()
            return redis_user
        except Exception, e:
            self.logger.error(str(e))
            return None

    def send_gcm_repo(self, user_id, type, msg, notification):

        from Classes.Core import GcmNotification
        from Models import DeviceMobile

        token_devices = []
        devices = self.con.query(DeviceMobile).filter(DeviceMobile.user_id == user_id).all()

        for device in devices:
            try:
                os = int(device.os)

                if os == 2:
                    os = "ios"
                else:
                    os = "android"
            except ValueError:
                pass

            token_devices.append({"token": device.token, "os": os})

        if token_devices:
            gcm = GcmNotification()
            gcm.add(token_devices)
            gcm.msg(msg)
            gcm.type(type)
            gcm.notification(notification)
            gcm.priority('high')
            return gcm.send()
        else:
            self.logger.error("The user_id: " + str(user_id) + " no have tokenDevice")

        return False

    def save_notification(self, user_id, data, order_id=None):

        data = json.dumps(data)

        # Set Notification
        notification = Notification()
        notification.user_id = user_id
        notification.data = data
        notification.order_id = order_id

        # Save
        self.save(notification)

        return notification.id

    def create_commerce_and_subsidiary(self, user_id, params):
        if params is None:
            return False

            # Get user
            user = self.con.query(User).filter(User.id == user_id).first()

            # Validate user if not have commerce
            user_with_commerce = self.con.query(Commerce).filter(Commerce.user_id).first()
            if user_with_commerce is not None:
                self.logger.error('User already have commerce')
                return False

            # Get vars
            name_for_all = user.name
            nin = self.param('nin')
            country_id = self.param('country_id')
            area_id = self.param('area_id')
            address = self.param('address')
            telephone = self.param('telephone')
            coordinates = self.param('coordinates')
            if telephone is None and user.telephone is not None:
                telephone = user.telephone

            # Init model commerce and set values
            commerce = Commerce()
            commerce.system_id = 1
            commerce.user_id = user.id
            commerce.nin = nin
            commerce.trade = 'Personal'
            commerce.country_id = country_id
            commerce.name = name_for_all
            commerce.address = address
            commerce.telephone = telephone
            commerce.is_person = True
            # Save commerce
            try:
                commerce.save()
            except Exception as e:
                self.logger.error(e.message)
                return self.error(e.message)

            # Init model subsidiary and set values
            subsidiary = Subsidiary()
            subsidiary.commerce_id = commerce.id
            subsidiary.alias = name_for_all
            subsidiary.address = address
            subsidiary.telephone = telephone
            subsidiary.area_id = area_id
            subsidiary.pap = 0
            subsidiary.dxl = True
            subsidiary.coordinates = coordinates
