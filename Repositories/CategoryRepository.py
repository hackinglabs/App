# -*- coding: utf-8 -*-
from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models import Compound
from Models.SubCategory import SubCategory


class CategoryRepository(Repository):
    def __init__(self, db_context):
        super(CategoryRepository, self).__init__(db_context)

    def get_all_sub_categories(self, id):
        try:
            sub_categories = self.con.query(SubCategory).filter(SubCategory.category_id == id).all()
        except:
            sub_categories = None

        if sub_categories is None or len(sub_categories) <= 0:
            raise NoneResult("We didn't found any sub_categories")

        return sub_categories

    def get_compounds(self, category_id):
        compounds = self.con.query(Compound).filter(Compound.category_id == category_id).all()

        return compounds
