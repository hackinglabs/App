# -*- coding: utf-8 -*-

from Classes.Core.Repository import Repository


class ReportRepository(Repository):
    def __init__(self, db_context):
        super(ReportRepository, self).__init__(db_context)
