# -*- coding: utf-8 -*-
from Classes.Core.Repository import Repository
from Models import Contact


class ContactRepository(Repository):
    def __init__(self, db_context):
        super(ContactRepository, self).__init__(db_context)

    def get_dispatchers(self):
        return self.con.query(Contact).filter(Contact.client == False).order_by(Contact.registration_date.asc()).all()

    def get_clients(self):
        return self.con.query(Contact).filter(Contact.client == True).order_by(Contact.registration_date.asc()).all()
