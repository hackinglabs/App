from Classes.Core import Repository
from Models import DeviceMobile
from sqlalchemy.orm import joinedload, contains_eager


class DeviceMobileRepository(Repository):
    def __init__(self, db_context):
        super(DeviceMobileRepository, self).__init__(db_context)

    def get_all_with_user(self):
        return self.con.query(DeviceMobile) \
            .join(DeviceMobile.user) \
            .options(contains_eager(DeviceMobile.user)) \
            .all()

    def update_user(self, token, user_id):
        device_token = self.con.query(DeviceMobile).filter(DeviceMobile.token == token).first()
        if device_token.user_id != user_id:
            device_token.user_id = user_id
            self.save(device_token)
