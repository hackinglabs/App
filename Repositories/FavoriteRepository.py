# -*- coding: utf-8 -*-
from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models.Favorite import Favorite


class FavoriteRepository(Repository):
    def __init__(self, db_context):
        super(FavoriteRepository, self).__init__(db_context)

    def get_all_favorite_for_user(self, id):
        try:
            users = self.con.query(Favorite).filter(Favorite.user_id == id).first()
        except:
            users = None

        if users is None:
            raise NoneResult("This user hasn't favorites")

        return users