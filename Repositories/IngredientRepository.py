# -*- coding: utf-8 -*-
from sqlalchemy.orm import joinedload

from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models.OptionGroup import OptionGroup
from Models.Ingredient import Ingredient
from Models.IngredientPerCategory import IngredientPerCategory


class IngredientRepository(Repository):
    def __init__(self, db_context):
        super(IngredientRepository, self).__init__(db_context)

    def get_in(self, ingredients_list):
        try:
            ingredients = self.con.query(Ingredient) \
                .options(joinedload(Ingredient.ingredient_per_category)) \
                .filter(Ingredient.id.in_(ingredients_list)) \
                .all()
        except:
            ingredients = None

        if ingredients is None or len(ingredients) == 0:
            raise NoneResult("We didn't found any ingredient")

        return ingredients

    def delete(self, id):
        ingredient = self.con.query(Ingredient).filter(Ingredient.id == id).first()

        if ingredient is not None:
            self.con.begin()
            try:
                self.con.delete(ingredient)
                self.con.commit()
                return True
            except Exception as e:
                self.con.rollback()
                self.logger.error("Error in delete ingredient:  " + e.message)
        else:
            return None

    def get_options(self, id, pid):
        options = self.con.query(OptionGroup.id)\
                      .join(IngredientPerCategory, OptionGroup.ingredient_per_category_id == IngredientPerCategory.id)\
                      .join(Ingredient, Ingredient.ingredient_per_category_id == IngredientPerCategory.id)\
                      .filter(Ingredient.id == id, OptionGroup.product_id == pid)\
                      .all()

        if not options:
            return []

        options = [o[0] for o in options]

        return options