from Classes.Core.Repository import Repository
from Models.DispatcherRejected import DispatcherRejected


class DispatcherRejectedRepository(Repository):
    def __init__(self, db_context):
        super(DispatcherRejectedRepository, self).__init__(db_context)
