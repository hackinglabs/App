# -*- coding: utf-8 -*-
import arrow

from Classes.Core.Repository import Repository
from Models.Coupon import Coupon
from Models.CouponUser import CouponUser
from sqlalchemy import func


class CouponRepository(Repository):
    def __init__(self, db_context):
        super(CouponRepository, self).__init__(db_context)

    def check_and_use(self, user_id, code, subsidiary, address, pickup=False):
        today = arrow.now('America/Santiago').to('utc').datetime
        coupon = self.con.query(Coupon).filter(Coupon.code == code,
                                               Coupon.expiration_date >= today,
                                               Coupon.enabled == True,
                                               Coupon.stock > 0).first()

        if coupon is None:
            return False, "El cupón no existe o ha caducado.", None

        ok, msg = self.check(user_id, coupon, subsidiary, address, pickup)

        if not ok:
            return ok, msg, coupon

        consumed = self.__consume_coupon(coupon, user_id)
        if not consumed:
            return False, "Se ha producido un problema al intentar validar el cupón.", None
        else:
            return True, "Cupón aplicado correctamente.", coupon

    def check(self, user_id, coupon, subsidiary, address, pickup=False):
        today = arrow.now('America/Santiago').to('utc').datetime

        if coupon is not None:
            if coupon.expiration_date < today:
                return False, "El cupón ha caducado."
            if coupon.stock <= 0:
                return False, "El cupón alcanzó su límite."

            validate, msg = self.validate_coupon(user_id, coupon, subsidiary, address, pickup)
            if not validate:
                return False, msg
            return True, None
        else:
            return False, "El cupón ha caducado."

    # TODO: Apply translate all messages return
    def validate_coupon(self, user_id, coupon, subsidiary, address, pickup=False):
        coupon_use = None

        if user_id is not None:
            coupon_use = self.con.query(CouponUser).filter(CouponUser.user_id == user_id,
                                                           CouponUser.coupon_id == coupon.id).first()
        if coupon_use is not None:
            return False, "Ya usaste este cupón"

        if pickup is True and coupon.allow_pickup is False:
            return False, "Este cupón no es válido para retiro en local."

        if pickup is False and coupon.allow_dispatch is False:
            return False, "Este cupón no es válido para entrega a domicilio."

        if coupon.coupon_type == "SUBSIDIARY":
            if coupon.type_id == subsidiary.id:
                return True, None
            else:
                return False, "El cupón no está asociado a esta sucursal."

        elif coupon.coupon_type == "COMMERCE":
            if coupon.type_id == subsidiary.commerce_id:
                return True, None
            else:
                return False, "El cupón no está asociado a este comercio."
        elif coupon.coupon_type == "AREA":
            if address is None:
                return False, "Antes de validar su cupón, debe seleccionar su dirección de despacho."
            if address is not None and coupon.type_id == address.area_id:
                return True, None
            elif coupon.allow_pickup and pickup and coupon.type_id == subsidiary.area_id:
                return True, None
            else:
                return False, "El cupón no se puede aplicar a su dirección o la dirección de la sucursal."
        elif coupon.coupon_type == "CITY":
            if address is None:
                return False, "Antes de validar su cupón, debe seleccionar su dirección de despacho."
            elif address is not None and coupon.type_id == address.area.city_id:
                return True, None
            elif coupon.allow_pickup and pickup and coupon.type_id == subsidiary.area.city_id:
                return True, None
            else:
                return False, "El cupón no se puede aplicar a su dirección o la dirección de la sucursal."
        elif coupon.coupon_type == "STATE":
            if address is None:
                return False, "Antes de validar su cupón, debe seleccionar su dirección de despacho."
            if address is not None and coupon.type_id == address.area.city.state_id:
                return True, None
            elif coupon.allow_pickup and pickup and coupon.type_id == subsidiary.area.city.state_id:
                return True, None
            else:
                return False, "El cupón no se puede aplicar a su dirección o la dirección de la sucursal."
        elif coupon.coupon_type == "COUNTRY":
            if address is None:
                return False, "Antes de validar su cupón, debe seleccionar su dirección de despacho."
            if address is not None and coupon.type_id == address.area.city.state.country_id:
                return True, None
            elif coupon.allow_pickup and pickup and coupon.type_id == subsidiary.area.city.state.country_id:
                return True, None
            else:
                return False, "El cupón no se puede aplicar a su dirección o la dirección de la sucursal."
        elif not pickup and coupon.allow_dispatch and coupon.coupon_type in ["AREA", "CITY", "COUNTRY"]:
            return False, "El cupón podría estar asociado a su dirección. Seleccione uno y vuelva a comprobarlo."
        else:
            return False, ""

    def __consume_coupon(self, coupon, user_id):
        # TODO: make it a transaction
        register_used = CouponUser(coupon_id=coupon.id, user_id=user_id)
        if self.save(register_used):
            coupon.stock -= 1
            self.save(coupon)
            return True
        else:
            return False




