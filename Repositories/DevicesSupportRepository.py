# -*- coding: utf-8 -*-

from Classes.Core.Repository import Repository


class DevicesSupportRepository(Repository):
    def __init__(self, db_context):
        super(DevicesSupportRepository, self).__init__(db_context)
