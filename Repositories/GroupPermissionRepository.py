# -*- coding: utf-8 -*-
from Classes.Core.Repository import Repository


class GroupPermissionRepository(Repository):
    def __init__(self, db_context):
        super(GroupPermissionRepository, self).__init__(db_context)
