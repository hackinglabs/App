from Classes.Core import Repository
from Models import IngredientPerCategory, Ingredient


class IngredientPerCategoryRepository(Repository):
    def __init__(self, db_context):
        super(IngredientPerCategoryRepository, self).__init__(db_context)

    def get_all_premium(self, id):
        return self.con.query(IngredientPerCategory).filter(IngredientPerCategory.subsidiary_id == id,
                                                            IngredientPerCategory.is_premium == True).all()

    def get_all_base(self, id):
        return self.con.query(IngredientPerCategory).filter(IngredientPerCategory.subsidiary_id == id,
                                                            IngredientPerCategory.is_base == True).all()

    def get_all_normal(self, id):
        return self.con.query(IngredientPerCategory).filter(IngredientPerCategory.subsidiary_id == id,
                                                            IngredientPerCategory.is_premium == False,
                                                            IngredientPerCategory.is_base == False).all()

    def get_products_category(self, id):
        return self.con.query(Ingredient).filter(Ingredient.ingredient_per_category_id == id).all()
