# -*- coding: utf-8 -*-
from Classes.Core.Repository import Repository


class UserTelephoneRepository(Repository):
    def __init__(self, db_context):
        super(UserTelephoneRepository, self).__init__(db_context)
