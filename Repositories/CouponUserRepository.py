
# -*- coding: utf-8 -*-
from sqlalchemy import func

from Classes.Core.Repository import Repository
from Models import User
from Models.CouponUser import CouponUser
from Models.Coupon import Coupon


class CouponUserRepository(Repository):
    def __init__(self, db_context):
        super(CouponUserRepository, self).__init__(db_context)

    def delete_used_coupon(self, user_id, coupon_id):
        self.con.begin()
        try:
            to_delete = self.con.query(CouponUser).filter(CouponUser.user_id == user_id,
                                                          CouponUser.coupon_id == coupon_id)

            to_delete.delete(synchronize_session=False)

            to_raise = self.con.query(Coupon).filter(Coupon.id == coupon_id)
            to_raise.stock += 1
            to_raise.save()

            self.con.commit()
            return True
        except:
            self.con.rollback()
            return False


    def get_coupons_used(self, start, finish=None):
        if finish is None:
            finish = start

        data = self.con.query(
            func.count(CouponUser.coupon_id),
            Coupon.code,
            Coupon.amount,
            Coupon.with_percentage,
            CouponUser.use_date
        ). \
            join(Coupon, Coupon.id == CouponUser.coupon_id).\
            join(User, User.id == CouponUser.user_id).\
            filter(CouponUser.use_date.between(start, finish)).\
            group_by(CouponUser.coupon_id,
                     Coupon.code,
                     Coupon.amount,
                     Coupon.with_percentage,
                     CouponUser.use_date,
                     ).\
            all()

        all = []
        for d in data:
            all.append({
                'quantity': d[0],
                'code': d[1],
                'amount': d[2],
                'with_percentage': d[3],
                'date': d[4].isoformat().strip().split("T")[0]
            })
        return all