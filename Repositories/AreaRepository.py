# -*- coding: utf-8 -*-
from sqlalchemy.orm import joinedload

from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models import City, State
from Models.Area import Area


class AreaRepository(Repository):
    def __init__(self, db_context):
        super(AreaRepository, self).__init__(db_context)

    def all(self):
        areas = self.con.query(Area).order_by(Area.name).all()

        if areas is None or len(areas) <= 0:
            raise NoneResult("We didn't found any areas")

        return areas

    def find(self, id):
        area = self.con.query(Area).options(joinedload(Area.city).
                                            joinedload(City.state).
                                            joinedload(State.country))\
            .filter(Area.id == id).first()
        return area

    def get_area_valid(self):
        areas = self.con.query(Area).filter(Area.id.in_((5, 1, 6, 4, 32, 43))).all()
        return areas
