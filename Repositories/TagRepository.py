# -*- coding: utf-8 -*-
from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models import CommerceTag
from Models.Tag import Tag
from sqlalchemy.orm import load_only


class TagRepository(Repository):
    def __init__(self, db_context):
        super(TagRepository, self).__init__(db_context)

    def all(self):
        tags = self.con.query(Tag).options(load_only("name", "id")).all()

        if tags is None or len(tags) <= 0:
            raise NoneResult("We didn't found tags")

        return tags

    def match_tags(self, strings):
        if type(strings) is list:
            tag_list = strings
        else:
            tag_list = strings.split(' ')
        tags = self.con.query(Tag).filter(Tag.name.in_(tag_list)).all()

    def all_names(self):
        tags = self.con.query(Tag)\
            .join(CommerceTag, CommerceTag.tag_id == Tag.id)\
            .filter(Tag.id == CommerceTag.tag_id)\
            .order_by(Tag.name.asc()).all()

        if tags is None or len(tags) <= 0:
            raise NoneResult("We didn't found tags")

        return tags
