from sqlalchemy.orm import joinedload

from Classes.Core.Repository import Repository
from Models import Tag
from Models.CommerceTag import CommerceTag


class CommerceTagRepository(Repository):
    def __init__(self, db_context):
        super(CommerceTagRepository, self).__init__(db_context)

    def find(self, id):
        commerce_tag = self.con.query(CommerceTag). \
            options(joinedload(CommerceTag.tag)).\
            options(joinedload(CommerceTag.commerce)).filter(CommerceTag.id == id).first()
        return commerce_tag

    def all(self):
        commerce_tags = self.con.query(CommerceTag).\
            options(joinedload(CommerceTag.commerce)).\
            options(joinedload(CommerceTag.tag)).\
            all()
        return commerce_tags

    def all_names(self):
        #Return commerceTag 'Id and Name' order Name
        names = self.con.query(CommerceTag, CommerceTag.tag_id, Tag.name).\
            options(joinedload(CommerceTag.tag)).\
            order_by(Tag.name.asc()).all()

        return names