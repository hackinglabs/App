# -*- coding: utf-8 -*-

from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models.Tag import Tag
from Models.UserTag import UserTag


class UserTagRepository(Repository):
    def __init__(self, db_context):
        super(UserTagRepository, self).__init__(db_context)

    def get_all_user_tag(self, id):
        try:
            tags = self.con.query(Tag).join(Tag.users_tag).\
                filter(UserTag.user_id == id).all()

        except:
            tags = None

        if tags is None or len(tags) <= 0:
            raise NoneResult("This user hasn't tags")

        return tags