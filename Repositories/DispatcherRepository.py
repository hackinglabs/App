from Classes.Core import Repository
import logging
from rom import util


class DispatcherRepository(Repository):
    def __init__(self, db_context):
        super(DispatcherRepository, self).__init__(db_context)
        self.logger = logging.getLogger(__name__)

    def find(self, lat, lon):
        for kms in range(1, 5):
            result = util.get_connection().georadius("geo:locations:geo_dispatchers", lon, lat, kms, 'km',
                                          withdist=True, sort='ASC')
            if len(result) > 0:
                break

        if len(result) > 0:
            if isinstance(result[0], (list, set,)):
                return result[0][0]
            else:
                return result[0]
        else:
            return None

    def remove_geo(self, id):
        res = self.redis.zrem("geo:locations:geo_dispatchers", id)
        return res == 1
