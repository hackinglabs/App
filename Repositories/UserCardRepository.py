# -*- coding: utf-8 -*-
from Classes.Core.Repository import Repository
from Models.UserCard import UserCard


class UserCardRepository(Repository):
    def __init__(self, db_context):
        super(UserCardRepository, self).__init__(db_context)

    def delete(self, user_id, card_id):
        self.con.begin()
        try:
            rows = self.con.query(UserCard).filter(UserCard.id == card_id,
                                                   UserCard.user_id == user_id). \
                delete(synchronize_session=False)
            self.con.commit()
            return True
        except:
            self.con.rollback()
            return False

    def delete_by_details(self, user_id, cc_type, last_digits, token=None):
        self.con.begin()
        try:
            query = self.con.query(UserCard).filter(UserCard.credit_card == cc_type,
                                                    UserCard.last_digits == last_digits,
                                                    UserCard.user_id == user_id)

            if token is not None:
                query = query.filter(UserCard.tbk_user == token)

            query.delete(synchronize_session=False)
            self.con.commit()
            return True
        except:
            self.con.rollback()
            return False
