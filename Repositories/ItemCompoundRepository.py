# -*- coding: utf-8 -*-

from Classes.Core.Repository import Repository
from Models import ItemCompound


class ItemCompoundRepository(Repository):
    def __init__(self, db_context):
        super(ItemCompoundRepository, self).__init__(db_context)

    def delete_all(self, id):
        self.con.begin()
        try:
            self.con.query(ItemCompound).filter(ItemCompound.option_compound_id == id).delete()
            self.con.commit()
            return True
        except:
            self.con.rollback()
            return False
