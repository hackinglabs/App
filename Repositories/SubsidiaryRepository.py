# -*- coding: utf-8 -*-
import logging
from rom import util
import arrow

from sqlalchemy import func
from sqlalchemy.orm import joinedload, contains_eager
from sqlalchemy.orm import Load

from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Classes.Geo import Geo
from Models import Discount
from Models import Favorite, SubCategory, Product, Tag
from Models import System
from Models.Category import Category
from Models.CommerceTag import CommerceTag
from Models.ProductTag import ProductTag
from Models.RatingSubsidiary import RatingSubsidiary
from Models.Subsidiary import Subsidiary
from Models.Commerce import Commerce
from Models.Area import Area
from Models.User import User
from Models import Redis


class SubsidiaryRepository(Repository):
    def __init__(self, db_context):
        super(SubsidiaryRepository, self).__init__(db_context)
        self.logger = logging.getLogger(__name__)

    def all(self):

        # Obtain subsidiaries from postgres filter by redis array
        subsidiaries = self.con.query(Subsidiary) \
            .join(Commerce, Commerce.id == Subsidiary.commerce_id) \
            .join(System, System.id == Commerce.system_id) \
            .options(Load(Subsidiary).load_only(Subsidiary.id,
                                                Subsidiary.alias,
                                                Subsidiary.address,
                                                Subsidiary.open_hours,
                                                Subsidiary.average_time,
                                                Subsidiary.pap,
                                                Subsidiary.is_premium,
                                                Subsidiary.coordinates,
                                                Subsidiary.short_description,
                                                Subsidiary.description,
                                                Subsidiary.enabled,
                                                Subsidiary.rating_average,
                                                Subsidiary.is_active,
                                                Subsidiary.commerce_id,
                                                Subsidiary.telephone,
                                                Subsidiary.cover
                                                ), \
                     contains_eager(Subsidiary.commerce).load_only(Commerce.id,
                                                                   Commerce.name,
                                                                   Commerce.system,
                                                                   Commerce.address,
                                                                   Commerce.logo,
                                                                   Commerce.alliance,
                                                                   Commerce.system_id). \
                     contains_eager(Commerce.system).load_only(System.name,
                                                               System.description,
                                                               System.logo)) \
            .all()

        #subsidiaries = self.con.query(Subsidiary) \
        #    .options(joinedload(Subsidiary.commerce)) \
        #    .all()

        return subsidiaries

    def get_premium(self):
        try:
            subsidiaries = self.con.query(Subsidiary) \
                .join(Subsidiary.commerce) \
                .options(Load(Subsidiary).load_only(Subsidiary.id,
                                                    Subsidiary.alias,
                                                    Subsidiary.address,
                                                    Subsidiary.average_time,
                                                    Subsidiary.pap,
                                                    Subsidiary.is_premium,
                                                    Subsidiary.rating_average,
                                                    Subsidiary.is_active,
                                                    Subsidiary.banner),
                         contains_eager(Subsidiary.commerce).load_only(Commerce.name,
                                                                       Commerce.id,
                                                                       Commerce.logo,
                                                                       Commerce.alliance)) \
                .filter(Subsidiary.is_premium == True, Subsidiary.enabled == True) \
                .limit(5).all()
        except:
            raise NoneResult("Error.")
        return subsidiaries

    def search(self, lat=None, lon=None, id=None, kms=10, offset=0,
               limit=20, order=None, type="radius", where=None):

        if order is None:
            order = [{'type': 'desc', 'by': 'rating'}]

        if type in ("radius", "tag"):
            # Obtain subsidiaries in a radius
            result = util.get_connection().georadius("geo_subsidiaries", lon,
                                                     lat, kms, 'km',
                                                     withdist=True, sort="ASC")

            result = [[int(i[0]), float(i[1])] for i in result]
            # If there is no subsidiaries, we return an empty array
            if len(result) <= 0:
                return []

            # id => distance dictionary
            all = {}
            within = []
            for s in result:
                within.append(s[0])
                all[s[0]] = s[1]

            if type == "radius":
                # Obtain subsidiaries from postgres filter by redis array
                subsidiaries = self.con.query(Subsidiary) \
                    .join(Commerce, Commerce.id == Subsidiary.commerce_id) \
                    .join(System, System.id == Commerce.system_id) \
                    .options(Load(Subsidiary).load_only(Subsidiary.id,
                                                        Subsidiary.alias,
                                                        Subsidiary.address,
                                                        Subsidiary.open_hours,
                                                        Subsidiary.average_time,
                                                        Subsidiary.pap,
                                                        Subsidiary.is_premium,
                                                        Subsidiary.coordinates,
                                                        Subsidiary.short_description,
                                                        Subsidiary.description,
                                                        Subsidiary.enabled,
                                                        Subsidiary.rating_average,
                                                        Subsidiary.is_active,
                                                        Subsidiary.commerce_id,
                                                        Subsidiary.cover
                                                        ), \
                             contains_eager(Subsidiary.commerce).load_only(Commerce.id,
                                                                           Commerce.name,
                                                                           Commerce.system,
                                                                           Commerce.address,
                                                                           Commerce.logo,
                                                                           Commerce.alliance,
                                                                           Commerce.system_id). \
                             contains_eager(Commerce.system).load_only(System.name,
                                                                       System.description,
                                                                       System.logo)) \
                    .filter(Subsidiary.id.in_(within), Subsidiary.enabled == True, Commerce.system_id == 1)
            else:
                sub1 = self.con.query(Subsidiary.id) \
                    .join(Category, Category.subsidiary_id == Subsidiary.id) \
                    .join(SubCategory, SubCategory.category_id == Category.id) \
                    .join(Product, Product.sub_category_id == SubCategory.id) \
                    .join(ProductTag, ProductTag.product_id == Product.id) \
                    .filter(ProductTag.tag_id == id, Subsidiary.id.in_(within))
                sub2 = self.con.query(Subsidiary.id) \
                    .join(
                    CommerceTag,
                    CommerceTag.commerce_id == Subsidiary.commerce_id
                ) \
                    .filter(CommerceTag.tag_id == id, Subsidiary.id.in_(within))

                q = sub1.union(sub2).subquery()

                subsidiaries = self.con.query(Subsidiary) \
                    .join(Subsidiary.commerce) \
                    .options(Load(Subsidiary).load_only(Subsidiary.id,
                                                        Subsidiary.alias,
                                                        Subsidiary.address,
                                                        Subsidiary.open_hours,
                                                        Subsidiary.average_time,
                                                        Subsidiary.pap,
                                                        Subsidiary.is_premium,
                                                        Subsidiary.coordinates,
                                                        Subsidiary.short_description,
                                                        Subsidiary.description,
                                                        Subsidiary.rating_average,
                                                        Subsidiary.is_active,
                                                        Subsidiary.commerce_id,
                                                        Subsidiary.cover
                                                        )) \
                    .options(contains_eager(Subsidiary.commerce).load_only(Commerce.id,
                                                                           Commerce.name,
                                                                           Commerce.address,
                                                                           Commerce.logo,
                                                                           Commerce.alliance,
                                                                           Commerce.system_id) \
                             .contains_eager(Commerce.system).load_only(System.name,
                                                                        System.description,
                                                                        System.logo)
                             ).filter(Subsidiary.id.in_(q), Subsidiary.enabled == True, Commerce.system_id == 1)

        elif type == "commerce":
            subsidiaries = self.con.query(Subsidiary). \
                options(joinedload(Subsidiary.commerce)) \
                .filter(Subsidiary.commerce_id == id,
                        Commerce.enabled == True,
                        Subsidiary.enabled == True,
                        Commerce.system_id == 1)

        # Count every subsidiaries in postgres
        row_count = self.con.query(func.count(Subsidiary.id)).scalar()

        # Set limit for row_count
        limit = row_count

        ob = ''
        for o in order:
            parsed = self.__parse_order(o['type'], o['by'])
            if parsed is not None:
                ob += parsed + " ,"
        ob = ob[0:-2]

        # Order and limit the query
        if ob is not None:
            subsidiaries = subsidiaries.order_by("subsidiaries.is_active DESC, " + ob)

        if where is not None and len(where) > 0:
            for w in where:
                quantity = int(w['type'])
                by = w['by']

                if by == "rating":
                    subsidiaries = subsidiaries.filter(Subsidiary.rating_average >= quantity)
                if by == "time":
                    subsidiaries = subsidiaries.filter(Subsidiary.average_time <= quantity)

        subsidiaries = subsidiaries.limit(limit).offset(offset).all()

        result = self.con.query(RatingSubsidiary) \
            .with_entities(RatingSubsidiary.subsidiary_id, func.count(RatingSubsidiary.subsidiary_id)) \
            .group_by(RatingSubsidiary.subsidiary_id) \
            .all()

        ratings = {}
        for res in result:
            ratings[res[0]] = res[1]

        result = self.con.query(Favorite) \
            .with_entities(Favorite.subsidiary_id, func.count(Favorite.subsidiary_id)) \
            .group_by(Favorite.subsidiary_id) \
            .all()

        favorites = {}
        for res in result:
            favorites[res[0]] = res[1]

        if subsidiaries is None or len(subsidiaries) == 0:
            sub_list = []
        else:
            sub_list = self.to_dict_new(subsidiaries)

        trade = []
        for d in sub_list:
            id_subsidiary = int(d['id'])

            if type != "commerce":
                d['distance'] = all[id_subsidiary]
                d['value_dispatch'] = self.__calculate_dispatch(d['distance'])

                # set mulitple trade
                system = {
                    "name": d["commerce"]["system"]["name"],
                    "logo": d["commerce"]["system"]["logo"],
                    "description": d["commerce"]["system"]["description"]
                }

                if system not in trade:
                    trade.append(system)

            if id_subsidiary in ratings:
                d['comments'] = ratings[id_subsidiary]
            else:
                d['comments'] = 0

            if id_subsidiary in favorites:
                d['favorites'] = favorites[id_subsidiary]
            else:
                d['favorites'] = 0

            # Get discount
            discount = self.con.query(Discount)\
                .options(Load(Discount).load_only(Discount.id, Discount.amount))\
                .filter(Discount.discount_type =='SUBSIDIARY',
                        Discount.type_id==d['id'],
                        Discount.expiration_date >= arrow.now('America/Santiago').to('utc').datetime)\
                .first()
            if discount is not None:
                d['discount'] = discount.amount


            # return average_time
            subsidiary_redis = Redis.Subsidiary.query.filter(id=d['id']).first()
            if subsidiary_redis is not None and subsidiary_redis.avg_time > 0:
                if subsidiary_redis.avg_time > d['average_time']:
                    d['average_time'] = subsidiary_redis.avg_time

            commerce_tags = self.con.query(CommerceTag).filter(CommerceTag.commerce_id == d['commerce_id']).limit(3)

            if commerce_tags is not None:
                tags = [t.tag.name for t in commerce_tags]
                d['tags'] = tags

        if type != "commerce":
            new_list = sorted(sub_list, key=lambda x: (-x['is_premium'], x['distance']))
        else:
            new_list = sub_list

        return {
            'response': new_list,
            'trade': trade,
            'total': row_count
        }

    def exists_geo(self):
        return util.get_connection().exists("geo_subsidiaries")

    def remove_geo(self, id):
        res = util.get_connection().zrem("geo_subsidiaries", id)
        return res == 1

    def save_geo(self):
        subsidiaries = self.all()
        now = arrow.now('America/Santiago').to('utc').replace(days=-1).date()
        # BACKWARD COMPATIBILITY
        all_values = []

        for subsidiary_redis in Redis.Subsidiary.query.all():
            subsidiary_redis.delete()

        for subsidiary in subsidiaries:
            geo = subsidiary.to_geo()

            # BACKWARD COMPATIBILITY
            all_values.extend([geo.lon, geo.lat, geo.id])

            rs = Redis.Subsidiary(id=subsidiary.id,
                                  full_name=subsidiary.commerce.name + " " + subsidiary.alias,
                                  alias=subsidiary.alias,
                                  avg_rating=subsidiary.rating_average,
                                  avg_time=subsidiary.average_time,
                                  avg_time_date=now,
                                  active=subsidiary.is_active,
                                  lat=geo.lat,
                                  lon=geo.lon)

            try:
                rs.save()
            except Exception as e:
                self.logger.error("Can't save Subsidiary %s to redis: %s", subsidiary.id, e.message)

        # BACKWARD COMPATIBILITY
        if len(all_values) > 0:
            util.get_connection().geoadd("geo_subsidiaries", *all_values)

    def get_details(self, id):

        try:
            subsidiary = self.con.query(Subsidiary) \
                .join(Commerce, Commerce.id == Subsidiary.commerce_id) \
                .join(Area, Area.id == Subsidiary.area_id) \
                .join(Category, Category.subsidiary_id == Subsidiary.id) \
                .join(SubCategory, SubCategory.category_id == Category.id) \
                .join(Product, Product.sub_category_id == SubCategory.id) \
                .filter(Category.enabled == True,
                        SubCategory.enabled == True,
                        Product.enabled == True,
                        Subsidiary.id == id) \
                .options(Load(Subsidiary).load_only(Subsidiary.id,
                                                    Subsidiary.alias,
                                                    Subsidiary.address,
                                                    Subsidiary.open_hours,
                                                    Subsidiary.average_time,
                                                    Subsidiary.pap,
                                                    Subsidiary.is_premium,
                                                    Subsidiary.coordinates,
                                                    Subsidiary.commerce_id,
                                                    Subsidiary.area_id,
                                                    Subsidiary.headquarter,
                                                    Subsidiary.enabled,
                                                    Subsidiary.short_description,
                                                    Subsidiary.description,
                                                    Subsidiary.rating_average,
                                                    Subsidiary.is_active,
                                                    Subsidiary.facebook_title,
                                                    Subsidiary.facebook_url,
                                                    Subsidiary.facebook_image,
                                                    Subsidiary.facebook_description,
                                                    Subsidiary.facebook_sitename,
                                                    Subsidiary.suggestion_type_vehicle,
                                                    Subsidiary.suggestion_require,
                                                    Subsidiary.banner,
                                                    Subsidiary.cover,
                                                    Subsidiary.telephone,
                                                    Subsidiary.suggestion_qualification_vehicle),
                         contains_eager(Subsidiary.area).load_only(Area.id,
                                                                   Area.name),
                         contains_eager(Subsidiary.commerce).load_only(Commerce.id,
                                                                       Commerce.name,
                                                                       Commerce.slug,
                                                                       Commerce.trade,
                                                                       Commerce.logo,
                                                                       Commerce.address,
                                                                       Commerce.enabled,
                                                                       Commerce.business_name,
                                                                       Commerce.alliance,
                                                                       Commerce.user_id,
                                                                       Commerce.colors),
                         contains_eager(Subsidiary.categories).load_only(Category.id,
                                                                         Category.name,
                                                                         Category.position,
                                                                         Category.description,
                                                                         Category.subsidiary_id,
                                                                         Category.image,
                                                                         Category.enabled) \
                         .contains_eager(Category.sub_categories).load_only(SubCategory.id,
                                                                            SubCategory.name,
                                                                            SubCategory.category_id,
                                                                            SubCategory.is_visible,
                                                                            SubCategory.enabled,
                                                                            SubCategory.image,
                                                                            SubCategory.navigable,
                                                                            SubCategory.description)
                         .contains_eager(SubCategory.products)) \
                .order_by(Category.position) \
                .order_by(Product.position) \
                .all()

        except Exception as e:
            self.logger.error("Error in get_details on Subsidiary: " + e.message)
            subsidiary = None

        if not subsidiary:
            raise NoneResult("We didn't foud this subsidiary")

        return subsidiary.pop(0)

    def get_details_for_system(self, id):

        try:
            subsidiary = self.con.query(Subsidiary) \
                .join(Commerce, Commerce.id == Subsidiary.commerce_id) \
                .join(Area, Area.id == Subsidiary.area_id) \
                .join(Category, Category.subsidiary_id == Subsidiary.id) \
                .join(SubCategory, SubCategory.category_id == Category.id) \
                .join(Product, Product.sub_category_id == SubCategory.id) \
                .filter(Subsidiary.id == id) \
                .options(Load(Subsidiary).load_only(Subsidiary.id,
                                                    Subsidiary.alias,
                                                    Subsidiary.address,
                                                    Subsidiary.open_hours,
                                                    Subsidiary.average_time,
                                                    Subsidiary.pap,
                                                    Subsidiary.is_premium,
                                                    Subsidiary.coordinates,
                                                    Subsidiary.commerce_id,
                                                    Subsidiary.telephone,
                                                    Subsidiary.area_id,
                                                    Subsidiary.headquarter,
                                                    Subsidiary.enabled,
                                                    Subsidiary.short_description,
                                                    Subsidiary.description,
                                                    Subsidiary.rating_average,
                                                    Subsidiary.is_active,
                                                    Subsidiary.facebook_title,
                                                    Subsidiary.facebook_url,
                                                    Subsidiary.facebook_image,
                                                    Subsidiary.facebook_description,
                                                    Subsidiary.facebook_sitename,
                                                    Subsidiary.suggestion_type_vehicle,
                                                    Subsidiary.suggestion_require,
                                                    Subsidiary.banner,
                                                    Subsidiary.cover,
                                                    Subsidiary.suggestion_qualification_vehicle),
                         contains_eager(Subsidiary.area).load_only(Area.id,
                                                                   Area.name),
                         contains_eager(Subsidiary.commerce).load_only(Commerce.id,
                                                                       Commerce.name,
                                                                       Commerce.slug,
                                                                       Commerce.trade,
                                                                       Commerce.logo,
                                                                       Commerce.address,
                                                                       Commerce.enabled,
                                                                       Commerce.business_name,
                                                                       Commerce.alliance,
                                                                       Commerce.user_id),
                         contains_eager(Subsidiary.categories).load_only(Category.id,
                                                                         Category.name,
                                                                         Category.position,
                                                                         Category.description,
                                                                         Category.subsidiary_id,
                                                                         Category.image,
                                                                         Category.enabled) \
                         .contains_eager(Category.sub_categories).load_only(SubCategory.id,
                                                                            SubCategory.name,
                                                                            SubCategory.category_id,
                                                                            SubCategory.is_visible,
                                                                            SubCategory.enabled,
                                                                            SubCategory.image,
                                                                            SubCategory.navigable,
                                                                            SubCategory.description)
                         .contains_eager(SubCategory.products)) \
                .first()
        except Exception as e:
            self.logger.error("Error in get_details on Subsidiary: " + e.message)
            raise NoneResult("We didn't foud this subsidiary")

        return subsidiary

    def products_for_client(self, id):

        try:
            products = self.con.query(Product).join(SubCategory, Product.sub_category_id == SubCategory.id) \
                .join(Category, Category.id == SubCategory.category_id) \
                .join(Subsidiary, Subsidiary.id == Category.subsidiary_id) \
                .filter(Subsidiary.id == id, Subsidiary.enabled == True).all()
        except:
            products = None

        if products is None:
            raise NoneResult("We didn't foud this subsidiary")

        return products

    def get_ratings(self, id):
        ratings = self.con.query(RatingSubsidiary) \
            .join(RatingSubsidiary.user) \
            .options(contains_eager(RatingSubsidiary.user)
                     .load_only(User.id,
                                User.first_name,
                                User.last_name,
                                User.avatar)
                     ) \
            .filter(RatingSubsidiary.subsidiary_id == id) \
            .all()

        return ratings

    def __parse_order(self, order_type, order_by):
        if order_type == "desc":
            ot = "desc"
        else:
            ot = "asc"

        if order_by == "name":
            ob = "commerces_1.name || ' ' || subsidiaries.alias {0}".format(ot)
        elif order_by == "time":
            ob = "subsidiaries.average_time ASC"
        elif order_by == "price":
            ob = "subsidiaries.pap {0}".format(ot)
        elif order_by == "distance":
            ob = None
        else:
            ob = "subsidiaries.rating_average DESC"

        return ob

    def favorites_count(self, id):
        favorites = self.con.query(func.count(Favorite.subsidiary_id)) \
            .filter(Favorite.subsidiary_id == id) \
            .scalar()
        return favorites

    def ratings_count(self, id):
        ratings = self.con.query(func.count(RatingSubsidiary.subsidiary_id)) \
            .filter(RatingSubsidiary.subsidiary_id == id) \
            .scalar()
        return ratings

    def get_commerce(self, id):
        commerce_id = self.con.query(Subsidiary.commerce_id) \
            .filter(Subsidiary.id == id) \
            .first()
        return commerce_id[0]

    def categories_for_subsidiary(self, id):
        categories = self.con.query(Category).filter(Category.subsidiary_id == id).order_by(Category.position).all()
        return categories

    def get_compounds(self, id):
        categories = self.con.query(Category) \
            .join(Category.compounds) \
            .options(contains_eager(Category.compounds)) \
            .filter(Category.subsidiary_id == id).all()

        return categories

    def save_tag_base(self, tags, commerce_id):
        try:
            self.con.query(CommerceTag).filter(CommerceTag.commerce_id == commerce_id).delete()
            self.con.commit()
        except:
            self.con.rollback()

        for tag in tags:
            tag_name = tag.capitalize()
            tag_exists = self.con.query(Tag).filter(Tag.name == tag_name).first()
            if tag_exists is None:
                tag_create = Tag(name=tag_name, system_id=1)
                if self.save(tag_create):
                    commerce_tag = CommerceTag(tag_id=tag_create.id, commerce_id=commerce_id)
                    self.save(commerce_tag)
            else:
                assigned = self.con.query(CommerceTag).filter(CommerceTag.commerce_id == commerce_id,
                                                              CommerceTag.tag_id == tag_exists.id).first()
                if assigned is None:
                    commerce_tag = CommerceTag(tag_id=tag_exists.id, commerce_id=commerce_id)
                    self.save(commerce_tag)
        return True

    def is_active(self, id):
        query = "select is_active from subsidiaries where id=%d" % id
        raws = self.con.execute(query)
        raw = [raw[0] for raw in raws]
        return raw[0]

    def increment_correlative(self, id):
        query = "select correlative from subsidiaries where id=%d" % id
        raws = self.con.execute(query)
        raw = [raw[0] for raw in raws]
        correlative = raw[0]

        self.con.begin()
        try:
            self.con.query(Subsidiary).filter(Subsidiary.id == id).update({'correlative': correlative + 1})
            self.con.commit()
        except:
            self.con.rollback()
        return correlative

    def all_active(self):
        subsidiaries = self.con.query(Subsidiary) \
            .join(Subsidiary.commerce) \
            .options(contains_eager(Subsidiary.commerce)) \
            .filter(
            Subsidiary.is_active == True).all()

        if len(subsidiaries) == 0:
            return None

        return subsidiaries

    def get_user_by_commerce(self, subsidiary_id):
        user = self.con.query(User) \
            .join(Subsidiary, Subsidiary.id == subsidiary_id) \
            .join(Commerce, Commerce.id == Subsidiary.commerce_id) \
            .filter(User.id == Commerce.user_id) \
            .first()

        return user

    def get_discount(self, subsidiary_id):
        discount = self.con.query(Discount).options(Load(Discount).load_only(Discount.id, Discount.amount)).filter(
            Discount.discount_type == 'SUBSIDIARY',
            Discount.type_id == subsidiary_id,
            Discount.expiration_date >= arrow.now('America/Santiago').to('utc').datetime).first()

        if discount is None:
            return None

        return discount.amount

    def __calculate_dispatch(self, distance):
        # Transform distance every 100 mts
        distance *= 10
        distance = round(distance, 1)

        # TODO: This must be configurable
        min_kms = 4
        min_price = 2000
        mts_price = 70
        amount = min_price

        min_hundred_meters = min_kms * 10
        if distance - min_hundred_meters > 0:
            amount += (distance - min_hundred_meters) * mts_price

        return amount
