# -*- coding: utf-8 -*-
from Classes.Core.Repository import Repository


class PermissionRepository(Repository):
    def __init__(self, db_context):
        super(PermissionRepository, self).__init__(db_context)
