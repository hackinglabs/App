import logging

from Classes import ConnectCassandra
from Classes.Core import Repository
from Classes.Core.Exceptions import NoneResult, SaveError


class LoggerRepository(Repository):
    def __init__(self, db_context):
        super(LoggerRepository, self).__init__(db_context)
        self.logger = logging.getLogger(__name__)
        self.cassandra = ConnectCassandra()


