# -*- coding: utf-8 -*-
from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models import User
from Models.Subsidiary import Subsidiary
from Models.RatingSubsidiary import RatingSubsidiary
from sqlalchemy import func, desc
from sqlalchemy.orm import joinedload


class RatingSubsidiaryRepository(Repository):
    def __init__(self, db_context):
        super(RatingSubsidiaryRepository, self).__init__(db_context)

    def find_with_user(self, id):
        rating = self.con.query(RatingSubsidiary) \
            .options(joinedload(RatingSubsidiary.user).load_only(User.id,
                                                                 User.first_name,
                                                                 User.last_name,
                                                                 User.email,
                                                                 User.avatar,
                                                                 User.gender,
                                                                 User.birth_date)) \
            .filter(RatingSubsidiary.id == id) \
            .first()
        return rating

    def ratings(self, id):
        try:
            ratings = self.con.query(RatingSubsidiary) \
                .options(joinedload(RatingSubsidiary.user).load_only(User.id,
                                                                     User.first_name,
                                                                     User.last_name,
                                                                     User.email,
                                                                     User.avatar,
                                                                     User.gender,
                                                                     User.birth_date)) \
                .filter(RatingSubsidiary.subsidiary_id == id).order_by(desc(RatingSubsidiary.date)).all()
        except Exception as e:
            self.logger.error("Error in ratings: " + e.message)
            ratings = None

        if not ratings:
            raise NoneResult("We didn't found any comment")

        return ratings

    def calculate_rating_average(self, subsidiary_id):
        av = self.con.query(func.avg(RatingSubsidiary.qualification)).filter(
            RatingSubsidiary.subsidiary_id == subsidiary_id).scalar()

        if av is not None:
            av = round(float(av), 1)
            subsidiary = self.con.query(Subsidiary).filter(Subsidiary.id == subsidiary_id).first()

            if subsidiary is None:
                return False

            subsidiary.rating_average = av

            try:
                subsidiary_save = self.save(subsidiary)
            except:
                return False

            if subsidiary_save:
                return True
            else:
                return False
        else:
            return False
