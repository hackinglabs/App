from Classes.Core.Repository import Repository
from Models.Product import Product


class SubCategoryRepository(Repository):
    def __init__(self, db_context):
        super(SubCategoryRepository, self).__init__(db_context)

    def get_products(self, id):
        products = self.con.query(Product).filter(Product.sub_category_id == id).order_by(Product.position).all()
        return products

    def get_products_isEnabled(self, id):
        products = self.con.query(Product)\
            .filter(Product.sub_category_id == id,
                    Product.enabled == True)\
            .order_by(Product.position).all()
        return products
