import logging

from Classes import ConnectCassandra
from Classes.Core import Repository
from Classes.Core.Exceptions import NoneResult, SaveError
from Models.Cassandra import OrdersUser, Order, Payment
from Models.Cassandra.WebpayOrder import WebpayOrder


class OrderRepository(Repository):
    def __init__(self, db_context):
        super(OrderRepository, self).__init__(db_context)
        self.logger = logging.getLogger(__name__)
        self.cassandra = ConnectCassandra()

    def get(self, uuid):
        order = Order.objects(id=uuid).first()

        if order is None:
            raise NoneResult("Can't found the order id")
        return order

    def user_get(self, uuid, user_id):
        order = OrdersUser.objects(id=uuid, user_id=user_id).first()

        if order is None:
            raise NoneResult("Can't found the order for that user.")
        return order

    def update_payment(self, uuid=None, user=None, token=None, data=None):
        if data is None:
            raise ValueError("update_payment function must have data.")
        payment = Payment(**data)

        try:
            if uuid is not None:
                Order.objects(id=uuid).update(payment=payment)

                if user is not None:
                    OrdersUser.objects(id=uuid, user_id=user).update(payment=payment)

            if token is not None:
                wo = WebpayOrder()
                temp_data = {}
                for k in wo.keys():
                    if k in data:
                        temp_data[k] = data[k]
                WebpayOrder.objects(token_ws=token).update(**temp_data)
        except Exception as e:
            self.logger.error(e.message)
            raise SaveError("Can't update payment data.")
