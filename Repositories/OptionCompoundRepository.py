# -*- coding: utf-8 -*-

from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models import OptionCompound
from Models.ItemCompound import ItemCompound


class OptionCompoundRepository(Repository):
    def __init__(self, db_context):
        super(OptionCompoundRepository, self).__init__(db_context)

    def get_compounds(self, id):
        try:
            item_compounds = self.con.query(ItemCompound).filter(ItemCompound.option_compound_id == id).all()
        except:
            item_compounds = None

        if item_compounds is None:
            raise NoneResult("We didn't found any data")

        ids_compounds = [option.compound_id for option in item_compounds]
        return ids_compounds

    def delete(self, id):
        op = self.con.query(OptionCompound).filter(OptionCompound.id == id).first()
        if op is not None:
            self.con.begin()
            try:
                self.con.query(ItemCompound).filter(ItemCompound.option_compound_id == op.id).delete()
                self.con.delete(op)
                self.con.commit()
                response = True
            except:
                self.con.rollback()
                response = False
        else:
            response = False

        return response
