# -*- coding: utf-8 -*-

from sqlalchemy.orm import load_only, joinedload, contains_eager

from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models import User
from Models.Commerce import Commerce
from Models.CommerceTag import CommerceTag
from Models.CommerceUser import CommerceUser
from Models.Subsidiary import Subsidiary
from Models.Tag import Tag
from Models.UserGroup import UserGroup
from Models.UserSubsidiary import UserSubsidiary


class CommerceRepository(Repository):
    def __init__(self, db_context):
        super(CommerceRepository, self).__init__(db_context)

    def get_all_names(self):
        all_commerces = self.con.query(Commerce).options(load_only("id", "name", "logo", "slug")).filter(Commerce.enabled == True).all()

        if all_commerces is None or len(all_commerces) <= 0:
            raise NoneResult("We didn't found any commerce")

        return all_commerces

    def get_subsidiaries(self, id):
        subsidiaries = self.con.query(Subsidiary).filter(Subsidiary.commerce_id == id).all()
        return subsidiaries

    def get_commerce_by_slug(self, slug):
        commerce = self.con.query(Commerce) \
            .join(Commerce.subsidiaries)\
            .options(contains_eager(Commerce.subsidiaries)) \
            .filter(Commerce.slug == slug, Commerce.enabled == True, Subsidiary.enabled == True) \
            .all()
        if not commerce:
            raise NoneResult("We didn't found any commerce with that slug.")
        return commerce[0]

    def get_admin(self, id):
        commerce = self.con.query(Commerce).filter(Commerce.id == id).first()
        if commerce is not None:
            admin = commerce.user
            return admin
        else:
            return None

    def create_commerce_user(self, user, commerce_id):
        commerce_user = CommerceUser(user_id=user, commerce_id=commerce_id)
        group_user = UserGroup(group_id=4, user_id=user.id)
        try:
            self.save(commerce_user)
            self.save(group_user)
            return True
        except Exception as e:
            self.logger.error("Error create_commerce_user: " + e.message)
            return False

    def get_users(self, id):
        commerce = self.con.query(Commerce).filter(Commerce.id == id).first()
        commerce_users = self.con.query(CommerceUser).options(
            joinedload(CommerceUser.user).joinedload(User.users_subsidiaries)).filter(
            CommerceUser.commerce_id == commerce.id).all()

        return commerce_users

    def get_users_subsidiaries(self, id):
        subsidiaries = self.con.query(Subsidiary).options(
            joinedload(Subsidiary.users_subsidiaries).joinedload(UserSubsidiary.user)).filter(
            Subsidiary.commerce_id == id).all()

        return subsidiaries

    def disable_all_subsidiaries(self, commerce_id):
        try:
            self.con.query(Subsidiary).filter(Subsidiary.commerce_id == commerce_id).update({'enabled': False})
        except Exception as e:
            self.logger.error("Error in update all subsidiaries from commerce: " + e.message)

    def get_commerce_tags(self, commerce_id):
        query = self.con.query(Tag.name)\
            .join(CommerceTag, CommerceTag.tag_id == Tag.id)\
            .filter(CommerceTag.commerce_id == commerce_id)\
            .limit(20)\
            .all()

        self.logger.error(query)

        tags = [tag[0] for tag in query]

        if tags is not None:
            return tags
        else:
            return None

    def get_commerce_for_user(self, user_id):
        commerce = self.con.query(Commerce).filter(Commerce.user_id == user_id).first()

        if commerce is None:
            return None
        else:
            return commerce