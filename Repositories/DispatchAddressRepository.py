# -*- coding: utf-8 -*-

from sqlalchemy.orm import joinedload, contains_eager
from Classes.Core.Exceptions import NoneResult
from Classes.Core.Repository import Repository
from Models.DispatchAddress import DispatchAddress
from Models.Area import Area
from Models.City import City
from Models.State import State


class DispatchAddressRepository(Repository):
    def __init__(self, db_context):
        super(DispatchAddressRepository, self).__init__(db_context)

    def filter_by_user(self, id, user):
        try:
            address = self.con.query(DispatchAddress) \
                .filter(DispatchAddress.user_id == user, DispatchAddress.id == id) \
                .first()
        except:
            address = None

        if address is None:
            raise NoneResult("This user hasn't addresses")

        return address

    def find_full(self, dispatch_id):
        dispatch_address = self.con.query(DispatchAddress) \
            .join(Area, Area.id == DispatchAddress.area_id) \
            .join(City, City.id == Area.city_id)\
            .join(State, State.id == City.state_id)\
            .options(contains_eager(DispatchAddress.area).
                     contains_eager(Area.city).
                     contains_eager(City.state).
                     contains_eager(State.country)) \
            .filter(DispatchAddress.id == dispatch_id) \
            .first()

        return dispatch_address
