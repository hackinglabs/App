from Classes.Core.Repository import Repository
from Models.DispatcherOrder import DispatcherOrder


class DispatcherOrderRepository(Repository):
    def __init__(self, db_context):
        super(DispatcherOrderRepository, self).__init__(db_context)

    def find(self, dispatcher_id, current):

        if dispatcher_id is None or current is None:
            print "Error params"
            return None

        dispatcher_order = self.con.query(DispatcherOrder) \
            .filter(DispatcherOrder.dispatcher_id == dispatcher_id,
                    DispatcherOrder.current == current).first()

        if dispatcher_order is None:
            return None

        return dispatcher_order

    def remove(self, dispatcher_order):
        if dispatcher_order is None:
            self.logger.error('DispatcherOrder is None')

            return False

        try:
            self.con.delete(dispatcher_order)
        except Exception as e:
            self.logger.error("Error in delete model DispatcherOrder: " + e.message)

            return False

        return True
