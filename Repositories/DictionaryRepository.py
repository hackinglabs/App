# -*- coding: utf-8 -*-

from Classes.Core.Repository import Repository
from Models.Dictionary import Dictionary


class DictionaryRepository(Repository):
    def __init__(self, db_context):
        super(DictionaryRepository, self).__init__(db_context)

    def fix(self, string):
        resp = self.con.query(Dictionary). \
            filter(Dictionary.name == string). \
            first()

        if resp is None:
            return string
        else:
            return resp.value
