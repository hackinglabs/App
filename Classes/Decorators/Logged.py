# -*- coding: utf-8 -*-
import json

from jose import jwt
from wheezy.http import unauthorized, bad_request

from Classes.Permission import Permission
from Models.User import User
from Models import Redis


def logged(perms=None, wrapped=None, bypass=False):
    def decorate(method):

        def check(http, *args, **kwargs):
            if "HTTP_AUTHORIZATION" not in http.request.environ:
                return unauthorized()

            secret = http.options["token_secret"]
            try:
                # Get JWT
                token = http.request.environ["HTTP_AUTHORIZATION"].split()[1]
                # Decode JWT
                decoded = jwt.decode(token, secret, algorithms=['HS256'])

                # Convert to dict whether is a string
                if isinstance(decoded, basestring):
                    decoded = json.loads(decoded)

                http.logged = decoded

                if perms is not None:
                    if isinstance(perms, basestring):
                        required = {perms: 0}
                    elif isinstance(perms, list):
                        required = {}
                        for p in perms:
                            required[p] = 0
                    elif not isinstance(perms, dict):
                        return bad_request()

                    user_permissions = decoded['permissions']
                    user_permissions = Permission(user_permissions)

                    has_one = False
                    for r in required:
                        if user_permissions.has_perm(r).with_level(required[r]):
                            has_one = True
                            break

                    if not has_one:
                        return unauthorized()

            except Exception as e:
                return unauthorized()

            return method(http, *args, **kwargs)

        return check

    if wrapped is None:
        return decorate
    else:
        return decorate(wrapped)
