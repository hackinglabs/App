# -*- coding: utf-8 -*-
import logging

import msgpack
from wheezy.http.response import HTTPResponse
import base64
from Models import Redis


def cache(wrapped=None, private=False, type=None):
    def decorate(method):
        def check(http, *args, **kwargs):
            logger = logging.getLogger(__name__)

            if type is None:
                return method(http, *args, **kwargs)

            # Get the requested path
            path = http.request.path

            if private:
                try:
                    path += str(http.logged['user'])
                except Exception as e:
                    logger.error(e.message)
                    return method(http, *args, **kwargs)

            # Find if is cached
            cached = Redis.Cache.get_by(url=path)

            # If is not cached we will going to do normal request
            if cached is None:
                response = method(http, *args, **kwargs)

                headers = msgpack.packb(response.headers)
                buffer = msgpack.packb(response.buffer)

                # Set cache redis object
                obj = Redis.Cache(
                    url=path,
                    headers=base64.b64encode(headers),
                    content_type=response.content_type,
                    status_code=response.status_code,
                    buffer=base64.b64encode(buffer),
                    type_cache=type
                )
                try:
                    obj.save()
                except:
                    pass
            else:
                headers = list(msgpack.unpackb(base64.b64decode(cached.headers), use_list=False))
                buffer = msgpack.unpackb(base64.b64decode(cached.buffer))

                encoding = http.options['ENCODING']
                response = HTTPResponse('application/json; charset=' + encoding, encoding)
                response.headers = headers
                response.content_type = cached.content_type
                response.status_code = cached.status_code
                response.buffer = buffer

            return response
        return check

    if wrapped is None:
        return decorate
    else:
        return decorate(wrapped)


def update_cache(wrapped=None):
    def decorate(method):
        def update(http, *args, **kwargs):
            # Get the requested path
            path = http.request.path

            #Steps to proced:
            """
            - Check Method
                - POST: Detele general and specific
            """
            cached = Redis.Cache.get_by(url=path)

            response = method(http, *args, **kwargs)
            return response
        return update
    if wrapped is None:
        return decorate
    else:
        return decorate(wrapped)

