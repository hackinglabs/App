from wheezy.http import method_not_allowed


def ajax(wrapped=None):
    def decorate(method):
        def check(http, *args, **kwargs):
            if not http.request.ajax:
                return method_not_allowed()
            return method(http, *args, **kwargs)
        return check

    if wrapped is None:
        return decorate
    else:
        return decorate(wrapped)