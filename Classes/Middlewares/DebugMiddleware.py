# -*- coding: utf-8 -*-
import json
import logging


def debug_middleware(request, following):
    if following is not None:
        logger = logging.getLogger(__name__)

        try:
            obj = request.form
        except Exception as e:
            obj = {}

        obj = json.dumps(obj, indent=4)

        try:
            logger.info("REQUESTED METHOD: %s", request.method)
            logger.info("REQUESTED PATH:   %s", request.path)
            logger.info("CONTENT TYPE:     %s", request.content_type)
            logger.info("DATA RECEIVED: \n%s", obj)
        except Exception as e:
            pass

        return following(request)
