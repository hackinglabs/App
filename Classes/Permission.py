
class Permission(object):

    _level = 0
    _name = ""

    _permission_list = None

    _final_bool = True
    _check_perm = False
    _actual_perm = None
    _temp_permission = None

    def __init__(self, name=None, level=None):
        # If is dict, is the detail of the user (jwt)
        if isinstance(name, dict):
            if "permissions" in name:
                self._permission_list = name["permissions"]
            else:
                self._permission_list = name
        if isinstance(name, list):
            self._permission_list = {}
        else:
            if level is not None:
                self._level = level

            if name is None or name == "":
                raise ValueError("Should have a name")
            self._name = name

    def to_dict(self):
        return self._permission_list

    def has_perm(self, name):
        name = name.lower()
        self._check_perm = True
        self._actual_perm = name

        if self._permission_list is not None:
            self._final_bool = name in self._permission_list
        return self

    def with_level(self, level):
        try:
            level = int(level)
        except:
            raise ValueError("Level should be an integer.")

        if self._check_perm is False:
            raise ValueError("Can't check level without check perm.")

        if self._final_bool is False:
            return False

        checking_level = self._permission_list[self._actual_perm]
        self._actual_perm = None

        if checking_level >= level:
            return True
        else:
            return False

    def has_every(self, perm):
        if not isinstance(perm, Permission):
            raise ValueError("Has to be a Permission Object")
        perm_dict = perm.to_dict()
        for v in perm_dict:
            if not self.has_perm(v).with_level(perm_dict[v]):
                return False
        return True

    def has_one(self, perm):
        if not isinstance(perm, Permission):
            raise ValueError("Has to be a Permission Object")
        perm_dict = perm.to_dict()
        for v in perm_dict:
            if self.has_perm(v).with_level(perm_dict[v]):
                return True
        return False

    def __nonzero__(self):
        return self._final_bool
