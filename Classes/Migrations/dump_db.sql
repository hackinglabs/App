--
-- Data for Name: countries; Type: TABLE DATA; Schema: public; Owner: lion
--

INSERT INTO countries VALUES (1, 'Chile', 56);

--
-- Data for configurations
--
INSERT INTO configurations VALUES(1, 1, 4000, 2000.0, 70, 0.00146, 'CLP', 'es_CL', 13.0, 19.0, '$', '.', ',', 0);

--
-- Data for Name: states; Type: TABLE DATA; Schema: public; Owner: lion
--

INSERT INTO states VALUES (1, 'Tarapacá', 1, -4);
INSERT INTO states VALUES (2, 'Antofagasta', 1, -4);
INSERT INTO states VALUES (3, 'Atacama', 1 ,-4);
INSERT INTO states VALUES (4, 'Coquimbo', 1, -4);
INSERT INTO states VALUES (5, 'Valparaiso', 1, -4);
INSERT INTO states VALUES (6, 'Lib. Bdo. OHiggins', 1, -4);
INSERT INTO states VALUES (7, 'Maule', 1, -4);
INSERT INTO states VALUES (8, 'Bío-Bío', 1, -4);
INSERT INTO states VALUES (9, 'Araucanía', 1, -4);
INSERT INTO states VALUES (10, 'Los Lagos', 1, -4);
INSERT INTO states VALUES (11, 'Aysén del General Carlos Ibáñez del Campo', 1, -4);
INSERT INTO states VALUES (12, 'Magallanes y Antartica Chilena', 1, -4);
INSERT INTO states VALUES (13, 'Metropolitana', 1, -4);
INSERT INTO states VALUES (14, 'Los Ríos', 1, -4);
INSERT INTO states VALUES (15, 'Arica y Parinacota', 1, -4);


--
-- Data for Name: cities; Type: TABLE DATA; Schema: public; Owner: lion
--

INSERT INTO cities VALUES (1, 'Santiago', 13);
INSERT INTO cities VALUES (2, 'Concepcion', 8);

--
-- Data for Name: areas; Type: TABLE DATA; Schema: public; Owner: lion
--

INSERT INTO areas VALUES (1, 'Providencia', 1);
INSERT INTO areas VALUES (2, 'Santiago', 1);
INSERT INTO areas VALUES (3, 'Ñuñoa', 1);
INSERT INTO areas VALUES (4, 'Las Condes', 1);
INSERT INTO areas VALUES (5, 'La Reina', 1);
INSERT INTO areas VALUES (6, 'Vitacura', 1);
INSERT INTO areas VALUES (7, 'Alto Jahuel', 1);
INSERT INTO areas VALUES (9, 'Batuco', 1);
INSERT INTO areas VALUES (10, 'Buin', 1);
INSERT INTO areas VALUES (11, 'Calera de Tango', 1);
INSERT INTO areas VALUES (12, 'Centro', 1);
INSERT INTO areas VALUES (13, 'Cerrillos', 1);
INSERT INTO areas VALUES (14, 'Cerro Navia', 1);
INSERT INTO areas VALUES (15, 'Champa', 1);
INSERT INTO areas VALUES (16, 'Colina', 1);
INSERT INTO areas VALUES (17, 'Conchalí', 1);
INSERT INTO areas VALUES (18, 'El Bosque', 1);
INSERT INTO areas VALUES (19, 'El Monte', 1);
INSERT INTO areas VALUES (20, 'Estación Central', 1);
INSERT INTO areas VALUES (21, 'Hospital', 1);
INSERT INTO areas VALUES (22, 'Huechuraba', 1);
INSERT INTO areas VALUES (23, 'Independencia', 1);
INSERT INTO areas VALUES (24, 'Isla de Maipo', 1);
INSERT INTO areas VALUES (25, 'La Cisterna', 1);
INSERT INTO areas VALUES (27, 'La Florida', 1);
INSERT INTO areas VALUES (28, 'La Granja', 1);
INSERT INTO areas VALUES (29, 'La Pintana', 1);
INSERT INTO areas VALUES (30, 'Lampa', 1);
INSERT INTO areas VALUES (31, 'Linderos', 1);
INSERT INTO areas VALUES (32, 'Lo Barnechea', 1);
INSERT INTO areas VALUES (33, 'Lo Espejo', 1);
INSERT INTO areas VALUES (34, 'Lo Prado', 1);
INSERT INTO areas VALUES (35, 'Macul', 1);
INSERT INTO areas VALUES (36, 'Maipo', 1);
INSERT INTO areas VALUES (37, 'Maipú', 1);
INSERT INTO areas VALUES (38, 'Melipilla', 1);
INSERT INTO areas VALUES (39, 'Padre Hurtado', 1);
INSERT INTO areas VALUES (40, 'Paine', 1);
INSERT INTO areas VALUES (41, 'Pedro Aguirre Cerda', 1);
INSERT INTO areas VALUES (42, 'Peñaflor', 1);
INSERT INTO areas VALUES (43, 'Peñalolén', 1);
INSERT INTO areas VALUES (44, 'Pirque', 1);
INSERT INTO areas VALUES (45, 'Pudahuel', 1);
INSERT INTO areas VALUES (46, 'Puente Alto', 1);
INSERT INTO areas VALUES (47, 'Quilicura', 1);
INSERT INTO areas VALUES (48, 'Quinta Normal', 1);
INSERT INTO areas VALUES (49, 'Recoleta', 1);
INSERT INTO areas VALUES (50, 'Renca', 1);
INSERT INTO areas VALUES (51, 'San Bernardo', 1);
INSERT INTO areas VALUES (52, 'San Diego', 1);
INSERT INTO areas VALUES (53, 'San Joaquín', 1);
INSERT INTO areas VALUES (54, 'San José de Maipo', 1);
INSERT INTO areas VALUES (55, 'San Miguel', 1);
INSERT INTO areas VALUES (56, 'San Ramón', 1);
INSERT INTO areas VALUES (57, 'Talagante', 1);
INSERT INTO areas VALUES (58, 'Viluco', 1);


--
-- Data for Name: systems; Type: TABLE DATA; Schema: public; Owner: lion
--

INSERT INTO systems VALUES (1, 'Restaurantes', 'Sistema de comida rapida');
INSERT INTO systems VALUES (2, 'Farmacias', 'Sistema de farmacias');

--
-- Data for groups
--

INSERT INTO groups VALUES(1, 'Administrator');
INSERT INTO groups VALUES(2, 'Monitor');
INSERT INTO groups VALUES(3, 'Supervisor');
INSERT INTO groups VALUES(4, 'Commerce Administrator');
INSERT INTO groups VALUES(5, 'Branch Manager');
INSERT INTO groups VALUES(6, 'Branch Cashier');
INSERT INTO groups VALUES(7, 'Dispatchers Boss');
INSERT INTO groups VALUES(8, 'Dispatcher');
INSERT INTO groups VALUES(9, 'User');

--
-- Data for permissions
--

INSERT INTO permissions VALUES
    (1, 'login_subsidiary', 0),
    (2, 'login_dispatcher', 0),
    (3, 'login_administrator', 0),
    (4, 'view_not_enabled', 0),
    (5, 'force_delete', 0),
    (6, 'mark_not_enabled', 0),
    (7, 'system', 0),
    (8, 'edit_products', 0);
    (9, 'accept_or_rejected_order', 0)
    (10, 'view statistics')


--
-- Data for group permissions
--
-- create, read, update, delete
--
INSERT INTO groups_permissions VALUES
    (1, 4, 100),
    (1, 5, 80),
    (1, 6, 20),
    (2, 7, 100),
    (2, 8, 20),
    (3, 1, 100),
    (4, 1, 100),
    (5, 1, 100),
    (6, 1, 100),
    (7, 1, 100),
    (8, 1, 100);

--
-- Data for user
--
INSERT INTO users VALUES (1, 'Marcelo', 'Salcedo', 'm', NULL, '1976-09-04', 'marcelo@hackinglabs.cl', '$argon2i$m=512,t=15,p=2$9/FXbDde9HYuyPUDFnUA/a0PurL3CPpzM8a0Wk+YvaGa0R9t$qy/d8ImDn25WPyhwRGbKw7eSccXuOIsxO8V8lZlkKnrLRgYe/Zfn2A', NULL, NULL, '19769f0ae6d4f3532efdd0d832a212681b1f2f2cf3ec1581dbbe40ce833e1f2af0a1e286e2b6dbcbec7e09e64b97eb3f2253f6bc812a9c072e17b23e9dd5c130', NULL, NULL, true, true, true, '2016-02-26', 11111111);
INSERT INTO users VALUES (2, 'Dxpress', 'Monitor', 'm', NULL, '1976-09-04', 'monitor@dxpress.cl', '$argon2i$m=512,t=15,p=2$9/FXbDde9HYuyPUDFnUA/a0PurL3CPpzM8a0Wk+YvaGa0R9t$qy/d8ImDn25WPyhwRGbKw7eSccXuOIsxO8V8lZlkKnrLRgYe/Zfn2A', NULL, NULL, '19769f0ae6d4f3532efdd0d832a212681b1f2f2cf3ec1581dbbe40ce833e1f2af0a1e286e2b6dbcbec7e09e64b97eb3f2253f6bc812a9c072e17b23e9dd5c130', NULL, NULL, true, true, true, '2016-02-26', 11111111);
INSERT INTO users VALUES (3, 'Dxpress', 'Supervisor', 'm', NULL, '1976-09-04', 'supervisor@dxpress.cl', '$argon2i$m=512,t=15,p=2$9/FXbDde9HYuyPUDFnUA/a0PurL3CPpzM8a0Wk+YvaGa0R9t$qy/d8ImDn25WPyhwRGbKw7eSccXuOIsxO8V8lZlkKnrLRgYe/Zfn2A', NULL, NULL, '19769f0ae6d4f3532efdd0d832a212681b1f2f2cf3ec1581dbbe40ce833e1f2af0a1e286e2b6dbcbec7e09e64b97eb3f2253f6bc812a9c072e17b23e9dd5c130', NULL, NULL, true, true, true, '2016-02-26', 11111111);
INSERT INTO users VALUES (4, 'Administrador', 'Nueva China', 'm', NULL, '1976-09-04', 'lascondes@nuevachina.cl', '$argon2i$m=512,t=15,p=2$9/FXbDde9HYuyPUDFnUA/a0PurL3CPpzM8a0Wk+YvaGa0R9t$qy/d8ImDn25WPyhwRGbKw7eSccXuOIsxO8V8lZlkKnrLRgYe/Zfn2A', NULL, NULL, '19769f0ae6d4f3532efdd0d832a212681b1f2f2cf3ec1581dbbe40ce833e1f2af0a1e286e2b6dbcbec7e09e64b97eb3f2253f6bc812a9c072e17b23e9dd5c130', NULL, NULL, true, true, true, '2016-02-26', 11111111);
INSERT INTO users VALUES (5, 'Jefe', 'Nueva China', 'm', NULL, '1976-09-04', 'premium@nuevachina.cl', '$argon2i$m=512,t=15,p=2$9/FXbDde9HYuyPUDFnUA/a0PurL3CPpzM8a0Wk+YvaGa0R9t$qy/d8ImDn25WPyhwRGbKw7eSccXuOIsxO8V8lZlkKnrLRgYe/Zfn2A', NULL, NULL, '19769f0ae6d4f3532efdd0d832a212681b1f2f2cf3ec1581dbbe40ce833e1f2af0a1e286e2b6dbcbec7e09e64b97eb3f2253f6bc812a9c072e17b23e9dd5c130', NULL, NULL, true, true, true, '2016-02-26', 11111111);
INSERT INTO users VALUES (6, 'Cajero', 'Nueva China', 'm', NULL, '1976-09-04', 'cajero@nuevachina.cl', '$argon2i$m=512,t=15,p=2$9/FXbDde9HYuyPUDFnUA/a0PurL3CPpzM8a0Wk+YvaGa0R9t$qy/d8ImDn25WPyhwRGbKw7eSccXuOIsxO8V8lZlkKnrLRgYe/Zfn2A', NULL, NULL, '19769f0ae6d4f3532efdd0d832a212681b1f2f2cf3ec1581dbbe40ce833e1f2af0a1e286e2b6dbcbec7e09e64b97eb3f2253f6bc812a9c072e17b23e9dd5c130', NULL, NULL, true, true, true, '2016-02-26', 11111111);
INSERT INTO users VALUES (7, 'Douglas', 'Mendez', 'm', NULL, '1976-09-04', 'douglas@hackinglabs.cl', '$argon2i$m=512,t=15,p=2$9/FXbDde9HYuyPUDFnUA/a0PurL3CPpzM8a0Wk+YvaGa0R9t$qy/d8ImDn25WPyhwRGbKw7eSccXuOIsxO8V8lZlkKnrLRgYe/Zfn2A', NULL, NULL, '19769f0ae6d4f3532efdd0d832a212681b1f2f2cf3ec1581dbbe40ce833e1f2af0a1e286e2b6dbcbec7e09e64b97eb3f2253f6bc812a9c072e17b23e9dd5c130', NULL, NULL, true, true, true, '2016-02-26', 11111111);
INSERT INTO users VALUES (8, 'Juan', 'Cuzmar', 'm', NULL, '1976-09-04', 'juan@hackinglabs.cl', '$argon2i$m=512,t=15,p=2$9/FXbDde9HYuyPUDFnUA/a0PurL3CPpzM8a0Wk+YvaGa0R9t$qy/d8ImDn25WPyhwRGbKw7eSccXuOIsxO8V8lZlkKnrLRgYe/Zfn2A', NULL, NULL, '19769f0ae6d4f3532efdd0d832a212681b1f2f2cf3ec1581dbbe40ce833e1f2af0a1e286e2b6dbcbec7e09e64b97eb3f2253f6bc812a9c072e17b23e9dd5c130', NULL, NULL, true, true, true, '2016-02-26', 11111111);
INSERT INTO users VALUES (9, 'Administrador', 'Tony Romas', 'm', NULL,'1976-09-04', 'cristobal@tonyromas.cl', '$argon2i$m=512,t=15,p=2$9/FXbDde9HYuyPUDFnUA/a0PurL3CPpzM8a0Wk+YvaGa0R9t$qy/d8ImDn25WPyhwRGbKw7eSccXuOIsxO8V8lZlkKnrLRgYe/Zfn2A', NULL, NULL, '19769f0ae6d4f3532efdd0d832a212681b1f2f2cf3ec1581dbbe40ce833e1f2af0a1e286e2b6dbcbec7e09e64b97eb3f2253f6bc812a9c072e17b23e9dd5c130', NULL, NULL, true, true, true, '2016-02-26', 11111111);


--
-- Data for user groups
--

INSERT INTO user_groups VALUES(1, 1, 100, 'Administrador de Dxpress');
INSERT INTO user_groups VALUES(2, 2, 100, 'Monitor de Dxpress');
INSERT INTO user_groups VALUES(3, 3, 100, 'Supervisor de Dxpress');
INSERT INTO user_groups VALUES(4, 4, 100, 'Administrador de Nueva China');
INSERT INTO user_groups VALUES(5, 5, 100, 'Jefe de Sucursal Nueva China Premium');
INSERT INTO user_groups VALUES(6, 6, 100, 'Cajero de Nueva China Premium');
INSERT INTO user_groups VALUES(7, 7, 100, 'Jefe de despachadores');
INSERT INTO user_groups VALUES(8, 8, 100, 'Despachador');

ALTER SEQUENCE countries_id_seq RESTART WITH 2;
ALTER SEQUENCE configurations_id_seq RESTART WITH 2;
ALTER SEQUENCE states_id_seq RESTART WITH 16;
ALTER SEQUENCE cities_id_seq RESTART WITH 3;
ALTER SEQUENCE areas_id_seq RESTART WITH 59;
ALTER SEQUENCE systems_id_seq RESTART WITH 3;
ALTER SEQUENCE groups_id_seq RESTART WITH 10;
ALTER SEQUENCE permissions_id_seq RESTART WITH 9;
ALTER SEQUENCE users_id_seq RESTART WITH 10;
