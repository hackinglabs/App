--
-- PostgreSQL database dump
--


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS postgis;

--
-- Name: os_devices; Type: TYPE; Schema: public; Owner: lion
--

CREATE TYPE os_devices AS ENUM (
    '1',
    '2',
    '3',
    '4'
);


ALTER TYPE os_devices OWNER TO lion;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: areas; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE areas (
    id integer NOT NULL,
    name character varying(45) NOT NULL,
    city_id integer NOT NULL
);


ALTER TABLE areas OWNER TO lion;

--
-- Name: areas_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE areas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE areas_id_seq OWNER TO lion;

--
-- Name: areas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE areas_id_seq OWNED BY areas.id;


--
-- Name: categories; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE categories (
    id integer NOT NULL,
    name character varying(45) NOT NULL,
    subsidiary_id integer NOT NULL,
    "position" integer NOT NULL,
    enabled boolean DEFAULT true,
    description text,
    image character varying
);


ALTER TABLE categories OWNER TO lion;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categories_id_seq OWNER TO lion;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.id;


--
-- Name: cities; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE cities (
    id integer NOT NULL,
    name character varying(45) NOT NULL,
    state_id integer NOT NULL
);


ALTER TABLE cities OWNER TO lion;

--
-- Name: cities_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE cities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cities_id_seq OWNER TO lion;

--
-- Name: cities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE cities_id_seq OWNED BY cities.id;


--
-- Name: commerce_tags; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE commerce_tags (
    id integer NOT NULL,
    tag_id integer NOT NULL,
    commerce_id integer NOT NULL,
    enabled boolean DEFAULT true
);


ALTER TABLE commerce_tags OWNER TO lion;

--
-- Name: commerce_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE commerce_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE commerce_tags_id_seq OWNER TO lion;

--
-- Name: commerce_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE commerce_tags_id_seq OWNED BY commerce_tags.id;


--
-- Name: commerce_users; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE commerce_users (
    id integer NOT NULL,
    commerce_id integer NOT NULL,
    user_id integer NOT NULL,
    registration_date date DEFAULT ('now'::text)::date
);


ALTER TABLE commerce_users OWNER TO lion;

--
-- Name: commerce_users_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE commerce_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE commerce_users_id_seq OWNER TO lion;

--
-- Name: commerce_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE commerce_users_id_seq OWNED BY commerce_users.id;


--
-- Name: commerces; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE commerces (
    id integer NOT NULL,
    name character varying NOT NULL,
    nin character varying(45) NOT NULL,
    trade character varying NOT NULL,
    address character varying NOT NULL,
    telephone integer,
    business_name character varying NOT NULL,
    logo character varying NOT NULL,
    description text,
    registration_date date DEFAULT ('now'::text)::date,
    user_id integer NOT NULL,
    country_id integer NOT NULL,
    system_id integer NOT NULL,
    enabled boolean DEFAULT true,
    banner character varying,
    profit_percentage integer,
    slug character varying DEFAULT ''::character varying NOT NULL,
    subdomain_id character varying
);


ALTER TABLE commerces OWNER TO lion;

--
-- Name: commerces_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE commerces_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE commerces_id_seq OWNER TO lion;

--
-- Name: commerces_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE commerces_id_seq OWNED BY commerces.id;


--
-- Name: configurations; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE configurations (
    id integer NOT NULL,
    country_id integer NOT NULL,
    min_meters integer NOT NULL,
    dispatch_amount_base double precision NOT NULL,
    dispatch_amount_extra double precision NOT NULL,
    exchange_rate double precision NOT NULL,
    code_currency character varying(5) NOT NULL,
    "language" character(6) NOT NULL,
    profit_percentage float NOT NULL,
    iva float NOT NULL,
    currency_symbol character(2) NOT NULL,
    thousands_separators character(1) NOT NULL,
    decimals_separator character(1) NOT NULL,
    decimals_quantity smallint NOT NULL
);


ALTER TABLE configurations OWNER TO lion;

--
-- Name: configurations_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE configurations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE configurations_id_seq OWNER TO lion;

--
-- Name: configurations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE configurations_id_seq OWNED BY configurations.id;


--
-- Name: contacts; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE contacts (
    id integer NOT NULL,
    first_name character varying,
    last_name character varying,
    contact character varying,
    nin character varying NOT NULL,
    phone character varying NOT NULL,
    email character varying NOT NULL,
    address character varying,
    area_id integer NOT NULL,
    client boolean DEFAULT false
);


ALTER TABLE contacts OWNER TO lion;

--
-- Name: contacts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE contacts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE contacts_id_seq OWNER TO lion;

--
-- Name: contacts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE contacts_id_seq OWNED BY contacts.id;


--
-- Name: countries; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE countries (
    id integer NOT NULL,
    name character varying(45) NOT NULL,
    phone_code integer
);


ALTER TABLE countries OWNER TO lion;

--
-- Name: countries_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE countries_id_seq OWNER TO lion;

--
-- Name: countries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE countries_id_seq OWNED BY countries.id;


--
-- Name: devices; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE devices (
    id integer NOT NULL,
    model character varying(45) NOT NULL,
    trademark character varying(45) NOT NULL,
    name character varying(45) NOT NULL,
    serial_number character varying NOT NULL,
    reception_date date DEFAULT ('now'::text)::date,
    state_id integer NOT NULL,
    contract boolean DEFAULT false,
    commerce_id integer NOT NULL,
    enabled boolean DEFAULT true
);


ALTER TABLE devices OWNER TO lion;

--
-- Name: devices_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE devices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devices_id_seq OWNER TO lion;

--
-- Name: devices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE devices_id_seq OWNED BY devices.id;


--
-- Name: devices_mobile; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE devices_mobile (
    id integer NOT NULL,
    token character varying NOT NULL,
    registration_date date DEFAULT ('now'::text)::date,
    os os_devices,
    user_id integer NOT NULL
);


ALTER TABLE devices_mobile OWNER TO lion;

--
-- Name: devices_mobile_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE devices_mobile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devices_mobile_id_seq OWNER TO lion;

--
-- Name: devices_mobile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE devices_mobile_id_seq OWNED BY devices_mobile.id;


--
-- Name: devices_support; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE devices_support (
    id integer NOT NULL,
    support_date date DEFAULT ('now'::text)::date NOT NULL,
    reception_date date,
    device_id integer NOT NULL,
    status character varying(60)
);


ALTER TABLE devices_support OWNER TO lion;

--
-- Name: devices_support_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE devices_support_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE devices_support_id_seq OWNER TO lion;

--
-- Name: devices_support_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE devices_support_id_seq OWNED BY devices_support.id;


--
-- Name: discounts_categories; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE discounts_categories (
    id integer NOT NULL,
    category_id integer NOT NULL,
    name character varying(45) NOT NULL,
    value double precision NOT NULL,
    percetange boolean DEFAULT FALSE,
    description text,
    date date DEFAULT ('now'::text)::date,
    stock int
);


ALTER TABLE discounts_categories OWNER TO lion;

--
-- Name: discounts_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE discounts_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE discounts_categories_id_seq OWNER TO lion;

--
-- Name: discounts_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE discounts_categories_id_seq OWNED BY discounts_categories.id;


--
-- Name: discounts_subsidiaries; Type: TABLE; Schema: public; Owner: lion
--



--
-- Name: dispatch_addresses; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE dispatch_addresses (
    id integer NOT NULL,
    name character varying(45) NOT NULL,
    address text NOT NULL,
    reference text,
    telephone integer NOT NULL,
    user_id integer NOT NULL,
    area_id integer NOT NULL,
    is_default boolean DEFAULT false,
    coordinates geography(Point,4326) NOT NULL,
    block character varying DEFAULT ''::character varying,
    apartament character varying DEFAULT ''::character varying
);


ALTER TABLE dispatch_addresses OWNER TO lion;

--
-- Name: dispatch_addresses_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE dispatch_addresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE dispatch_addresses_id_seq OWNER TO lion;

--
-- Name: dispatch_addresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE dispatch_addresses_id_seq OWNED BY dispatch_addresses.id;


--
-- Name: driver_vehicles; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE driver_vehicles (
    id integer NOT NULL,
    assignation_date date DEFAULT ('now'::text)::date,
    vehicle_id integer NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE driver_vehicles OWNER TO lion;

--
-- Name: driver_vehicles_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE driver_vehicles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE driver_vehicles_id_seq OWNER TO lion;

--
-- Name: driver_vehicles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE driver_vehicles_id_seq OWNED BY driver_vehicles.id;


--
-- Name: favorites; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE favorites (
    id integer NOT NULL,
    user_id integer NOT NULL,
    subsidiary_id integer NOT NULL,
    description character varying(200)
);


ALTER TABLE favorites OWNER TO lion;

--
-- Name: favorites_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE favorites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE favorites_id_seq OWNER TO lion;

--
-- Name: favorites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE favorites_id_seq OWNED BY favorites.id;


--
-- Name: ingredients; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE ingredients (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    ingredient_per_category_id integer NOT NULL,
    price double precision NOT NULL,
    enabled boolean DEFAULT true
);


ALTER TABLE ingredients OWNER TO lion;

--
-- Name: ingredients_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE ingredients_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ingredients_id_seq OWNER TO lion;

--
-- Name: ingredients_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE ingredients_id_seq OWNED BY ingredients.id;


--
-- Name: ingredients_per_category; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE ingredients_per_category (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    is_base boolean DEFAULT false,
    subsidiary_id integer NOT NULL,
    enabled boolean DEFAULT true,
    is_premium boolean DEFAULT false
);


ALTER TABLE ingredients_per_category OWNER TO lion;

--
-- Name: ingredients_per_category_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE ingredients_per_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ingredients_per_category_id_seq OWNER TO lion;

--
-- Name: ingredients_per_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE ingredients_per_category_id_seq OWNED BY ingredients_per_category.id;


--
-- Name: options_group; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE options_group (
    id integer NOT NULL,
    product_id integer NOT NULL,
    quantity integer NOT NULL,
    ingredient_per_category_id integer NOT NULL,
    defaults integer[],
    edit boolean DEFAULT false,
    enabled boolean DEFAULT true
);


ALTER TABLE options_group OWNER TO lion;

--
-- Name: options_group_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE options_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE options_group_id_seq OWNER TO lion;

--
-- Name: options_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE options_group_id_seq OWNED BY options_group.id;


--
-- Name: product_tags; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE product_tags (
    id integer NOT NULL,
    tag_id integer NOT NULL,
    product_id integer NOT NULL,
    enabled boolean DEFAULT true
);


ALTER TABLE product_tags OWNER TO lion;

--
-- Name: product_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE product_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE product_tags_id_seq OWNER TO lion;

--
-- Name: product_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE product_tags_id_seq OWNED BY product_tags.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE products (
    id integer NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    name character varying(120) NOT NULL,
    description text,
    is_compounds boolean DEFAULT false,
    ingredients_extra integer,
    sub_category_id integer NOT NULL,
    "position" integer NOT NULL,
    price double precision NOT NULL,
    image character varying,
    options_ingredient boolean DEFAULT false,
    stock boolean default true
);


ALTER TABLE products OWNER TO lion;

--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE products_id_seq OWNER TO lion;

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE products_id_seq OWNED BY products.id;


--
-- Name: ratings_subsidiary; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE ratings_subsidiary (
    id integer NOT NULL,
    subsidiary_id integer NOT NULL,
    user_id integer NOT NULL,
    qualification integer NOT NULL,
    comment text,
    date date DEFAULT ('now'::text)::date
);


ALTER TABLE ratings_subsidiary OWNER TO lion;

--
-- Name: ratings_subsidiary_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
-- Name: ratings_subsidiary_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE ratings_subsidiary_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ratings_subsidiary_id_seq OWNER TO lion;

--
-- Name: ratings_subsidiary_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE ratings_subsidiary_id_seq OWNED BY ratings_subsidiary.id;

--
-- Name: states; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE states (
    id integer NOT NULL,
    name character varying(45) NOT NULL,
    country_id integer NOT NULL,
    gmt smallint NOT NULL
);


ALTER TABLE states OWNER TO lion;

--
-- Name: states_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE states_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE states_id_seq OWNER TO lion;

--
-- Name: states_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE states_id_seq OWNED BY states.id;


--
-- Name: sub_categories; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE sub_categories (
    id integer NOT NULL,
    name character varying(60) NOT NULL,
    category_id integer NOT NULL,
    enabled boolean DEFAULT true NOT NULL,
    is_visible boolean DEFAULT true,
    description character varying,
    image character varying,
    navigable boolean DEFAULT true
);


ALTER TABLE sub_categories OWNER TO lion;

--
-- Name: sub_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE sub_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sub_categories_id_seq OWNER TO lion;

--
-- Name: sub_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE sub_categories_id_seq OWNED BY sub_categories.id;


--
-- Name: subsidiaries; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE subsidiaries (
    id integer NOT NULL,
    alias character varying(45) NOT NULL,
    address character varying NOT NULL,
    coordinates geography(Point,4326) NOT NULL,
    telephone integer NOT NULL,
    commerce_id integer NOT NULL,
    area_id integer NOT NULL,
    open_hours jsonb,
    average_time integer,
    headquarter boolean DEFAULT false NOT NULL,
    pap double precision DEFAULT 0 NOT NULL,
    enabled boolean DEFAULT true,
    short_description character varying(50),
    description text,
    rating_average integer DEFAULT 0,
    is_active boolean DEFAULT false,
    is_premium boolean DEFAULT false,
    correlative integer DEFAULT 0,
    facebook_title character varying,
    facebook_url character varying,
    facebook_image character varying,
    facebook_description character varying,
    facebook_sitename character varying,
    suggestion_type_vehicle character varying(12) DEFAULT ''::character varying,
    suggestion_qualification_vehicle character varying(6) DEFAULT ''::character varying,
    suggestion_require boolean DEFAULT false,
    force_offline boolean DEFAULT false,
    banner jsonb
);


ALTER TABLE subsidiaries OWNER TO lion;

--
-- Name: subsidiaries_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE subsidiaries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE subsidiaries_id_seq OWNER TO lion;

--
-- Name: subsidiaries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE subsidiaries_id_seq OWNED BY subsidiaries.id;


--
-- Name: systems; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE systems (
    id integer NOT NULL,
    name character varying(45) NOT NULL,
    routing character varying(45) NOT NULL ,
    description text
);


ALTER TABLE systems OWNER TO lion;

--
-- Name: systems_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE systems_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE systems_id_seq OWNER TO lion;

--
-- Name: systems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE systems_id_seq OWNED BY systems.id;


--
-- Name: tags; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE tags (
    id integer NOT NULL,
    name character varying(45) NOT NULL,
    system_id integer NOT NULL,
    enabled boolean DEFAULT true
);


ALTER TABLE tags OWNER TO lion;

--
-- Name: tags_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tags_id_seq OWNER TO lion;

--
-- Name: tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE tags_id_seq OWNED BY tags.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE users (
    id integer NOT NULL,
    first_name character varying(45),
    last_name character varying(45),
    gender character(1),
    nin character varying(45),
    birth_date date,
    email character varying(45) NOT NULL,
    password character varying(128) NOT NULL,
    facebook character varying(120),
    google character varying(120),
    xcsrf character(128),
    recover_token character(128),
    avatar character varying(250),
    enabled boolean DEFAULT true,
    active boolean DEFAULT false,
    tutorial boolean DEFAULT true,
    registration_date date DEFAULT ('now'::text)::date,
    telephone integer
);


ALTER TABLE users OWNER TO lion;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO lion;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: users_subsidiaries; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE users_subsidiaries (
    user_id integer NOT NULL,
    subsidiary_id integer NOT NULL
);


ALTER TABLE users_subsidiaries OWNER TO lion;

--
-- Name: users_tag; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE users_tag (
    id integer NOT NULL,
    user_id integer NOT NULL,
    tags_id integer NOT NULL
);


ALTER TABLE users_tag OWNER TO lion;

--
-- Name: users_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE users_tag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_tag_id_seq OWNER TO lion;

--
-- Name: users_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE users_tag_id_seq OWNED BY users_tag.id;


--
-- Name: vehicles; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE vehicles (
    id integer NOT NULL,
    patent character varying(10) NOT NULL,
    registration_date date DEFAULT ('now'::text)::date NOT NULL,
    trademark character varying(45) NOT NULL,
    enabled boolean DEFAULT true,
    year integer NOT NULL,
    user_id integer NOT NULL,
    qualification character varying(6) DEFAULT ''::character varying NOT NULL,
    type character varying(45) DEFAULT ''::character varying NOT NULL,
    model character varying(120) NOT NULL,
    status character varying(60) NOT NULL
);


ALTER TABLE vehicles OWNER TO lion;

--
-- Name: vehicles_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE vehicles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE vehicles_id_seq OWNER TO lion;

--
-- Name: vehicles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE vehicles_id_seq OWNED BY vehicles.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--




--
-- Name: groups; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE groups (
    id integer NOT NULL,
    name character varying(45) NOT NULL
);

ALTER TABLE groups OWNER TO lion;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE groups_id_seq OWNER TO lion;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--


--
-- Name: permissions; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE permissions (
    id integer NOT NULL,
    name character varying(120) NOT NULL UNIQUE,
    level integer NOT NULL DEFAULT 0
);


ALTER TABLE permissions OWNER TO lion;

--
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE permissions_id_seq OWNER TO lion;

--
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE permissions_id_seq OWNED BY permissions.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--






--
-- Name: permissions; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE groups_permissions (
    permission_id int NOT NULL,
    group_id int NOT NULL,
    level smallint DEFAULT 0
);


ALTER TABLE groups_permissions OWNER TO lion;



--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

--
-- Name: user_groups; Type: TABLE; Schema: public; Owner: lion
--


CREATE TABLE user_groups (
    group_id integer NOT NULL,
    user_id integer NOT NULL,
    level integer NOT NULL DEFAULT 0,
    description character varying(45)
);


ALTER TABLE user_groups OWNER TO lion;

--
-- Name: user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

--
-- Name: coupons; Type: TABLE; Schema: public; Owner: lion
--

--APLY DEV: YES AND PROD: ?

CREATE TYPE coupon_discount_type AS ENUM ('COUNTRY', 'STATE', 'CITY', 'AREA', 'COMMERCE', 'SUBSIDIARY');

CREATE TABLE coupons (
    id INTEGER NOT NULL,
    code CHARACTER varying(45) NOT NULL,
    description CHARACTER varying(256),
    registration_date TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL ,
    with_expiration BOOLEAN NOT NULL DEFAULT TRUE,
    expiration_date TIMESTAMP WITH TIME ZONE NOT NULL,
    stock INTEGER NOT NULL DEFAULT 0,
    with_percentage BOOLEAN DEFAULT TRUE NOT NULL,
    amount FLOAT NOT NULL,
    coupon_type coupon_discount_type,
    type_id INTEGER NOT NULL,
    enabled BOOLEAN DEFAULT TRUE NOT NULL
);

ALTER TABLE coupons OWNER TO lion;

--
-- Name: coupons_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE coupons_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE coupons_id_seq OWNER TO lion;

--
-- Name: coupons_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE coupons_id_seq OWNED BY coupons.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--



CREATE TABLE coupons_users (
    id integer NOT NULL,
    user_id integer NOT NULL,
    coupon_id integer NOT NULL,
    use_date date DEFAULT ('now'::text)::date
);

ALTER TABLE coupons_users OWNER TO lion;

--
-- Name: coupons_users_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE coupons_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE coupons_users_id_seq OWNER TO lion;

--
-- Name: coupons_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE coupons_users_id_seq OWNED BY coupons_users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

CREATE TABLE users_telephones (
    id integer NOT NULL,
    user_id integer NOT NULL,
    telephone integer NOT NULL,
    registration_date date DEFAULT ('now'::text)::date
);

ALTER TABLE users_telephones OWNER TO lion;

--
-- Name: users_telephones_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE users_telephones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_telephones_id_seq OWNER TO lion;

--
-- Name: users_telephones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE users_telephones_id_seq OWNED BY users_telephones.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--


--
-- Create compound table
--

CREATE TABLE compounds (
    id integer NOT NULL,
    name character varying NOT NULL,
    price float NOT NULL,
    category_id integer NOT NULL,
    subsidiary_id integer NOT NULL,
    registration_date date DEFAULT ('now'::text)::date
);


ALTER TABLE compounds OWNER TO lion;

--
-- Name: compounds_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE compounds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE compounds_id_seq OWNER TO lion;

--
-- Name: users_telephones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE compounds_id_seq OWNED BY compounds.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--


--
-- Create options_compounds table
--

CREATE TABLE options_compounds (
    id integer NOT NULL,
    product_id integer NOT NULL,
    compound_id integer NOT NULL,
    change_for_all boolean DEFAULT false,
    quantity integer
);

ALTER TABLE options_compounds OWNER TO lion;

--
-- Name: options_compounds_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE options_compounds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE options_compounds_id_seq OWNER TO lion;

--
-- Name: options_compounds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE options_compounds_id_seq OWNED BY options_compounds.id;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--



--
-- Create items_compounds table
--

CREATE TABLE items_compounds (
    id integer NOT NULL,
    option_compound_id integer NOT NULL,
    compound_id integer NOT NULL
);

ALTER TABLE items_compounds OWNER TO lion;

--
-- Name: items_compounds_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE items_compounds_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE items_compounds_id_seq OWNER TO lion;

--
-- Name: items_compounds_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE items_compounds_id_seq OWNED BY items_compounds.id;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--



--
-- Name: temp_users; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE temp_users (
    id integer NOT NULL,
    user_id integer NOT NULL,
    first_name character varying(45),
    last_name character varying(45),
    gender character(1),
    nin character varying(45),
    birth_date date,
    email character varying(45) NOT NULL,
    password character varying(128) NOT NULL,
    xcsrf character(128),
    registration_date date DEFAULT ('now'::text)::date,
    telephone integer
);




ALTER TABLE temp_users OWNER TO lion;

--
-- Name: temp_users_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE temp_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE temp_users_id_seq OWNER TO lion;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE temp_users_id_seq OWNED BY temp_users.id;


--
-- Name: owner_dispatchers; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE owner_dispatchers (
    owner_id integer NOT NULL,
    user_id integer NOT NULL,
    registration_date date DEFAULT ('now'::text)::date,
    enabled boolean DEFAULT true,
);



ALTER TABLE owner_dispatchers OWNER TO lion;



--
-- Name: red_zones; Type: TABLE; Schema: public; Owner: lion
--

CREATE TABLE red_zones (
    id integer NOT NULL,
    name character varying(45),
    area geography(Polygon,4326) NOT NULL,
);


ALTER TABLE red_zones OWNER TO lion;

--
-- Name: red_zones_id_seq; Type: SEQUENCE; Schema: public; Owner: lion
--

CREATE SEQUENCE red_zones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE red_zones_id_seq OWNER TO lion;

--
-- Name: red_zones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: lion
--

ALTER SEQUENCE red_zones_id_seq OWNED BY red_zones.id;



CREATE TABLE ratings_dispatchers(
  id integer NOT NULL,
  dispatcher_id integer NOT NULL,
  user_id integer NOT NULL,
  qualification integer NOT NULL,
  comment text,
  order_id character varying,
  date date DEFAULT ('now'::text)::date
);

ALTER TABLE ratings_dispatchers OWNER TO lion;


CREATE SEQUENCE ratings_dispatchers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


  ALTER TABLE ratings_dispatchers_id_seq OWNER TO lion;


ALTER SEQUENCE ratings_dispatchers_id_seq OWNED BY ratings_dispatchers.id;






ALTER TABLE ONLY areas ALTER COLUMN id SET DEFAULT nextval('areas_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY categories ALTER COLUMN id SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY cities ALTER COLUMN id SET DEFAULT nextval('cities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerce_tags ALTER COLUMN id SET DEFAULT nextval('commerce_tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerce_users ALTER COLUMN id SET DEFAULT nextval('commerce_users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerces ALTER COLUMN id SET DEFAULT nextval('commerces_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY configurations ALTER COLUMN id SET DEFAULT nextval('configurations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contacts ALTER COLUMN id SET DEFAULT nextval('contacts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY countries ALTER COLUMN id SET DEFAULT nextval('countries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY devices ALTER COLUMN id SET DEFAULT nextval('devices_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY devices_mobile ALTER COLUMN id SET DEFAULT nextval('devices_mobile_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY devices_support ALTER COLUMN id SET DEFAULT nextval('devices_support_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY discounts_categories ALTER COLUMN id SET DEFAULT nextval('discounts_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY discounts_subsidiaries ALTER COLUMN id SET DEFAULT nextval('discounts_subsidiaries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY dispatch_addresses ALTER COLUMN id SET DEFAULT nextval('dispatch_addresses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY driver_vehicles ALTER COLUMN id SET DEFAULT nextval('driver_vehicles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY favorites ALTER COLUMN id SET DEFAULT nextval('favorites_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY ingredients ALTER COLUMN id SET DEFAULT nextval('ingredients_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY ingredients_per_category ALTER COLUMN id SET DEFAULT nextval('ingredients_per_category_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY options_group ALTER COLUMN id SET DEFAULT nextval('options_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY product_tags ALTER COLUMN id SET DEFAULT nextval('product_tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY products ALTER COLUMN id SET DEFAULT nextval('products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY ratings_subsidiary ALTER COLUMN id SET DEFAULT nextval('ratings_subsidiary_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY states ALTER COLUMN id SET DEFAULT nextval('states_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY sub_categories ALTER COLUMN id SET DEFAULT nextval('sub_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY subsidiaries ALTER COLUMN id SET DEFAULT nextval('subsidiaries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY systems ALTER COLUMN id SET DEFAULT nextval('systems_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY tags ALTER COLUMN id SET DEFAULT nextval('tags_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY users_tag ALTER COLUMN id SET DEFAULT nextval('users_tag_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: lion
--

ALTER TABLE ONLY vehicles ALTER COLUMN id SET DEFAULT nextval('vehicles_id_seq'::regclass);

ALTER TABLE ONLY groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);

ALTER TABLE ONLY permissions ALTER COLUMN id SET DEFAULT nextval('permissions_id_seq'::regclass);

ALTER TABLE ONLY coupons ALTER COLUMN id SET DEFAULT nextval('coupons_id_seq'::regclass);

ALTER TABLE ONLY coupons_users ALTER COLUMN id SET DEFAULT nextval('coupons_users_id_seq'::regclass);


ALTER TABLE ONLY users_telephones ALTER COLUMN id SET DEFAULT nextval('users_telephones_id_seq'::regclass);

ALTER TABLE ONLY compounds ALTER COLUMN id SET DEFAULT nextval('compounds_id_seq'::regclass);

ALTER TABLE ONLY options_compounds ALTER COLUMN id SET DEFAULT nextval('options_compounds_id_seq'::regclass);

ALTER TABLE ONLY items_compounds ALTER COLUMN id SET DEFAULT nextval('items_compounds_id_seq'::regclass);

ALTER TABLE ONLY red_zones ALTER COLUMN id SET DEFAULT nextval('red_zones_id_seq'::regclass);

ALTER TABLE ONLY ratings_dispatchers ALTER COLUMN id SET DEFAULT nextval('ratings_dispatchers_id_seq'::regclass);

ALTER TABLE ONLY temp_users ALTER COLUMN id SET DEFAULT nextval('temp_users_id_seq'::regclass);

--
-- Name: areas_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY areas
    ADD CONSTRAINT areas_pkey PRIMARY KEY (id);


--
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: cities_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY cities
    ADD CONSTRAINT cities_pkey PRIMARY KEY (id);


--
-- Name: commerce_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerce_tags
    ADD CONSTRAINT commerce_tags_pkey PRIMARY KEY (id);


--
-- Name: commerce_users_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerce_users
    ADD CONSTRAINT commerce_users_pkey PRIMARY KEY (id);


--
-- Name: commerces_nin_key; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerces
    ADD CONSTRAINT commerces_nin_key UNIQUE (nin);


--
-- Name: commerces_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerces
    ADD CONSTRAINT commerces_pkey PRIMARY KEY (id);


--
-- Name: configurations_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY configurations
    ADD CONSTRAINT configurations_pkey PRIMARY KEY (id);


--
-- Name: contacts_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contacts
    ADD CONSTRAINT contacts_pkey PRIMARY KEY (id);


--
-- Name: countries_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY countries
    ADD CONSTRAINT countries_pkey PRIMARY KEY (id);


--
-- Name: devices_mobile_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY devices_mobile
    ADD CONSTRAINT devices_mobile_pkey PRIMARY KEY (id);


--
-- Name: devices_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT devices_pkey PRIMARY KEY (id);


--
-- Name: devices_support_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY devices_support
    ADD CONSTRAINT devices_support_pkey PRIMARY KEY (id);


--
-- Name: discounts_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY discounts_categories
    ADD CONSTRAINT discounts_categories_pkey PRIMARY KEY (id);


--
-- Name: discounts_subsidiaries_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY discounts_subsidiaries
    ADD CONSTRAINT discounts_subsidiaries_pkey PRIMARY KEY (id);


--
-- Name: dispatch_addresses_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY dispatch_addresses
    ADD CONSTRAINT dispatch_addresses_pkey PRIMARY KEY (id);


--
-- Name: driver_vehicles_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY driver_vehicles
    ADD CONSTRAINT driver_vehicles_pkey PRIMARY KEY (id);


--
-- Name: favorites_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY favorites
    ADD CONSTRAINT favorites_pkey PRIMARY KEY (id);


--
-- Name: ingredients_per_category_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY ingredients_per_category
    ADD CONSTRAINT ingredients_per_category_pkey PRIMARY KEY (id);


--
-- Name: ingredients_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY ingredients
    ADD CONSTRAINT ingredients_pkey PRIMARY KEY (id);


--
-- Name: options_group_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY options_group
    ADD CONSTRAINT options_group_pkey PRIMARY KEY (id);


--
-- Name: product_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY product_tags
    ADD CONSTRAINT product_tags_pkey PRIMARY KEY (id);


--
-- Name: products_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: ratings_subsidiary_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY ratings_subsidiary
    ADD CONSTRAINT ratings_subsidiary_pkey PRIMARY KEY (id);


--
-- Name: states_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY states
    ADD CONSTRAINT states_pkey PRIMARY KEY (id);


--
-- Name: sub_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY sub_categories
    ADD CONSTRAINT sub_categories_pkey PRIMARY KEY (id);


--
-- Name: subsidiaries_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY subsidiaries
    ADD CONSTRAINT subsidiaries_pkey PRIMARY KEY (id);


--
-- Name: systems_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY systems
    ADD CONSTRAINT systems_pkey PRIMARY KEY (id);


--
-- Name: tags_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users_subsidiaries_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY users_subsidiaries
    ADD CONSTRAINT users_subsidiaries_pkey PRIMARY KEY (user_id, subsidiary_id);


--
-- Name: users_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY users_tag
    ADD CONSTRAINT users_tag_pkey PRIMARY KEY (id);


--
-- Name: vehicles_pkey; Type: CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY vehicles
    ADD CONSTRAINT vehicles_pkey PRIMARY KEY (id);



ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);

ALTER TABLE ONLY user_groups
    ADD CONSTRAINT user_groups_pkey PRIMARY KEY (group_id, user_id);

ALTER TABLE ONLY groups_permissions
    ADD CONSTRAINT groups_permissions_pkey PRIMARY KEY (permission_id, group_id);

ALTER TABLE ONLY permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);

ALTER TABLE ONLY coupons
    ADD CONSTRAINT coupons_pkey PRIMARY KEY (id);

ALTER TABLE ONLY coupons_users
    ADD CONSTRAINT coupons_users_pkey PRIMARY KEY (id);


ALTER TABLE ONLY users_telephones
    ADD CONSTRAINT users_telephones_pkey PRIMARY KEY (id);


ALTER TABLE ONLY compounds
    ADD CONSTRAINT compounds_pkey PRIMARY KEY (id);

ALTER TABLE ONLY options_compounds
    ADD CONSTRAINT options_compounds_pkey PRIMARY KEY (id);

ALTER TABLE ONLY items_compounds
    ADD CONSTRAINT items_compounds_pkey PRIMARY KEY (id);

ALTER TABLE ONLY red_zones
    ADD CONSTRAINT red_zones_pkey PRIMARY KEY (id);


ALTER TABLE ONLY owner_dispatchers
  ADD CONSTRAINT owner_dispatchers_pkey PRIMARY KEY (owner_id, user_id);


ALTER TABLE ONLY ratings_dispatchers
  ADD CONSTRAINT ratings_dispatchers_pkey PRIMARY KEY (id);

  ALTER TABLE ONLY temp_users
  ADD CONSTRAINT temp_users_pkey PRIMARY KEY (id);




--
-- Name: users_enabled_index; Type: INDEX; Schema: public; Owner: lion
--

CREATE INDEX users_enabled_index ON users USING btree (enabled);


--
-- Name: users_id_idx; Type: INDEX; Schema: public; Owner: lion
--

CREATE INDEX users_id_idx ON users USING btree (id) WHERE (enabled = true);


--
-- Name: areas_city_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY areas
    ADD CONSTRAINT areas_city_id_fkey FOREIGN KEY (city_id) REFERENCES cities(id);


--
-- Name: categories_subsidiary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_subsidiary_id_fkey FOREIGN KEY (subsidiary_id) REFERENCES subsidiaries(id);


--
-- Name: cities_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY cities
    ADD CONSTRAINT cities_state_id_fkey FOREIGN KEY (state_id) REFERENCES states(id);


--
-- Name: commerce_tags_commerce_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerce_tags
    ADD CONSTRAINT commerce_tags_commerce_id_fkey FOREIGN KEY (commerce_id) REFERENCES commerces(id);


--
-- Name: commerce_tags_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerce_tags
    ADD CONSTRAINT commerce_tags_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES tags(id);


--
-- Name: commerce_users_commerce_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerce_users
    ADD CONSTRAINT commerce_users_commerce_id_fkey FOREIGN KEY (commerce_id) REFERENCES commerces(id);


--
-- Name: commerce_users_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerce_users
    ADD CONSTRAINT commerce_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: commerces_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerces
    ADD CONSTRAINT commerces_country_id_fkey FOREIGN KEY (country_id) REFERENCES countries(id);


--
-- Name: commerces_system_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerces
    ADD CONSTRAINT commerces_system_id_fkey FOREIGN KEY (system_id) REFERENCES systems(id);


--
-- Name: commerces_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY commerces
    ADD CONSTRAINT commerces_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: configuratios_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY configurations
    ADD CONSTRAINT configuratios_country_id_fkey FOREIGN KEY (country_id) REFERENCES countries(id);


--
-- Name: contacts_area_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contacts
    ADD CONSTRAINT contacts_area_id_fkey FOREIGN KEY (area_id) REFERENCES areas(id);


--
-- Name: devices_commerce_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT devices_commerce_id_fkey FOREIGN KEY (commerce_id) REFERENCES commerces(id);


--
-- Name: devices_mobile_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY devices_mobile
    ADD CONSTRAINT devices_mobile_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: devices_state_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY devices
    ADD CONSTRAINT devices_state_id_fkey FOREIGN KEY (state_id) REFERENCES states(id);


--
-- Name: devices_support_device_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY devices_support
    ADD CONSTRAINT devices_support_device_id_fkey FOREIGN KEY (device_id) REFERENCES devices(id);


--
-- Name: discounts_categories_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY discounts_categories
    ADD CONSTRAINT discounts_categories_category_id_fkey FOREIGN KEY (category_id) REFERENCES categories(id);


--
-- Name: discounts_subsidiaries_subsidiary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY discounts_subsidiaries
    ADD CONSTRAINT discounts_subsidiaries_subsidiary_id_fkey FOREIGN KEY (subsidiary_id) REFERENCES subsidiaries(id);


--
-- Name: dispatch_addresses_area_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY dispatch_addresses
    ADD CONSTRAINT dispatch_addresses_area_id_fkey FOREIGN KEY (area_id) REFERENCES areas(id);


--
-- Name: dispatch_addresses_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY dispatch_addresses
    ADD CONSTRAINT dispatch_addresses_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: driver_vehicles_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY driver_vehicles
    ADD CONSTRAINT driver_vehicles_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: driver_vehicles_vehicle_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY driver_vehicles
    ADD CONSTRAINT driver_vehicles_vehicle_id_fkey FOREIGN KEY (vehicle_id) REFERENCES vehicles(id);


--
-- Name: favorites_subsidiary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY favorites
    ADD CONSTRAINT favorites_subsidiary_id_fkey FOREIGN KEY (subsidiary_id) REFERENCES subsidiaries(id);


--
-- Name: favorites_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY favorites
    ADD CONSTRAINT favorites_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: ingredients_ingredient_per_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY ingredients
    ADD CONSTRAINT ingredients_ingredient_per_category_id_fkey FOREIGN KEY (ingredient_per_category_id) REFERENCES ingredients_per_category(id);


--
-- Name: ingredients_per_category_subsidiary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY ingredients_per_category
    ADD CONSTRAINT ingredients_per_category_subsidiary_id_fkey FOREIGN KEY (subsidiary_id) REFERENCES subsidiaries(id);


--
-- Name: options_group_ingredient_per_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY options_group
    ADD CONSTRAINT options_group_ingredient_per_category_id_fkey FOREIGN KEY (ingredient_per_category_id) REFERENCES ingredients_per_category(id);


--
-- Name: options_group_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY options_group
    ADD CONSTRAINT options_group_product_id_fkey FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: product_tags_product_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY product_tags
    ADD CONSTRAINT product_tags_product_id_fkey FOREIGN KEY (product_id) REFERENCES products(id);


--
-- Name: product_tags_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY product_tags
    ADD CONSTRAINT product_tags_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES tags(id);


--
-- Name: products_sub_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_sub_category_id_fkey FOREIGN KEY (sub_category_id) REFERENCES sub_categories(id);


--
-- Name: ratings_subsidiary_subsidiary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY ratings_subsidiary
    ADD CONSTRAINT ratings_subsidiary_subsidiary_id_fkey FOREIGN KEY (subsidiary_id) REFERENCES subsidiaries(id);


--
-- Name: ratings_subsidiary_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY ratings_subsidiary
    ADD CONSTRAINT ratings_subsidiary_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: states_country_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY states
    ADD CONSTRAINT states_country_id_fkey FOREIGN KEY (country_id) REFERENCES countries(id);


--
-- Name: sub_categories_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY sub_categories
    ADD CONSTRAINT sub_categories_category_id_fkey FOREIGN KEY (category_id) REFERENCES categories(id);


--
-- Name: subsidiaries_area_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY subsidiaries
    ADD CONSTRAINT subsidiaries_area_id_fkey FOREIGN KEY (area_id) REFERENCES areas(id);


--
-- Name: subsidiaries_commerce_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY subsidiaries
    ADD CONSTRAINT subsidiaries_commerce_id_fkey FOREIGN KEY (commerce_id) REFERENCES commerces(id);


--
-- Name: tags_system_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY tags
    ADD CONSTRAINT tags_system_id_fkey FOREIGN KEY (system_id) REFERENCES systems(id);

--
-- Name: users_subsidiaries_subsidiary_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY users_subsidiaries
    ADD CONSTRAINT users_subsidiaries_subsidiary_id_fkey FOREIGN KEY (subsidiary_id) REFERENCES subsidiaries(id);


--
-- Name: users_subsidiaries_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY users_subsidiaries
    ADD CONSTRAINT users_subsidiaries_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: users_tag_tags_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY users_tag
    ADD CONSTRAINT users_tag_tags_id_fkey FOREIGN KEY (tags_id) REFERENCES tags(id);


--
-- Name: users_tag_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY users_tag
    ADD CONSTRAINT users_tag_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


--
-- Name: vehicles_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lion
--

ALTER TABLE ONLY vehicles
    ADD CONSTRAINT vehicles_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);

ALTER TABLE ONLY user_groups
    ADD CONSTRAINT user_groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES groups(id);

ALTER TABLE ONLY groups_permissions
    ADD CONSTRAINT groups_permissions_permissions_id_fkey FOREIGN KEY (permission_id) REFERENCES permissions(id);

ALTER TABLE ONLY groups_permissions
    ADD CONSTRAINT groups_permissions_groups_id_fkey FOREIGN KEY (group_id) REFERENCES groups(id);

ALTER TABLE ONLY coupons_users
    ADD CONSTRAINT coupons_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);

ALTER TABLE ONLY coupons_users
    ADD CONSTRAINT coupons_users_coupon_id_fkey FOREIGN KEY (coupon_id) REFERENCES coupons(id);

ALTER TABLE ONLY users_telephones
    ADD CONSTRAINT users_telephones_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);

ALTER TABLE ONLY options_compounds
    ADD CONSTRAINT options_compounds_product_id_fkey FOREIGN KEY (product_id) REFERENCES  products(id);

ALTER TABLE ONLY options_compounds
    ADD CONSTRAINT options_compounds_compound_id_fkey FOREIGN KEY (compound_id) REFERENCES compounds(id);

ALTER TABLE ONLY items_compounds
    ADD CONSTRAINT items_compounds_option_compound_id_fkey FOREIGN KEY (option_compound_id) REFERENCES options_compounds(id);

ALTER TABLE ONLY items_compounds
    ADD CONSTRAINT items_compounds_compound_id_fkey FOREIGN KEY (compound_id) REFERENCES compounds(id);

ALTER TABLE ONLY compounds
    ADD CONSTRAINT compounds_subsidiary_id_fkey FOREIGN KEY (subsidiary_id) REFERENCES subsidiaries(id);

ALTER TABLE ONLY compounds
    ADD CONSTRAINT compounds_category_id_fkey FOREIGN KEY (category_id) REFERENCES categories(id);

 ALTER TABLE ONLY temp_users
     ADD CONSTRAINT temp_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


ALTER TABLE ONLY owner_dispatchers
  ADD CONSTRAINT owner_dispatchers_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


ALTER TABLE ONLY owner_dispatchers
  ADD CONSTRAINT owner_dispatchers_owner_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);

ALTER TABLE ONLY ratings_dispatchers
  ADD CONSTRAINT ratings_dispatchers_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


ALTER TABLE ONLY ratings_dispatchers
  ADD CONSTRAINT ratings_dispatchers_dispatcher_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);
