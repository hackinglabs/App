-- (Dev/Prod) ? NO:YES
ALTER TABLE subsidiaries ADD COLUMN cover character varying DEFAULT 'https://storage.googleapis.com/dxpress/default_cover.jpg';
