-- (Dev/Prod) ? YES:YES
--Table dispatcher_rejected
CREATE TABLE dispatcher_rejected (
      id SERIAL NOT NULL PRIMARY KEY,
      created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
      user_id integer references users(id) NOT NULL,
      order_id varchar(250)
);


ALTER TABLE dispatcher_rejected OWNER TO lion;