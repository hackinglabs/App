-- (Dev/Prod) ? NO:YES
CREATE TABLE black_list_phone (
    id serial NOT NULL PRIMARY KEY,
    user_id integer NOT NULL references users(id),
    order_id character varying NOT NULL,
    telephone integer NOT NULL,
    created_at date DEFAULT ('now'::text)::date
);

ALTER TABLE black_list_phone OWNER TO lion;

