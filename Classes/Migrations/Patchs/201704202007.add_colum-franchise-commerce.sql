
-- (Dev/Prod) ? NO:YES
ALTER TABLE commerces ADD COLUMN franchise boolean DEFAULT FALSE ;

-- (Dev/Prod) ? NO:YES
ALTER TYPE dxpress.commerce ADD franchise boolean;
ALTER TABLE dxpress.client_orders ADD franchise boolean;
ALTER TABLE dxpress.client_orders ADD user frozen<user>;
ALTER TABLE dxpress.client_orders ADD address_order frozen<address>;
