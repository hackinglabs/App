-- (Dev/Prod) ? NO:YES
CREATE TABLE dispatcher_orders (
    dispatcher_id integer NOT NULL PRIMARY KEY,
    order_id character varying NOT NULL,
    current boolean NOT NULL,
    created_at date DEFAULT ('now'::text)::date
);

CREATE INDEX dispatcher_orders_dispatcher_id_current_idx ON dispatcher_orders (dispatcher_id, current);

ALTER TABLE dispatcher_orders OWNER TO lion;

