-- (Dev/Prod) ? NO:NO
CREATE TABLE dxl_order_hash (
    id_temp integer NOT NULL PRIMARY KEY,
    order_id character varying NOT NULL
);

ALTER TABLE dxl_order_hash OWNER TO lion;
