import psycopg2
from sqlalchemy.engine import reflection
from sqlalchemy import create_engine

from sqlalchemy.schema import (
    MetaData,
    Table,
    DropTable,
    ForeignKeyConstraint,
    DropConstraint,
    )



class SchemaTools(object):
    def __init__(self):
        self.cursor = None
        #try:
        self.engine = create_engine('postgresql+psycopg2://kitten:MN7SCZaJbJrjqpzErDpWrFRgXFGQPgjk@10.240.0.6:5432/tiger', client_encoding='utf8', echo=True)
        self.conna = self.engine.connect()
        self.conn = psycopg2.connect(
            "dbname=tiger user=kitten password=MN7SCZaJbJrjqpzErDpWrFRgXFGQPgjk host=10.240.0.6 port=5432")
        self.cursor = self.conn.cursor()
        #except:
        #    print "Error dataBase"

    def drop_all(self):
        if self.cursor is not None:
            # the transaction only applies if the DB supports
            # transactional DDL, i.e. Postgresql, MS SQL Server
            trans = self.conna.begin()

            inspector = reflection.Inspector.from_engine(self.engine)

            # gather all data first before dropping anything.
            # some DBs lock after things have been dropped in
            # a transaction.

            metadata = MetaData()

            tbs = []
            all_fks = []

            for table_name in inspector.get_table_names():
                fks = []
                for fk in inspector.get_foreign_keys(table_name):
                    if not fk['name']:
                        continue
                    fks.append(
                        ForeignKeyConstraint((),(),name=fk['name'])
                        )
                t = Table(table_name,metadata,*fks)
                tbs.append(t)
                all_fks.extend(fks)

            for fkc in all_fks:
                self.conna.execute(DropConstraint(fkc))

            for table in tbs:
                if str(table) != "spatial_ref_sys":
                    self.conna.execute(DropTable(table))
            trans.commit()
        else:
            exit()

    def run_schema(self):
        if self.cursor is not None:
            self.cursor.execute(open("db.sql", "r").read())
            self.cursor.execute('commit')
        else:
            exit()

    def run_dump(self):
        if self.cursor is not None:
            self.cursor.execute(open("dump_db_3.sql", "r").read())
            self.cursor.execute('commit')
        else:
            exit()


schema = SchemaTools()
schema.drop_all()
schema.run_schema()
schema.run_dump()
