
# -*- coding: utf-8 -*-
from Classes.OAuth2 import OAuth2
from Models import User


class FacebookOAuth2(OAuth2):

    ACCESS_TOKEN_URL = 'https://graph.facebook.com/v2.5/oauth/access_token'
    GRAPH_API_URL = 'https://graph.facebook.com/v2.5/me?fields=first_name,last_name,gender,email'

    RESPONSE_KEYS = frozenset(['first_name', 'last_name', 'email'])
    type = 'facebook'

    def __init__(self, forms):
        super(FacebookOAuth2, self).__init__(forms)

        web = frozenset(["clientId", "redirectUri", "code"])
        android = frozenset(["expires", "token"])

        values = frozenset(forms.keys())

        if not web.difference(values):
            # All parameters Web
            self.source = "web"
            self._parse = {
                'client_id':     forms['clientId'],
                'redirect_uri':  forms['redirectUri'],
                'client_secret': self.secrets["facebook-secret"],
                'code':          forms['code']
            }
        elif not android.difference(values):
            # All parameters android
            self.source = "android"
            self._parse = {
                'token_type':   'bearer',
                'expires_in':   forms["expires"],
                'access_token': forms["token"]
            }
        else:
            raise Exception("There is a problem with Facebook Login.")

    def get_data(self):
        if self.source is "web":
            data = self._do_request(self.ACCESS_TOKEN_URL)
            data.update(fields=','.join(self.RESPONSE_KEYS))
            self._parse = data
        data = self._do_request(self.GRAPH_API_URL)
        for key in data.keys():
            if key in frozenset(["type", "source"]):
                pass
            setattr(self, key, data[key])

    def get_user(self, user=None):
        response = getattr(self, '_response', None)
        if response is None:
            raise NotImplementedError("You should execute get_data first.")

        response_keys = frozenset(response.keys())

        if self.RESPONSE_KEYS.difference(response_keys):
            raise Exception("Facebook response mal-formatted.")

        if user is None:
            user = User()
            user.email = self._response["email"]
            user.password = ''
        user.first_name = self._response["first_name"]
        user.last_name = self._response["last_name"]
        user.facebook = self._response["id"]
        user.avatar = "https://graph.facebook.com/%s/picture?type=large" % self._response["id"]
        if "gender" in self._response:
            user.gender = self._response["gender"][:1].upper()
        user.active = True

        return user



