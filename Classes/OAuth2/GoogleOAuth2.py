# -*- coding: utf-8 -*-
from Classes.OAuth2 import OAuth2
from Models import User


class GoogleOAuth2(OAuth2):
    ACCESS_TOKEN_URL = 'https://www.googleapis.com/oauth2/v4/token'
    PEOPLE_API_URL = 'https://www.googleapis.com/plus/v1/people/me/openIdConnect'
    TOKEN_INFO_URL = 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token='

    RESPONSE_KEYS = frozenset(['family_name', 'given_name', 'email', 'sub'])
    type = 'google'

    def __init__(self, forms):
        super(GoogleOAuth2, self).__init__(forms)

        web = frozenset(["clientId", "redirectUri", "code"])
        android = frozenset(["token"])

        values = frozenset(forms.keys())

        if not web.difference(values):
            # All parameters Web
            self.source = "web"
            self._parse = {
                'grant_type':    'authorization_code',
                'client_id':     forms['clientId'],
                'redirect_uri':  forms['redirectUri'],
                'client_secret': self.secrets["google-secret"],
                'code':          forms['code']
            }
        elif not android.difference(values):
            # All parameters android
            self.source = "android"
            self.TOKEN_INFO_URL += forms['token']
        else:
            raise Exception("There is a problem with Google Login.")

    def get_data(self):
        if self.source is "web":
            data = self._do_request(self.ACCESS_TOKEN_URL, "POST")
            self._parse = data
            data = self._do_request(self.PEOPLE_API_URL)
        else:
            data = self._do_request(self.TOKEN_INFO_URL)
        for key in data.keys():
            if key in frozenset(["type", "source"]):
                pass
            new_attr = key
            if key == "sub":
                new_attr = "id"
            setattr(self, new_attr, data[key])

    def get_user(self, user=None):
        response = getattr(self, '_response', None)

        if response is None:
            raise NotImplementedError("You should execute get_data first.")

        response_keys = frozenset(response.keys())
        if self.RESPONSE_KEYS.difference(response_keys):
            raise Exception("Google response mal-formatted.")

        if user is None:
            user = User()
            user.email = self._response["email"]
            user.password = ''
        user.first_name = self._response["given_name"]
        user.last_name = self._response["family_name"]
        user.google = self._response["sub"]

        if "picture" in response_keys:
            user.avatar = self._response["picture"]

        if "gender" in response_keys:
            user.gender = self._response["gender"][:1].upper()
        user.active = True

        return user
