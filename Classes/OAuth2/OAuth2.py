# -*- coding: utf-8 -*-
import json
from abc import ABCMeta, abstractmethod
from Configs import Config
import requests


class OAuth2(object):
    __metaclass__ = ABCMeta

    @property
    def source(self):
        return getattr(self, '_source', "web")

    @source.setter
    def source(self, value):
        only = frozenset(["web", "android", "ios", "windowsphone"])
        if value not in only:
            raise AttributeError("You can only define web, android, ios or windowsphone")
        self._source = value

    @abstractmethod
    def __init__(self, forms):
        if forms is None or len(forms) <= 0:
            raise TypeError("Forms value can't be None or Empty")

        config = Config()
        self.secrets = config.social()

    @abstractmethod
    def get_data(self):
        pass

    @abstractmethod
    def get_user(self, user):
        pass

    def _do_request(self, url, req_type="GET"):
        try:
            params = getattr(self, '_parse', None)
            headers = getattr(self, '_header', None)

            if req_type not in frozenset(["GET", "POST"]):
                req_type = "GET"

            data = {}
            if headers is not None:
                data['headers'] = headers

            r = requests.request(req_type, url, params=params, **data)
            data = json.loads(r.text)
            self._response = data
            return data
        except:
            social_type = getattr(self, 'type', 'Social login')
            raise Exception("We can't communicate with %s." % social_type.capitalize())
