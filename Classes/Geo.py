class Geo(object):
    _require = frozenset(['lat', 'lon', 'id'])

    def __init__(self):
        self.lat = None
        self.lon = None
        self.id = None

    def add(self, *args, **kwargs):
        if len(args) == 1 or len(args) == 3:
            if isinstance(args[0], dict) and self._require <= set(args[0]):
                self.lat = args[0]["lat"]
                self.lon = args[0]["lon"]
                self.id = args[0]["id"]
                return self
            else:
                self.lat = args[0]
                self.lon = args[1]
                self.id = args[2]
                return self
        elif len(kwargs) == 3:
            if self._require <= set(kwargs):
                self.lat = kwargs["lat"]
                self.lon = kwargs["lon"]
                self.id = kwargs["id"]
                return self
        return False

    def is_valid(self):
        if isinstance(self.lat, float) and \
                isinstance(self.lon, float) and \
                isinstance(self.id, basestring):
            return True
        return False

    def to_tuple(self):
        return self.lat, self.lon, self.id
