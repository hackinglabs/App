import subprocess
import json


class Webpay(object):

    __commerce_code = None

    def __init__(self, commerce_code, path):
        self.__commerce_code = commerce_code

        if path.endswith(".php"):
            self.__path = path
        else:
            path = path.rstrip("/") + "/webpay.php"
            self.__path = path

    def init_transaction(self, order_id, total,
                         session_id, return_url, final_url):
        data = {
            'total': total,
            'session_id': session_id,
            'order_id': order_id,
            'return_url': return_url,
            'final_url': final_url
        }

        return self.__do_request("initTransaction" ,data)

    def result_transaction(self, token):
        data = {
            'token': token,
        }
        return self.__do_request("resultTransaction", data)

    def ack_transaction(self, token):
        data = {
            'token': token
        }
        return self.__do_request("ackTransaction", data)

    def init_inscription(self, email, username, final_url):
        data = {
            'username': username,
            'email': email,
            'final_url': final_url
        }
        return self.__do_request("initInscription", data, True)

    def finish_inscription(self, token):
        data = {
            'token': token
        }
        return self.__do_request("finishInscription", data, True)

    def remove_user(self, tbk_user, username):
        data = {
            'tbk_user': tbk_user,
            'username': username
        }
        return self.__do_request("removeUser", data, True)

    def authorize(self, amount, tbk_user, username, buy_order):
        data = {
            'amount': amount,
            'tbk_user': tbk_user,
            'username': username,
            'buy_order': buy_order
        }
        return self.__do_request("authorize", data, True)

    def reverse(self, buy_order):
        data = {
            'buy_order': buy_order
        }
        return self.__do_request("reverse", data, True)

    def __do_request(self, operation, data, oneclick=False):
        if oneclick:
            webpay_type = "oneclick"
        else:
            webpay_type = "normal"

        cmd = [
            "/usr/bin/php7.0",
            self.__path,
            "-t",
            webpay_type,
            "-c",
            self.__commerce_code,
            "-o",
            operation,
            "-d",
            json.dumps(data)
        ]

        out = subprocess.check_output(cmd)
        try:
            out = json.loads(out)
        except:
            out = {"error", "Invalid response from script"}
        return out
