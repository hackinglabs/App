# -*- coding: utf-8 -*-
from pycloudflare.services import CloudFlareService
from sqlalchemy import event
from slugify import slugify

from Configs.Config import Config
import logging

from Models import Redis


class CommerceActions(object):

    @staticmethod
    def before_insert_update(mapper, connection, target):
        logger = logging.getLogger(__name__)

        if target.slug is None or target.slug == '':
            slug = slugify(target.name.replace(" ", "").replace("'", ""))
        else:
            slug = target.slug

        try:
            conf = Config().cloudflare()

            logger.debug("CONFIG: %s", conf)

            cf = CloudFlareService(conf['key'], conf['mail'])

            logger.debug("Subdomain id %s", target.subdomain_id)
            if not target.subdomain_id:
                # New subdomain
                new_dns = {
                    'type': 'A',
                    'name': slug,
                    'proxied': True,
                    'content': conf['ip']
                }
                res = cf.create_dns_record(conf['zone'], new_dns)
                target.subdomain_id = res['id']
            else:
                # Update subdomain
                res = cf.update_dns_record(conf['zone'],
                                           target.subdomain_id,
                                           {'name': slug, 'proxied': True})
                target.subdomain_id = res['id']
        except:
            logger.error("Error creating subdomain")
            pass

        target.slug = slug

    @staticmethod
    def after_insert_update(mapper, connection, target):
        for c in Redis.Cache.get_by(type_cache="commerce"):
            c.delete()

        for c in Redis.Cache.get_by(type_cache="subsidiary"):
            c.delete()


    @classmethod
    def register(cls):
        event.listen(cls, 'before_update', cls.before_insert_update)
        event.listen(cls, 'before_insert', cls.before_insert_update)
        event.listen(cls, 'after_insert', cls.after_insert_update)
        event.listen(cls, 'after_update', cls.after_insert_update)
