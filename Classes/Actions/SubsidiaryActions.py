# -*- coding: utf-8 -*-
import logging
from rom import util

from geoalchemy2 import WKTElement
from sqlalchemy import event

from Models.Redis.Cache import Cache


class SubsidiaryActions(object):

    @staticmethod
    def after_insert_update(mapper, connection, target):

        logger = logging.getLogger(__name__)
        from Classes import ConnectRedis
        redis = ConnectRedis()
        geo = target.to_geo()
        logger.debug("Latitude: %s - Longitude: %s", geo.lat, geo.lon)
        util.get_connection().geoadd("geo_subsidiaries", geo.lon, geo.lat, geo.id)

        for c in Cache.query.filter(type_cache="commerce"):
            c.delete()

        for c in Cache.query.filter(type_cache="subsidiary"):
            c.delete()

    @staticmethod
    def before_insert_update(mapper, connection, target):
        if isinstance(target.coordinates, basestring):
            # They send us lat, lon.. and postgis save it like lon, lat.
            # FUCKING POSTGIS
            logger = logging.getLogger(__name__)
            try:
                xy = target.coordinates.split(" ")
                logger.debug("OLD - Latitude: %s - Longitude: %s", xy[0], xy[1])
                logger.debug("NEW - Latitude: %s - Longitude: %s", xy[1], xy[0])
                target.coordinates = WKTElement(
                    'Point(%s %s)' % (xy[1], xy[0],), srid=4326)
            except:
                logger.error("There isn't a valid coordinate")

    @classmethod
    def register(cls):
        event.listen(cls, 'after_insert', cls.after_insert_update)
        event.listen(cls, 'after_update', cls.after_insert_update)
        event.listen(cls, 'before_update', cls.before_insert_update)
        event.listen(cls, 'before_insert', cls.before_insert_update)
