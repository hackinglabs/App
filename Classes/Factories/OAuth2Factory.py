# -*- coding: utf-8 -*-

from importlib import import_module


class OAuth2Factory(object):

    def create(self, social_network, forms):
        valid_types = frozenset(['facebook', 'google'])
        if social_network.lower() in valid_types:
            oauth2_class = social_network.capitalize() + "OAuth2"
            module = import_module('Classes.OAuth2.' + oauth2_class)
            class_ = getattr(module, oauth2_class)
            return class_(forms)
        else:
            raise Exception("We don't support {0} OAuth2 Login.".format(social_network.capitalize()))
