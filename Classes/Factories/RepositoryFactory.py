from importlib import import_module

from Classes.Permission import Permission


class RepositoryFactory(object):

    user = None
    db = None
    model = None
    extra = None
    permissions = None

    def __init__(self, model, **kwargs):
        self.extra = kwargs

        if isinstance(model, basestring):
            model_name = model
        else:
            if type(model).__name__ == "DeclarativeMeta":
                model = model()
            self.model = model
            model_name = type(model).__name__
        self.model_class = model_name
        self.repo_class = self.model_class + "Repository"

    def set_db(self, db):
        self.db = db

    def set_user(self, user):
        self.user = user
        self.permissions = Permission(user)

    def set_model(self, model):
        self.model = model

    def create(self):
        module = import_module("Repositories." + self.repo_class)
        class_ = getattr(module, self.repo_class)
        class_ = class_(self.db)
        if self.user is not None \
                and self.user is not False:
            class_.user = self.user
            class_.permissions = self.permissions
        if self.model is not None:
            class_.model = self.model
        if self.extra is not None:
            for key in self.extra:
                setattr(class_, key, self.extra[key])
        return class_

