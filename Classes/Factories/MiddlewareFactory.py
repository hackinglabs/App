# -*- coding: utf-8 -*-
from importlib import import_module


class MiddlewareFactory(object):

    def __init__(self, name):
        middleware_file = '{0}Middleware'.format(name.capitalize())
        middleware_function = '{0}_middleware'.format(name.lower())

        module = import_module('Classes.Middlewares.{0}'.format(middleware_file))
        self._method = getattr(module, middleware_function)

    def create(self, options):
        return self._method
