# -*- coding: utf-8 -*-
import redis
from rom import util

from cassandra.cqlengine import connection
from cassandra.auth import PlainTextAuthProvider

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

from Configs.Config import Config
import logging
import geoip2.database


# MetaClass for Apply Singleton
class SingletonMetaClass(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(SingletonMetaClass, cls)\
                .__call__(*args, **kwargs)
        return cls._instances[cls]


class ConnectPSQL(object):
    """
    class connect to PostgresSQL
    """
    __metaclass__ = SingletonMetaClass
    _engine = None

    def __init__(self, *args, **kwargs):
        try:
            options = Config().pgsql_data()
            engine = create_engine(options['postgres'], client_encoding='utf8', echo=False)
            self._engine = engine
        except Exception as e:
            logger = logging.getLogger(__name__)
            logger.error("PGSQL Error Connect to database")
            logger.error(e.message)
            exit()

    def create_session(self):
        return scoped_session(sessionmaker(autoflush=False, autocommit=True, bind=self._engine))


class GeoIPDB(object):
    __metaclass__ = SingletonMetaClass
    _session = None

    def __new__(cls, *args, **kwargs):
        reader = None
        try:
            options = Config().geo_ip()
            reader = geoip2.database.Reader(options['file_path'])
        except Exception as e:
            logger = logging.getLogger(__name__)
            logger.error("Can't connect geo ip")
            logger.error(e.message)

        return reader


class ConnectTile(object):
    """
    class connect to Redis
    """

    __metaclass__ = SingletonMetaClass
    _session = None

    def __new__(cls, *args, **kawrgs):
        try:
            options = Config().tile_data()
            pool = redis.ConnectionPool(**options)
            session = redis.Redis(connection_pool=pool)
            cls._session = session
        except Exception as e:
            logger = logging.getLogger(__name__)
            logger.error("Error connect Database Tile")
            logger.error(e.message)
            exit()

        return cls._session


class ConnectRedis(object):
    """
    class connect to Redis
    """

    __metaclass__ = SingletonMetaClass
    _session = None

    def __new__(cls, *args, **kawrgs):
        try:
            options = Config().redis_data()

            util.CONNECTION = redis.StrictRedis(**options)
        except Exception as e:
            logger = logging.getLogger(__name__)
            logger.error("Error connect Database Redis")
            logger.error(e.message)
            exit()

        return cls._session


class ConnectCassandra(object):
    """
    class connect to Cassandra
    """
    __metaclass__ = SingletonMetaClass
    _session = None

    def __new__(cls):
        options = Config().cassandra_data()
        try:
            if 'password' in options and 'username' in options:
                auth_provider = PlainTextAuthProvider(username=options['username'],
                                                      password=options['password'])
                connection.setup([options['host']],
                                 options['database'],
                                 auth_provider=auth_provider,
                                 protocol_version=options['protocol'])
            else:
                connection.setup([options['host']],
                                 options['database'],
                                 protocol_version=options['protocol'])

            cls._session = connection.get_session()
        except Exception as e:
            logger = logging.getLogger(__name__)
            logger.error(e.message)
            return "Error Connection Cassandra"

        return cls._session

