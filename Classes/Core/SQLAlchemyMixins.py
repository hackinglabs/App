import datetime

from geoalchemy2 import WKBElement
from geoalchemy2.shape import to_shape
from sqlalchemy import inspect
from sqlalchemy.orm.collections import InstrumentedList


class DictSerializableMixin(object):
    def get_columns(self, model=None):
        if model is None:
            model = self

        # Get object inspection and mapper
        obj_inspect = inspect(model)
        mapper = obj_inspect.mapper

        # Get every column from model
        columns = [column.key for column in mapper.columns]
        # Get every relation from model
        relations = mapper.relationships.items()
        # Get unloaded columns
        unloaded_columns = obj_inspect.unloaded

        # Get the loaded columns only
        loaded_columns = [column for column in columns if column not in unloaded_columns]

        return loaded_columns, unloaded_columns, relations, columns

    def to_dict(self, loaded_columns=None,
                unloaded_columns=None, relations=None,
                columns=None, model=None, replace=None):

        if model is None:
            model = self

        # This is going to be the final dict
        final_dictionary = {}

        if loaded_columns is None or unloaded_columns is None or \
                relations is None or columns is None:
            loaded_columns, unloaded_columns, relations, columns = self.get_columns(model)

        hybrid_fields = getattr(model, 'hybrid_fields', [])

        for column in hybrid_fields:
            # Get the current value for column
            value = getattr(model, column, None)

            final_dictionary[column] = value

        # Get value of every loaded column
        for column in loaded_columns:
            # Get the current value for column
            value = getattr(model, column, None)

            # Convert if it has a special format
            if isinstance(value, datetime.datetime):
                value = value.isoformat()
            elif isinstance(value, datetime.date):
                value = value.isoformat()
            elif isinstance(value, WKBElement):
                shape = to_shape(value)
                try:
                    value = {'lng': shape.x, 'lat': shape.y, 'x': shape.x, 'y': shape.y}
                except:
                    coords = list(shape.exterior.coords)
                    coords.pop()
                    value = []
                    for s in coords:
                        value.append({"lng": s[0], "lat": s[1]})

            # Add column and value to dict
            final_dictionary[column] = value

        # Do the same for every relation of this model
        for column, relation in relations:
            # Bypass unloaded columns
            if column in unloaded_columns:
                continue

            # Get the relation object
            relation_object = getattr(model, column, None)

            value = None

            # Recursive load, but checking if is a List of objects
            if isinstance(relation_object, InstrumentedList):
                value = []

                if len(relation_object) > 0:
                    lcols, ucols, rel, cols = self.get_columns(relation_object[0])
                    for r_o in relation_object:
                        value.append(self.to_dict(model=r_o,
                                                  loaded_columns=lcols,
                                                  unloaded_columns=ucols,
                                                  relations=rel,
                                                  columns=cols,
                                                  replace=replace))

            elif relation_object is not None:
                value = self.to_dict(model=relation_object)

            if replace and column in replace:
                column_name = replace[column]
            else:
                column_name = column

            # Add column and value to dict
            final_dictionary[column_name] = value

        return final_dictionary
