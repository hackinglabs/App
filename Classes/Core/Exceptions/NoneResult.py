# -*- coding: utf-8 -*-


class NoneResult(Exception):
    def __init__(self, value=None):
        if value is None:
            self.value = "Some operation return None"
        else:
            self.value = value

    def __str__(self):
        return repr(self.value)
