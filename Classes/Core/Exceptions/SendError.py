# -*- coding: utf-8 -*-


class SendError(Exception):
    def __init__(self, value=None):
        if value is None:
            self.value = "We can't send the message."
        else:
            self.value = value

    def __str__(self):
        return repr(self.value)
