# -*- coding: utf-8 -*-


class UploadError(Exception):
    def __init__(self, value=None):
        if value is None:
            self.value = "Can't upload file."
        else:
            self.value = value

    def __str__(self):
        return repr(self.value)
