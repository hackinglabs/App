# -*- coding: utf-8 -*-


class SaveError(Exception):
    def __init__(self, value=None):
        if value is None:
            self.value = "Can't save data."
        else:
            self.value = value

    def __str__(self):
        return repr(self.value)
