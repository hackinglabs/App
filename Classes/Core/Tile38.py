#!/usr/bin/env python

import socket
import json


class ClientError(Exception):
    def __init__(self, value=None):
        self.value = value

    def __str__(self):
        return repr(self.value)


class Response(object):
    result = None
    elapsed = None
    response = ''

    def __init__(self, **kwargs):
        if 'ok' in kwargs:
            self.result = kwargs["ok"]
        else:
            self.result = False

        if 'elapsed' in kwargs:
            self.elapsed = kwargs["elapsed"]

        if 'ping' in kwargs:
            self.response = kwargs["ping"]

    def __str__(self):
        base = "<Tile38 Response> Result was: {result}, elapsed time: {elapsed}, response: {response}"
        return base.format(result=self.result, elapsed=self.elapsed.encode("utf-8"), response=self.response)


class Tile38(object):

    __HOST = '127.0.0.1'
    __PORT = 9851
    __TIMEOUT = None
    __BUFFER_SIZE = 1024
    __MAX_MESSAGE_SIZE = 16777215
    __BASE_CMD = "${length} {cmd}\r\n"

    __SOCKET = None

    @property
    def host(self):
        return self.__HOST

    @property
    def port(self):
        return self.__PORT

    @property
    def timeout(self):
        return self.__TIMEOUT

    def __init__(self, **kwargs):
        if 'host' in kwargs:
            try:
                host = kwargs['host']
                socket.inet_aton(host)
                self.__HOST = host
            except socket.error:
                raise ClientError("Invalid HOST IP Address")

        if 'port' in kwargs:
            try:
                port = int(kwargs['port'])
                if port < 0 or port > 65535:
                    raise ClientError()
                self.__PORT = port
            except (ValueError, ClientError):
                raise ClientError("Port number must be an integer between 0 and 65535")

        if 'timeout' in kwargs:
            try:
                timeout = int(kwargs['timeout'])
                if timeout < 1:
                    raise ClientError()
                self.__TIMEOUT = timeout
            except (ValueError, ClientError):
                raise ClientError("Timeout value must be an integer greater that 0")

    def connect(self):
        if self.__SOCKET is None:
            s = socket.create_connection((self.__HOST, self.__PORT))
            s.settimeout(self.__TIMEOUT)
            self.__SOCKET = s
            return self

    def close(self):
        self.__write_message("QUIT")
        self.__SOCKET.close()

    def execute(self, cmd):
        cmd = cmd.strip()
        self.__write_message(cmd)
        return self.__read_message()

    def __parse_message(self, cmd):
        cmd = cmd.strip()
        length = len(cmd)

        data = {
            'length': length,
            'cmd': cmd
        }
        cmd = self.__BASE_CMD.format(**data)
        return cmd

    def __write_message(self, cmd):
        cmd = self.__parse_message(cmd)
        if self.__SOCKET is None:
            raise ClientError("Session was destroyed before command execution")

        self.__SOCKET.send(cmd)

    def __read_message(self):
        if self.__SOCKET is None:
            raise ClientError("Session was destroyed before command read")
        data = self.__SOCKET.recv(self.__BUFFER_SIZE)
        data = data.split(' ', 1)[1].strip()

        json_data = json.loads(data)
        return Response(**json_data)

    def __del__(self):
        self.close()