import datetime
import logging
from Classes.Core.Exceptions import NoneResult
from geoalchemy2.elements import WKBElement
from geoalchemy2.shape import to_shape
from sqlalchemy.orm import class_mapper


class Repository(object):
    def __init__(self, db_context):
        self.con = db_context
        self.logger = logging.getLogger(__name__)

    user = None
    model = None
    permissions = None

    def __filter_enabled(self, query, model):
        if (self.permissions is None or
                not self.permissions.has_perm("view_not_enabled")) and \
                        'enabled' in dir(model):
            query = query.filter(model.enabled == True)
        return query

    def save(self, model):
        self.con.begin()
        try:
            self.con.add(model)
            self.con.commit()
        except Exception as e:
            self.logger.error("Error in save 'model': " + e.message)
            self.con.rollback()
            return False

        return True

    def all(self, **kwargs):
        model = self.model.__class__
        query = self.con.query(model)

        filter_fields = {}

        for k in kwargs:
            if k in self.model.__class__.__dict__:
                filter_fields[k] = kwargs[k]

        query = self.__filter_enabled(query, model)
        if len(filter_fields) > 0:
            query = query.filter_by(**filter_fields)

        res = query.all()

        # TODO: Translate
        if res is None:
            raise NoneResult("%s hasn't data", model.__name__)
        else:
            return res

    def exists(self, **kwargs):
        model = self.model.__class__
        keys = kwargs.keys()

        if len(keys) < 1:
            return None

        filter_fields = {}

        for k in keys:
            if k in self.model.__class__.__dict__:
                filter_fields[k] = kwargs[k]

        try:
            quantity = self.con.query(model).filter_by(**filter_fields)
            exists = self.con.query(quantity.exists()).scalar()
        except Exception as e:
            self.logger.error("Error in EXISTS on Repository: " + e.message)
            exists = False

        return exists

    def find(self, **kwargs):
        model = self.model.__class__

        self.logger.debug(model)

        keys = kwargs.keys()
        if len(keys) < 1:
            return None

        filter_fields = {}

        for k in keys:
            if k in self.model.__class__.__dict__:
                filter_fields[k] = kwargs[k]

        self.logger.debug(filter_fields)

        try:
            query = self.con.query(model).filter_by(**filter_fields)
            query = self.__filter_enabled(query, model)
            model = query.first()
        except Exception as e:
            self.logger.error("Error in FIND on Repository" + e.message)
            model = None

        # TODO: Translate
        if model is None:
            raise NoneResult("We didn't found any data")
        else:
            return model

    def delete(self, id, force=False):
        model = self.model.__class__
        obj = self.con.query(model).filter(model.id == id).first()

        if obj is None:
            return False

        obj.enabled = False
        return self.save(obj)

    def __convert_prop(self, obj, c):
        """
        Convert value from model to a valid value for json
        :param obj: Model object
        :param c: Field to convert
        :return: value
        """
        trans = getattr(obj, c)
        if isinstance(trans, datetime.datetime):
            return trans.isoformat()
        elif isinstance(trans, WKBElement):
            shape = to_shape(trans)
            return {'x': shape.x, 'y': shape.y}
        else:
            return trans

    def to_dict_new(self, models, replace=None):
        obj = models.pop(0)
        loaded_columns, unloaded_columns, relations, columns = obj.get_columns()

        all = [temp.to_dict(loaded_columns, unloaded_columns, relations, columns, replace=replace) for temp in models]
        all.insert(0, obj.to_dict(loaded_columns, unloaded_columns, relations, columns, replace=replace))
        return all

    def to_dict(self, obj=None, depth=0, rel=None, only=False, force=False, force_enabled=False):
        """
        Transform SQLAlchemy model to dict
        :param force_enabled:
        :param obj: Model object
        :param depth: Depth of relationships
        :param rel: Dict of fields for models
        :param only: Just bring rel fields
        :param force:
        :return: dict
        """

        if obj is None:
            return None

        if type(obj) is not list:
            found = set()
            mapper = class_mapper(obj.__class__)

            class_name = obj.__class__.__name__.lower()
            columns = [column.key for column in mapper.columns]

            if rel != '*':
                if isinstance(rel, list):
                    rel = {class_name: rel}

                if rel is not None and class_name in rel:
                    if only is False and hasattr(obj, 'default_fields'):
                        rel[class_name].extend(obj.default_fields)
                    elif only is True:
                        columns = frozenset(columns).intersection(
                            rel[class_name])

                if (rel is None or class_name not in rel) and \
                        hasattr(obj, 'default_fields'):
                    columns = obj.default_fields

            if hasattr(obj, 'hidden_fields') and force is False:
                columns = frozenset(columns).difference(obj.hidden_fields)

            out = {}
            for column in columns:
                out[column] = self.__convert_prop(obj, column)

            if depth > 0:
                for name, relation in mapper.relationships.items():
                    if rel is not None and \
                                    class_name in rel and \
                                    name in rel[class_name] and \
                                    relation not in found:
                        found.add(relation)
                        related_obj = getattr(obj, name)
                        if related_obj is not None:
                            if relation.uselist:
                                out[name] = []
                                for child in related_obj:
                                    if force_enabled:
                                        out[name].append(self.to_dict(child, depth=(depth - 1), rel=rel, force=force))
                                    else:
                                        if hasattr(child, 'enabled') and child.enabled:
                                            if hasattr(child, 'stock') and child.stock == False:
                                                continue
                                            else:
                                                out[name].append(
                                                    self.to_dict(child, depth=(depth - 1), rel=rel, force=force))
                                        elif hasattr(child, 'enabled') and child.enabled == False:
                                            continue
                                        else:
                                            out[name].append(
                                                self.to_dict(child, depth=(depth - 1), rel=rel, force=force))
                            else:
                                if force_enabled:
                                    out[name] = self.to_dict(related_obj, depth=(depth - 1), rel=rel, force=force)
                                else:
                                    if hasattr(related_obj, 'enabled') and related_obj.enabled:
                                        out[name] = self.to_dict(related_obj, depth=(depth - 1), rel=rel, force=force)
                                    if hasattr(related_obj, 'enabled') and related_obj.enabled == False:
                                        continue
                                    else:
                                        out[name] = self.to_dict(related_obj, depth=(depth - 1), rel=rel, force=force)
            return out
        else:
            all_dicts = []
            for o in obj:
                if isinstance(o, tuple) or isinstance(o, dict):
                    all_dicts = obj
                    break
                all_dicts.append(
                    self.to_dict(o, depth=depth, rel=rel,
                                 only=only, force=force)
                )
            return all_dicts
