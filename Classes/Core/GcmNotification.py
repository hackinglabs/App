# -*- coding: utf-8 -*-
import logging

from Configs import Config
from gcm import GCM

from Models.DeviceMobile import DeviceMobile


class GcmNotification(object):
    _gcm = None

    _android_token_list = []
    _ios_token_list = []

    _message = ''
    _message_type = None
    _priority = None
    _package = None
    _notification = None
    _notification_ios_only = True

    remove = []
    try_later = []
    canonical = []

    errors = []

    defaults = {
        'os': 'android',
        'type': 1,
        'priority': 'normal',
        'ios_only_notification': True,
        'package': 'this_value_is_the_one_from_config'
    }

    def __init__(self, defaults=None, for_clients=False):
        # RESET
        self._android_token_list = []
        self._ios_token_list = []
        self._message = ''
        self._message_type = None
        self._priority = None
        self._package = None
        self._notification = None
        self._notification_ios_only = True
        self.remove = []
        self.try_later = []
        self.canonical = []
        self.errors = []

        c = Config().gcm()
        self.defaults['package'] = c['package']
        self._gcm = GCM(c['key'])

        # Set user defaults
        if defaults and isinstance(defaults, dict):
            for key in defaults:
                if key in self.defaults:
                    self.defaults[key] = defaults[key]

        # Send to our clients
        if for_clients:
            self.defaults['package'] = c['client_package']

        self._message_type = self.defaults['type']
        self._priority = self.defaults['priority']
        self._notification_ios_only = self.defaults['ios_only_notification']
        self._package = self.defaults['package']

        self.logger = logging.getLogger(__name__)

    def add(self, tokens):
        if isinstance(tokens, basestring):
            if self.defaults['os'] == 'ios':
                self._ios_token_list.append(tokens)
            else:
                self._android_token_list.append(tokens)

        if isinstance(tokens, list) and tokens:
            if isinstance(tokens[0], basestring):
                for token in tokens:
                    if self.defaults['os'] == 'ios':
                        self._ios_token_list.append(token)
                    else:
                        self._android_token_list.append(token)

            if isinstance(tokens[0], dict):
                for token in tokens:
                    if 'token' not in token:
                        continue

                    token_os = self.defaults['os']
                    if 'os' in token:
                        token_os = token['os']

                    if token_os == 'ios':
                        self._ios_token_list.append(token['token'])
                    else:
                        self._android_token_list.append(token['token'])

    def msg(self, message):
        if not isinstance(message, basestring):
            raise ValueError("The message must be an string")
        self._message = message

    def type(self, message_type):
        """
        Set type of Notification:

        ORDER_STATUS_AVAILABLE = 1
        ORDER_STATUS_ACCEPTED = 2
        ORDER_STATUS_REJECTED = 3
        ORDER_STATUS_READY = 4
        ORDER_STATUS_DISPATCHER_FOUND = 5
        ORDER_STATUS_INCOMING = 6
        ORDER_STATUS_TRACK = 7
        ORDER_STATUS_ARRIVAL = 8
        """
        if not isinstance(message_type, int):
            raise ValueError("Message type must be an integer")
        self._message_type = message_type

    def priority(self, priority):
        if priority not in ['normal', 'high']:
            raise ValueError("Priority must be an integer with value normal or high")
        self._priority = priority

    def package(self, package):
        if not isinstance(package, basestring):
            raise ValueError("Package name must be an string")
        self._package = package

    def notification(self, notification, ios_only=True):
        if not isinstance(notification, dict):
            raise ValueError("Notification must be a dict")

        if not ios_only:
            self._notification_ios_only = False

        self._notification = notification

    def _remove_bad_ids(self, ids):
        self.logger.error(ids)
        from Classes.db import ConnectPSQL
        con = ConnectPSQL().create_session()
        try:
            con.query(DeviceMobile).filter(DeviceMobile.token.in_(ids)).delete(synchronize_session='fetch')
        except Exception as e:
            self.logger.error("Error in remove_bad_ids: " + e.message)

    def send(self):
        gcm_params = {
            'data': {
                'type': self._message_type,
                'data': self._message
            },
            'restricted_package_name': self._package,
            'priority': self._priority,
            'notification': None,
            'delay_while_idle': False,
            'content_available': True,
            'time_to_live': 60
        }

        if self._android_token_list:
            android_gcm_params = {
                'registration_ids': self._android_token_list
            }
            android_gcm_params.update(gcm_params)

            if not self._notification_ios_only:
                android_gcm_params['notification'] = self._notification,

            response = self._gcm.json_request(**android_gcm_params)

            if 'errors' in response:
                for error, ids in response['errors'].items():
                    self.logger.error(error)

                    if error in ['NotRegistered', 'InvalidRegistration']:
                        self.remove.append(ids)
                        self.errors.append("You have to remove from DB the ID: %s" % ids)
                        self._remove_bad_ids(ids)
                    else:
                        self.try_later.append(ids)
                        self.errors.append("Error: %s; ID: %s" % (error, ids,))
            if 'canonical' in response:
                for reg_id, can_id in response['canonical'].items():
                    self.canonical.append({
                        'old_id': reg_id,
                        'new_id': can_id
                    })

        if self._ios_token_list:
            ios_gcm_params = {
                'registration_ids': self._ios_token_list,
            }
            ios_gcm_params.update(gcm_params)
            ios_gcm_params['notification'] = self._notification

            response = self._gcm.json_request(**ios_gcm_params)

            if 'errors' in response:
                for error, ids in response['errors'].items():
                    self.logger.error(error)

                    if error in ['NotRegistered', 'InvalidRegistration']:
                        self.remove.append(ids)
                        self.errors.append("You have to remove from DB the ID: %s" % ids)
                        self._remove_bad_ids(ids)
                    else:
                        self.try_later.append(ids)
                        self.errors.append("Error: %s; ID: %s" % (error, ids,))

            if 'canonical' in response:
                for reg_id, can_id in response['canonical'].items():
                    self.canonical.append({
                        'old_id': reg_id,
                        'new_id': can_id
                    })

        if len(self.remove) > 0 or len(self.errors) > 0 or len(self.canonical) > 0:
            self.logger.error(self.remove)
            self.logger.error(self.errors)
            return False
        else:
            return True
