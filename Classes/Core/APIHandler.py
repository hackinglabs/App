# -*- coding: utf-8 -*-
import logging
import rom.util
import requests
from importlib import import_module

import arrow
from mailgun2 import Mailgun

import six
from wheezy.http.response import HTTPResponse
from wheezy.http.response import method_not_allowed
from wheezy.web.handlers import BaseHandler

from Classes.db import ConnectPSQL

from Classes.Core.Exceptions.UploadError import UploadError
from Classes.Factories.RepositoryFactory import RepositoryFactory
from Classes.Permission import Permission
from Configs import Config
from Helpers.Strings import Strings
from jose import jwt
from Classes.Core.JsonCustomEncoder import JsonCustomEncoder
import json
from gcloud import storage
from oauth2client.client import GoogleCredentials


class APIHandler(BaseHandler):
    _con = None
    cassandra = None

    is_production = False
    base_url = 'https://aquinoesta.xyz'
    logger = None

    def __call__(self):
        self.logger = logging.getLogger(__name__)

        self._con = ConnectPSQL().create_session()

        self.is_production = self.options["production"]

        if self.is_production:
            self.base_url = 'https://dxpress.cl'

        method = self.request.method

        if hasattr(self, 'start') and callable(self.start):
            self.start()

        if method == 'GET':
            return self.get()
        elif method == 'POST':
            return self.post()
        elif method == 'PUT':
            return self.put()
        elif method == 'DELETE':
            return self.delete()
        elif method == 'OPTIONS':
            return self.cors()
        return method_not_allowed()

    @property
    def rabbit(self):
        return self.options["rabbit_class"]

    @property
    def mailer(self):
        conf = Config()
        data = conf.mailgun()
        mailer = Mailgun(data['domain'], data['private'], data['public'])
        return mailer

    def redis(self):
        return rom.util.get_connection()

    def delete(self):
        return method_not_allowed()

    def get_param(self, name):
        try:
            valor = self.request.get_param(name)
            return valor.strip()
        except:
            return None

    def post_param(self, name):
        try:
            value = self.request.form[name]
            if isinstance(value, list) and len(value) == 1:
                if isinstance(value[0], basestring):
                    return value[0].strip()
                return value[0]
            elif isinstance(value, basestring):
                return value.strip()
            else:
                return value
        except:
            return None

    def get_user_agent(self):
        if 'HTTP_USER_AGENT' in self.request.environ:
            return self.request.environ['HTTP_USER_AGENT']
        else:
            return None

    def remove_post_param(self, name):
        if name in self.request.form:
            del self.request.form[name]

    def param(self, name):
        post = self.post_param(name)
        if post is None:
            return self.get_param(name)
        else:
            return post

    def route(self, name):
        if name in self.route_args:
            valor = self.route_args[name]
            if valor is None:
                return None

            if valor.strip() == '':
                return None
            else:
                return valor
        else:
            return None

    def is_logged(self):

        if "HTTP_AUTHORIZATION" not in self.request.environ:
            return False
        secret = self.request.options["token_secret"]

        token = self.request.environ["HTTP_AUTHORIZATION"].split()[1]

        try:
            decoded = jwt.decode(token, secret, algorithms=['HS256'])
            if isinstance(decoded, basestring):
                decoded = json.loads(decoded)
            return decoded
        except Exception, e:
            self.logger.debug("We can't decode the JWT")
            self.logger.error(e.message)
            return False

    def permissions(self):
        temp = self.is_logged()
        if temp is False:
            return Permission({})
        else:
            return Permission(temp)

    def result(self, success=False):
        if success is True:
            return self.success()
        else:
            return self.error()

    def success(self, msg=None, obj=None, custom=None):
        return self.json_response(msg=msg, obj=obj, custom=custom, success=True)

    def error(self, errors=None, msg=None, status_code=400):
        if errors is None:
            errors = self.errors
        if not errors or errors is None:
            errors = msg

        return self.json_response(msg=errors, success=False,
                                  status_code=status_code)

    def json_response(self, obj=None, msg=None, success=None, custom=None, status_code=200):
        responses = {}

        if msg is not None:
            if status_code >= 400:
                responses['errors'] = msg
            elif status_code == 200:
                responses['response'] = msg
        if obj is None:
            obj = responses
        if success is not None and isinstance(obj, dict):
            obj['success'] = success

        if 'response' in obj and obj['response'] == '':
            obj.pop('response', None)

        # MAX chagen

        custom_headers = [
            ('Access-Control-Allow-Origin', '*'),
            ('Access-Control-Allow-Credentials', 'true'),
            ('Access-Control-Allow-Headers', 'X-Requested-With'),
            ('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS'),
            ('Access-Control-Allow-Headers',
             'Authorization, Content-Type, Accept'),
        ]

        encoding = self.options['ENCODING']
        response = HTTPResponse('application/json; charset=' + encoding,
                                encoding)
        response.headers.extend(custom_headers)
        response.status_code = status_code

        if custom:
            response.write_bytes(json.dumps(obj, cls=JsonCustomEncoder).encode(encoding))
        else:
            response.write_bytes(json.dumps(obj).encode(encoding))
        return response

    def repository(self, model, **kwargs):
        if isinstance(model, list):
            for m in model:
                self.__create_repository(m, **kwargs)
        else:
            return self.__create_repository(model, **kwargs)

    def __create_repository(self, model, **kwargs):
        """

        :rtype: object
        """
        factory = RepositoryFactory(model, **kwargs)
        factory.set_db(self._con)
        factory.set_user(self.is_logged())
        instance = factory.create()
        setattr(self, factory.model_class.lower(), instance)
        return instance

    def cors(self):
        try:
            method = self.request.environ['HTTP_ACCESS_CONTROL_REQUEST_METHOD']
        except:
            return method_not_allowed()

        if method in ['POST', 'PUT', 'GET', 'DELETE']:

            custom_headers = [
                ('Access-Control-Allow-Origin', '*'),
                ('Access-Control-Allow-Credentials', 'true'),
                ('Access-Control-Allow-Methods',
                 'GET, POST, PUT, DELETE, OPTIONS'),
                ('Access-Control-Allow-Headers',
                 'Authorization, Content-Type, Accept, X-Requested-With'),
                ('Access-Control-Max-Age', '86400')
            ]

            response = HTTPResponse()
            response.headers.extend(custom_headers)
            response.write('')
            return response
        else:
            return method_not_allowed()

    def to_dict(self, models, replace=None):
        if not models:
            return []

        obj = models.pop(0)
        loaded_columns, unloaded_columns, relations, columns = obj.get_columns()

        all = [temp.to_dict(loaded_columns, unloaded_columns, relations, columns, replace=replace) for temp in models]
        all.insert(0, obj.to_dict(loaded_columns, unloaded_columns, relations, columns, replace=replace))
        return all

    def file(self, name):
        if self.request.files is None:
            return None

        if name not in self.request.files:
            return None

        # Get just one
        upload_file = self.request.files[name]
        if isinstance(upload_file, list):
            if upload_file:
                upload_file = upload_file[0]
            else:
                upload_file = None

        return upload_file

    def upload(self, upload_file, errors=False):
        # Get allowed extensions
        allowed_extensions = self.options["allowed_extensions"]

        # Extensions content-type
        content_type = {
            "png": "image/png",
            "jpg": "image/jpeg",
            "jpeg": "image/jpeg",
            "gif": "image/gif"
        }

        # Get the filename
        filename = upload_file.filename

        # Check file extension
        if ('.' not in filename or
                    filename.split('.').pop().lower() not in allowed_extensions):
            raise UploadError("Invalid file extension.")

        if errors:
            return None

        # Secure and format the filename
        filename = Strings.secure_filename(filename)
        date = arrow.now('America/Santiago').format('YYYYMMDDHHmmss')
        basename, extension = filename.rsplit('.', 1)
        extension = extension.lower()
        new_filename = "{0}-{1}.{2}".format(basename, date, extension)

        # Construct the Cloud storage client
        credentials = GoogleCredentials.get_application_default()
        client = storage.Client(project=self.options['project'],
                                credentials=credentials)

        # Get the bucket
        bucket = client.get_bucket(self.options['bucket'])

        # Define the filename in cloud storage
        new_filename = self.options['upload_folder'] + "/" + new_filename
        blob = bucket.blob(new_filename)

        blob.upload_from_string(upload_file.file.read(),
                                content_type=content_type[extension])

        blob.make_public()
        url = blob.public_url

        if isinstance(url, six.binary_type):
            url = url.decode('utf-8')

        return url

    def model_process(self, model, force=False, required=None, bypass=False):
        """method for validate model before save or create

        Args:
            model: Model object
            force:
            required:
            bypass:

        Returns:

        """
        has_errors = False
        restricted_update_keys = None
        restricted_insert_keys = None

        # must have a model
        if model is None:
            return None

        try:
            self._con.expunge(model)
        except:
            self._con.rollback()
            self.logger.error("No sera eliminado de la sesion")

        if self.errors:
            has_errors = True

        multipart = False
        if "CONTENT-TYPE" in self.request.environ and "multipart/form-data" in self.request.content_type:
            multipart = True

        # Get the repository of this model
        repository = getattr(self, type(model).__name__.lower())

        # It will check whether exists the params of required list
        if isinstance(required, list):
            for param in required:
                if not self.post_param(param):
                    # Add to errors management
                    self.errors[param] = [
                        "Required field cannot be left blank."]
                    has_errors = True

        # Get a dict with every field and required permissions
        # that can update.
        restricted_update = getattr(model, 'restricted_update', None)
        if restricted_update is not None and self.request.method == 'PUT':
            # Get the fields only if the method is PUT
            restricted_update_keys = restricted_update.keys()

        # Get a dict with every field and required permissions
        # that can insert.
        restricted_insert = getattr(model, 'restricted_insert', None)
        if restricted_insert is not None and self.request.method == 'POST':
            # Get the fields only if the method is POST
            restricted_insert_keys = restricted_insert.keys()

        images_fields = []
        temp_images_fields = getattr(model, 'image_fields', [])

        # This will leave only the necessary fields
        for field in temp_images_fields:
            if field in self.request.form:
                del self.request.form[field]

            image_file = self.file(field)
            is_required = temp_images_fields[field]

            if image_file is None:
                # Just check required if method is POST
                if is_required and self.request.method == 'POST':
                    self.errors[field] = ['Required file upload.']
                continue
            images_fields.append(field)


        form_keys = self.request.form.keys()

        try:
            for param in form_keys:
                # Can't update or insert id
                if param == "id":
                    continue

                # Insert only if the user has the perm edit_products
                if restricted_insert_keys is not None and \
                                param in restricted_insert_keys:
                    field_permission = Permission(restricted_insert[param])
                    if not self.permissions().has_one(field_permission):
                        del self.request.form[param]
                        continue

                # Update only if the user has the perm edit_products
                if restricted_update_keys is not None and \
                                param in restricted_update_keys:
                    field_permission = Permission(restricted_update[param])
                    if not self.permissions().has_one(field_permission):
                        del self.request.form[param]
                        continue

                temp = getattr(model, param, None)

                value = self.post_param(param)

                if multipart and value == 'null':
                    del self.request.form[param]
                    continue

                if value is not None and temp != value:
                    setattr(model, param, value)
                else:
                    del self.request.form[param]
                    continue

                if not param in model.__class__.__dict__:
                    del self.request.form[param]
                    continue

                if param[-3:] == '_id':
                    try:
                        value = self.request.form[param] = int(value)
                    except:
                        continue

                    model_relation = param[:-3]
                    if '_' in model_relation:
                        model_relation = Strings.underscore_to_camelcase(
                            model_relation)
                    else:
                        model_relation = model_relation.capitalize()
                    module = import_module("Models." + model_relation)
                    class_ = getattr(module, model_relation)
                    instance = class_()
                    relation_repository = self.repository(instance)

                    if relation_repository.exists(id=value) is False:
                        self.errors['relation'] = ['Not found.']
                        has_errors = True

                if not bypass and \
                        ('unique' in dir(model) and
                                 param in self.request.form and
                                 param in model.unique()):

                    query = {param: self.post_param(param)}
                    if repository.exists(**query) is True:
                        self.errors[param] = ['Already exists.']
                        has_errors = True

        except KeyError as e:
            self.logger.error("Error: KeyErrror: " + e.message)
            if force is True:
                return None
            pass

        if not self.validate(model, model.validate()) or has_errors is True:
            return None

        # self.request.method
        for field in images_fields:
            image_file = self.file(field)

            try:
                url = self.upload(image_file, has_errors)
            except UploadError, e:
                self.errors[field] = [e.value]
            except:
                self.errors[field] = ["There is an error with file uploading."]
            else:
                if url is not None:
                    setattr(model, field, url)
        return model

    def send_message(self, number, message):
        #Check number
        if type(number) == int:
            number = str(number)
            if len(number) == 8:
                if number[0] == '2':
                    self.logger.error('First number is 2')
                    return False
                else:
                    number = '9' + number
            elif len(number) == 9:
                if number[0] != '9':
                    self.logger.error('First number is different 9')
                    return False
        else:
            return False

        # Declared route
        # url = 'http://190.196.157.158:9710/http/send-message?username={0}&password={1}&to={2}&message-type=sms.automatic&message={3}'

        try:
            url = 'http://190.196.157.158:9710/http/send-message'
            args = {'username': 'dxpress', 'password': '31dx31', 'to': number, 'message-type': 'sms.automatic',
                    'message': message}

            r = requests.get(url, params=args)
            if r.status_code != 200:
                self.logger.error('Request Error status: ')
                self.logger.error(r.status_code)
                return False

            return True

        except requests.exceptions.Timeout:
            self.send_message(number, message)
        except requests.exceptions.TooManyRedirects:
            self.logger.error('Error TooManyRedirects')
            return False
        except requests.exceptions.RequestException as e:
            self.logger.error('Error RequestException')
            return False
