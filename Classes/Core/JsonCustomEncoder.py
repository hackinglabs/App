import json
import collections

from Classes.Permission import Permission
from Models.Cassandra import Address, Compound, GeoDispatcher, Ingredient, Payment, DispatcherSummary
from Models.Cassandra import Order, OrdersUser, Point, Product, Subsidiary, User
from Models.Cassandra.Discount import Discount
import uuid
import datetime

from Models.Cassandra.ClientOrders import ClientOrders
from Models.Cassandra.Commerce import Commerce
from Models.Cassandra.Coupon import Coupon
from Models.Cassandra.DispatcherReceive import DispatcherReceive


class JsonCustomEncoder(json.JSONEncoder):
    
    def default(self, obj):
        if isinstance(obj, collections.Set):
            return list(obj)
        elif isinstance(obj, (Address, Compound, ClientOrders, GeoDispatcher, Ingredient, Order, OrdersUser, Point,
                              Product, Subsidiary, User, Payment, Commerce, Coupon, DispatcherSummary,
                              DispatcherReceive, Discount)):
            return dict(obj)
        elif isinstance(obj, uuid.UUID):
            return str(obj)
        elif isinstance(obj, Permission):
            return obj.to_dict()
        elif isinstance(obj, datetime.datetime):
            temp_date = obj.isoformat().strip().split("T")
            return "{0} {1}".format(temp_date[0], temp_date[1].strip().split(".")[0])

        elif isinstance(obj, datetime.date):
            temp_date = obj.isoformat().strip().split("T")
            return "{0}".format(temp_date[0])
        else:
            return json.JSONEncoder.default(self, obj)