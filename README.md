![Build Status](https://codeship.com/projects/b31f5ba0-871d-0133-c4a2-36a0203442ba/status?branch=develop)

### Requirements
 * python2.7 
 * virtualbox 
 * vagrant
 * curl

### Install

### clone vagrant
 ```
 mkdir project && cd $_ && git clone git@github.com:HackingLabsChile/vagrant.git
 ```

##### clone project
 ```
 mkdir app && cd $_ && git clone git@github.com:HackingLabsChile/app.git
 ```
 
###### vagrant box
 ````
 cd ../project && vagrant up
 ````
###### login into vagrant
 ```
 vagrant ssh
 ```
###### install project's libs
 ```
 cd /vagrant/app && sudo pip install -r requirements.txt
 ```
###### install tools python-sha3
 ```
 cd /vagrant/app && git submodule update --init && cd Tools/python-sha3 && sudo python setup.py install
 ```
 
### Run project
 ```
 cd /vagrant/app && ./run.sh # server up to accept requests
 ```

### Example requests (local machine)
 ```
 curl -X GET "http://localhost:8080/es/users/profile/1"
 ```
 
### Login into postgres and create  database
  ```
  sudo -i -u postgres && psql
  create database dondeviene
  \c dondeviene
  create extension postgis
  ```
